﻿using IntegracionSaintFE.Framework;
using IntegracionSaintFE.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntegracionSaintFE
{
    class Funciones
    {
        public ConfigModel LeerConfiguracion()
        {
            ConfigModel c = new ConfigModel();
            try
            {
                string pat = Assembly.GetExecutingAssembly().Location;
                pat = System.IO.Path.GetDirectoryName(pat);
                Directory.SetCurrentDirectory(pat);
                string jsonconf = File.ReadAllText("Config.json");
                c = JsonConvert.DeserializeObject<ConfigModel>(jsonconf);
            }
            catch (Exception e)
            {
                throw;
                //LogE("No se pudo leer la configuración: {0}", e.Message);
            }
            return c;
        }

        public void Log(string line, params object[] values)
        {
            ConsoleHarness.WriteToConsole(ConsoleColor.DarkYellow, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss -- ") + line, values);
        }

        public void LogE(string line, params object[] values)
        {
            ConsoleHarness.WriteToConsole(ConsoleColor.Red, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss -- ") + line, values);
            string l = string.Format(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss -- ") + line, values);            
            System.IO.File.AppendAllLines(@"logE_IntegracionSaintFE.txt", new string[] { l });
        }

        public string EncriptaMD5Hash(string t)
        {

            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(t));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public bool ValidarEmail (string email)
        {
            //string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            string expresion = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";           

            if (Regex.IsMatch(email.ToLower(), expresion))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
