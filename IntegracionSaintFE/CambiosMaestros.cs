﻿using DbLink;
using IntegracionSaintFE.Framework;
using IntegracionSaintFE.Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IntegracionSaintFE
{
    public partial class ServiceImplementation : IWindowsService
    {
        private async Task CambiosMaestros()
        {
            DbLinkClass db = new DbLinkClass(Config.SQLConnectionString);

            await Task.Run(() =>
            {
                try
                {
                    List<CambiosMaestrosFE> maestros = new List<CambiosMaestrosFE>();
                    maestros = db.SelectObject<CambiosMaestrosFE>(@"SELECT TOP(10) * FROM [dbo].[CambiosMaestrosFE] WHERE [estado_sync] = 0 ORDER BY id");
                    int procesados = maestros.Count;

                    if (procesados > 0)
                    {
                        foreach (CambiosMaestrosFE item in maestros)
                        {
                            string error = "";
                            switch (item.tipo)
                            {
                                case "CLI":
                                    try
                                    {
                                        db.Ejecutar(Qrys.clientes
                                            , new SqlParameter("@codclie", item.codigo)
                                            , new SqlParameter("@id", item.Id));
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        FF.LogE("Error desbloqueando comprobantes por clientes id de cambio {0}: {1}", item.Id, ex.Message);
                                    }
                                    break;
                                case "PRV":
                                    try
                                    {
                                        db.Ejecutar(Qrys.proveedores
                                            , new SqlParameter("@codprov", item.codigo)
                                            , new SqlParameter("@id", item.Id));
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        FF.LogE("Error desbloqueando comprobantes por proveedor id de cambio {0}: {1}", item.Id, ex.Message);
                                    }
                                    break;
                                case "PRD":
                                    try
                                    {
                                        db.Ejecutar(Qrys.productos
                                            , new SqlParameter("@codprod", item.codigo)
                                            , new SqlParameter("@id", item.Id));
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        FF.LogE("Error desbloqueando comprobantes por productos id de cambio {0}: {1}", item.Id, ex.Message);
                                    }
                                    break;
                                case "SRV":
                                    try
                                    {
                                        db.Ejecutar(Qrys.servicios
                                            , new SqlParameter("@codserv", item.codigo)
                                            , new SqlParameter("@id", item.Id));
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        FF.LogE("Error desbloqueando comprobantes por servicios id de cambio {0}: {1}", item.Id, ex.Message);
                                    }
                                    break;
                                case "OCG":
                                    try
                                    {
                                        db.Ejecutar(Qrys.otros_cargos
                                            , new SqlParameter("@codmeca", item.codigo)
                                            , new SqlParameter("@id", item.Id));
                                    }
                                    catch (Exception ex)
                                    {
                                        error = ex.Message;
                                        FF.LogE("Error desbloqueando comprobantes por terceros de otros cargos id de cambio {0}: {1}", item.Id, ex.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }

                            if (!string.IsNullOrEmpty(error))
                            {
                                db.Ejecutar(@"UPDATE [dbo].[CambiosMaestrosFE] SET [estado_sync] = 2, [mensaje_sync] = CONCAT('Error: ', LEFT(@error,243)) WHERE Id = @id"
                                , new SqlParameter("@id", item.Id)
                                , new SqlParameter("@error", error));
                                FF.LogE("Error procesando un cambio para desbloquear comprobantes por maestros: {0}", error);
                                procesados--;
                            }
                        }

                        FF.Log("Se procesaron {0}/{1} por cambios en maestros para desbloquear comprobantes", procesados.ToString(), maestros.Count.ToString());
                    }
                    else
                    {
                        FF.Log("No hay cambios de maestros para desbloquear comprobantes pendientes");
                    }
                }
                catch (Exception ex)
                {
                    FF.LogE("Error Bucando cambios en maestros para desbloquear comprobantes: {0}", ex.Message);
                }
            });
        }

        private async Task LimpiarHCambiosMaestros()
        {
            DbLinkClass dblh= new DbLinkClass(Config.SQLConnectionString);

            await Task.Run(() =>
            {
                try
                {
                    int registros = 0;

                    if (Config.MonthsHistMaster >= 1)
                    {
                        if (Config.LastTime.AddHours(6) < DateTime.Now)
                        {
                            registros = int.Parse(dblh.Consultar(@"
SET DATEFORMAT YMD;
DECLARE
--@meses int = 6,
@total_registros int,
@fecha datetime

SET @fecha = DATEADD(MONTH, (@meses*-1), GETDATE());

SELECT @total_registros = COUNT(*)
FROM [dbo].[CambiosMaestrosFE]
WHERE (CAST([fechaCE] AS DATE) <= CAST(@fecha AS DATE))

IF (@total_registros > 0)
	BEGIN
		DELETE FROM [dbo].[CambiosMaestrosFE]
		WHERE (CAST([fechaCE] AS DATE) <= CAST(@fecha AS DATE))
	END;

SELECT @total_registros [total_registros];",
                                new SqlParameter("@meses", Config.MonthsHistMaster)).Rows[0][0].ToString());

                            Config.LastTime = DateTime.Now;
                            FF.Log("Limpiando historial de cambios en maestros: {0} registros eliminados", registros.ToString());
                        }
                    }
                                    
                }
                catch (Exception ex)
                {
                    FF.LogE("Error limpiando historial de cambios en maestros: ", ex.Message);
                }
            });

        }
    }
}
