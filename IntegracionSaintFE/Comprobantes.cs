﻿using DbLink;
using IntegracionSaintFE.Framework;
using IntegracionSaintFE.Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntegracionSaintFE
{
    public partial class ServiceImplementation : IWindowsService
    {
        private async Task Comprobantes()
        {
            await Task.Run(() =>
            {
                DbLinkClass dbc = new DbLinkClass(Config.SQLConnectionString);

                try
                {
                    List<ComprobantesFE> comprobantes = new List<ComprobantesFE>();
                    comprobantes = dbc.SelectObject<ComprobantesFE>(@"SELECT TOP(5) * FROM [dbo].[ComprobantesFE] WHERE [EstadoFE] = 0 ORDER BY id");
                    int procesados = comprobantes.Count;

                    if (procesados > 0)
                    {
                        foreach (ComprobantesFE item in comprobantes)
                        {
                            string error = "";

                            if (item.TipoDoc == "A" || item.TipoDoc == "B")
                            {
                                try
                                {
                                    error = GenerarFacDev(item);
                                }
                                catch (Exception ex)
                                {
                                    error = string.Format("Error procesando {0} nro. {1}: {2}", item.TipoDoc == "A" ? "Factura" : "Devolución", item.NumeroD, ex.Message);
                                }

                            }
                            else if (item.TipoDoc == "20" || item.TipoDoc == "31")
                            {
                                try
                                {
                                    error = GenerarNdNc(item);
                                }
                                catch (Exception ex)
                                {
                                    error = string.Format("Error procesando {0} nro. {1}: {2}", item.TipoDoc == "20" ? "ND" : "NC", item.NumeroD, ex.Message);
                                }
                            }
                            else if (item.TipoDoc == "H")
                            {
                                try
                                {
                                    error = GenerarCompRC(item);
                                }
                                catch (Exception ex)
                                {
                                    error = string.Format("Error procesando Comp. RC nro. {0}: {1}", item.NumeroD, ex.Message);
                                }
                            }

                            if (!string.IsNullOrEmpty(error))
                            {
                                string tipo = "";
                                switch (item.TipoDoc)
                                {
                                    case "A":
                                        tipo = "Factura";
                                        break;
                                    case "B":
                                        tipo = "Devolución";
                                        break;
                                    case "20":
                                        tipo = "Nota Débito";
                                        break;
                                    case "31":
                                        tipo = "Nota Crédito";
                                        break;
                                    case "H":
                                        tipo = "Compra RC";
                                        break;
                                    default:
                                        break;
                                }

                                dbc.Ejecutar(@"UPDATE [dbo].[ComprobantesFE] SET [EstadoFE] = 2, [MensajeFE] = CONCAT('Error: ', LEFT(@error,493)) WHERE Id = @id"
                                , new SqlParameter("@id", item.Id)
                                , new SqlParameter("@error", error ?? ""));
                                FF.LogE("Error procesando comprobantes {0} Nro. {1}: {2}", tipo, item.NumeroD, error);
                                procesados--;
                            }
                        }

                        FF.Log("Se procesaron {0}/{1} comprobantes", procesados.ToString(), comprobantes.Count.ToString());
                    }
                    else
                    {
                        FF.Log("No comprobantes pendientes por procesar");
                    }
                }
                catch (Exception ex)
                {
                    FF.LogE("Error procesando comprobantes: {0}", ex.Message);
                }
            });
        }

        private string GenerarCompRC(ComprobantesFE item)
        {
            string error = "";

            return error;
        }

        private string GenerarNdNc(ComprobantesFE item)
        {
            string error = "";

            return error;
        }

        private string GenerarFacDev(ComprobantesFE item)
        {
            DbLinkClass dbv = new DbLinkClass(Config.SQLConnectionString);
            List<SAFACT_0X> exoneracion_vta = new List<SAFACT_0X>();
            List<SAFACT_0X> exoneracion_vta_doc_ref = new List<SAFACT_0X>();
            List<SAFACT> safact = new List<SAFACT>();
            List<SAITEMFAC> saitemfac = new List<SAITEMFAC>();
            List<SATAXVTA> sataxvta = new List<SATAXVTA>();
            List<SATAXITF> sataxitf = new List<SATAXITF>();
            List<SACLIE_0X_1> exoneracion_cliente = new List<SACLIE_0X_1>();
            List<SACLIE_0X> datos_adic_cliente = new List<SACLIE_0X>();
            List<SACONF> configuracion = new List<SACONF>();
            List<SADEPO_0X> sucursal = new List<SADEPO_0X>();
            List<RECEPTOR> receptor = new List<RECEPTOR>();
            List<FECodAreaPais> CodAreaPais = new List<FECodAreaPais>();
            List<SAPAIS> pais = new List<SAPAIS>();
            List<SAESTADO> provincia = new List<SAESTADO>();
            List<SACIUDAD> canton = new List<SACIUDAD>();
            List<SAMUNICIPIO> distrito = new List<SAMUNICIPIO>();
            List<FEActividades_Eco> actividad_economica = new List<FEActividades_Eco>();

            string error = "";

            try
            {
                //buscando datos en tabla intermedia de facturacion para exoneraciones y otros datos
                exoneracion_vta = dbv.SelectObject<SAFACT_0X>(@"
DECLARE
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50)

SET @numgrp = 0;
SET @nom_table_m = 'SAFACT';
SET @nombregrp = 'Exoneración_y_Datos_Adicionales';

SELECT @numgrp = isnull(numgrp,0)
FROM saagrupos WITH (NOLOCK)
WHERE codtbl = @nom_table_m and nombregrp = @nombregrp;

SET @nom_table = concat(@nom_table_m, '_',replicate('0',2 - len(cast(@numgrp as varchar(2)))),cast(@numgrp as varchar(2)));

EXEC (N'SELECT TOP(1) * FROM [dbo].[' + @nom_table + '] WHERE TipoFac = ' + '''' + @tipofac + '''' + ' AND NumeroD = ' + '''' + @numerod +'''');",
                    new SqlParameter("@tipofac", item.TipoDoc ?? ""),
                    new SqlParameter("@numerod", item.NumeroD ?? ""));

                if (exoneracion_vta.Count > 0)
                {
                    //buscando datos en cabecera de saint
                    safact = dbv.SelectObject<SAFACT>(@"SELECT TOP(1) * FROM [dbo].[SAFACT] WHERE (TipoFac = @tipofac) AND (NumeroD = @numerod)",
                        new SqlParameter("@tipofac", item.TipoDoc ?? ""),
                        new SqlParameter("@numerod", item.NumeroD ?? ""));

                    if (safact.Count > 0)
                    {
                        //ubicando datos de exoneracion de ficha del cliente
                        exoneracion_cliente = dbv.SelectObject<SACLIE_0X_1>(@"
DECLARE
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50)

SET @numgrp = 0;
SET @nom_table_m = 'SACLIE';
SET @nombregrp = 'Exoneracion';

SELECT @numgrp = isnull(numgrp,0)
FROM saagrupos WITH (NOLOCK)
WHERE codtbl = @nom_table_m and nombregrp = @nombregrp;

SET @nom_table = concat(@nom_table_m, '_',replicate('0',2 - len(cast(@numgrp as varchar(2)))),cast(@numgrp as varchar(2)));

EXEC(N'SELECT TOP(1)
	[CodClie],
	[Tipo_documento],
	[Numero_Documento],
	[Nombre_Institucion],
	[Fecha_Documento],
	[Porcentaje]
FROM [dbo].[' + @nom_table + '] WITH (NOLOCK)
WHERE [CodClie] = (' + '''' + @codclie + '''' + ');');",
                            new SqlParameter("@codclie", safact[0].CodClie));

                        //ubicando datos de configuracion
                        configuracion = dbv.SelectObject<SACONF>(@"SELECT TOP(1) * FROM [dbo].[SACONF] WHERE [CodSucu] = '00000'");

                        //ubicando sadtos de sucursal
                        sucursal = dbv.SelectObject<SADEPO_0X>(@"
DECLARE
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50)

SET @numgrp = 0;
SET @nom_table_m = 'SACLIE';
SET @nombregrp = 'Exoneracion';

SELECT @numgrp = isnull(numgrp,0)
FROM saagrupos WITH (NOLOCK)
WHERE codtbl = @nom_table_m and nombregrp = @nombregrp;

SET @nom_table = concat(@nom_table_m, '_',replicate('0',2 - len(cast(@numgrp as varchar(2)))),cast(@numgrp as varchar(2)));

EXEC (N'SELECT TOP(1) [CodUbic],
    [Sucursal],
	[Llave_Sucursal],
	[Codigo_Actividad_S]
FROM [dbo].[' + @nom_table + N'] WITH (NOLOCK)
WHERE (CodUbic = ' + '''' + @CodUbic + ''''+')');",
                            new SqlParameter("@CodUbic", safact[0].CodUbic));

                        //ubicando datos adicionales de cliente
                        datos_adic_cliente = dbv.SelectObject<SACLIE_0X>(@"
DECLARE
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50)

SET @numgrp = 0;
SET @nom_table_m = 'SACLIE';
SET @nombregrp = 'Exoneracion';

SELECT @numgrp = isnull(numgrp,0)
FROM saagrupos WITH (NOLOCK)
WHERE codtbl = @nom_table_m and nombregrp = @nombregrp;

SET @nom_table = concat(@nom_table_m, '_',replicate('0',2 - len(cast(@numgrp as varchar(2)))),cast(@numgrp as varchar(2)));

EXEC (N'SELECT CodClie, 
	Correo_Adicional_1,
	Correo_Adicional_2,
	Correo_Adicional_3,
	Correo_Adicional_4,
	No_Ubicacion,
	Codigo_Pais,
	receptor_direccion_extranjero,
	REPLACE(REPLACE(WM_Vendedor,CHAR(34),''''), CHAR(92),'''') WM_Vendedor,
	REPLACE(REPLACE(WM_GLN,CHAR(34),''''), CHAR(92),'''') WM_GLN,
	receptor_incluir,
	Ubicacion_Incluir,
	GS1,
	Impuesto,
	Usa_Descrip_Ampli
FROM [dbo].[' + @nom_table +'] WITH (NOLOCK)
WHERE (CodClie = ' + '''' + @codclie + ''''+')');",
                            new SqlParameter("@codclie", safact[0].CodClie));

                        //ubicando datos de receptor
                        receptor = dbv.SelectObject<RECEPTOR>(@"
SELECT	IIF(Descrip LIKE '?%CONTADO%', 1, 0) [Contado],
		IIF(LEFT(Descrip,1) = '?', @Descrip, CASE WHEN @Usa_Descrip_Ampli = 1 THEN IIF(LTRIM(RTRIM(DescripExt))+'' = '', Descrip, LEFT(DescripExt,100)) ELSE Descrip END) [Receptor_Nombre],
		CASE WHEN Clase = 'N' THEN IIF(TipoID3 = 1, '01', '02') WHEN Clase = 'ED' THEN '03' WHEN Clase = 'EN' THEN '04' WHEN Clase IN ('P','E') THEN '' ELSE IIF(TipoID3 = 1, '01', '02') END [Receptor_Tipo],
		IIF(Clase IN ('P','E'), '', DBO.RemoveChars(ID3)) [Receptor_Identificacion],
		IIF(Clase IN ('P','E'), DBO.RemoveChars(ID3), '') [Pasaporte],
		Pais,
		Estado,
		Ciudad,
		Municipio,
		IIF(LEFT(Descrip,1) = '?', CONCAT(@Direc1, ' ', @Direc2), CONCAT(Direc1, ' ', Direc2)) [Receptor_Direccion],
		LEFT(IIF(LEFT(Descrip,1) = '?', DBO.RemoveChars(@Telef), DBO.RemoveChars(Telef)),8) [Receptor_Telefono],
		LEFT(IIF(LEFT(Descrip,1) = '?', '', DBO.RemoveChars(Fax)),20) [Receptor_Fax],
		REPLACE(REPLACE(IIF(LEFT(Descrip,1) = '?', Email, Email), CHAR(0x1f), ''), CHAR(32), '') [Receptor_Email],
		TipoCli,
		Clase
FROM SACLIE WITH (NOLOCK)
WHERE (CodClie = @codclie);",
                            new SqlParameter("@codclie", safact[0].CodClie ?? ""),
                            new SqlParameter("@Descrip", safact[0].Descrip ?? ""),
                            new SqlParameter("@Usa_Descrip_Ampli", datos_adic_cliente[0].Usa_Descrip_Ampli),
                            new SqlParameter("@Direc1", safact[0].Direc1 ?? ""),
                            new SqlParameter("@Direc2", safact[0].Direc2 ?? ""),
                            new SqlParameter("@Telef", safact[0].Telef ?? ""));

                        if (configuracion.Count == 1)
                        {
                            try
                            {
                                //Variables
                                decimal descto1 = 0;
                                decimal descto2 = 0;
                                int sucu = 0;
                                string mails_adic_clie = "";
                                string receptor_provincia = "";
                                string receptor_canton = "";
                                string receptor_distrito = "";
                                int receptor_incluir = 1;
                                int ubicacion_incluir = 1;

                                //Recalculando descuentos globales
                                if ((safact[0].Descto1 + safact[0].Descto2) > 0)
                                {
                                    descto1 = (safact[0].TotalPrd + safact[0].TotalSrv) - (safact[0].MtoTotal - safact[0].MtoTax - safact[0].Fletes);
                                    safact[0].Descto1 = descto1;
                                    safact[0].Descto1 = descto2;
                                }

                                //Determinando sucursal para comprobante
                                if (sucursal.Count != 1)
                                {
                                    sucursal[0].Sucursal = 0;
                                }

                                if (sucursal[0].Sucursal == 0)
                                {
                                    sucursal[0].Sucursal = configuracion[0].Sucursal;
                                    sucursal[0].Llave_Sucursal = configuracion[0].Llave_Sucursal;
                                    sucursal[0].Codigo_Actividad_S = 0;
                                }

                                if (safact[0].TipoFac == "B")
                                {
                                    if (item.ClaveRef == "00000000000000000000000000000000000000000000000000")
                                    {
                                        sucu = int.Parse(item.ClaveRef.Substring(22, 3));
                                        sucursal = dbv.SelectObject<SADEPO_0X>(@"
DECLARE
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50)

SET @numgrp = 0;
SET @nom_table_m = 'SACLIE';
SET @nombregrp = 'Exoneracion';

SELECT @numgrp = isnull(numgrp,0)
FROM saagrupos WITH (NOLOCK)
WHERE codtbl = @nom_table_m and nombregrp = @nombregrp;

SET @nom_table = concat(@nom_table_m, '_',replicate('0',2 - len(cast(@numgrp as varchar(2)))),cast(@numgrp as varchar(2)));

EXEC (N'SELECT TOP(1) [Sucursal],
	[Llave_Sucursal],
	[Codigo_Actividad_S]
FROM [dbo].[' + @nom_table + N'] WITH (NOLOCK)
WHERE (Sucursal = ' + '''' + @sucu + ''''+')');",
                                            new SqlParameter("@sucu", sucu));

                                        if (sucu == 0)
                                        {
                                            sucursal[0].Sucursal = configuracion[0].Sucursal;
                                            sucursal[0].Llave_Sucursal = configuracion[0].Llave_Sucursal;
                                            sucursal[0].Codigo_Actividad_S = 0;
                                        }
                                    }
                                }

                                //ubicando impuesto asignado al cliente
                                if (datos_adic_cliente[0].Impuesto >= 2)
                                {
                                    DataTable Imp = dbv.Consultar(@"
SELECT TOP(1) A.CodTaxs
	FROM
	(SELECT ROW_NUMBER() OVER(ORDER BY Orden) Linea,
			CodTaxs
	FROM SATAXES WITH (NOLOCK)
WHERE Orden >= 2) A
WHERE (A.Linea = @Impuesto - 1)",
                                        new SqlParameter(@"", datos_adic_cliente[0].Impuesto));

                                    safact[0].CodTaxsClie = Imp.Rows[0][0].ToString();
                                }

                                //determinando cuales datos de GS1 tomar
                                if (safact[0].TipoFac == "A")
                                {
                                    if (!string.IsNullOrEmpty(datos_adic_cliente[0].WM_GLN))
                                    {
                                        exoneracion_vta[0].WM_GLN = datos_adic_cliente[0].WM_GLN;
                                    }

                                    if (string.IsNullOrEmpty(safact[0].OrdenC))
                                    {
                                        exoneracion_vta[0].Fecha_Orden = default(DateTime);
                                    }

                                    if (string.IsNullOrEmpty(exoneracion_vta[0].Nro_Reclamo))
                                    {
                                        exoneracion_vta[0].Fecha_Reclamo = default(DateTime);
                                    }
                                }

                                //ubicando prefijo de pais para numero de telefono extranjeros
                                if (datos_adic_cliente[0].Codigo_Pais == 0)
                                {
                                    CodAreaPais[0].Id = 0;
                                }
                                else
                                {
                                    CodAreaPais = dbv.SelectObject<FECodAreaPais>(@"SELECT * FROM [dbo].[FECodAreaPais] WITH (NOLOCK) WHERE [Id] = @Id_Prefijo_Pais;",
                                        new SqlParameter("@Id_Prefijo_Pais", datos_adic_cliente[0].Codigo_Pais));
                                }

                                //validando y determinando correos adicionales
                                if (FF.ValidarEmail(datos_adic_cliente[0].Correo_Adicional_1))
                                {
                                    datos_adic_cliente[0].Correo_Adicional_1 = datos_adic_cliente[0].Correo_Adicional_1.ToLower();
                                }
                                else
                                {
                                    datos_adic_cliente[0].Correo_Adicional_1 = "";
                                }

                                if (FF.ValidarEmail(datos_adic_cliente[0].Correo_Adicional_2))
                                {
                                    datos_adic_cliente[0].Correo_Adicional_2 = datos_adic_cliente[0].Correo_Adicional_2.ToLower();
                                }
                                else
                                {
                                    datos_adic_cliente[0].Correo_Adicional_2 = "";
                                }

                                if (FF.ValidarEmail(datos_adic_cliente[0].Correo_Adicional_3))
                                {
                                    datos_adic_cliente[0].Correo_Adicional_3 = datos_adic_cliente[0].Correo_Adicional_3.ToLower();
                                }
                                else
                                {
                                    datos_adic_cliente[0].Correo_Adicional_3 = "";
                                }

                                if (FF.ValidarEmail(datos_adic_cliente[0].Correo_Adicional_4))
                                {
                                    datos_adic_cliente[0].Correo_Adicional_4 = datos_adic_cliente[0].Correo_Adicional_4.ToLower();
                                }
                                else
                                {
                                    datos_adic_cliente[0].Correo_Adicional_4 = "";
                                }

                                List<string> emails = new List<string> { datos_adic_cliente[0].Correo_Adicional_1, datos_adic_cliente[0].Correo_Adicional_2, datos_adic_cliente[0].Correo_Adicional_3, datos_adic_cliente[0].Correo_Adicional_4 };

                                foreach (string email in emails)
                                {
                                    if (!string.IsNullOrEmpty(email))
                                    {
                                        mails_adic_clie += email + ";";
                                    }
                                }

                                mails_adic_clie = mails_adic_clie.Substring(0, mails_adic_clie.Length - 1);

                                //determinando ubicacion pais, provincia, canton y distrito
                                pais = dbv.SelectObject<SAPAIS>(@"SELECT TOP(1) * FROM [dbo].[SAPAIS] WITH (NOLOCK) WHERE [IDPais] = 506 ORDER BY [Pais];");

                                provincia = dbv.SelectObject<SAESTADO>(@"
SELECT TOP(1) *
FROM [dbo].[SAESTADO] WITH (NOLOCK)
WHERE [Estado] = @EstadoR AND Pais = @Pais",
                                    new SqlParameter("@pais", pais[0].Pais),
                                    new SqlParameter("@EstadoR", receptor[0].Estado));

                                receptor_provincia = provincia.Count == 1 ? provincia[0].IDESTADOFE.ToString() : "1"; 

                                canton = dbv.SelectObject<SACIUDAD>(@"
SELECT TOP(1) *
FROM [dbo].[SACIUDAD] WITH (NOLOCK)
WHERE [Ciudad] = @CiudadR AND [Pais] = @Pais;",
                                    new SqlParameter("@pais", pais[0].Pais),
                                    new SqlParameter("@CiudadR", receptor[0].Ciudad));
                                
                                receptor_canton = canton.Count == 1 ? ("000" + canton[0].IDCIUDADFE.ToString()).Substring(("000" + canton[0].IDCIUDADFE.ToString()).Length - 2, 2) : "01";

                                distrito = dbv.SelectObject<SAMUNICIPIO>(@"
SELECT TOP(1)
FROM [dbo].[SAMUNICIPIO] WITH (NOLOCK)
WHERE [Municipio] = @MunicipioR AND [Pais] = @Pais;",
                                    new SqlParameter("@pais", pais[0].Pais),
                                    new SqlParameter("@MunicipioR", receptor[0].Municipio));

                                receptor_distrito = distrito.Count == 1 ? ("000" + distrito[0].IDMUNICIPIOFE.ToString()).Substring(("000" + distrito[0].IDMUNICIPIOFE.ToString()).Length - 2, 2) : "01";

                                //ubicando codigo de actividad economica
                                exoneracion_vta[0].Codigo_Actividad_S = exoneracion_vta[0].Codigo_Actividad_S.ToString().Length > 0 ? exoneracion_vta[0].Codigo_Actividad_S : 0;
                                if (exoneracion_vta[0].Codigo_Actividad_S == 0)
                                {
                                    if (sucursal[0].Codigo_Actividad_S > 0)
                                    {
                                        exoneracion_vta[0].Codigo_Actividad_S = sucursal[0].Codigo_Actividad_S;
                                    }
                                    else
                                    {
                                        exoneracion_vta[0].Codigo_Actividad_S = 1;
                                    }
                                }

                                actividad_economica = dbv.SelectObject<FEActividades_Eco>(@"
SELECT TOP(1) *
FROM [dbo].[FEActividades_Eco] WITH (NOLOCK)
WHERE [Nivel] = @NroUnicoAC;",
                                    new SqlParameter("@NroUnicoAC", exoneracion_vta[0].Codigo_Actividad_S));

                                //determinar si incluir receptor
                                if (safact[0].TipoFac == "A")
                                {
                                    if (receptor[0].Clase == "P" || receptor[0].Clase == "T")
                                    {
                                        receptor_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                        ubicacion_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                    }
                                    else
                                    {
                                        if (receptor[0].Contado == 1)
                                        {
                                            receptor_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                            ubicacion_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                        }
                                        else
                                        {
                                            receptor_incluir = 1;
                                            ubicacion_incluir = 1;
                                        }
                                    }
                                }
                                else
                                {
                                    DataTable dt = new DataTable();
                                    dt = dbv.Consultar(@"SELECT TOP(1) [receptor_incluir], [ubicacion_incluir] FROM [dbo].[encabezado] WITH (NOLOCK) WHERE [clave] = @clave", 
                                        new SqlParameter("@clave", item.ClaveRef));

                                    if (dt.Rows.Count == 1)
                                    {
                                        receptor_incluir = int.Parse(dt.Rows[0][0].ToString());
                                        ubicacion_incluir = int.Parse(dt.Rows[0][1].ToString());
                                    }
                                    else
                                    {
                                        if (receptor[0].Clase == "P" || receptor[0].Clase == "T")
                                        {
                                            receptor_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                            ubicacion_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                        }
                                        else
                                        {
                                            if (receptor[0].Contado == 1)
                                            {
                                                receptor_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                                ubicacion_incluir = configuracion[0].Incluir_Receptor == 0 ? datos_adic_cliente[0].Receptor_Incluir : 0;
                                            }
                                            else
                                            {
                                                receptor_incluir = 1;
                                                ubicacion_incluir = 1;
                                            }
                                        }
                                    }
                                }

                                //determinar si incluir exoneracion

                            }
                            catch (Exception)
                            {
                                throw;
                            }

                            //buscando impuestos de cabecera de saint
                            sataxvta = dbv.SelectObject<SATAXVTA>(@"SELECT * FROM [dbo].[SATAXVTA] WHERE (TipoFac = @tipofac) AND (NumeroD = @numerod)",
                                new SqlParameter("@tipofac", item.TipoDoc),
                                new SqlParameter("@numerod", item.NumeroD));

                            //buscando lineas en saint
                            saitemfac = dbv.SelectObject<SAITEMFAC>(@"SELECT * FROM [dbo].[SAITEMFAC] WHERE (TipoFac = @tipofac) AND (NumeroD = @numerod) AND (NroLineaC = 0) ORDER BY NroLinea",
                                new SqlParameter("@tipofac", item.TipoDoc),
                                new SqlParameter("@numerod", item.NumeroD));

                            //buscando impuestos de linea de saint
                            sataxitf = dbv.SelectObject<SATAXITF>(@"SELECT * FROM [dbo].[SATAXITF] WHERE (TipoFac = @tipofac) AND (NumeroD = @numerod) AND (NroLineaC = 0) ORDER BY NroLinea",
                                new SqlParameter("@tipofac", item.TipoDoc),
                                new SqlParameter("@numerod", item.NumeroD));
                        }
                        else
                        {
                            error = "No se encontraron datos de configuración en SACONF";
                        }
                    }
                    else
                    {
                        error = "No se encontro registro en tabla SAFACT de este documento";
                    }
                }
                else
                {
                    error = "No se encontro registro en tabla SAFACT_0X de este documento";
                }

            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            return error;
        }

        private async Task CerrarComprobantes()
        {
            await Task.Run(() =>
            {

            });
        }

    }
}
