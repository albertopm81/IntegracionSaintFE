﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE
{
    public static class Qrys
    {
        public static string clientes = @"
SET DATEFORMAT YMD;
DECLARE
@id3 varchar(25),
@tipoid3 smallint,
@telef varchar(30),
@clase varchar(10),
@contador int,
@descrip varchar(60),
@descripext varchar(250),
@direc1 varchar(60),
@direc2 varchar(60),
@estado int,
@ciudad int,
@municipio int,
@email varchar(60),
@fax varchar(30),
@tipocli smallint,
@receptor_direccion_extranjero varchar(300),
-- VARIABLES TABLA ENCABEZADO
@receptor_nombre varchar(100),
@receptor_tipo varchar(2),
@receptor_identificacion varchar(12),
@pasaporte varchar(20),
@receptor_provincia varchar(1),
@receptor_canton varchar(2),
@receptor_distrito varchar(2),
@receptor_direccion varchar(160),
@receptor_telefono varchar(20),
@receptor_fax varchar(20),
@receptor_email varchar(100),
-- TABLA ADICIONAL
@numgrp as int, 
@nom_table_m as varchar(50),
@nom_table as varchar(50),
@nombregrp as varchar(50),
@string as nvarchar(max),
@pais as int,
@impuesto int,
@usa_descrip_ampli smallint,
@represent varchar(40)

SET NOCOUNT ON;

SELECT	@id3 = ID3,
		@tipoid3 = TipoID3,
		@telef = Telef,
		@clase = Clase,
		@descrip = RTRIM(LTRIM(Descrip)),
		@direc1 = RTRIM(LTRIM(Direc1)),
		@direc2 = RTRIM(LTRIM(Direc2)),
		@estado = Estado,
		@ciudad = Ciudad,
		@municipio = Municipio,
		@email = RTRIM(LTRIM(REPLACE(Email, ' ',''))),
		@tipocli = TipoCli,
		@fax = LEFT(Fax,20),
		@represent = Represent,
		@descripext = RTRIM(LTRIM(DescripExt))
FROM [dbo].[SACLIE] WITH (NOLOCK)
WHERE ([CodClie] = @codclie);

SET @clase = IIF(@clase NOT IN ('EN','ED','P','T','E'), 'N', @clase);

-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
SET @numgrp = 0;
SET @nom_table_m = 'SACLIE';
SET @nombregrp = 'Correos_y_Datos_Adiconales';

SELECT @numgrp = ISNULL(NumGrp,0)
FROM [dbo].[SAAGRUPOS] WITH (NOLOCK)
WHERE (CodTbl = @nom_table_m) AND (NombreGrp = @nombregrp);

SET @nom_table = CONCAT(@nom_table_m, '_',REPLICATE('0',2 - LEN(CAST(@numgrp AS VARCHAR(2)))),CAST(@numgrp AS VARCHAR(2)));

SET @string = CONCAT(
'SELECT @impuesto = Impuesto,
		@usa_descrip_ampli = Usa_Descrip_Ampli,
		@receptor_direccion_extranjero = RTRIM(LTRIM(receptor_direccion_extranjero))
FROM ', QUOTENAME(@NOM_TABLE),' WITH (NOLOCK)
WHERE (CodClie = ', '''',@codclie, '''',')');

EXECUTE sp_executesql @STRING, N'
@impuesto AS int OUTPUT,		
@usa_descrip_ampli smallint OUTPUT,
@receptor_direccion_extranjero varchar(300) OUTPUT',
@impuesto = @impuesto OUTPUT,		
@usa_descrip_ampli = @usa_descrip_ampli OUTPUT,
@receptor_direccion_extranjero = @receptor_direccion_extranjero OUTPUT;

SET @string = NULL;
SET @usa_descrip_ampli = ISNULL(@usa_descrip_ampli,0);
-- CONTANDO COMPROBANTES BLOQUEADOS DE CLIENTE
SET @Contador = 0;

SELECT @Contador = COUNT(*)
FROM [dbo].[encabezado] WITH (NOLOCK)
WHERE (estado_envio = '7') AND (tipo_documento IN ('01','02','03','04','09')) AND (CodClie = @codclie);

-- VALIDANDO SI HAY COMPROBANTES BLOQUEADOS
IF (@contador > 0)
	BEGIN
		-- UBICANDO DATOS DE UBICACION
		SELECT @pais = Pais
		FROM [dbo].[SAPAIS] WITH (NOLOCK)
		WHERE (IDPais = 506);

		SET @receptor_provincia = NULL
		SELECT @receptor_provincia = CAST(IDESTADOFE AS VARCHAR(1))
		FROM [dbo].[SAESTADO] WITH (NOLOCK)
		WHERE (Estado = @estado) AND (Pais = @pais);

		SET @receptor_canton = NULL
		SELECT @receptor_canton = IIF(LEN(CAST(IDCIUDADFE AS VARCHAR(2))) = 1, CONCAT('0', CAST(IDCIUDADFE AS VARCHAR(2))), CAST(IDCIUDADFE AS VARCHAR(2)))
		FROM [dbo].[SACIUDAD] WITH (NOLOCK)
		WHERE (Ciudad = @ciudad) AND (Pais = @pais);

		SET @receptor_distrito = NULL
		SELECT @receptor_distrito = IIF(LEN(CAST(IDMUNICIPIOFE AS VARCHAR(2))) = 1, CONCAT('0', CAST(IDMUNICIPIOFE AS VARCHAR(2))), CAST(IDMUNICIPIOFE AS VARCHAR(2))) 
		FROM [dbo].[SAMUNICIPIO] WITH (NOLOCK)
		WHERE (Municipio = @municipio) AND (Pais = @pais);

		IF (@receptor_provincia IS NULL) OR (@receptor_provincia = '')
			BEGIN
				SET @receptor_provincia = '1';
			END;
		IF (@receptor_canton IS NULL) OR (@receptor_canton = '')
			BEGIN
				SET @receptor_canton = '01';
			END;
		IF (@receptor_distrito IS NULL) OR (@receptor_distrito = '')
			BEGIN
				SET @receptor_distrito = '01';
			END;

		--UBICANDO DATOS DEL RECEPTOR
		SET @receptor_nombre = CASE WHEN @usa_descrip_ampli = 1 THEN IIF(LTRIM(RTRIM(@descripext))+'' = '', @descrip, LEFT(@descripext,100)) ELSE @descrip END;
		SET @receptor_tipo = CASE WHEN @clase = 'N' THEN IIF(@tipoid3 = 1, '01', '02') WHEN @clase = 'ED' THEN '03' WHEN @clase = 'EN' THEN '04' WHEN @clase IN ('P','E') THEN '' ELSE IIF(@tipoid3 = 1, '01', '02') END;
		SET @receptor_identificacion = IIF(@clase IN ('P','E'), '', DBO.RemoveChars(@id3));
		SET @pasaporte = IIF(@clase IN ('P','E'), DBO.RemoveChars(@id3), '');
		SET @receptor_direccion = CONCAT(@direc1, ' ', @direc2);
		SET @receptor_telefono = LEFT(DBO.RemoveChars(@telef),8);
		SET @receptor_fax = LEFT(DBO.RemoveChars(@fax),20);
		SET @receptor_email = REPLACE(REPLACE(@email, CHAR(0x1f), ''), CHAR(32), '');

		-- ACTUALIZANDO SAFACT
		UPDATE X SET 
			X.Descrip = @descrip,
			X.Direc1 = @direc1,
			X.Direc2 = @direc2,
			X.ID3 = @id3,
			X.Telef = @telef	
		FROM [dbo].[SAFACT] AS X INNER JOIN 
		(SELECT	clave,
				estado_envio,
				CodClie
		FROM [dbo].[encabezado]
		WHERE (estado_envio = '7') AND (tipo_documento IN ('01','02','03','04','09')) AND (CodClie = @codclie)) AS Y
		ON (X.clave = Y.clave);

		-- ACTUALIZANDO ENCABEZADO
		UPDATE [dbo].[encabezado] SET 
			receptor_nombre = @receptor_nombre,
			receptor_tipo = @receptor_tipo,
			receptor_identificacion = @receptor_identificacion,
			pasaporte = @pasaporte,
			receptor_direccion = @receptor_direccion,
			receptor_telefono = @receptor_telefono,
			receptor_fax = @receptor_fax,
			receptor_email = @receptor_email,
			receptor_provincia = @receptor_provincia,
			receptor_canton = @receptor_canton,
			receptor_distrito = @receptor_distrito,
			receptor_direccion_extranjero = IIF(tipo_documento = '09', @receptor_direccion_extranjero, receptor_direccion_extranjero),
			estado_envio = '0'	
		WHERE (estado_envio = '7') AND (tipo_documento IN ('01','02','03','04','09')) AND (CodClie = @codclie)

		-- ACTUALIZANO TABLA CAMBIOSMAESTROS
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = CONCAT('Procesado con Exito. ',cast(@contador as varchar(100)), ' comprobantes desbloqueados.'),
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;
ELSE
	BEGIN
		-- ACTUALIZANO TABLA CAMBIOSMAESTROS
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = 'Procesado con Exito. No hay comprobantes bloqueados.',
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;";

        public static string proveedores = @"
SET DATEFORMAT YMD;
DECLARE
@id3 varchar(25),
@tipoid3 smallint,
@telef varchar(30),
@clase varchar(10),
@contador int,
@descrip varchar(60),
@direc1 varchar(60),
@direc2 varchar(60),
@estado int,
@ciudad int,
@municipio int,
@email varchar(60),
@fax varchar(30),
@esreten smallint,
-- variables tabla encabezado
@receptor_nombre varchar(100),
@receptor_tipo varchar(2),
@receptor_identificacion varchar(12),
@pasaporte varchar(20),
@receptor_provincia varchar(1),
@receptor_canton varchar(2),
@receptor_distrito varchar(2),
@receptor_direccion varchar(160),
@receptor_telefono varchar(20),
@receptor_fax varchar(20),
@receptor_email varchar(100),
@pais as int

SET NOCOUNT ON;

SELECT	@id3 = ID3,
		@tipoid3 = TipoID3,
		@telef = Telef,
		@clase = Clase,
		@descrip = RTRIM(LTRIM(Descrip)),
		@direc1 = RTRIM(LTRIM(Direc1)),
		@direc2 = RTRIM(LTRIM(Direc2)),
		@estado = Estado,
		@ciudad = Ciudad,
		@municipio = Municipio,
		@email = RTRIM(LTRIM(REPLACE(Email, ' ',''))),
		@esreten = EsReten,
		@fax = LEFT(Fax,20)			
FROM [dbo].[SAPROV]
WHERE ([CodProv] = @codprov);

SET @clase = IIF(@clase NOT IN ('EN', 'ED', 'P'), 'N', @clase);

IF (@esreten = 0)
	BEGIN		
		-- CONTANDO COMPROBANTES BLOQUEADOS DE CLIENTE
		SET @contador = 0;

		SELECT @contador = COUNT(*)
		FROM [dbo].[encabezado] WITH (NOLOCK)
		WHERE (estado_envio = '7') AND (tipo_documento IN ('08')) AND (CodProv = @codprov);

		-- VALIDANDO SI HAY COMPROBANTES BLOQUEADOS
		IF (@contador > 0)
			BEGIN
				-- UBICANDO DATOS DE UBICACION
				SELECT @pais = Pais
				FROM [dbo].[SAPAIS] WITH (NOLOCK)
				WHERE (IDPais = 506);

				SET @receptor_provincia = NULL
				SELECT @receptor_provincia = CAST(IDESTADOFE AS VARCHAR(1))
				FROM [dbo].[SAESTADO] WITH (NOLOCK)
				WHERE (Estado = @estado) AND (Pais = @pais);

				SET @receptor_canton = NULL
				SELECT @receptor_canton = IIF(LEN(CAST(IDCIUDADFE AS VARCHAR(2))) = 1, CONCAT('0', CAST(IDCIUDADFE AS VARCHAR(2))), CAST(IDCIUDADFE AS VARCHAR(2)))
				FROM [dbo].[SACIUDAD] WITH (NOLOCK)
				WHERE (Ciudad = @ciudad) AND (Pais = @pais);

				SET @receptor_distrito = NULL
				SELECT @receptor_distrito = IIF(LEN(CAST(IDMUNICIPIOFE AS VARCHAR(2))) = 1, CONCAT('0', CAST(IDMUNICIPIOFE AS VARCHAR(2))), CAST(IDMUNICIPIOFE AS VARCHAR(2))) 
				FROM [dbo].[SAMUNICIPIO] WITH (NOLOCK)
				WHERE (Municipio = @municipio) AND (Pais = @pais);

				IF (@receptor_provincia IS NULL) OR (@receptor_provincia = '')
					BEGIN
						SET @receptor_provincia = '1';
					END;
				IF (@receptor_canton IS NULL) OR (@receptor_canton = '')
					BEGIN
						SET @receptor_canton = '01';
					END;
				IF (@receptor_distrito IS NULL) OR (@receptor_distrito = '')
					BEGIN
						SET @receptor_distrito = '01';
					END;

				-- UBICANDO DATOS DEL RECEPTOR
				SET @receptor_nombre = @descrip;
				SET @receptor_tipo = CASE @clase WHEN 'N' THEN IIF(@tipoid3 = 1, '01', '02') WHEN 'ED' THEN '03' WHEN 'EN' THEN '04' WHEN 'P' THEN '' ELSE IIF(@tipoid3 = 1, '01', '02') END;
				SET @receptor_identificacion = IIF(@clase = 'P', '', DBO.RemoveChars(@id3));
				SET @pasaporte = IIF(@clase = 'P', DBO.RemoveChars(@id3), '');
				SET @receptor_direccion = CONCAT(@direc1, ' ', @direc2);
				SET @receptor_telefono = LEFT(DBO.RemoveChars(@telef),8);
				SET @receptor_fax = LEFT(DBO.RemoveChars(@fax),20);
				SET @receptor_email = REPLACE(REPLACE(@email, CHAR(0x1f), ''), CHAR(32), '');

				-- ACTUALIZANDO SACOMP
				UPDATE X SET X.Descrip = @descrip,
							 X.Direc1 = @direc1,
							 X.Direc2 = @direc2,
							 X.ID3 = @id3,
							 X.Telef = @Telef	
				FROM [dbo].[SAFACT] AS X INNER JOIN 
				(SELECT	clave,
						estado_envio,
						CodProv
				FROM [dbo].[encabezado]
				WHERE (estado_envio = '7') AND (tipo_documento IN ('08')) AND (CodProv = @codprov)) AS Y
				ON (X.clave = Y.clave);

				-- ACTUALIZANDO ENCABEZADO DESBLOQUEANDO COMPROBANTES
				UPDATE [dbo].[encabezado] SET 
					receptor_nombre = @receptor_nombre,
					receptor_tipo = @receptor_tipo,
					receptor_identificacion = @receptor_identificacion,
					pasaporte = @pasaporte,
					receptor_direccion = @receptor_direccion,
					receptor_telefono = @receptor_telefono,
					receptor_fax = @receptor_fax,
					receptor_email = @receptor_email,
					receptor_provincia = @receptor_provincia,
					receptor_canton = @receptor_canton,
					receptor_distrito = @receptor_distrito,
					estado_envio = '0'				
				WHERE (estado_envio = '7') AND (tipo_documento IN ('08')) AND (CodProv = @codprov)

				-- ACTUALIZANO TABLA CAMBIOSMAESTROS
				UPDATE [dbo].[CambiosMaestrosFE] SET 
					estado_sync = 1,
					mensaje_sync = CONCAT('Procesado con Exito. ',cast(@contador as varchar(100)), ' comprobantes desbloqueados.'),
					fechaCE = GETDATE()
				WHERE (Id = @id);
			END;
		ELSE
			BEGIN
				-- ACTUALIZANO TABLA CAMBIOSMAESTROS
				UPDATE [dbo].[CambiosMaestrosFE] SET 
					estado_sync = 1,
					mensaje_sync = 'Procesado con Exito. No hay comprobantes bloqueados.',
					fechaCE = GETDATE()
				WHERE (Id = @id);
			END;
	END;";

        public static string productos = @"
SET DATEFORMAT YMD;
DECLARE 
@partida_arancelaria varchar(15),
@codigo_hacienda varchar(13),
@contador int,
@totallineas int,
@condicion int,
@id_comprobante int,
-- tabla adicional
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50),
@string nvarchar(max),
@desbloqueados int

DECLARE @COMPROBANTES TABLE (id int, Linea int)
SET NOCOUNT ON;

-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
SET @numgrp = 0;
SET @nom_table_m = 'SAPROD';
SET @nombregrp = 'Datos_Adicionales';

SELECT @numgrp = ISNULL(NumGrp,0)
FROM [dbo].[SAAGRUPOS] WITH (NOLOCK)
WHERE (CodTbl = @nom_table_m) AND (NombreGrp = @nombregrp);

SET @nom_table = CONCAT(@nom_table_m, '_',REPLICATE('0',2 - LEN(CAST(@numgrp AS VARCHAR(2)))),CAST(@numgrp AS VARCHAR(2)));

SET @string = CONCAT(
'SELECT @partida_arancelaria = partida_arancelaria,
		@codigo_hacienda = codigo_hacienda
FROM ', QUOTENAME(@NOM_TABLE),' WITH (NOLOCK)
WHERE ([CodProd] = ', '''',@codprod, '''',')');

EXECUTE sp_executesql @string, N'
@partida_arancelaria varchar(15) OUTPUT,
@codigo_hacienda varchar(13) OUTPUT',
@partida_arancelaria = @partida_arancelaria OUTPUT,		
@codigo_hacienda = @codigo_hacienda OUTPUT;

SET @string = NULL;
SET @contador = 0;
SET @desbloqueados = 0;

-- SECCION PARA FACTURAS DE EXPORTACION
IF (LEN(@partida_arancelaria) > 0)
	BEGIN
		IF (LEN(@codigo_hacienda) = 13)
			BEGIN
				-- BUSCANDO EL TOTAL DE FACTURAS DE EXPORTACION BLOQUEADAS QUE TENGAN ESTE PRODUCTO
				SELECT @contador = COUNT(*)
				FROM
				(SELECT DISTINCT A.id, B.codigo_producto
				FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B WITH (NOLOCK)
				ON (A.id = B.id_encabezado)
				WHERE (A.tipo_documento = '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0)) T;

				SET @contador = ISNULL(@contador,0);

				INSERT INTO @COMPROBANTES (id, Linea)
				SELECT	M.id,
						ROW_NUMBER() OVER(ORDER BY M.id) Linea
				FROM
				(SELECT DISTINCT A.id, B.codigo_producto
				FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B  WITH (NOLOCK)
				ON (A.id = B.id_encabezado)
				WHERE (A.tipo_documento = '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0)) M;

				-- VALIDANDO SI HAY COMPROBANTES PARA DESBLOQUEAR
				IF (@contador > 0)
					BEGIN
						-- ACTUALIZANDO TABLA LINEA DE ESTE PRODUCTO DE TODAS LAS FACTURAS DE EXPORTACION QUE ESTEN BLOQUEADAS
						UPDATE B SET B.partida_arancelaria = @partida_arancelaria,
									 B.codigo_hacienda = @codigo_hacienda
						FROM [dbo].[linea] AS B INNER JOIN [dbo].[encabezado] A
						ON (B.id_encabezado = A.id)
						WHERE (A.tipo_documento = '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0);

						-- RECORRIENDO FACTURAS DE EXPORTACION
						WHILE (@contador > 0)
							BEGIN
								SET @id_comprobante = 0;
								SET @totallineas = 0;
								SET @condicion = 0;

								-- UBICANDO ID DE FACTURA DE EXP
								SELECT @id_comprobante = id
								FROM @COMPROBANTES
								WHERE (Linea = @contador);

								-- VALIDANDO QUE SEA UN ID VALIDO
								IF (@id_comprobante > 0)
									BEGIN
										-- BUSCANDO TOTAL DE LINEAS
										SELECT @totallineas = COUNT(*)
										FROM [dbo].[linea] WITH (NOLOCK)
										WHERE (id_encabezado = @id_comprobante);

										-- BUSCANDO TOTAL DE LINEAS CON PARTIDA ARANCELARIA Y CABYS CORRECTOS
										SELECT @condicion = COUNT(*)
										FROM
										(SELECT id_encabezado										
										FROM [dbo].[linea] WITH (NOLOCK)
										WHERE (id_encabezado = @id_comprobante) AND (LEN(partida_arancelaria) > 0) AND (LEN(codigo_hacienda) = 13) AND (servicio = 0)
										UNION ALL
										SELECT id_encabezado
										FROM [dbo].[linea] WITH (NOLOCK)
										WHERE (id_encabezado = @id_comprobante) AND (LEN(codigo_hacienda) = 13) AND (servicio = 1)) C;

										SET @totallineas = ISNULL(@totallineas,0);
										SET @condicion = ISNULL(@condicion,0);

										-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP SEA MAYOR A 0
										IF (@totallineas > 0)
											BEGIN
												-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP QUE CUMPLAN CONDICION SEA MAYOR A 0
												IF (@condicion > 0)
													BEGIN
														-- VALIDANDO QUE TODAS LAS LINEAS ESTEN CORRECTAS PARA ENVIAR FACTURA DE EXP
														IF (@totallineas = @condicion)
															BEGIN
																-- ACTUALIZANDO TABLA ENCABEZADO
																UPDATE [dbo].[encabezado] SET estado_envio = '0'				
																WHERE (estado_envio = '7') AND (id = @id_comprobante);

																SET @desbloqueados +=1;
															END;
													END;
											END;
									END;

								SET @contador -=1;	
							END;

						DELETE FROM @COMPROBANTES;
					END;
			END;
	END;

SET @contador = 0;

-- SECCION PARA COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION
IF (LEN(@codigo_hacienda) = 13)
	BEGIN
		-- BUSCANDO EL TOTAL DE COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION BLOQUEADAS QUE TENGAN ESTE PRODUCTO
		SELECT @contador = COUNT(*)
		FROM
		(SELECT DISTINCT A.id, B.codigo_producto
		FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B WITH (NOLOCK)
		ON (A.id = B.id_encabezado)
		WHERE (A.tipo_documento <> '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0)) T;

		SET @contador = ISNULL(@contador,0);

		INSERT INTO @COMPROBANTES (id, Linea)
		SELECT M.id,
				ROW_NUMBER() OVER(ORDER BY M.id) Linea
		FROM
		(SELECT DISTINCT A.id, B.codigo_producto
		FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B  WITH (NOLOCK)
		ON (A.id = B.id_encabezado)
		WHERE (A.tipo_documento <> '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0)) M;

		-- VALIDANDO SI HAY COMPROBANTES PARA DESBLOQUEAR
		IF (@contador > 0)
			BEGIN
				-- ACTUALIZANDO TABLA LINEA DE ESTE PRODUCTO DE TODAS LOS COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION QUE ESTEN BLOQUEADAS
				UPDATE B SET B.codigo_hacienda = @codigo_hacienda
				FROM [dbo].[linea] AS B INNER JOIN encabezado A
				ON (B.id_encabezado = A.id)
				WHERE (A.tipo_documento <> '09') AND (B.codigo_producto = @codprod) AND (A.estado_envio = '7') AND (B.servicio = 0);

				-- RECORRIENDO FACTURAS DE EXPORTACION
				WHILE (@contador > 0)
					BEGIN
						SET @id_comprobante = 0;
						SET @totallineas = 0;
						SET @condicion = 0;

						-- UBICANDO ID DE FACTURA DE EXP
						SELECT @id_comprobante = id
						FROM @COMPROBANTES
						WHERE (Linea = @contador);

						-- VALIDANDO QUE SEA UN ID VALIDO
						IF (@id_comprobante > 0)
							BEGIN
								-- BUSCANDO TOTAL DE LINEAS
								SELECT @totallineas = COUNT(*)
								FROM [dbo].[linea] WITH (NOLOCK)
								WHERE (id_encabezado = @id_comprobante);

								-- BUSCANDO TOTAL DE LINEAS CON CABYS CORRECTOS
								SELECT @condicion = COUNT(*)
								FROM [dbo].[linea] WITH (NOLOCK)
								WHERE (id_encabezado = @id_comprobante) AND (LEN(codigo_hacienda) = 13);

								SET @totallineas = ISNULL(@totallineas,0);
								SET @condicion = ISNULL(@condicion,0);

								-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP SEA MAYOR A 0
								IF (@totallineas > 0)
									BEGIN
										-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP QUE CUMPLAN CONDICION SEA MAYOR A 0
										IF (@condicion > 0)
											BEGIN
												-- VALIDANDO QUE TODAS LAS LINEAS ESTEN CORRECTAS PARA ENVIAR FACTURA DE EXP
												IF (@totallineas = @condicion)
													BEGIN
														-- ACTUALIZANDO TABLA ENCABEZADO
														UPDATE [dbo].[encabezado] SET estado_envio = '0'		
														WHERE (estado_envio = '7') AND (id = @id_comprobante);

														SET @desbloqueados +=1;
													END;
											END;
									END;
							END;

						SET @contador -=1;						
					END;

				DELETE FROM @COMPROBANTES;
			END;
	END;

-- ACTUALIZANO TABLA CAMBIOSMAESTROS
IF (@desbloqueados > 0)
	BEGIN
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = CONCAT('Procesado con Exito. ',cast(@desbloqueados as varchar(100)), ' comprobantes desbloqueados.'),
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;
ELSE
	BEGIN
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = 'Procesado con Exito. No hay comprobantes bloqueados, o existen mas items con la misma condicion que este y se debe aplicar la corrección en la ficha',
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;";

        public static string servicios = @"
SET DATEFORMAT YMD;
DECLARE 
@codigo_hacienda as varchar(13),
@contador as int,
@totallineas int,
@condicion int,
@id_comprobantes int,
-- tabla adicional
@numgrp int, 
@nom_table_m varchar(50),
@nom_table varchar(50),
@nombregrp varchar(50),
@string nvarchar(max),
@desbloqueados int

DECLARE @COMPROBANTES TABLE (id int, Linea int)
SET NOCOUNT ON;

-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
SET @numgrp = 0;
SET @nom_table_m = 'SASERV';
SET @nombregrp = 'Datos_Adicionales';

SELECT @numgrp = ISNULL(NumGrp,0)
FROM [dbo].[SAAGRUPOS] WITH (NOLOCK)
WHERE (CodTbl = @nom_table_m) AND (NombreGrp = @nombregrp);

SET @nom_table = CONCAT(@nom_table_m, '_',REPLICATE('0',2 - LEN(CAST(@numgrp AS VARCHAR(2)))),CAST(@numgrp AS VARCHAR(2)));

SET @string = CONCAT(
'SELECT @codigo_hacienda = codigo_hacienda
FROM ', QUOTENAME(@NOM_TABLE),' WITH (NOLOCK)
WHERE ([CodServ] = ', '''',@codserv, '''',')');

EXECUTE sp_executesql @string, N'
@codigo_hacienda varchar(13) OUTPUT',
@codigo_hacienda = @codigo_hacienda OUTPUT;

SET @string = NULL;
SET @contador = 0;
SET @desbloqueados = 0;

-- SECCION PARA COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION
IF (LEN(@codigo_hacienda) = 13)
	BEGIN
		-- BUSCANDO EL TOTAL DE COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION BLOQUEADAS QUE TENGAN ESTE PRODUCTO
		SELECT @contador = COUNT(*)
		FROM
		(SELECT DISTINCT A.id, B.codigo_producto
		FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B WITH (NOLOCK)
		ON (A.id = B.id_encabezado)
		WHERE (B.codigo_producto = @codserv) AND (A.estado_envio = '7') AND (B.servicio = 1)) T;

		SET @contador = ISNULL(@Contador,0);

		INSERT INTO @COMPROBANTES (id, Linea)
		SELECT M.id,
				ROW_NUMBER() OVER(ORDER BY M.id) Linea
		FROM
		(SELECT DISTINCT A.id, B.codigo_producto
		FROM [dbo].[encabezado] AS A WITH (NOLOCK) INNER JOIN [dbo].[Linea] B  WITH (NOLOCK)
		ON (A.id = B.id_encabezado)
		WHERE (B.codigo_producto = @codserv) AND (A.estado_envio = '7') AND (B.servicio = 1)) M;

		-- VALIDANDO SI HAY COMPROBANTES PARA DESBLOQUEAR
		IF (@contador > 0)
			BEGIN
				-- ACTUALIZANDO TABLA LINEA DE ESTE PRODUCTO DE TODAS LOS COMPROBANTES QUE NO SEAN FACTURAS DE EXPORTACION QUE ESTEN BLOQUEADAS---
				UPDATE B SET B.codigo_hacienda = @codigo_hacienda
				FROM [dbo].[linea] AS B INNER JOIN [dbo].[encabezado] A
				ON (B.id_encabezado = A.id)
				WHERE (B.codigo_producto = @codserv) AND (A.estado_envio = '7') AND (B.servicio = 1);

				-- RECORRIENDO FACTURAS DE EXPORTACION
				WHILE (@contador > 0)
					BEGIN
						SET @id_comprobantes = 0;
						SET @totallineas = 0;
						SET @condicion = 0;

						-- UBICANDO ID DE FACTURA DE EXP
						SELECT @id_comprobantes = id
						FROM @COMPROBANTES
						WHERE (Linea = @contador);

						-- VALIDANDO QUE SEA UN ID VALIDO
						IF (@id_comprobantes > 0)
							BEGIN
								-- BUSCANDO TOTAL DE LINEAS
								SELECT @totallineas = COUNT(*)
								FROM [dbo].[linea] WITH (NOLOCK)
								WHERE (id_encabezado = @id_comprobantes);

								-- BUSCANDO TOTAL DE LINEAS CON CABYS CORRECTOS
								SELECT @condicion = COUNT(*)
								FROM [dbo].[linea] WITH (NOLOCK)
								WHERE (id_encabezado = @id_comprobantes) AND (LEN(codigo_hacienda) = 13);

								SET @totallineas = ISNULL(@totallineas,0);
								SET @condicion = ISNULL(@condicion,0);

								-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP SEA MAYOR A 0
								IF (@totallineas > 0)
									BEGIN
										-- VALIDANDO QUE TOTAL DE LINEAS DE FACTURA DE EXP QUE CUMPLAN CONDICION SEA MAYOR A 0
										IF (@condicion > 0)
											BEGIN
												-- VALIDANDO QUE TODAS LAS LINEAS ESTEN CORRECTAS PARA ENVIAR FACTURA DE EXP
												IF (@totallineas = @condicion)
													BEGIN
														-- ACTUALIZANDO TABLA ENCABEZADO
														UPDATE [dbo].[encabezado] SET estado_envio = '0'
														WHERE (estado_envio = '7') AND (id = @id_comprobantes);

														SET @desbloqueados +=1;
													END;
											END;
									END;
							END;

						SET @Contador -=1;
					END;

				DELETE FROM @COMPROBANTES;
			END;
	END;

-- ACTUALIZANO TABLA CAMBIOSMAESTROS
IF (@desbloqueados > 0)
	BEGIN
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = CONCAT('Procesado con Exito. ',cast(@desbloqueados as varchar(100)), ' comprobantes desbloqueados.'),
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;
ELSE
	BEGIN
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = 'Procesado con Exito. No hay comprobantes bloqueados, o existen mas items con la misma condicion que este y se debe aplicar la corrección en la ficha',
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;";

        public static string otros_cargos = @"
SET DATEFORMAT YMD;
DECLARE
@id3 varchar(25),
@descrip varchar(60),
@contador int

SET NOCOUNT ON;

SELECT	@id3 = LEFT(ID3,12),
		@descrip = Descrip			
FROM [dbo].[SAMECA]
WHERE ([CodMeca] = @codmeca);

SET @contador = 0;

-- CONTANDO LINEAS DE COMPROBANTES CON CARGOS BLOQUEADOS CON ESTE TERCERO
SELECT @contador = COUNT(*)
FROM [dbo].[encabezado] AS X WITH (NOLOCK) INNER JOIN
(SELECT   A.TipoFac,
		 A.NumeroD,
		 A.Clave,
		 B.NroLinea,
		 B.CodMeca
FROM [dbo].[SAFACT] AS A WITH (NOLOCK) INNER JOIN
(SELECT  TipoFac,
		 NumeroD,
		 NroLinea,
		 CodMeca
FROM [dbo].[SAITEMFAC] WITH (NOLOCK)
WHERE (TipoFac IN ('A','B')) AND (EsServ = 1) AND (CodMeca IS NOT NULL)) AS B
ON (A.TipoFac = B.TipoFac) AND (A.NumeroD = B.NumeroD)) AS Y
ON (X.clave = Y.Clave)
WHERE (X.estado_envio = '7') AND (Y.CodMeca = @codmeca);

IF (@contador > 0)
	BEGIN
		-- ACTUALIZANDO TABLA OTROS_CARGOS
		UPDATE O SET O.otc_identificacion = @id3,
					 O.otc_nombre = @descrip
		FROM [dbo].[otros_cargos] AS O INNER JOIN
		(SELECT   X.Clave,
				 Y.NroLinea,
				 Y.CodMeca
		FROM [dbo].[encabezado] AS X INNER JOIN
		(SELECT   A.TipoFac,
				 A.NumeroD,
				 A.Clave,
				 B.NroLinea,
				 B.CodMeca
		FROM [dbo].[SAFACT] AS A INNER JOIN
		(SELECT  TipoFac,
				 NumeroD,
				 NroLinea,
				 CodMeca
		FROM [dbo].[SAITEMFAC]
		WHERE (TipoFac IN ('A','B')) AND (EsServ = 1) AND (CodMeca IS NOT NULL)) AS B
		ON (A.TipoFac = B.TipoFac) AND (A.NumeroD = B.NumeroD)) AS Y
		ON (X.clave = Y.Clave)
		WHERE (X.estado_envio = '7') AND (Y.CodMeca = @codmeca)) AS P
		ON (O.clave = P.clave) AND (O.otc_nrolinea = P.NroLinea);

		-- ACTUALIZANDO TABLA ENCABEZADO
		UPDATE O SET O.estado_envio = '0'
		FROM [dbo].[encabezado] AS O INNER JOIN
		(SELECT DISTINCT X.Clave,
						 Y.CodMeca
		FROM [dbo].[encabezado] AS X INNER JOIN
		(SELECT   A.TipoFac,
				 A.NumeroD,
				 A.Clave,
				 B.NroLinea,
				 B.CodMeca
		FROM [dbo].[SAFACT] AS A INNER JOIN
		(SELECT  TipoFac,
				 NumeroD,
				 NroLinea,
				 CodMeca
		FROM [dbo].[SAITEMFAC]
		WHERE (TipoFac IN ('A','B')) AND (EsServ = 1) AND (CodMeca IS NOT NULL)) AS B
		ON (A.TipoFac = B.TipoFac) AND (A.NumeroD = B.NumeroD)) AS Y
		ON (X.clave = Y.Clave)
		WHERE (X.estado_envio = '7') AND (Y.CodMeca = @codmeca)) AS P
		ON (O.clave = P.clave);

		-- ACTUALIZANO TABLA CAMBIOSMAESTROS
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = CONCAT('Procesado con Exito. ',cast(@contador as varchar(100)), ' comprobantes desbloqueados.'),
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;
ELSE
	BEGIN
		-- ACTUALIZANO TABLA CAMBIOSMAESTROS
		UPDATE [dbo].[CambiosMaestrosFE] SET 
			estado_sync = 1,
			mensaje_sync = 'Procesado con Exito. No hay comprobantes bloqueados, o existen mas items con la misma condicion que este y se debe aplicar la corrección en la ficha',
			fechaCE = GETDATE()
		WHERE (Id = @id);
	END;";
    }
}
