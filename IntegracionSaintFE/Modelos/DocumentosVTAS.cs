﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    public class SAFACT_0X
    {
        public string TipoFac { get; set; }

        public string NumeroD { get; set; }

        public int Tipo_documento { get; set; }

        public string Numero_Documento { get; set; }

        public string Nombre_Institucion { get; set; }

        public DateTime Fecha_Documento { get; set; }

        public int Porcentaje { get; set; }

        public short Factor_Cambio { get; set; }

        public string WM_GLN { get; set; }

        public short Guardar_Datos_Exoneracion { get; set; }

        public short No_Exonerar { get; set; }

        public int Codigo_Actividad_S { get; set; }

        public string Pedido_ICE { get; set; }

        public int Moneda { get; set; }

        public string Nro_Recepcion { get; set; }

        public DateTime Fecha_Orden { get; set; }

        public string Nro_Reclamo { get; set; }

        public DateTime Fecha_Reclamo { get; set; }

        public SAFACT_0X()
        {
            TipoFac = "";
            NumeroD = "";
            Numero_Documento = "";
            Nombre_Institucion = "";
            WM_GLN = "";
            Pedido_ICE = "";
            Nro_Recepcion = "";
            Nro_Reclamo = "";
        }
    }

    public class SAFACT
    {
        public string TipoFac { get; set; }

        public string NumeroD { get; set; }

        public int NroUnico { get; set; }

        public string NumeroR { get; set; }

        public decimal Factor { get; set; }

        public string CodClie { get; set; }

        public string CodUbic { get; set; }

        public string Descrip { get; set; }

        public string Direc1 { get; set; }

        public string Direc2 { get; set; }

        public string Telef { get; set; }

        public string ID3 { get; set; }

        public decimal MtoTax { get; set; }

        public decimal Fletes { get; set; }

        public decimal TGravable { get; set; }

        public decimal TExento { get; set; }

        public DateTime FechaE { get; set; }

        public DateTime FechaV { get; set; }

        public decimal MtoTotal { get; set; }

        public decimal Contado { get; set; }

        public decimal Credito { get; set; }

        public decimal CancelI { get; set; }

        public decimal CancelA { get; set; }

        public decimal CancelE { get; set; }

        public decimal CancelC { get; set; }

        public decimal CancelT { get; set; }

        public decimal CancelG { get; set; }

        public decimal CancelP { get; set; }

        public decimal Descto1 { get; set; }

        public decimal Descto2 { get; set; }

        public decimal TotalPrd { get; set; }

        public decimal TotalSrv { get; set; }

        public string CodOper { get; set; }

        public string Notas1 { get; set; }

        public string Notas2 { get; set; }

        public string Notas3 { get; set; }

        public string Notas4 { get; set; }

        public string Notas5 { get; set; }

        public string Notas6 { get; set; }

        public string Notas7 { get; set; }

        public string Notas8 { get; set; }

        public string Notas9 { get; set; }

        public string Notas10 { get; set; }

        public string NumeroDE { get; set; }

        public string Clave { get; set; }

        public string TipoDocFE { get; set; }

        public string ConsecutivoDE { get; set; }

        public string ClaveRef { get; set; }

        public short ExoneracionE { get; set; }

        public int NroItemT { get; set; }

        public decimal iva_devuelto { get; set; }

        public decimal Monto_Exonerado { get; set; }

        public string Num_Original { get; set; }

        public string CodTaxsClie { get; set; }

        public int EstadoFE { get; set; }

        public string MensajeFE { get; set; }

        public string OrdenC { get; set; }

        public SAFACT()
        {
            TipoFac = "";
            NumeroD = "";
            NumeroR = "";
            CodClie = "";
            CodUbic = "";
            Descrip = "";
            Direc1 = "";
            Direc2 = ""; ;
            Telef = "";
            ID3 = "";
            CodOper = "";
            Notas1 = "";
            Notas2 = "";
            Notas3 = "";
            Notas4 = "";
            Notas5 = "";
            Notas6 = "";
            Notas7 = "";
            Notas8 = "";
            Notas9 = "";
            Notas10 = "";
            NumeroDE = "";
            Clave = "";
            TipoDocFE = "";
            ConsecutivoDE = "";
            ClaveRef = "";
            Num_Original = "";
            CodTaxsClie = "";
            MensajeFE = "";
            OrdenC = "";
        }
    }

    public class SATAXVTA
    {
        public string TipoFac { get; set; }

        public string NumeroD { get; set; }

        public string CodTaxs { get; set; }

        public decimal Monto { get; set; }

        public decimal MtoTax { get; set; }

        public decimal TGravable { get; set; }

        public short EsReten { get; set; }

        public string Status { get; set; }

        public SATAXVTA()
        {
            TipoFac = "";
            NumeroD = "";
            CodTaxs = "";
            Status = "";
        }
    }

    public class SAITEMFAC
    {
        public string TipoFac { get; set; }

        public string NumeroD { get; set; }

        public int NroLinea { get; set; }

        public int NroLineaC { get; set; }

        public string CodItem { get; set; }

        public string CodMeca { get; set; }

        public string Descrip1 { get; set; }

        public string Descrip2 { get; set; }

        public string Descrip3 { get; set; }

        public string Descrip4 { get; set; }

        public string Descrip5 { get; set; }

        public string Descrip6 { get; set; }

        public string Descrip7 { get; set; }

        public string Descrip8 { get; set; }

        public string Descrip9 { get; set; }

        public string Descrip10 { get; set; }

        public decimal Cantidad { get; set; }

        public decimal TotalItem { get; set; }

        public decimal Precio { get; set; }

        public decimal MtoTax { get; set; }

        public decimal PriceO { get; set; }

        public decimal Descto { get; set; }

        public short EsServ { get; set; }

        public short EsExento { get; set; }

        public int NroLineaFE { get; set; }

        public SAITEMFAC()
        {
            TipoFac = "";
            NumeroD = "";
            CodItem = "";
            CodMeca = "";
            Descrip1 = "";
            Descrip2 = "";
            Descrip3 = "";
            Descrip4 = "";
            Descrip5 = "";
            Descrip6 = "";
            Descrip7 = "";
            Descrip8 = "";
            Descrip9 = "";
            Descrip10 = "";
        }
    }

    public class SATAXITF
    {
        public string TipoFac { get; set; }

        public string NumeroD { get; set; }

        public int NroLinea { get; set; }

        public int NroLineaC { get; set; }

        public string CodTaxs { get; set; }

        public string CodItem { get; set; }

        public decimal Monto { get; set; }

        public decimal TGravable { get; set; }

        public decimal MtoTax { get; set; }

        public string Status { get; set; }

        public SATAXITF()
        {
            TipoFac = "";
            NumeroD = "";
            CodTaxs = "";
            CodItem = "";
            Status = "";
        }
    }
}
