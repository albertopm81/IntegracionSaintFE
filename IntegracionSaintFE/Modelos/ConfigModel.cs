﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    class ConfigModel
    {
        public string SQLConnectionString { get; set; }
        public int SyncTicksSeconds { get; set; }
        public int SyncAllEachSeconds { get; set; }
        public string Pais { get; set; }
        public int MonthsHistMaster { get; set; }
        public DateTime LastTime { get; set; }
    }
}
