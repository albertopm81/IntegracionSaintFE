﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    class ComprobantesFE
    {
        public long Id { get; set; }

        public long NroUnico { get; set; }

        public string TipoDoc { get; set; }

        public string NumeroD { get; set; }

        public string NumeroDE { get; set; }

        public string Clave { get; set; }

        public string TipoDocFE { get; set; }

        public string ConsecutivoDE { get; set; }

        public string ClaveRef { get; set; }

        public string RefTipoDoc { get; set; }

        public DateTime RefFecha { get; set; }

        public string RefCodigo { get; set; }

        public string RefRazon { get; set; }

        public short ExoneracionE { get; set; }

        public int NroItemT { get; set; }

        public decimal Iva_Devuelto { get; set; }

        public decimal Monto_Exonerado { get; set; }

        public string CodTaxsClie { get; set; }

        public int EstadoFE { get; set; }

        public string MensajeFE { get; set; }

        public ComprobantesFE()
        {
            TipoDoc = "";
            NumeroD = "";
            NumeroDE = "";
            Clave = "";
            TipoDocFE = "";
            ConsecutivoDE = "";
            ClaveRef = "";
            RefTipoDoc = "";
            RefCodigo = "";
            RefRazon = "";
            CodTaxsClie = "";
            MensajeFE = "";
        }
    }
}
