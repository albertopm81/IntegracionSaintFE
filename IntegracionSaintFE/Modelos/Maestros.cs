﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    public class SACONF
    {
        public string CodSucu { get; set; }

        public string Descrip { get; set; }

        public string Direc1 { get; set; }

        public string Direc2 { get; set; }

        public string Email { get; set; }

        public string Telef { get; set; }

        public string RIF { get; set; }

        public int Municipio { get; set; }

        public int Ciudad { get; set; }

        public int Estado { get; set; }

        public int Pais { get; set; }

        public decimal Factor { get; set; }

        public decimal FactorM { get; set; }

        public short UsaFactorM { get; set; }

        public string SimbFac { get; set; }

        public short EsMoneda { get; set; }

        public string CodMoneda { get; set; }

        public string CodMonedaR { get; set; }

        public string UsuaFE { get; set; }

        public string PassFE { get; set; }

        public string EmisorTipo { get; set; }

        public string DescripFletesFE { get; set; }

        public string NombreComercial { get; set; }

        public string correos_adicionales { get; set; }

        public int Sucursal { get; set; }

        public string VersionFE { get; set; }

        public string Llave_Sucursal { get; set; }

        public short Incluir_Receptor { get; set; }

        public short Unidad_Servicios { get; set; }

        public string Cabys_Fletes { get; set; }

        public short Regalias { get; set; }

        public SACONF()
        {
            CodSucu = "";
            Descrip = "";
            Direc1 = "";
            Direc2 = "";
            Email = "";
            Telef = "";
            RIF = "";
            SimbFac = "";
            CodMoneda = "";
            CodMonedaR = "";
            UsuaFE = "";
            PassFE = "";
            EmisorTipo = "";
            DescripFletesFE = "";
            NombreComercial = "";
            correos_adicionales = "";
            VersionFE = "";
            Llave_Sucursal = "";
            Cabys_Fletes = "";
        }
    }

    public class RECEPTOR
    {
        public short Contado { get; set; }

        public string Receptor_Nombre { get; set; }

        public string Receptor_Tipo { get; set; }

        public string Receptor_Identificacion { get; set; }

        public string Pasaporte { get; set; }

        public int Pais { get; set; }

        public int Estado { get; set; }

        public int Ciudad { get; set; }

        public int Municipio { get; set; }

        public string Receptor_Direccion { get; set; }

        public string Receptor_Telefono { get; set; }

        public string Receptor_Fax { get; set; }

        public string Receptor_Email { get; set; }

        public short TipoCli { get; set; }

        public string Clase { get; set; }

        public RECEPTOR()
        {
            Receptor_Nombre = "";
            Receptor_Tipo = "";
            Receptor_Identificacion = "";
            Pasaporte = "";
            Receptor_Telefono = "";
            Receptor_Fax = "";
            Receptor_Email = "";
            Clase = "";
        }
    }

    public class SACLIE_0X
    {
        public string CodClie { get; set; }

        public string Correo_Adicional_1 { get; set; }

        public string Correo_Adicional_2 { get; set; }

        public string Correo_Adicional_3 { get; set; }

        public string Correo_Adicional_4 { get; set; }

        public short No_Ubicacion { get; set; }

        public int Codigo_Pais { get; set; }

        public string WM_Vendedor { get; set; }

        public string receptor_direccion_extranjero { get; set; }

        public string WM_GLN { get; set; }

        public short Receptor_Incluir { get; set; }

        public short Ubicacion_Incluir { get; set; }

        public short GS1 { get; set; }

        public int Impuesto { get; set; }

        public int Usa_Representante { get; set; }

        public int Usa_Descrip_Ampli { get; set; }

        public SACLIE_0X()
        {
            CodClie = "";
            Correo_Adicional_1 = "";
            Correo_Adicional_2 = "";
            Correo_Adicional_3 = "";
            Correo_Adicional_4 = "";
            WM_Vendedor = "";
            receptor_direccion_extranjero = "";
            WM_GLN = "";
        }
    }
    public class SACLIE_0X_1
    {
        public string CodClie { get; set; }

        public int Tipo_documento { get; set; }

        public string Numero_Documento { get; set; }

        public string Nombre_Institucion { get; set; }

        public DateTime Fecha_Documento { get; set; }

        public int Porcentaje { get; set; }

        public SACLIE_0X_1()
        {
            CodClie = "";
            Numero_Documento = "";
            Nombre_Institucion = "";
        }
    }

    public class ConsecutivosFE
    {
        public int Sucursal { get; set; }

        public int Estacion { get; set; }

        public int LenCorrelFE { get; set; }

        public int PrxFactFE { get; set; }

        public int PrxNDFE { get; set; }

        public int PrxNCFE { get; set; }

        public int PrxTickEFE { get; set; }

        public int PrxCompCFE { get; set; }

        public int PrxComFE { get; set; }

        public int PrxFExpFE { get; set; }
    }

    public class SAMECA
    {
        public string CodMeca { get; set; }

        public string Descrip { get; set; }

        public short TipoID3 { get; set; }

        public short TipoID { get; set; }

        public string ID3 { get; set; }

        public string DescOrder { get; set; }

        public string Clase { get; set; }

        public short Activo { get; set; }

        public string Direc1 { get; set; }

        public string Direc2 { get; set; }

        public string Telef { get; set; }

        public string Movil { get; set; }

        public string Email { get; set; }

        public SAMECA()
        {
            CodMeca = "";
            Descrip = "";
            ID3 = "";
            DescOrder = "";
            Clase = "";
            Direc1 = "";
            Direc2 = "";
            Telef = "";
            Movil = "";
            Email = "";
        }
    }

    public class SADEPO_0X
    {
        public string CodUbic { get; set; }

        public int Sucursal { get; set; }

        public string Llave_Sucursal { get; set; }

        public int Codigo_Actividad_S { get; set; }

        public SADEPO_0X()
        {
            CodUbic = "";
            Llave_Sucursal = "";
        }
    }

    public class FECodAreaPais
    {
        public int Id { get; set; }

        public string Descrip { get; set; }

        public string Prefijo { get; set; }

        public string Pais { get; set; }

        public FECodAreaPais()
        {
            Descrip = "";
            Prefijo = "";
            Pais = "";
        }
    }

    public class SAPAIS
    {
        public string Descrip { get; set; }

        public int Pais { get; set; }

        public string SPais { get; set; }

        public string SEstado { get; set; }

        public string SCiudad { get; set; }

        public string SMunicipio { get; set; }

        public int IDPais { get; set; }

        public string PaisTV { get; set; }

        public SAPAIS()
        {
            Descrip = "";
            SPais = "";
            SEstado = "";
            SCiudad = "";
            SMunicipio = "";
            PaisTV = "";
        }
    }

    public class SAESTADO
    {
        public string Descrip { get; set; }

        public int Estado { get; set; }

        public int Pais { get; set; }

        public int IDESTADOFE { get; set; }

        public string ProvinciaTV { get; set; }

        public SAESTADO()
        {
            Descrip = "";
            ProvinciaTV = "";
        }
    }

    public class SACIUDAD
    {
        public string Descrip { get; set; }

        public int Ciudad { get; set; }

        public int Estado { get; set; }

        public int Pais { get; set; }

        public int IDCIUDADFE { get; set; }

        public string CantonTV { get; set; }

        public SACIUDAD()
        {
            Descrip = "";
            CantonTV = "";
        }
    }

    public class SAMUNICIPIO
    {
        public string Descrip { get; set; }

        public int Municipio { get; set; }

        public int Ciudad { get; set; }

        public int Estado { get; set; }

        public int Pais { get; set; }

        public int IDMUNICIPIOFE { get; set; }

        public string DistritoTV { get; set; }

        public SAMUNICIPIO()
        {
            Descrip = "";
            DistritoTV = "";
        }
    }

    public class FEActividades_Eco
    {
        public int NroUnico { get; set; }

        public string Codigo_Actividad { get; set; }

        public string Nombre { get; set; }

        public short Nivel { get; set; }

        public FEActividades_Eco()
        {
            Codigo_Actividad = "";
            Nombre = "";
        }
    }


}
