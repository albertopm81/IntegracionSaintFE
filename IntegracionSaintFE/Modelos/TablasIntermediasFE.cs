﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    public class encabezado
    {
        public int id { get; set; }

        public string usuario { get; set; }

        public string password { get; set; }

        public string clave { get; set; }

        public string tipo_documento { get; set; }

        public DateTime fecha { get; set; }

        public string consecutivo_origen { get; set; }

        public string consecutivo_incompleto { get; set; }

        public string consecutivo_completo { get; set; }

        public string emisor_nombre { get; set; }

        public string emisor_nom_comercial { get; set; }

        public string emisor_tipo { get; set; }

        public string emisor_identificacion { get; set; }

        public string emisor_provincia { get; set; }

        public string emisor_canton { get; set; }

        public string emisor_distrito { get; set; }

        public string emisor_direccion { get; set; }

        public string emisor_telefono { get; set; }

        public string emisor_fax { get; set; }

        public string emisor_email { get; set; }

        public int receptor_incluir { get; set; }

        public string receptor_nombre { get; set; }

        public string receptor_tipo { get; set; }

        public string receptor_identificacion { get; set; }

        public string pasaporte { get; set; }

        public int ubicacion_incluir { get; set; }

        public string receptor_provincia { get; set; }

        public string receptor_canton { get; set; }

        public string receptor_distrito { get; set; }

        public string receptor_direccion { get; set; }

        public string receptor_telefono { get; set; }

        public string receptor_fax { get; set; }

        public string receptor_email { get; set; }

        public string condicion_venta { get; set; }

        public int plazo_credito { get; set; }

        public string medio_pago1 { get; set; }

        public string medio_pago2 { get; set; }

        public string medio_pago3 { get; set; }

        public string medio_pago4 { get; set; }

        public string moneda { get; set; }

        public decimal tipo_cambio { get; set; }

        public string ref_tipo_doc { get; set; }

        public string ref_clave { get; set; }

        public DateTime ref_fecha { get; set; }

        public string ref_codigo { get; set; }

        public string ref_razon { get; set; }

        public string Otros { get; set; }

        public string xml { get; set; }

        public string estado_envio { get; set; }

        public string mensaje_envio { get; set; }

        public string estado_hacienda { get; set; }

        public string mensaje_hacienda { get; set; }

        public string correos_adicionales { get; set; }

        public int Reintentos { get; set; }

        public DateTime FechaCE { get; set; }

        public string codigo_actividad { get; set; }

        public string receptor_direccion_extranjero { get; set; }

        public decimal iva_devuelto { get; set; }

        public string tipodoc { get; set; }

        public string numerod { get; set; }

        public short reenviado { get; set; }

        public string llave_sucursal { get; set; }

        public string codclie { get; set; }

        public string codprov { get; set; }

        public List<linea> linea { get; set; }

        public List<otros_cargos> otros_cargos { get; set; }

        public encabezado()
        {
            usuario = "";
            password = "";
            clave = "";
            tipo_documento = "";
            consecutivo_origen = "";
            consecutivo_incompleto = "";
            consecutivo_completo = "";
            emisor_nombre = "";
            emisor_nom_comercial = "";
            emisor_tipo = "";
            emisor_identificacion = "";
            emisor_provincia = "";
            emisor_canton = "";
            emisor_distrito = "";
            emisor_direccion = "";
            emisor_telefono = "";
            emisor_fax = "";
            emisor_email = "";
            receptor_nombre = "";
            receptor_tipo = "";
            receptor_identificacion = "";
            pasaporte = "";
            receptor_provincia = "";
            receptor_canton = "";
            receptor_distrito = "";
            receptor_direccion = "";
            receptor_telefono = "";
            receptor_fax = "";
            receptor_email = "";
            condicion_venta = "";
            medio_pago1 = "";
            medio_pago2 = "";
            medio_pago3 = "";
            medio_pago4 = "";
            moneda = "";
            ref_tipo_doc = "";
            ref_clave = "";
            ref_codigo = "";
            ref_razon = "";
            Otros = "";
            xml = "";
            estado_envio = "";
            mensaje_envio = "";
            estado_hacienda = "";
            mensaje_hacienda = "";
            correos_adicionales = "";
            codigo_actividad = "";
            receptor_direccion_extranjero = "";
            tipodoc = "";
            numerod = "";
            llave_sucursal = "";
            codclie = "";
            codprov = "";
        }
    }

    public class linea
    {
        public int id_encabezado { get; set; }

        public string clave { get; set; }

        public int numero_linea { get; set; }

        public string tipo_cod_producto { get; set; }

        public string codigo_producto { get; set; }

        public int servicio { get; set; }

        public decimal cantidad { get; set; }

        public string unidad_medida { get; set; }

        public string detalle { get; set; }

        public decimal precio_unitario { get; set; }

        public decimal monto_total { get; set; }

        public decimal monto_descuento { get; set; }

        public string naturaleza_descuento { get; set; }

        public decimal subtotal { get; set; }

        public string impuesto1_codigo { get; set; }

        public decimal impuesto1_porcentaje { get; set; }

        public decimal impuesto1_monto { get; set; }

        public int exoneracion1_incluir { get; set; }

        public string exoneracion1_tipo { get; set; }

        public string exoneracion1_numero_doc { get; set; }

        public string exoneracion1_institucion { get; set; }

        public DateTime exoneracion1_fecha_doc { get; set; }

        public decimal exoneracion1_monto_imp { get; set; }

        public int exoneracion1_porc_compra { get; set; }

        public string impuesto2_codigo { get; set; }

        public decimal impuesto2_porcentaje { get; set; }

        public decimal impuesto2_monto { get; set; }

        public int exoneracion2_incluir { get; set; }

        public string exoneracion2_tipo { get; set; }

        public string exoneracion2_numero_doc { get; set; }

        public string exoneracion2_institucion { get; set; }

        public DateTime exoneracion2_fecha_doc { get; set; }

        public decimal exoneracion2_monto_imp { get; set; }

        public int exoneracion2_porc_compra { get; set; }

        public decimal monto_total_linea { get; set; }

        public string codigo_hacienda { get; set; }

        public string partida_arancelaria { get; set; }

        public decimal monto_descuento2 { get; set; }

        public string naturaleza_descuento2 { get; set; }

        public decimal monto_descuento3 { get; set; }

        public string naturaleza_descuento3 { get; set; }

        public decimal monto_descuento4 { get; set; }

        public string naturaleza_descuento4 { get; set; }

        public decimal monto_descuento5 { get; set; }

        public string naturaleza_descuento5 { get; set; }

        public decimal base_imponible { get; set; }

        public decimal factor_iva1 { get; set; }

        public string codigo_tarifa1 { get; set; }

        public decimal factor_iva2 { get; set; }

        public string codigo_tarifa2 { get; set; }

        public decimal monto_impuesto_exportacion1 { get; set; }

        public decimal monto_impuesto_exportacion2 { get; set; }

        public decimal monto_impuesto_neto { get; set; }

        public string unidad_medida_comercial { get; set; }

        public string descripcion_larga_linea { get; set; }

        public linea()
        {
            clave = "";
            tipo_cod_producto = "";
            codigo_producto = "";
            unidad_medida = "";
            detalle = "";
            naturaleza_descuento = "";
            impuesto1_codigo = "";
            exoneracion1_tipo = "";
            exoneracion1_numero_doc = "";
            exoneracion1_institucion = "";
            impuesto2_codigo = "";
            exoneracion2_tipo = "";
            exoneracion2_numero_doc = "";
            exoneracion2_institucion = "";
            codigo_hacienda = "";
            partida_arancelaria = "";
            naturaleza_descuento2 = "";
            naturaleza_descuento3 = "";
            naturaleza_descuento4 = "";
            naturaleza_descuento5 = "";
            codigo_tarifa1 = "";
            codigo_tarifa2 = "";
            unidad_medida_comercial = "";
            descripcion_larga_linea = "";
        }
    }

    public class otros_cargos
    {
        public int id_encabezado { get; set; }

        public string clave { get; set; }

        public string otc_tipo { get; set; }

        public decimal otc_monto { get; set; }

        public string otc_detalle { get; set; }

        public decimal otc_porcentaje { get; set; }

        public string otc_nombre { get; set; }

        public string otc_identificacion { get; set; }

        public int otc_nrolinea { get; set; }

        public otros_cargos()
        {
            clave = "";
            otc_tipo = "";
            otc_detalle = "";
            otc_nombre = "";
            otc_identificacion = "";
        }
    }

}
