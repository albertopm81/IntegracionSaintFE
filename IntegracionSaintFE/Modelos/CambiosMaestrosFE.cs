﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE.Modelos
{
    class CambiosMaestrosFE
    {
        public long Id { get; set; }

        public string tipo { get; set; }

        public string codigo { get; set; }

        public int estado_sync { get; set; }

        public string mensaje_sync { get; set; }

        public DateTime fechaCE { get; set; }

        public CambiosMaestrosFE()
        {
            tipo = "";
            codigo = "";
            mensaje_sync = "";
        }
    }
}
