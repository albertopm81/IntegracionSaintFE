﻿using IntegracionSaintFE.Framework;
using IntegracionSaintFE.Modelos;
using System;
using System.Reflection;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Timers;

namespace IntegracionSaintFE
{
    /// <summary>
    /// The actual implementation of the windows service goes here...
    /// </summary>
    [WindowsService("IntegradorSaintFE",
        DisplayName = "IntegradorSaintFE",
        Description = "Servicio Integrador Saint con FE Soportec",
        EventLogSource = "IntegradorSaintFE",
        StartMode = ServiceStartMode.Automatic)]
    public partial class ServiceImplementation : IWindowsService
    {
        ConfigModel Config = new ConfigModel();
        Timer Timer1 = new Timer();

        Funciones FF = new Funciones();
        bool EnProceso = false;
        DateTime tiempo_de_busqueda = new DateTime();

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
        }

        /// <summary>
        /// This method is called when the service gets a request to start.
        /// </summary>
        /// <param name="args">Any command line arguments</param>
        public void OnStart(string[] args)
        {
            FF.Log("Versión:  " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            try
            {
                Config = FF.LeerConfiguracion();
            }
            catch (Exception ex)
            {
                FF.LogE("No se pudo leer la configuración: {0}", ex.Message);
                return;
            }

            Timer1.Elapsed += Tick;

            Timer1.Interval = Config.SyncTicksSeconds * 1000;

            Timer1.Enabled = true;
            FF.Log("Servicio iniciado.");
            FF.Log("Buscando cada {0} segundos.", Config.SyncAllEachSeconds);
        }

        /// <summary>
        /// This method is called when the service gets a request to start.
        /// </summary>
        /// <param name="args">Any command line arguments</param>
        //public async void OnStartAsync(string[] args)
        //{
        //    object objeto = null;

        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri("192.168.2.204:2536");
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    client.Timeout = TimeSpan.FromSeconds(30);
        //    HttpResponseMessage response = await client.PostAsJsonAsync("Api/Clientes/InputClientData", objeto);
        //    client.Dispose();
        //    if (response.IsSuccessStatusCode)
        //    {
        //        //si la respuesta es un objeto se toma asi

        //        //objetor = await response.Content.ReadAsAsync<TIPODEOBJETOR>();                
        //    }
        //}

        /// <summary>
        /// This method is called when the service gets a request to stop.
        /// </summary>
        public void OnStop()
        {
            Timer1.Enabled = false;
            FF.Log("Servicio Detenido.");
        }

        /// <summary>
        /// This method is called when a service gets a request to pause,
        /// but not stop completely.
        /// </summary>
        public void OnPause()
        {
            Timer1.Enabled = false;
            FF.Log("Servicio Detenido.");
        }

        /// <summary>
        /// This method is called when a service gets a request to resume 
        /// after a pause is issued.
        /// </summary>
        public void OnContinue()
        {
            Config = FF.LeerConfiguracion();
            Timer1.Interval = Config.SyncTicksSeconds * 1000;

            Timer1.Enabled = true;
            FF.Log("Servicio iniciado.");
        }

        /// <summary>
        /// This method is called when the machine the service is running on
        /// is being shutdown.
        /// </summary>
        public void OnShutdown()
        {
            Timer1.Stop();
            FF.Log("Servicio Detenido.");
        }

        /// <summary>
        /// This method is called when a custom command is issued to the service.
        /// </summary>
        /// <param name="command">The command identifier to execute.</param >
        public void OnCustomCommand(int command)
        {
        }

        public async void Tick(object sender, ElapsedEventArgs e)
        {
            //para debug, descomentar esta linea
            //Timer1.Enabled = false;

            if (DateTime.Now >= tiempo_de_busqueda)
            {
                try
                {
                    System.Globalization.CultureInfo cultureInfo =
                    new System.Globalization.CultureInfo("en-US");
                }
                catch (Exception ex)
                {
                    FF.LogE("Error en System.Globalization: " + ex.Message);
                }

                if (!EnProceso)
                {
                    tiempo_de_busqueda = DateTime.Now.AddSeconds(Config.SyncAllEachSeconds);
                    EnProceso = true;

                    await Comprobantes();
                    await CerrarComprobantes();
                    await CambiosMaestros();

                    if (Config.LastTime.AddHours(6) < DateTime.Now) //validando que cada 6 horas se valide el proceso
                    {
                        await LimpiarHCambiosMaestros();
                    }

                    EnProceso = false;
                }
            }
        }
    }

}
