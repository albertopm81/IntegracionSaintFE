﻿using IntegracionSaintFE.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionSaintFE
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // If install was a command line flag, then run the installer at runtime.
                if (args.Contains("--install", StringComparer.InvariantCultureIgnoreCase))
                {
                    if (args.Length == 2)
                    {
                        WindowsServiceInstaller.RuntimeInstall<ServiceImplementation>(args[1]);
                    }
                    else
                    {
                        WindowsServiceInstaller.RuntimeInstall<ServiceImplementation>();
                    }
                }

                // If uninstall was a command line flag, run uninstaller at runtime.
                else if (args.Contains("--uninstall", StringComparer.InvariantCultureIgnoreCase))
                {
                    if (args.Length == 2)
                    {
                        WindowsServiceInstaller.RuntimeUnInstall<ServiceImplementation>(args[1]);
                    }
                    else
                    {
                        WindowsServiceInstaller.RuntimeUnInstall<ServiceImplementation>();
                    }
                }

                // Otherwise, fire up the service as either console or windows service based on UserInteractive property.
                else
                {
                    var implementation = new ServiceImplementation();

                    // If started from console, file explorer, etc, run as console app.
                    if (Environment.UserInteractive || args.Contains("--console", StringComparer.InvariantCultureIgnoreCase))
                    {
                        ConsoleHarness.Run(args, implementation);
                    }

                    // Otherwise run as a windows service
                    else
                    {
                        ServiceBase.Run(new WindowsServiceHarness(implementation));
                    }
                }
            }

            catch (Exception ex)
            {
                ConsoleHarness.WriteToConsole(ConsoleColor.Red, "An exception occurred in Main(): {0}", ex);
            }
        }
    }

    //static class Program
    //{
    //    /// <summary>
    //    /// Punto de entrada principal para la aplicación.
    //    /// </summary>
    //    static void Main()
    //    {
    //        ServiceBase[] ServicesToRun;
    //        ServicesToRun = new ServiceBase[]
    //        {
    //            new Service1()
    //        };
    //        ServiceBase.Run(ServicesToRun);
    //    }
    //}
}
