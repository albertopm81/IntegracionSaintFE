/*cabys NC y ND: 8312900000000*/
/*cabys Flete: 6511900009900*/
/*** CAMPOS NUEVOS EN TABLAS SAINT ***/
/*TABLAS DE UBICACION*/
IF COL_LENGTH('SAESTADO', 'IDEstadoFE') IS NULL
	BEGIN
		ALTER TABLE SAESTADO ADD IDEstadoFE INT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAESTADO ALTER COLUMN IDEstadoFE INT NULL;
	END;

IF COL_LENGTH('SACIUDAD', 'IDCiudadFE') IS NULL
	BEGIN
		ALTER TABLE SACIUDAD ADD IDCiudadFE INT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACIUDAD ALTER COLUMN IDCiudadFE INT NULL;
	END;

IF COL_LENGTH('SAMUNICIPIO', 'IDMunicipioFE') IS NULL
	BEGIN
		ALTER TABLE SAMUNICIPIO ADD IDMunicipioFE INT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAMUNICIPIO ALTER COLUMN IDMunicipioFE INT NULL;
	END;

IF COL_LENGTH('SAPAIS', 'IDPais') IS NULL
	BEGIN
		ALTER TABLE SAPAIS ADD IDPais INT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAPAIS ALTER COLUMN IDPais INT NULL;
	END;

IF COL_LENGTH('SAPAIS', 'PaisTV') IS NULL
	BEGIN
		ALTER TABLE SAPAIS ADD PaisTV varchar(100) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAPAIS ALTER COLUMN PaisTV varchar(100) NULL;		
	END;

IF COL_LENGTH('SAESTADO', 'ProvinciaTV') IS NULL
	BEGIN
		ALTER TABLE SAESTADO ADD ProvinciaTV varchar(100) NULL;		
	END;
ELSE
	BEGIN
		ALTER TABLE SAESTADO ALTER COLUMN ProvinciaTV varchar(100) NULL;		
	END;

IF COL_LENGTH('SACIUDAD', 'CantonTV') IS NULL
	BEGIN
		ALTER TABLE SACIUDAD ADD CantonTV varchar(100) NULL;		
	END;
ELSE
	BEGIN
		ALTER TABLE SACIUDAD ALTER COLUMN CantonTV varchar(100) NULL;		
	END;

IF COL_LENGTH('SAMUNICIPIO', 'DistritoTV') IS NULL
	BEGIN
		ALTER TABLE SAMUNICIPIO ADD DistritoTV varchar(100) NULL;		
	END;
ELSE
	BEGIN
		ALTER TABLE SAMUNICIPIO ALTER COLUMN DistritoTV varchar(100) NULL;		
	END;

/*CAMPOS EN SAFACT*/
IF COL_LENGTH('SAFACT', 'NumeroDE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD NumeroDE VARCHAR(10) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN NumeroDE VARCHAR(10) NULL;
	END;

IF COL_LENGTH('SAFACT', 'Clave') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD Clave VARCHAR(50) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN Clave VARCHAR(50) NULL;
	END;

IF COL_LENGTH('SAFACT', 'TipoDocFE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD TipoDocFE VARCHAR(2) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN TipoDocFE VARCHAR(2) NULL;
	END;

IF COL_LENGTH('SAFACT', 'ConsecutivoDE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD ConsecutivoDE VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN ConsecutivoDE VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SAFACT', 'ClaveRef') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD ClaveRef VARCHAR(50) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN ClaveRef VARCHAR(50) NULL;
	END;

IF COL_LENGTH('SAFACT', 'ExoneracionE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD ExoneracionE smallint DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN ExoneracionE smallint NOT NULL;     
	END;

IF COL_LENGTH('SAFACT', 'NroItemT') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD NroItemT int DEFAULT(0) NOT NULL;    
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN NroItemT int NOT NULL;     
	END;

IF COL_LENGTH('SAFACT', 'iva_devuelto') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD iva_devuelto DECIMAL(28,4) DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN iva_devuelto DECIMAL(28,4) NOT NULL;     
	END;

IF COL_LENGTH('SAFACT', 'Monto_Exonerado') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD Monto_Exonerado DECIMAL(28,4) DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN Monto_Exonerado DECIMAL(28,4) NOT NULL;     
	END;

IF COL_LENGTH('SAFACT', 'Num_Original') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD Num_Original VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN Num_Original VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SAFACT', 'CodTaxsClie') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD CodTaxsClie VARCHAR(5) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN CodTaxsClie VARCHAR(5) NULL;
	END;

IF COL_LENGTH('SAFACT', 'EstadoFE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD EstadoFE tinyint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN EstadoFE tinyint NOT NULL;
	END;

IF COL_LENGTH('SAFACT', 'MensajeFE') IS NULL
	BEGIN
		ALTER TABLE SAFACT ADD MensajeFE VARCHAR(250) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAFACT ALTER COLUMN MensajeFE VARCHAR(250) NULL;
	END;

/*CAMPOS EN SAITEMFAC*/
IF COL_LENGTH('SAITEMFAC', 'NroLineaFE') IS NULL
	BEGIN
		ALTER TABLE SAITEMFAC ADD NroLineaFE int DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAITEMFAC ALTER COLUMN NroLineaFE int NOT NULL;     
	END;

/*CAMPOS EN SAACXC*/
IF COL_LENGTH('SAACXC', 'NumeroDE') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD NumeroDE VARCHAR(10) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN NumeroDE VARCHAR(10) NULL;
	END;

IF COL_LENGTH('SAACXC', 'Clave') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD Clave VARCHAR(50) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN Clave VARCHAR(50) NULL;
	END;

IF COL_LENGTH('SAACXC', 'TipoDocFE') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD TipoDocFE VARCHAR(2) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN TipoDocFE VARCHAR(2) NULL;
	END;

IF COL_LENGTH('SAACXC', 'ConsecutivoDE') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD ConsecutivoDE VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN ConsecutivoDE VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SAACXC', 'ClaveRef') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD ClaveRef VARCHAR(50) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN ClaveRef VARCHAR(50) NULL;
	END;

IF COL_LENGTH('SAACXC', 'NroItemT') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD NroItemT int DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN NroItemT int NOT NULL;     
	END;

IF COL_LENGTH('SAACXC', 'Num_Original') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD Num_Original VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN Num_Original VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SAACXC', 'EstadoFE') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD EstadoFE tinyint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN EstadoFE tinyint NOT NULL;
	END;

IF COL_LENGTH('SAACXC', 'MensajeFE') IS NULL
	BEGIN
		ALTER TABLE SAACXC ADD MensajeFE VARCHAR(250) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SAACXC ALTER COLUMN MensajeFE VARCHAR(250) NULL;
	END;

/*CAMPOS EN SACOMP*/
IF COL_LENGTH('SACOMP', 'NumeroDE') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD NumeroDE VARCHAR(10) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN NumeroDE VARCHAR(10) NULL;
	END;

IF COL_LENGTH('SACOMP', 'Clave') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD Clave VARCHAR(50) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN Clave VARCHAR(50) NULL;
	END;

IF COL_LENGTH('SACOMP', 'TipoDocFE') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD TipoDocFE VARCHAR(2) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN TipoDocFE VARCHAR(2) NULL;
	END;

IF COL_LENGTH('SACOMP', 'ConsecutivoDE') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD ConsecutivoDE VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN ConsecutivoDE VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SACOMP', 'EstadoFE') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD EstadoFE tinyint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN EstadoFE tinyint NOT NULL;
	END;

IF COL_LENGTH('SACOMP', 'MensajeFE') IS NULL
	BEGIN
		ALTER TABLE SACOMP ADD MensajeFE VARCHAR(250) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACOMP ALTER COLUMN MensajeFE VARCHAR(250) NULL;
	END;

/*CRANDO CAMPOS EN SAITEMCOM*/
IF COL_LENGTH('SAITEMCOM', 'NroLineaFE') IS NULL
	BEGIN
		ALTER TABLE SAITEMCOM ADD NroLineaFE int DEFAULT(0) NOT NULL;     
	END;
ELSE
	BEGIN
		ALTER TABLE SAITEMCOM ALTER COLUMN NroLineaFE int NOT NULL;     
	END;

/*CAMPOS EN TABLA SACONF*/
IF COL_LENGTH('SACONF', 'CodMoneda') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD CodMoneda VARCHAR(5) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN CodMoneda VARCHAR(5) NULL;
	END;

IF COL_LENGTH('SACONF', 'CodMonedaR') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD CodMonedaR VARCHAR(5) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN CodMonedaR VARCHAR(5) NULL;
	END;

IF COL_LENGTH('SACONF', 'UsuaFE') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD UsuaFE VARCHAR(100) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN UsuaFE VARCHAR(100) NULL;
	END;

IF COL_LENGTH('SACONF', 'PassFE') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD PassFE VARCHAR(100) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN PassFE VARCHAR(100) NULL;
	END;

IF COL_LENGTH('SACONF', 'EmisorTipo') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD EmisorTipo VARCHAR(2) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN EmisorTipo VARCHAR(2) NULL;
	END;

IF COL_LENGTH('SACONF', 'DescripFletesFE') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD DescripFletesFE VARCHAR(60) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN DescripFletesFE VARCHAR(60) NULL;
	END;

IF COL_LENGTH('SACONF', 'NombreComercial') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD NombreComercial VARCHAR(80) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN NombreComercial VARCHAR(80) NULL;
	END;

IF COL_LENGTH('SACONF', 'correos_adicionales') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD correos_adicionales VARCHAR(500) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN correos_adicionales VARCHAR(500) NULL;
	END;

IF COL_LENGTH('SACONF', 'Sucursal') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Sucursal int DEFAULT(1) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Sucursal int NOT NULL;
	END;

IF COL_LENGTH('SACONF', 'Si_FC') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Si_FC smallint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Si_FC smallint NOT NULL;
	END;

IF COL_LENGTH('SACONF', 'Incluir_DSPDF') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Incluir_DSPDF smallint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Incluir_DSPDF smallint NOT NULL;
	END;

IF COL_LENGTH('SACONF', 'VersionFE') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD VersionFE VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN VersionFE VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SACONF', 'Llave_Sucursal') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Llave_Sucursal VARCHAR(20) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Llave_Sucursal VARCHAR(20) NULL;
	END;

IF COL_LENGTH('SACONF', 'Incluir_Receptor') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Incluir_Receptor smallint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Incluir_Receptor smallint NOT NULL;
	END;

IF COL_LENGTH('SACONF', 'Unidad_Servicios') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Unidad_Servicios smallint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Unidad_Servicios smallint NOT NULL;
	END;

IF COL_LENGTH('SACONF', 'Cabys_Fletes') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Cabys_Fletes varchar(13) NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Cabys_Fletes varchar(13) NULL;
	END;

IF COL_LENGTH('SACONF', 'Regalias') IS NULL
	BEGIN
		ALTER TABLE SACONF ADD Regalias smallint DEFAULT(0) NOT NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SACONF ALTER COLUMN Regalias smallint NOT NULL;
	END;

/*CAMPOS NUEVO SATAXES*/
IF COL_LENGTH('SATAXES', 'Orden') IS NULL
	BEGIN
		ALTER TABLE SATAXES ADD Orden smallint NULL;
	END;
ELSE
	BEGIN
		ALTER TABLE SATAXES ALTER COLUMN Orden smallint NULL;
	END;

/*SAFIEL CAMPOS NUEVOS SAFACT*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'NUMERODE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','NUMERODE','NUMERODE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'NUMERODE', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'NUMERODE';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'CLAVE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','CLAVE','CLAVE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CLAVE', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'CLAVE';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'TIPODOCFE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','TIPODOCFE','TIPODOCFE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'TIPODOCFE', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'TIPODOCFE';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'CONSECUTIVODE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','CONSECUTIVODE','CONSECUTIVODE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CONSECUTIVODE', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'CONSECUTIVODE';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'CLAVEREF') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','CLAVEREF','CLAVEREF','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CLAVEREF', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'CLAVEREF';
	END;	
		
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'iva_devuelto') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','iva_devuelto','iva_devuelto','dtDouble','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'iva_devuelto', 
							datatype = 'dtDouble'
		WHERE tablename = 'SAFACT' AND fieldname = 'iva_devuelto';
	END;	

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'Monto_Exonerado') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','Monto_Exonerado','Monto_Exonerado','dtDouble','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Monto_Exonerado', 
							datatype = 'dtDouble'
		WHERE tablename = 'SAFACT' AND fieldname = 'Monto_Exonerado';
	END;	

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAFACT' AND fieldname = 'Num_Original') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAFACT','Num_Original','Num_Original','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Num_Original', 
							datatype = 'dtString'
		WHERE tablename = 'SAFACT' AND fieldname = 'Num_Original';
	END;

/*SAFIEL CAMPOS NUEVOS SAACXC*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'NUMERODE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','NUMERODE','NUMERODE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'NUMERODE', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'NUMERODE';
	END;		
		
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'CLAVE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','CLAVE','CLAVE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CLAVE', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'CLAVE';
	END;		

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'TIPODOCFE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','TIPODOCFE','TIPODOCFE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'TIPODOCFE', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'TIPODOCFE';
	END;	

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'CONSECUTIVODE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','CONSECUTIVODE','CONSECUTIVODE','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CONSECUTIVODE', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'CONSECUTIVODE';
	END;		
		
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'CLAVEREF') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','CLAVEREF','CLAVEREF','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'CLAVEREF', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'CLAVEREF';
	END;			

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAACXC' AND fieldname = 'Num_Original') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES ('SAACXC','Num_Original','Num_Original','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Num_Original', 
							datatype = 'dtString'
		WHERE tablename = 'SAACXC' AND fieldname = 'Num_Original';
	END;

/*SAFIEL CAMPOS NUEVOS SACONF*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SACONF' AND fieldname = 'NombreComercial') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES 	('SACONF','NombreComercial','NombreComercial','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'NombreComercial', 
							datatype = 'dtString'
		WHERE tablename = 'SACONF' AND fieldname = 'NombreComercial';
	END;	

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = 'SAITEMFAC' AND fieldname = 'NroLineaFE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES 	('SAITEMFAC','NroLineaFE','NroLineaFE','dtInteger','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'NroLineaFE', 
							datatype = 'dtInteger'
		WHERE tablename = 'SAITEMFAC' AND fieldname = 'NroLineaFE';
	END;
GO
/*** CREANDO TABLAS INTERMEDIAS ***/
/*TABLA ENCABEZADO*/
IF OBJECT_ID('encabezado') IS NULL
	BEGIN
		CREATE TABLE encabezado (
		  id int IDENTITY(1,1) NOT NULL,
		  clave varchar(50) DEFAULT NULL,
		  tipo_documento varchar(2) DEFAULT NULL,
		  fecha datetime DEFAULT '1900-01-01 00:00:00',
		  consecutivo_origen varchar(10) NOT NULL,
		  consecutivo_incompleto varchar(8) DEFAULT NULL,
		  consecutivo_completo varchar(20) DEFAULT NULL,
		  emisor_nombre varchar(80) DEFAULT NULL,
		  emisor_nom_comercial varchar(80) DEFAULT NULL,
		  emisor_tipo varchar(2) DEFAULT NULL,
		  emisor_identificacion varchar(12) DEFAULT NULL,
		  emisor_provincia varchar(1) DEFAULT NULL,
		  emisor_canton varchar(2) DEFAULT NULL,
		  emisor_distrito varchar(2) DEFAULT NULL,
		  emisor_direccion varchar(160) DEFAULT NULL,
		  emisor_telefono varchar(20) DEFAULT NULL,
		  emisor_fax varchar(20) DEFAULT NULL,
		  emisor_email varchar(100) DEFAULT NULL,
		  receptor_incluir int DEFAULT NULL,
		  receptor_nombre varchar(100) DEFAULT NULL,			--- MODIFICADO PARA V4.3 ---
		  receptor_tipo varchar(2) DEFAULT NULL,
		  receptor_identificacion varchar(12) DEFAULT NULL,
		  pasaporte varchar(20) DEFAULT NULL,
		  ubicacion_incluir int DEFAULT NULL,
		  receptor_provincia varchar(1) DEFAULT NULL,
		  receptor_canton varchar(2) DEFAULT NULL,
		  receptor_distrito varchar(2) DEFAULT NULL,
		  receptor_direccion varchar(160) DEFAULT NULL,
		  receptor_telefono varchar(20) DEFAULT NULL,
		  receptor_fax varchar(20) DEFAULT NULL,
		  receptor_email varchar(100) DEFAULT NULL,
		  condicion_venta varchar(2) DEFAULT NULL,
		  plazo_credito int DEFAULT (0),
		  medio_pago1 varchar(2) DEFAULT NULL,
		  medio_pago2 varchar(2) DEFAULT NULL,
		  medio_pago3 varchar(2) DEFAULT NULL,
		  medio_pago4 varchar(2) DEFAULT NULL,
		  moneda varchar(3) DEFAULT NULL,
		  tipo_cambio decimal(18,5) DEFAULT (1) NULL,
		  ref_tipo_doc varchar(2) DEFAULT NULL,
		  ref_clave varchar(50) DEFAULT NULL,
		  ref_fecha datetime DEFAULT '1900-01-01 00:00:00',
		  ref_codigo varchar(2) DEFAULT NULL,
		  ref_razon varchar(180) DEFAULT NULL,
		  Otros varchar(1000) DEFAULT NULL,
		  xml varchar(8000) DEFAULT NULL,
		  estado_envio varchar(2) DEFAULT('0') NULL,
		  mensaje_envio varchar(5000) DEFAULT('') NULL,
		  estado_hacienda varchar(2) DEFAULT('0') NULL,
		  mensaje_hacienda varchar(5000) DEFAULT('') NULL,
		  correos_adicionales varchar(1000) DEFAULT NULL,
		  Reintentos int DEFAULT (0) NOT NULL,
		  FechaCE datetime DEFAULT '1900-01-01 00:00:00',
		  codigo_actividad varchar(6) DEFAULT NULL,							--- CAMPO NUEVO PARA V4.3 ---
		  receptor_direccion_extranjero varchar(300) DEFAULT NULL,			--- CAMPO NUEVO PARA V4.3 ---
		  iva_devuelto decimal(18,5) DEFAULT (0) NOT NULL,					--- CAMPO NUEVO PARA V4.3 ---
		  tipodoc varchar(2) DEFAULT NULL,									--- CAMPO NUEVO PARA V4.3 ---
		  numerod  varchar(20) DEFAULT NULL,								--- CAMPO NUEVO PARA V4.3 ---
		  reenviado smallint DEFAULT(0) NOT NULL,
		  llave_sucursal  varchar(20) DEFAULT NULL,
		  codclie varchar(15) DEFAULT NULL,
		  codprov varchar(15) DEFAULT NULL,
		  PRIMARY KEY (id));
	END;
ELSE
	BEGIN
		IF COL_LENGTH('encabezado', 'usuario') IS NOT NULL
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN usuario varchar(100) NULL;
			END;

		IF COL_LENGTH('encabezado', 'password') IS NOT NULL
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN password varchar(100) NULL;
			END;

		IF COL_LENGTH('encabezado', 'clave') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD clave varchar(50) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN clave varchar(50) NULL;
			END;

		IF COL_LENGTH('encabezado', 'tipo_documento') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD tipo_documento varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN tipo_documento varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'fecha') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD fecha datetime DEFAULT '1900-01-01 00:00:00';
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN fecha datetime NULL;
			END;

		IF COL_LENGTH('encabezado', 'consecutivo_origen') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD consecutivo_origen varchar(10) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN consecutivo_origen varchar(10) NOT NULL;
			END;

		IF COL_LENGTH('encabezado', 'consecutivo_incompleto') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD consecutivo_incompleto varchar(8) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN consecutivo_incompleto varchar(8) NULL;
			END;

		IF COL_LENGTH('encabezado', 'consecutivo_completo') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD consecutivo_completo varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN consecutivo_completo varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_nombre') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_nombre varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_nombre varchar(80) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_nom_comercial') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_nom_comercial varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_nom_comercial varchar(80) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_tipo') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_tipo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_tipo varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_identificacion') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_identificacion varchar(12) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_identificacion varchar(12) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_provincia') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_provincia varchar(1) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_provincia varchar(1) NULL;
			END;	

		IF COL_LENGTH('encabezado', 'emisor_canton') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_canton varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_canton varchar(2) NULL;
			END;		

		IF COL_LENGTH('encabezado', 'emisor_distrito') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_distrito varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_distrito varchar(2) NULL;
			END;	

		IF COL_LENGTH('encabezado', 'emisor_direccion') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_direccion varchar(160) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_direccion varchar(160) NULL;
			END;	

		IF COL_LENGTH('encabezado', 'emisor_telefono') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_telefono varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_telefono varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_fax') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_fax varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_fax varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'emisor_email') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD emisor_email varchar(100) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN emisor_email varchar(100) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_incluir') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_incluir int DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_incluir int NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_nombre') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_nombre varchar(100) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_nombre varchar(100) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_tipo') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_tipo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_tipo varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_identificacion') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_identificacion varchar(12) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_identificacion varchar(12) NULL;
			END;

		IF COL_LENGTH('encabezado', 'pasaporte') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD pasaporte varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN pasaporte varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'ubicacion_incluir') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ubicacion_incluir int DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ubicacion_incluir int NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_provincia') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_provincia varchar(1) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_provincia varchar(1) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_canton') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_canton varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_canton varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_distrito') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_distrito varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_distrito varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_direccion') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_direccion varchar(160) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_direccion varchar(160) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_telefono') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_telefono varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_telefono varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_fax') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_fax varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_fax varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_email') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_email varchar(100) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_email varchar(100) NULL;
			END;

		IF COL_LENGTH('encabezado', 'condicion_venta') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD condicion_venta varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN condicion_venta varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'plazo_credito') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD plazo_credito int DEFAULT (0) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN plazo_credito int NULL;
			END;

		IF COL_LENGTH('encabezado', 'medio_pago1') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD medio_pago1 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN medio_pago1 varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'medio_pago2') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD medio_pago2 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN medio_pago2 varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'medio_pago2') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD medio_pago2 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN medio_pago2 varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'medio_pago3') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD medio_pago3 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN medio_pago3 varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'medio_pago4') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD medio_pago4 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN medio_pago4 varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'moneda') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD moneda varchar(3) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN moneda varchar(3) NULL;
			END;

		IF COL_LENGTH('encabezado', 'tipo_cambio') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD tipo_cambio decimal(18,5) DEFAULT (1) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN tipo_cambio decimal(18,5) NULL;
			END;

		IF COL_LENGTH('encabezado', 'ref_tipo_doc') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ref_tipo_doc varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ref_tipo_doc varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'ref_clave') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ref_clave varchar(50) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ref_clave varchar(50) NULL;
			END;

		IF COL_LENGTH('encabezado', 'ref_clave') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ref_fecha datetime DEFAULT '1900-01-01 00:00:00';
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ref_fecha datetime NULL;
			END;

		IF COL_LENGTH('encabezado', 'ref_codigo') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ref_codigo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ref_codigo varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'ref_razon') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD ref_razon varchar(180) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN ref_razon varchar(180) NULL;
			END;

		IF COL_LENGTH('encabezado', 'Otros') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD Otros varchar(1000) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN Otros varchar(1000) NULL;
			END;

		IF COL_LENGTH('encabezado', 'xml') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD xml varchar(8000) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN xml varchar(8000) NULL;
			END;

		IF COL_LENGTH('encabezado', 'estado_envio') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD estado_envio varchar(2) DEFAULT('0') NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN estado_envio varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'mensaje_envio') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD mensaje_envio varchar(5000) DEFAULT('') NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN mensaje_envio varchar(5000) NULL;
			END;

		IF COL_LENGTH('encabezado', 'estado_hacienda') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD estado_hacienda varchar(2) DEFAULT('0') NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN estado_hacienda varchar(2) NULL;
			END;

		IF COL_LENGTH('encabezado', 'mensaje_hacienda') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD mensaje_hacienda varchar(5000) DEFAULT('') NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN mensaje_hacienda varchar(5000) NULL;
			END;

		IF COL_LENGTH('encabezado', 'correos_adicionales') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD correos_adicionales varchar(1000) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN correos_adicionales varchar(1000) NULL;
			END;

		IF COL_LENGTH('encabezado', 'Reintentos') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD Reintentos int DEFAULT (0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN Reintentos int NOT NULL;
			END;

		IF COL_LENGTH('encabezado', 'FechaCE') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD FechaCE datetime DEFAULT '1900-01-01 00:00:00';
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN FechaCE datetime NULL;
			END;

		IF COL_LENGTH('encabezado', 'codigo_actividad') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD codigo_actividad varchar(6) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN codigo_actividad varchar(6) NULL;
			END;

		IF COL_LENGTH('encabezado', 'receptor_direccion_extranjero') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD receptor_direccion_extranjero varchar(300) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN receptor_direccion_extranjero varchar(300) NULL;
			END;

		IF COL_LENGTH('encabezado', 'iva_devuelto') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD iva_devuelto decimal(18,5) DEFAULT (0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN iva_devuelto decimal(18,5) NOT NULL;
			END;		

		IF COL_LENGTH('encabezado', 'tipodoc') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD tipodoc varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN tipodoc varchar(2) NULL;
			END;	

		IF COL_LENGTH('encabezado', 'numerod') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD numerod varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN numerod varchar(20) NULL;
			END;		

		IF COL_LENGTH('encabezado', 'reenviado') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD reenviado smallint DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN reenviado smallint NOT NULL;
			END;

		IF COL_LENGTH('encabezado', 'llave_sucursal') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD llave_sucursal varchar(20) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN llave_sucursal varchar(20) NULL;
			END;

		IF COL_LENGTH('encabezado', 'codclie') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD codclie varchar(15) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN codclie varchar(15) NULL;
			END;	

		IF COL_LENGTH('encabezado', 'codprov') IS NULL
			BEGIN
				ALTER TABLE encabezado ADD codprov varchar(15) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE encabezado ALTER COLUMN codprov varchar(15) NULL;
			END;
	END;

/*CREANDO INDICES EN TABLA ENCABEZADO*/
IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'encabezado_IX1')
	BEGIN
		CREATE NONCLUSTERED INDEX encabezado_IX1 ON encabezado
		(clave, consecutivo_completo, estado_envio)
	END;

/*MODIFICANDO SATABL TABLA ENCABEZADO*/
IF NOT EXISTS (SELECT tablename FROM SATABL WHERE tablename = 'encabezado')
	BEGIN
		INSERT INTO SATABL 
		VALUES ('encabezado','encabezado');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'encabezado'
		WHERE tablename = 'encabezado';
	END;

/*MODIFICANDO SAFIEL CON CAMPOS DE TABLA ENCABEZADO*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'encabezado';

INSERT INTO @FIELS 
VALUES	
(1,'tipo_documento','dtString'),
(2,'clave','dtString'),
(3,'fecha','dtDateTime'),
(4,'consecutivo_completo','dtString'),
(5,'estado_envio','dtInteger'),
(6,'mensaje_envio','dtString'),
(7,'estado_hacienda','dtInteger'),
(8,'mensaje_hacienda','dtString'),
(9,'receptor_direccion','dtString'),
(10,'receptor_telefono','dtString'),
(11,'receptor_email','dtString'),
(12,'receptor_identificacion','dtString'),
(13,'receptor_tipo','dtString'),
(14,'receptor_fax','dtString'),
(15,'tipodoc','dtString'),
(16,'numerod','dtString'),
(17,'llave_sucursal','dtString'),
(18,'codclie','dtString'),
(19,'codprov','dtString')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA LINEA*/
IF OBJECT_ID('linea') IS NULL
	BEGIN
		CREATE TABLE linea (
		  id_encabezado int NOT NULL,
		  clave varchar(50) DEFAULT NULL,
		  numero_linea int DEFAULT NULL,
		  tipo_cod_producto varchar(2) DEFAULT NULL,
		  codigo_producto varchar(20) DEFAULT NULL,
		  servicio int DEFAULT NULL,
		  cantidad decimal(16,3) DEFAULT NULL,
		  unidad_medida varchar(20) DEFAULT NULL,
		  detalle varchar(200) DEFAULT NULL,						--- MODIFICADO PARA V4.3 ---
		  precio_unitario decimal(18,5) DEFAULT NULL,
		  monto_total decimal(18,5) DEFAULT NULL,
		  monto_descuento decimal(18,5) DEFAULT (0),			
		  naturaleza_descuento varchar(80) DEFAULT NULL,
		  subtotal decimal(18,5) DEFAULT NULL,
		  impuesto1_codigo varchar(2) DEFAULT NULL,
		  impuesto1_porcentaje decimal(4,2) DEFAULT (0),		
		  impuesto1_monto decimal(18,5) DEFAULT (0),			
		  exoneracion1_incluir int DEFAULT (0),				
		  exoneracion1_tipo varchar(2) DEFAULT NULL,
		  exoneracion1_numero_doc varchar(40) DEFAULT NULL,				--- MODIFICADO PARA V4.3 ---
		  exoneracion1_institucion varchar(160) DEFAULT NULL,			--- MODIFICADO PARA V4.3 ---
		  exoneracion1_fecha_doc datetime DEFAULT '1900-01-01 00:00:00',
		  exoneracion1_monto_imp decimal(18,5) DEFAULT (0),		
		  exoneracion1_porc_compra int DEFAULT (0),				
		  impuesto2_codigo varchar(2) DEFAULT NULL,
		  impuesto2_porcentaje decimal(4,2) DEFAULT (0),		
		  impuesto2_monto decimal(18,5) DEFAULT (0),		
		  exoneracion2_incluir int DEFAULT (0),			
		  exoneracion2_tipo varchar(2) DEFAULT NULL,
		  exoneracion2_numero_doc varchar(40) DEFAULT NULL,				--- MODIFICADO PARA V4.3 ---
		  exoneracion2_institucion varchar(160) DEFAULT NULL,			--- MODIFICADO PARA V4.3 ---
		  exoneracion2_fecha_doc datetime DEFAULT '1900-01-01 00:00:00',
		  exoneracion2_monto_imp decimal(18,5) DEFAULT (0),		
		  exoneracion2_porc_compra int DEFAULT (0),				
		  monto_total_linea decimal(18,5) DEFAULT (0),		
		  codigo_hacienda varchar(13) DEFAULT NULL, 						--- CAMPO NUEVO PARA V4.3 ---
		  partida_arancelaria varchar(15) DEFAULT NULL,						--- CAMPO NUEVO PARA V4.3 ---
		  monto_descuento2 decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 ---
		  naturaleza_descuento2 varchar(80) DEFAULT NULL,					--- CAMPO NUEVO PARA V4.3 ---
		  monto_descuento3 decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 ---
		  naturaleza_descuento3 varchar(80) DEFAULT NULL,					--- CAMPO NUEVO PARA V4.3 ---
		  monto_descuento4 decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 ---
		  naturaleza_descuento4 varchar(80) DEFAULT NULL,					--- CAMPO NUEVO PARA V4.3 ---
		  monto_descuento5 decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 ---
		  naturaleza_descuento5 varchar(80) DEFAULT NULL,					--- CAMPO NUEVO PARA V4.3 ---
		  base_imponible decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 ---
		  factor_iva1 decimal(5,4) DEFAULT (0),						--- CAMPO NUEVO PARA V4.3 ---
		  codigo_tarifa1 varchar(2) DEFAULT NULL,							--- CAMPO NUEVO PARA V4.3 ---
		  factor_iva2 decimal(5,4) DEFAULT (0),		  				--- CAMPO NUEVO PARA V4.3 ---
		  codigo_tarifa2 varchar(2) DEFAULT NULL,							--- CAMPO NUEVO PARA V4.3 ---
		  monto_impuesto_exportacion1 decimal(18,5) DEFAULT (0),		--- CAMPO NUEVO PARA V4.3 ---	
		  monto_impuesto_exportacion2 decimal(18,5) DEFAULT (0),		--- CAMPO NUEVO PARA V4.3 ---	
		  monto_impuesto_neto decimal(18,5) DEFAULT (0),					--- CAMPO NUEVO PARA V4.3 --- 
		  unidad_medida_comercial varchar(20) DEFAULT NULL,
		  descripcion_larga_linea varchar(1000) DEFAULT NULL); 			--- CAMPO NUEVO PARA V4.3 --- 
	END;
ELSE
	BEGIN
		IF COL_LENGTH('linea', 'clave') IS NULL
			BEGIN
				ALTER TABLE linea ADD clave varchar(50) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN clave varchar(50) NULL;
			END;

		IF COL_LENGTH('linea', 'numero_linea') IS NULL
			BEGIN
				ALTER TABLE linea ADD numero_linea int DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN numero_linea int NULL;
			END;

		IF COL_LENGTH('linea', 'tipo_cod_producto') IS NULL
			BEGIN
				ALTER TABLE linea ADD tipo_cod_producto varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN tipo_cod_producto varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'codigo_producto') IS NULL
			BEGIN
				ALTER TABLE linea ADD codigo_producto varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN codigo_producto varchar(20) NULL;
			END;

		IF COL_LENGTH('linea', 'servicio') IS NULL
			BEGIN
				ALTER TABLE linea ADD servicio int DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN servicio int NULL;
			END;

		IF COL_LENGTH('linea', 'cantidad') IS NULL
			BEGIN
				ALTER TABLE linea ADD cantidad decimal(16,3) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN cantidad decimal(16,3) NULL;
			END;

		IF COL_LENGTH('linea', 'unidad_medida') IS NULL
			BEGIN
				ALTER TABLE linea ADD unidad_medida varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN unidad_medida varchar(20) NULL;
			END;

		IF COL_LENGTH('linea', 'detalle') IS NULL
			BEGIN
				ALTER TABLE linea ADD detalle varchar(200) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN detalle varchar(200) NULL;
			END;

		IF COL_LENGTH('linea', 'precio_unitario') IS NULL
			BEGIN
				ALTER TABLE linea ADD precio_unitario decimal(18,5) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN precio_unitario decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_total') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_total decimal(18,5) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_total decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_descuento') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_descuento decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_descuento decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'naturaleza_descuento') IS NULL
			BEGIN
				ALTER TABLE linea ADD naturaleza_descuento varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN naturaleza_descuento varchar(80) NULL;
			END;

		IF COL_LENGTH('linea', 'subtotal') IS NULL
			BEGIN
				ALTER TABLE linea ADD subtotal decimal(18,5) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN subtotal decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto1_codigo') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto1_codigo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto1_codigo varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto1_porcentaje') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto1_porcentaje decimal(4,2) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto1_porcentaje decimal(4,2) NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto1_monto') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto1_monto decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto1_monto decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_incluir') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_incluir int DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_incluir int NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_tipo') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_tipo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_tipo varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_numero_doc') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_numero_doc varchar(40) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_numero_doc varchar(40) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_institucion') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_institucion varchar(160) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_institucion varchar(160) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_fecha_doc') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_fecha_doc datetime DEFAULT '1900-01-01 00:00:00';
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_fecha_doc datetime NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_monto_imp') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_monto_imp decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_monto_imp decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion1_porc_compra') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion1_porc_compra int DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion1_porc_compra int NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto2_codigo') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto2_codigo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto2_codigo varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto2_porcentaje') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto2_porcentaje decimal(4,2) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto2_porcentaje decimal(4,2) NULL;
			END;

		IF COL_LENGTH('linea', 'impuesto2_monto') IS NULL
			BEGIN
				ALTER TABLE linea ADD impuesto2_monto decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN impuesto2_monto decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_incluir') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_incluir int DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_incluir int NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_tipo') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_tipo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_tipo varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_numero_doc') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_numero_doc varchar(40) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_numero_doc varchar(40) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_institucion') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_institucion varchar(160) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_institucion varchar(160) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_fecha_doc') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_fecha_doc datetime DEFAULT '1900-01-01 00:00:00';
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_fecha_doc datetime NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_monto_imp') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_monto_imp decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_monto_imp decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'exoneracion2_porc_compra') IS NULL
			BEGIN
				ALTER TABLE linea ADD exoneracion2_porc_compra int DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN exoneracion2_porc_compra int NULL;
			END;

		IF COL_LENGTH('linea', 'monto_total_linea') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_total_linea decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_total_linea decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'codigo_hacienda') IS NULL
			BEGIN
				ALTER TABLE linea ADD codigo_hacienda varchar(13) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN codigo_hacienda varchar(13) NULL;
			END;

		IF COL_LENGTH('linea', 'partida_arancelaria') IS NULL
			BEGIN
				ALTER TABLE linea ADD partida_arancelaria varchar(15) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN partida_arancelaria varchar(15) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_descuento2') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_descuento2 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_descuento2 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'naturaleza_descuento2') IS NULL
			BEGIN
				ALTER TABLE linea ADD naturaleza_descuento2 varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN naturaleza_descuento2 varchar(80) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_descuento3') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_descuento3 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_descuento3 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'naturaleza_descuento3') IS NULL
			BEGIN
				ALTER TABLE linea ADD naturaleza_descuento3 varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN naturaleza_descuento3 varchar(80) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_descuento4') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_descuento4 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_descuento4 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'naturaleza_descuento4') IS NULL
			BEGIN
				ALTER TABLE linea ADD naturaleza_descuento4 varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN naturaleza_descuento4 varchar(80) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_descuento5') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_descuento5 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_descuento5 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'naturaleza_descuento5') IS NULL
			BEGIN
				ALTER TABLE linea ADD naturaleza_descuento5 varchar(80) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN naturaleza_descuento5 varchar(80) NULL;
			END;

		IF COL_LENGTH('linea', 'base_imponible') IS NULL
			BEGIN
				ALTER TABLE linea ADD base_imponible decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN base_imponible decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'factor_iva1') IS NULL
			BEGIN
				ALTER TABLE linea ADD factor_iva1 decimal(5,4) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN factor_iva1 decimal(5,4) NULL;
			END;

		IF COL_LENGTH('linea', 'codigo_tarifa1') IS NULL
			BEGIN
				ALTER TABLE linea ADD codigo_tarifa1 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN codigo_tarifa1 varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'factor_iva2') IS NULL
			BEGIN
				ALTER TABLE linea ADD factor_iva2 decimal(5,4) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN factor_iva2 decimal(5,4) NULL;
			END;

		IF COL_LENGTH('linea', 'codigo_tarifa2') IS NULL
			BEGIN
				ALTER TABLE linea ADD codigo_tarifa2 varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN codigo_tarifa2 varchar(2) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_impuesto_exportacion1') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_impuesto_exportacion1 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_impuesto_exportacion1 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_impuesto_exportacion2') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_impuesto_exportacion2 decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_impuesto_exportacion2 decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'monto_impuesto_neto') IS NULL
			BEGIN
				ALTER TABLE linea ADD monto_impuesto_neto decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN monto_impuesto_neto decimal(18,5) NULL;
			END;

		IF COL_LENGTH('linea', 'unidad_medida_comercial') IS NULL
			BEGIN
				ALTER TABLE linea ADD unidad_medida_comercial varchar(20) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN unidad_medida_comercial varchar(20) NULL;
			END;

		IF COL_LENGTH('linea', 'descripcion_larga_linea') IS NULL
			BEGIN
				ALTER TABLE linea ADD descripcion_larga_linea varchar(1000) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE linea ALTER COLUMN descripcion_larga_linea varchar(1000) NULL;
			END;
	END;

/*CREANDO INDICES EN TABLA linea*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'linea')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'linea_IX0')
			BEGIN
				CREATE CLUSTERED INDEX linea_IX0 ON linea
				(id_encabezado)
			END;
	END;

IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'linea_IX1')
	BEGIN
		CREATE NONCLUSTERED INDEX linea_IX1 ON linea
		(clave, numero_linea, codigo_producto)
	END;
GO
/*TABLA OTROS_COSTOS*/
IF OBJECT_ID('otros_cargos') IS NULL
	BEGIN
		CREATE TABLE otros_cargos (
			id_encabezado int NOT NULL,
			clave varchar(50) DEFAULT NULL,
			otc_tipo varchar(2) DEFAULT NULL,
			otc_monto decimal(18,5) DEFAULT (0),
			otc_detalle varchar(160) DEFAULT NULL,
			otc_porcentaje decimal(6,5) DEFAULT (0),
			otc_nombre varchar(100) DEFAULT NULL,
			otc_identificacion varchar(12) DEFAULT NULL,
			otc_nrolinea INT DEFAULT (0) NOT NULL);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('otros_cargos', 'id_encabezado') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD id_encabezado int NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN id_encabezado int NOT NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'clave') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD clave varchar(50) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN clave varchar(50) NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'otc_tipo') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_tipo varchar(2) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_tipo varchar(2) NULL;
			END;	

		IF COL_LENGTH('otros_cargos', 'otc_monto') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_monto decimal(18,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_monto decimal(18,5) NOT NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'otc_detalle') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_detalle varchar(160) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_detalle varchar(160) NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'otc_porcentaje') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_porcentaje decimal(6,5) DEFAULT (0);
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_porcentaje decimal(6,5) NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'otc_nombre') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_nombre varchar(100) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_nombre varchar(100) NULL;
			END;

		IF COL_LENGTH('otros_cargos', 'otc_identificacion') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_identificacion varchar(12) DEFAULT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_identificacion varchar(12) NULL;
			END;	

		IF COL_LENGTH('otros_cargos', 'otc_nrolinea') IS NULL
			BEGIN
				ALTER TABLE otros_cargos ADD otc_nrolinea INT DEFAULT (0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE otros_cargos ALTER COLUMN otc_nrolinea INT NOT NULL;
			END;	
	END;

/*CREANDO INDICES EN TABLA otros_cargos*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'otros_cargos')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'otros_cargos_IX0')
			BEGIN
				CREATE CLUSTERED INDEX otros_cargos_IX0 ON otros_cargos
				(id_encabezado)
			END;
	END;

IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'otros_cargos_IX1')
	BEGIN
		CREATE NONCLUSTERED INDEX otros_cargos_IX1 ON otros_cargos
		(clave, otc_nrolinea, otc_tipo)
	END;
GO
/*TABLA COMPROBANTESFE*/
IF OBJECT_ID('ComprobantesFE') IS NULL
	BEGIN
		CREATE TABLE [dbo].[ComprobantesFE](
			[Id] [bigint] IDENTITY(1,1) NOT NULL,
			[NroUnico] [bigint] DEFAULT ((0)) NOT NULL,
			[TipoDoc] [varchar](2) NOT NULL,
			[NumeroD] [varchar](20) NOT NULL,
			[NumeroDE] [varchar](10) NULL,
			[Clave] [varchar](50) NULL,
			[TipoDocFE] [varchar](2) NULL,
			[ConsecutivoDE] [varchar](20) NULL,
			[ClaveRef] [varchar](50) NULL,
			[RefTipoDoc] [varchar](2) NULL,
			[RefFecha] [datetime] NULL,
			[RefCodigo] [varchar](2) NULL,
			[RefRazon] [varchar](180) NULL,
			[ExoneracionE] [smallint] DEFAULT ((0)) NOT NULL,
			[NroItemT] [int] DEFAULT ((0)) NOT NULL,
			[Iva_Devuelto] [decimal](28, 4) DEFAULT ((0)) NOT NULL,
			[Monto_Exonerado] [decimal](28, 4) DEFAULT ((0)) NOT NULL,
			[CodTaxsClie] [varchar](5) NULL,
			[EstadoFE] [tinyint] DEFAULT ((0)) NOT NULL,
			[MensajeFE] [varchar](500) NULL,
		 CONSTRAINT [PK_COMPROBANTESFE] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC,
			[NroUnico] ASC,
			[TipoDoc] ASC,
			[NumeroD] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END;
ELSE
	BEGIN
		IF COL_LENGTH('ComprobantesFE', 'NroUnico') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [NroUnico] [bigint] DEFAULT (0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [NroUnico] [bigint] NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'TipoDoc') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [TipoDoc] [varchar](2) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [TipoDoc] [varchar](2) NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'NumeroD') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [NumeroD] [varchar](20) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [NumeroD] [varchar](20) NOT NULL;
			END;
			
		IF COL_LENGTH('ComprobantesFE', 'NumeroDE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [NumeroDE] [varchar](10) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [NumeroDE] [varchar](10) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'Clave') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [Clave] [varchar](50) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [Clave] [varchar](50) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'TipoDocFE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [TipoDocFE] [varchar](2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [TipoDocFE] [varchar](2) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'ConsecutivoDE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [ConsecutivoDE] [varchar](20) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [ConsecutivoDE] [varchar](20) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'ClaveRef') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [ClaveRef] [varchar](50) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [ClaveRef] [varchar](50) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'RefTipoDoc') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [RefTipoDoc] [varchar](2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [RefTipoDoc] [varchar](2) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'RefFecha') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [RefFecha] [datetime] NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [RefFecha] [datetime] NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'RefCodigo') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [RefCodigo] [varchar](2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [RefCodigo] [varchar](2) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'RefCodigo') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [RefCodigo] [varchar](2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [RefCodigo] [varchar](2) NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'RefRazon') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [RefRazon] [varchar](180) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [RefRazon] [varchar](180) NULL;
			END;			

		IF COL_LENGTH('ComprobantesFE', 'ExoneracionE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [ExoneracionE] [smallint] DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [ExoneracionE] [smallint] NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'NroItemT') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [NroItemT] [int] DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [NroItemT] [int] NOT NULL;
			END;
			
		IF COL_LENGTH('ComprobantesFE', 'Iva_Devuelto') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [Iva_Devuelto] [decimal](28, 4) DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [Iva_Devuelto] [decimal](28, 4) NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'Monto_Exonerado') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [Monto_Exonerado] [decimal](28, 4) DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [Monto_Exonerado] [decimal](28, 4) NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'CodTaxsClie') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [CodTaxsClie] [varchar](5) DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [CodTaxsClie] [varchar](5) NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'EstadoFE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [EstadoFE] [tinyint] DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [EstadoFE] [tinyint] NOT NULL;
			END;

		IF COL_LENGTH('ComprobantesFE', 'MensajeFE') IS NULL
			BEGIN
				ALTER TABLE ComprobantesFE ADD [MensajeFE] [varchar](500) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ComprobantesFE ALTER COLUMN [MensajeFE] [varchar](500) NULL;
			END;
	END;

/*MODIFICANDO SATABL TABLA COMPROBANTESFE*/
IF NOT EXISTS (SELECT tablename FROM SATABL WHERE tablename = 'ComprobantesFE')
	BEGIN
		INSERT INTO SATABL 
		VALUES ('ComprobantesFE','ComprobantesFE');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'ComprobantesFE'
		WHERE tablename = 'ComprobantesFE';
	END;

/*MODIFICANDO SAFIEL CON CAMPOS DE TABLA COMPROBANTESFE*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'ComprobantesFE';

INSERT INTO @FIELS 
VALUES	
(1,'Id','dtLongInt'),
(2,'NroUnico','dtLongInt'),
(3,'TipoDoc','dtString'),
(4,'NumeroD','dtString'),
(5,'NumeroDE','dtString'),
(6,'Clave','dtString'),
(7,'TipoDocFE','dtString'),
(8,'ConsecutivoDE','dtString'),
(9,'ClaveRef','dtString'),
(10,'ExoneracionE','dtInteger'),
(11,'NroItemT','dtInteger'),
(12,'Iva_Devuelto','dtDouble'),
(13,'Monto_Exonerado','dtDouble'),
(14,'CodTaxsClie','dtString'),
(15,'EstadoFE','dtInteger'),
(16,'MensajeFE','dtString')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA CAMBIOSMAESTROS*/
IF OBJECT_ID('CambiosMaestrosFE') IS NULL
	BEGIN
		CREATE TABLE [dbo].[CambiosMaestrosFE](
			[Id] [bigint] IDENTITY(1,1) NOT NULL,
			[tipo] [varchar](5) NULL,
			[codigo] [varchar](50) NULL,
			[estado_sync] [tinyint] DEFAULT ((0)) NOT NULL,
			[mensaje_sync] [varchar](250) NULL,
			[fechaCE] [datetime] NULL,
		 CONSTRAINT [PK_CambiosMaestrosFE] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END;
ELSE
	BEGIN
		IF COL_LENGTH('CambiosMaestrosFE', 'tipo') IS NULL
			BEGIN
				ALTER TABLE CambiosMaestrosFE ADD [tipo] [varchar](5) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE CambiosMaestrosFE ALTER COLUMN [tipo] [varchar](5) NULL;
			END;

		IF COL_LENGTH('CambiosMaestrosFE', 'codigo') IS NULL
			BEGIN
				ALTER TABLE CambiosMaestrosFE ADD [codigo] [varchar](50) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE CambiosMaestrosFE ALTER COLUMN [codigo] [varchar](50) NULL;
			END;

		IF COL_LENGTH('CambiosMaestrosFE', 'estado_sync') IS NULL
			BEGIN
				ALTER TABLE CambiosMaestrosFE ADD [estado_sync] [tinyint] DEFAULT ((0)) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE CambiosMaestrosFE ALTER COLUMN [estado_sync] [tinyint] NOT NULL;
			END;

		IF COL_LENGTH('CambiosMaestrosFE', 'mensaje_sync') IS NULL
			BEGIN
				ALTER TABLE CambiosMaestrosFE ADD [mensaje_sync] [varchar](250) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE CambiosMaestrosFE ALTER COLUMN [mensaje_sync] [varchar](250) NULL;
			END;

		IF COL_LENGTH('CambiosMaestrosFE', 'fechaCE') IS NULL
			BEGIN
				ALTER TABLE CambiosMaestrosFE ADD [fechaCE] [datetime] NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE CambiosMaestrosFE ALTER COLUMN [fechaCE] [datetime] NULL;
			END;		
	END;

/*MODIFICANDO SATABL TABLA CAMBIOSMAESTROS*/
IF NOT EXISTS (SELECT tablename FROM SATABL WHERE tablename = 'CambiosMaestrosFE')
	BEGIN
		INSERT INTO SATABL 
		VALUES ('CambiosMaestrosFE','CambiosMaestrosFE');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'CambiosMaestrosFE'
		WHERE tablename = 'CambiosMaestrosFE';
	END;

/*MODIFICANDO SAFIEL CON CAMPOS DE TABLA CAMBIOSMAESTROS*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'CambiosMaestrosFE';

INSERT INTO @FIELS 
VALUES	
(1,'Id','dtLongInt'),
(2,'codigo','dtString'),
(3,'estado_sync','dtInteger'),
(4,'mensaje_sync','dtString'),
(5,'fechaCE','dtDateTime')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLAS CONSECUTIVO Y ESTACIONES*/
IF OBJECT_ID('ConsecutivosFE') IS NULL
	BEGIN
		CREATE TABLE ConsecutivosFE
		(Sucursal INT DEFAULT (0) NOT NULL,
		Estacion INT NOT NULL,
		LenCorrelFE INT NOT NULL,
		PrxFactFE INT DEFAULT (1) NOT NULL,
		PrxNDFE INT DEFAULT (1) NOT NULL,
		PrxNCFE INT DEFAULT (1) NOT NULL,
		PrxTickEFE INT DEFAULT (1) NOT NULL,
		PrxCompCFE INT DEFAULT (1) NOT NULL,
		PrxComFE INT DEFAULT (1) NOT NULL,
		PrxFExpFE INT DEFAULT (1) NOT NULL);

		/*INSERTAR VALOR POR DEFECTO DE ESTACION 00001*/
		INSERT INTO ConsecutivosFE 	(Estacion,LenCorrelFE)
		VALUES	(1,10);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('ConsecutivosFE', 'Estacion') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD Estacion INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN Estacion INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'LenCorrelFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD LenCorrelFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN LenCorrelFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxFactFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxFactFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxFactFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxNDFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxNDFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxNDFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxNCFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxNCFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxNCFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxTickEFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxTickEFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxTickEFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxCompCFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxCompCFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxCompCFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxComFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxComFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxComFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'PrxFExpFE') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD PrxFExpFE INT DEFAULT (1) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN PrxFExpFE INT NOT NULL;
			END;

		IF COL_LENGTH('ConsecutivosFE', 'Sucursal') IS NULL
			BEGIN
				ALTER TABLE ConsecutivosFE ADD Sucursal INT DEFAULT (0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE ConsecutivosFE ALTER COLUMN Sucursal INT NOT NULL;
			END;			
	END;
GO
/*TABLA CODIGOS DE MONEDA*/
IF OBJECT_ID('FECodigoMoneda') IS NULL
	BEGIN
		CREATE TABLE FECodigoMoneda
		(CODMONEDA VARCHAR(5) NULL,
		PAIS VARCHAR(100) NULL,
		NOMBRE_MONEDA VARCHAR(100) NULL,
		ID INT DEFAULT(0) NOT NULL,
		Nivel INT DEFAULT(0) NOT NULL);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('FECodigoMoneda', 'CODMONEDA') IS NULL
			BEGIN
				ALTER TABLE FECodigoMoneda ADD CODMONEDA varchar(5) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoMoneda ALTER COLUMN CODMONEDA varchar(5) NULL;
			END;

		IF COL_LENGTH('FECodigoMoneda', 'PAIS') IS NULL
			BEGIN
				ALTER TABLE FECodigoMoneda ADD PAIS varchar(100) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoMoneda ALTER COLUMN PAIS varchar(100) NULL;
			END;

		IF COL_LENGTH('FECodigoMoneda', 'NOMBRE_MONEDA') IS NULL
			BEGIN
				ALTER TABLE FECodigoMoneda ADD NOMBRE_MONEDA varchar(100) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoMoneda ALTER COLUMN NOMBRE_MONEDA varchar(100) NULL;
			END;

		IF COL_LENGTH('FECodigoMoneda', 'ID') IS NULL
			BEGIN
				ALTER TABLE FECodigoMoneda ADD ID INT DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoMoneda ALTER COLUMN ID INT NOT NULL;
			END;

		IF COL_LENGTH('FECodigoMoneda', 'Nivel') IS NULL
			BEGIN
				ALTER TABLE FECodigoMoneda ADD Nivel INT DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoMoneda ALTER COLUMN Nivel INT NOT NULL;
			END;
	END;

/*CREANDO INDICE*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'FECodigoMoneda')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'FECodigoMoneda_IX0')
			BEGIN
				CREATE CLUSTERED INDEX FECodigoMoneda_IX0 ON FECodigoMoneda
				(ID)
			END;
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FECodigoMoneda') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FECodigoMoneda','FECodigoMoneda');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FECodigoMoneda'
		WHERE tablename = 'FECodigoMoneda';
	END;

/*MODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FECodigoMoneda';

INSERT INTO @FIELS 
VALUES	
(1,'CODMONEDA','dtString'),
(2,'PAIS','dtString'),
(3,'NOMBRE_MONEDA','dtString'),
(4,'ID','dtInteger'),
(5,'Nivel','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA UNIDADES DE MEDIDA*/
IF OBJECT_ID('FEUnidades') IS NULL
	BEGIN
		CREATE TABLE FEUnidades 
		(CodHacienda VARCHAR(20) NULL,
		CodSaint VARCHAR(3) NULL,
		Descrip VARCHAR (100) NULL,
		ID INT DEFAULT(0) NOT NULL);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('FEUnidades', 'CodHacienda') IS NULL
			BEGIN
				ALTER TABLE FEUnidades ADD CodHacienda varchar(20) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FEUnidades ALTER COLUMN CodHacienda varchar(20) NULL;
			END;

		IF COL_LENGTH('FEUnidades', 'CodSaint') IS NULL
			BEGIN
				ALTER TABLE FEUnidades ADD CodSaint varchar(3) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FEUnidades ALTER COLUMN CodSaint varchar(3) NULL;
			END;

		IF COL_LENGTH('FEUnidades', 'Descrip') IS NULL
			BEGIN
				ALTER TABLE FEUnidades ADD Descrip varchar(100) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FEUnidades ALTER COLUMN Descrip varchar(100) NULL;
			END;

		IF COL_LENGTH('FEUnidades', 'ID') IS NULL
			BEGIN
				ALTER TABLE FEUnidades ADD ID INT DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FEUnidades ALTER COLUMN ID INT NOT NULL;
			END;
	END;

/*CREANDO INDICES*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'FEUnidades')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'FEUnidades_IX0')
			BEGIN
				CREATE CLUSTERED INDEX FEUnidades_IX0 ON FEUnidades
				(ID)
			END;
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FEUnidades') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FEUnidades','FEUnidades');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FEUnidades'
		WHERE tablename = 'FEUnidades';
	END;

/*MODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FEUnidades';

INSERT INTO @FIELS 
VALUES	
(1,'CodHacienda','dtString'),
(2,'CodSaint','dtString'),
(3,'Descrip','dtString'),
(4,'ID','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA TIPO DE IDENTIFICACION*/
IF OBJECT_ID('FETipoIdentificacion') IS NULL
	BEGIN
		CREATE TABLE FETipoIdentificacion
		(CodTipo varchar(2) NULL,
		Descrip varchar(60) NULL,
		ID INT DEFAULT(0) NOT NULL);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('FETipoIdentificacion', 'CodTipo') IS NULL
			BEGIN
				ALTER TABLE FETipoIdentificacion ADD CodTipo varchar(2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FETipoIdentificacion ALTER COLUMN CodTipo varchar(2) NULL;
			END;

		IF COL_LENGTH('FETipoIdentificacion', 'Descrip') IS NULL
			BEGIN
				ALTER TABLE FETipoIdentificacion ADD Descrip varchar(60) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FETipoIdentificacion ALTER COLUMN Descrip varchar(60) NULL;
			END;

		IF COL_LENGTH('FETipoIdentificacion', 'ID') IS NULL
			BEGIN
				ALTER TABLE FETipoIdentificacion ADD ID INT DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FETipoIdentificacion ALTER COLUMN ID INT NOT NULL;
			END;
	END;

/*CREANDO INDICE*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'FETipoIdentificacion')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'FETipoIdentificacion_IX0')
			BEGIN
				CREATE CLUSTERED INDEX FETipoIdentificacion_IX0 ON FETipoIdentificacion
				(ID)
			END;
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FETipoIdentificacion') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FETipoIdentificacion','FETipoIdentificacion');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FETipoIdentificacion'
		WHERE tablename = 'FETipoIdentificacion';
	END;

/*MODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FETipoIdentificacion';

INSERT INTO @FIELS 
VALUES	
(1,'CodTipo','dtString'),
(2,'Descrip','dtString'),
(3,'ID','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA CODIGO DE EXONERACION*/
IF OBJECT_ID('FECodigoExoneracion') IS NULL
	BEGIN
		CREATE TABLE FECodigoExoneracion
		(CodExoneracion varchar(2) NULL,
		Descrip varchar(100),
		ID INT DEFAULT(0) NOT NULL);
	END;
ELSE
	BEGIN
		IF COL_LENGTH('FECodigoExoneracion', 'CodExoneracion') IS NULL
			BEGIN
				ALTER TABLE FECodigoExoneracion ADD CodExoneracion varchar(2) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoExoneracion ALTER COLUMN CodExoneracion varchar(2) NULL;
			END;

		IF COL_LENGTH('FECodigoExoneracion', 'Descrip') IS NULL
			BEGIN
				ALTER TABLE FECodigoExoneracion ADD Descrip varchar(100) NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoExoneracion ALTER COLUMN Descrip varchar(100) NULL;
			END;

		IF COL_LENGTH('FECodigoExoneracion', 'ID') IS NULL
			BEGIN
				ALTER TABLE FECodigoExoneracion ADD ID INT DEFAULT(0) NOT NULL;
			END;
		ELSE 
			BEGIN
				ALTER TABLE FECodigoExoneracion ALTER COLUMN ID INT NOT NULL;
			END;
	END;

/*CREANDO INDICES*/
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'FECodigoExoneracion')
	BEGIN
		IF NOT EXISTS (SELECT * FROM SYS.indexes WHERE NAME = 'FECodigoExoneracion_IX0')
			BEGIN
				CREATE CLUSTERED INDEX FECodigoExoneracion_IX0 ON FECodigoExoneracion
				(ID)
			END;
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FECodigoExoneracion') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FECodigoExoneracion','FECodigoExoneracion');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FECodigoExoneracion'
		WHERE tablename = 'FECodigoExoneracion';
	END;

/*MODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FECodigoExoneracion';

INSERT INTO @FIELS 
VALUES	
(1,'CodExoneracion','dtString'),
(2,'Descrip','dtString'),
(3,'ID','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA CODIGOS DE AREAS DE PAIS*/
IF OBJECT_ID('FECodAreaPais') IS NULL
	BEGIN
		--- CREAR TABLA CODIGOS DE AREAS DE PAIS ----
		CREATE TABLE FECodAreaPais
		(Id	INT NOT NULL,
		Descrip VARCHAR(50) NULL,	
		Prefijo	VARCHAR(5) NULL,
		Pais VARCHAR(50) NULL,
		PRIMARY KEY (Id));
	END;
ELSE
	BEGIN
		--- CREANDO CAMPOS NUEVOS O ACTUALIZANDO LOS YA EXISTENTES ---
		IF COL_LENGTH('FECodAreaPais', 'Descrip') IS NULL
			BEGIN
				ALTER TABLE FECodAreaPais ADD Descrip VARCHAR(50) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FECodAreaPais ALTER COLUMN Descrip VARCHAR(50) NULL;
			END;
		IF COL_LENGTH('FECodAreaPais', 'Prefijo') IS NULL
			BEGIN
				ALTER TABLE FECodAreaPais ADD Prefijo VARCHAR(5) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FECodAreaPais ALTER COLUMN Prefijo VARCHAR(5) NULL;
			END;
		IF COL_LENGTH('FECodAreaPais', 'Pais') IS NULL
			BEGIN
				ALTER TABLE FECodAreaPais ADD Pais VARCHAR(50) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FECodAreaPais ALTER COLUMN Pais VARCHAR(50) NULL;
			END;
	END;
GO
/*TABLA AUDITORIA VIEJA*/
IF OBJECT_ID('Auditoria_Mod_FE') IS NOT NULL
	BEGIN
		DROP TABLE Auditoria_Mod_FE;
	END;

/*MODIFICANDO SATABL*/
DELETE FROM SATABL
WHERE tablename = 'Auditoria_Mod_FE';

DELETE FROM SAFIEL
WHERE tablename = 'Auditoria_Mod_FE'; 
GO
/*TABLA AUDITORIA NUEVA*/
IF OBJECT_ID('FEAuditoria') IS NULL
	BEGIN
		CREATE TABLE FEAuditoria	(NroUnico int IDENTITY(1,1) NOT NULL,
									Nro_Factura varchar(20) NULL,
									Tipo varchar(3)	NULL,
									Mensaje nvarchar(max) NULL,
									FechaP datetime NULL,
									PRIMARY KEY (NroUnico));
	END;
ELSE
	BEGIN
		--- CREANDO CAMPOS NUEVOS O ACTUALIZANDO LOS YA EXISTENTES ---
		IF COL_LENGTH('FEAuditoria', 'Nro_Factura') IS NULL
			BEGIN
				ALTER TABLE FEAuditoria ADD Nro_Factura varchar(20) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEAuditoria ALTER COLUMN Nro_Factura varchar(20) NULL;
			END;

		IF COL_LENGTH('FEAuditoria', 'Tipo') IS NULL
			BEGIN
				ALTER TABLE FEAuditoria ADD Tipo varchar(3)	NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEAuditoria ALTER COLUMN Tipo varchar(3) NULL;
			END;	

		IF COL_LENGTH('FEAuditoria', 'Mensaje') IS NULL
			BEGIN
				ALTER TABLE FEAuditoria ADD Mensaje varchar(max) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEAuditoria ALTER COLUMN Mensaje varchar(max) NULL;
			END;		

		IF COL_LENGTH('FEAuditoria', 'FechaP') IS NULL
			BEGIN
				ALTER TABLE FEAuditoria ADD FechaP datetime NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEAuditoria ALTER COLUMN FechaP datetime NULL;
			END;	
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FEAuditoria') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FEAuditoria','FEAuditoria');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FEAuditoria'
		WHERE tablename = 'FEAuditoria';
	END;

/*MODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FEAuditoria';

INSERT INTO @FIELS 
VALUES	
(1,'Nro_Factura','dtString'),
(2,'Tipo','dtString'),
(3,'Mensaje','dtMemo'),
(4,'FechaP','dtDateTime')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/*TABLA ACTIVIDADES ECONOMICAS*/
IF OBJECT_ID('FEActividades_Eco') IS NULL
	BEGIN
		CREATE TABLE FEActividades_Eco	(NroUnico int NOT NULL,
										Codigo_Actividad varchar(6) NULL,
										Nombre varchar(250) NULL,
										Nivel smallint NULL,
										PRIMARY KEY (NroUnico));
	END;
ELSE
	BEGIN
		--- CREANDO CAMPOS NUEVOS O ACTUALIZANDO LOS YA EXISTENTES ---
		IF COL_LENGTH('FEActividades_Eco', 'Codigo_Actividad') IS NULL
			BEGIN
				ALTER TABLE FEActividades_Eco ADD Codigo_Actividad varchar(6) NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEActividades_Eco ALTER COLUMN Codigo_Actividad varchar(6) NULL;
			END;		

		IF COL_LENGTH('FEActividades_Eco', 'Nombre') IS NULL
			BEGIN
				ALTER TABLE FEActividades_Eco ADD Nombre varchar(250)	NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEActividades_Eco ALTER COLUMN Nombre varchar(250) NULL;
			END;		

		IF COL_LENGTH('FEActividades_Eco', 'Nivel') IS NULL
			BEGIN
				ALTER TABLE FEActividades_Eco ADD Nivel smallint NULL;
			END;
		ELSE
			BEGIN
				ALTER TABLE FEActividades_Eco ALTER COLUMN Nivel smallint NULL;
			END;			
	END;

/*MODIFICANDO SATABL*/
IF (SELECT tablename FROM SATABL WHERE tablename = 'FEActividades_Eco') IS NULL
	BEGIN
		INSERT INTO SATABL 
		VALUES ('FEActividades_Eco','FEActividades_Eco');
	END;
ELSE 
	BEGIN
		UPDATE SATABL SET 	tablealias = 'FEActividades_Eco'
		WHERE tablename = 'FEActividades_Eco';
	END;

/*ODIFICANDO SAFIEL*/
DECLARE
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int,
@linea int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

SET @tablename = 'FEActividades_Eco';

INSERT INTO @FIELS 
VALUES	
(1,'NroUnico','dtString'),
(2,'Codigo_Actividad','dtString'),
(3,'Nombre','dtMemo'),
(4,'Nivel','dtDateTime')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE @linea <= @contador
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
GO
/***CARGANDO DATOS DE FE EN TABLAS NUEVAS Y DE SAINT***/
/*CARGANDO UBICACION*/
DECLARE 
@PAIS AS INT,
@PROVINCIA AS INT,
@PROVINCIAFE AS INT,
@CANTON AS INT, 
@CANTONFE AS INT,
@DISTRITO AS INT,
@DISTRITOFE AS INT,
@CONTPROV AS INT,
@CONTCANTON AS INT,
@CONTDIST AS INT,
@LINEAP AS INT,
@LINEAC AS INT,
@LINEAD AS INT,
@DESCRIP AS VARCHAR(40),
@ProvinciaTV AS varchar(100),
@CantonTV AS varchar(100),
@DistritoTV AS varchar(100)

DECLARE @TABLA_PROVINCIAS TABLE (Descrip varchar(40), Pais INT, IDESTADOFE INT, ProvinciaTV varchar(100))
DECLARE @TABLA_CANTONES TABLE (Descrip varchar(40), Pais INT, IDESTADOFE INT, IDCIUDADFE INT, CantonTV varchar(100))
DECLARE @TABLA_DISTRITOS TABLE (Descrip varchar(40), Pais INT, IDESTADOFE INT, IDCIUDADFE INT, IDMUNICIPIOFE INT, DistritoTV varchar(100))

--- INGRESANDO PAIS ---
IF (SELECT Descrip FROM SAPAIS WHERE Descrip LIKE '%COSTA%RICA%') IS NULL
	BEGIN
		INSERT INTO SAPAIS (Descrip, SPais, SEstado, SCiudad, SMunicipio, IDPais, PaisTV)
		VALUES ('COSTA RICA','País','Provincia','Canton','Distrito',506,'CR');
	END;
ELSE
	BEGIN
		UPDATE SAPAIS SET 	Descrip = 'COSTA RICA', 
							SPais = 'País', 
							SEstado = 'Provincia', 
							SCiudad = 'Canton', 
							SMunicipio = 'Distrito',
							IDPais = 506,
							PaisTV = 'CR'
		WHERE Descrip LIKE '%COSTA%RICA%';
	END;

--- CARGANDO TABLAS TEMPORALES ---
SELECT @PAIS = Pais  
FROM SAPAIS WITH (NOLOCK)
WHERE Descrip LIKE '%COSTA%RICA%';

INSERT INTO @TABLA_PROVINCIAS 
VALUES	('SAN JOSE',@PAIS,1,'SJ'),
		('ALAJUELA',@PAIS,2,'AL'),
		('CARTAGO',@PAIS,3,'CG'),
		('HEREDIA',@PAIS,4,'HD'),
		('GUANACASTE',@PAIS,5,'GT'),
		('PUNTARENAS',@PAIS,6,'PT'),
		('LIMON',@PAIS,7,'LM')

INSERT INTO @TABLA_CANTONES VALUES
('SAN JOSÉ',@PAIS,1,1,'San José'),
('ESCAZÚ',@PAIS,1,2,'Escazú'),
('DESAMPARADOS',@PAIS,1,3,'Desamparados'),
('PURISCAL',@PAIS,1,4,'Puriscal'),
('TARRAZÚ',@PAIS,1,5,'Tarrazu'),
('ASERRÍ',@PAIS,1,6,'Aserrí'),
('MORA',@PAIS,1,7,'Mora'),
('GOICOCHEA',@PAIS,1,8,'Goicoechea'),
('SANTA ANA',@PAIS,1,9,'Santa Ana'),
('ALAJUELITA',@PAIS,1,10,'Alajuelita'),
('VÁSQUEZ DE CORONADO',@PAIS,1,11,'Vazquez de Coronado'),
('ACOSTA',@PAIS,1,12,'Acosta'),
('TIBÁS',@PAIS,1,13,'Tibás'),
('MORAVIA',@PAIS,1,14,'Moravia'),
('MONTES DE OCA',@PAIS,1,15,'Montes de Oca'),
('TURRUBARES',@PAIS,1,16,'Turrubares'),
('DOTA',@PAIS,1,17,'Dota'),
('CURRIDABAT',@PAIS,1,18,'Curridabat'),
('PÉREZ ZELEDÓN',@PAIS,1,19,'Pérez Zeledón'),
('LEÓN CORTES',@PAIS,1,20,'León Cortes'),
('ALAJUELA',@PAIS,2,1,'Alajuela'),
('SAN RAMON',@PAIS,2,2,'San Ramon'),
('GRECIA',@PAIS,2,3,'Grecia'),
('SAN MATEO',@PAIS,2,4,'San Mateo'),
('ATENAS',@PAIS,2,5,'Atenas'),
('NARANJO',@PAIS,2,6,'Naranjo'),
('PALMARES',@PAIS,2,7,'Palmares'),
('POAS',@PAIS,2,8,'Poás'),
('OROTINA',@PAIS,2,9,'Orotina'),
('SAN CARLOS',@PAIS,2,10,'San Carlos'),
('ALFARO RUIZ',@PAIS,2,11,'Alfaro Ruiz'),
('VALVERDE VEGA',@PAIS,2,12,'Valverde Vega'),
('UPALA',@PAIS,2,13,'Upala'),
('LOS CHILES',@PAIS,2,14,'Los Chiles'),
('GUATUSO',@PAIS,2,15,'Guatuso'),
('RIO CUARTO',@PAIS,2,16,''),
('CARTAGO',@PAIS,3,1,'Cartago'),
('PARAISO',@PAIS,3,2,'Paraíso'),
('LA UNION',@PAIS,3,3,'La Unión'),
('JIMENEZ',@PAIS,3,4,'Jiménez'),
('TURRIALBA',@PAIS,3,5,'Turrialba'),
('ALVARADO',@PAIS,3,6,'Alvarado'),
('OREAMUNO',@PAIS,3,7,'Oreamuno'),
('EL GUARCO',@PAIS,3,8,'El Guarco'),
('HEREDIA',@PAIS,4,1,'Heredia'),
('BARVA',@PAIS,4,2,'Barva'),
('SANTO DOMINGO',@PAIS,4,3,'Santo Domingo'),
('SANTA BARBARA',@PAIS,4,4,'Santa Bárbara'),
('SAN RAFAEL',@PAIS,4,5,'San Rafael'),
('SAN ISIDRO',@PAIS,4,6,'San Isidro'),
('BELEN',@PAIS,4,7,'Belén'),
('FLORES',@PAIS,4,8,'San Joaquín de Flores'),
('SAN PABLO',@PAIS,4,9,'San Pablo'),
('SARAPIQUI',@PAIS,4,10,'Sarapiquí'),
('LIBERIA',@PAIS,5,1,'Liberia'),
('NICOYA',@PAIS,5,2,'Nicoya'),
('SANTA CRUZ',@PAIS,5,3,'Santa Cruz'),
('BAGACES',@PAIS,5,4,'Bagaces'),
('CARRILLO',@PAIS,5,5,'Carrillo'),
('CAÑAS',@PAIS,5,6,'Cañas'),
('ABANGARES',@PAIS,5,7,'Abangares'),
('TILARAN',@PAIS,5,8,'Tilarán'),
('NANDAYURE',@PAIS,5,9,'Nandayure'),
('LA CRUZ',@PAIS,5,10,'La Cruz'),
('HOJANCHA',@PAIS,5,11,'Hojancha'),
('PUNTARENAS',@PAIS,6,1,'Puntarenas'),
('ESPARZA',@PAIS,6,2,'Esparza'),
('BUENOS AIRES',@PAIS,6,3,'Buenos Aires'),
('MONTES DE ORO',@PAIS,6,4,'Montes de Oro'),
('OSA',@PAIS,6,5,'Osa'),
('AGUIRRE',@PAIS,6,6,'Aguirre'),
('GOLFITO',@PAIS,6,7,'Golfito'),
('COTO BRUS',@PAIS,6,8,'Coto Brus'),
('PARRITA',@PAIS,6,9,'Parrita'),
('CORREDORES',@PAIS,6,10,'Corredores'),
('GARABITO',@PAIS,6,11,'Garabito'),
('LIMON',@PAIS,7,1,'Limón'),
('POCOCI',@PAIS,7,2,'Pococí'),
('SIQUIRRES',@PAIS,7,3,'Siquirres'),
('TALAMANCA',@PAIS,7,4,'Talamanca'),
('MATINA',@PAIS,7,5,'Matina'),
('GUACIMO',@PAIS,7,6,'Guácimo')

INSERT INTO @TABLA_DISTRITOS VALUES
('CARMEN',@PAIS,1,1,1,'Carmen'),
('MERCED',@PAIS,1,1,2,'Merced'),
('HOSPITAL',@PAIS,1,1,3,'Hospital'),
('CATEDRAL',@PAIS,1,1,4,'Catedral'),
('ZAPOTE',@PAIS,1,1,5,'Zapote'),
('SAN FRANCISCO DE DOS RÍOS',@PAIS,1,1,6,'San Francisco de Dos Ríos'),
('URUCA',@PAIS,1,1,7,'Uruca'),
('MATA REDONDA',@PAIS,1,1,8,'Mata Redonda'),
('PAVAS',@PAIS,1,1,9,'Pavas'),
('HATILLO',@PAIS,1,1,10,'Hatillo'),
('SAN SEBÁSTIAN',@PAIS,1,1,11,'San Sebastián'),
('ESCAZÚ',@PAIS,1,2,1,'Escazú'),
('SAN ANTONIO',@PAIS,1,2,2,'San Antonio'),
('SAN RAFAEL',@PAIS,1,2,3,'San Rafael'),
('DESAMPARADOS',@PAIS,1,3,1,'Desamparados'),
('SAN MIGUEL',@PAIS,1,3,2,'San Miguel'),
('SAN JUAN DE DIOS',@PAIS,1,3,3,'San Juan de Dios'),
('SAN RAFAÉL ARRIBA',@PAIS,1,3,4,'San Rafael Arriba'),
('SAN ANTONIO',@PAIS,1,3,5,'San Antonio'),
('FRAILES',@PAIS,1,3,6,'Frailes'),
('PATARRA',@PAIS,1,3,7,'Patarra'),
('SAN CRISTÓBAL',@PAIS,1,3,8,'San Cristobal'),
('ROSARIO',@PAIS,1,3,9,'Rosario'),
('DAMAS',@PAIS,1,3,10,'Damas'),
('SAN RAFAÉL ABAJO',@PAIS,1,3,11,'San Rafael Abajo'),
('GRAVILIAS',@PAIS,1,3,12,'Gravilias'),
('LOS GUIDO',@PAIS,1,3,13,'Los Guido'),
('SANTIAGO',@PAIS,1,4,1,'Santiago'),
('MERCEDES SUR',@PAIS,1,4,2,'Mercedes Sur'),
('BARBACOAS',@PAIS,1,4,3,'Barbacoas'),
('GRIFO ALTO',@PAIS,1,4,4,'Grifo Alto'),
('SAN RAFAÉL',@PAIS,1,4,5,'San Rafael'),
('CANDELARITA',@PAIS,1,4,6,'Candelaria'),
('DESAMPARADITOS',@PAIS,1,4,7,'Desamparaditos'),
('SAN ANTONIO',@PAIS,1,4,8,'San Antonio'),
('CHIRES',@PAIS,1,4,9,'Chires'),
('SAN MARCOS',@PAIS,1,5,1,'San Marcos'),
('SAN LORENZO',@PAIS,1,5,2,'San Lorenzo'),
('SAN CARLOS',@PAIS,1,5,3,'San Carlos'),
('ASERRÍ',@PAIS,1,6,1,'Aserrí'),
('TARBACA',@PAIS,1,6,2,'Tarbaca'),
('VUELTA DEL JORCO',@PAIS,1,6,3,'Vuelta de Jorco'),
('SAN GABRIEL',@PAIS,1,6,4,'San Gabriel'),
('LEGUA',@PAIS,1,6,5,'Legua'),
('MONTERREY',@PAIS,1,6,6,'Monterrey'),
('SALITRILLOS',@PAIS,1,6,7,'Salitrillos'),
('COLÓN',@PAIS,1,7,1,'Colón'),
('GUAYABO',@PAIS,1,7,2,'Guayabo'),
('TABARCIA',@PAIS,1,7,3,'Tabarcia'),
('PIEDRAS NEGRAS',@PAIS,1,7,4,'Piedras Negras'),
('PICAGRES',@PAIS,1,7,5,'Picagres'),
('JARIS',@PAIS,1,7,6,'Jaris'),
('QUITIRRISI',@PAIS,1,7,7,'Quitirrisi'),
('GUADALUPE',@PAIS,1,8,1,'Guadalupe'),
('SAN FRANCISCO',@PAIS,1,8,2,'San Francisco'),
('CALLE BLANCOS',@PAIS,1,8,3,'Calle Blancos'),
('MATA DE PLATANO',@PAIS,1,8,4,'Mata de Plátano'),
('IPIS',@PAIS,1,8,5,'Ipís'),
('RANCHO REDONDO',@PAIS,1,8,6,'Rancho Redondo'),
('PURRAL',@PAIS,1,8,7,'Purral'),
('SANTA ANA',@PAIS,1,9,1,'Santa Ana'),
('SALITRAL',@PAIS,1,9,2,'Salitral'),
('POZOS',@PAIS,1,9,3,'Pozos'),
('URUCA',@PAIS,1,9,4,'Uruca'),
('PIEDADES',@PAIS,1,9,5,'Piedades'),
('BRASIL',@PAIS,1,9,6,'Brasil'),
('ALAJUELITA',@PAIS,1,10,1,'Alajuelita'),
('SAN JOSECITO',@PAIS,1,10,2,'San Josecito'),
('SAN ANTONIO',@PAIS,1,10,3,'San Antonio'),
('CONCEPCIÓN',@PAIS,1,10,4,'Concepción'),
('SAN FELIPE',@PAIS,1,10,5,'San Felipe'),
('SAN ISIDRO',@PAIS,1,11,1,'San Isidro'),
('SAN RAFAÉL',@PAIS,1,11,2,'San Rafael'),
('DULCE NOMBRE DE JESÚS',@PAIS,1,11,3,'Dulce Nombre de Jesús'),
('PATALILLO',@PAIS,1,11,4,'Patalillo'),
('CASCAJAL',@PAIS,1,11,5,'Cascajal'),
('SAN IGNACIO',@PAIS,1,12,1,'San Ignacio de Acosta'),
('GUAITÍL',@PAIS,1,12,2,'Guaitil'),
('PALMICHAL',@PAIS,1,12,3,'Palmichal'),
('CANGREJAL',@PAIS,1,12,4,'Cangrejal'),
('SABANILLAS',@PAIS,1,12,5,'Sabanillas'),
('SAN JUAN',@PAIS,1,13,1,'San Juan'),
('CINCO ESQUINAS',@PAIS,1,13,2,'Cinco esquinas'),
('ANSELMO LLORENTE',@PAIS,1,13,3,'Anselmo Llorente'),
('LEÓN XIII',@PAIS,1,13,4,'Leon XIII'),
('COLIMA',@PAIS,1,13,5,'Colima'),
('SAN VICENTE',@PAIS,1,14,1,'San Vicente'),
('SAN JERÓNIMO',@PAIS,1,14,2,'San Jerónimo'),
('TRINIDAD',@PAIS,1,14,3,'Trinidad'),
('SAN PEDRO',@PAIS,1,15,1,'San Pedro'),
('SABANILLA',@PAIS,1,15,2,'Sabanilla'),
('MERCEDES',@PAIS,1,15,3,'Mercedes'),
('SAN RAFAÉL',@PAIS,1,15,4,'San Rafael'),
('SAN PABLO',@PAIS,1,16,1,'San Pablo'),
('SAN PEDRO',@PAIS,1,16,2,'San Pedro'),
('SAN JUAN DE MATA',@PAIS,1,16,3,'San Juan de Mata'),
('SAN LUIS',@PAIS,1,16,4,'San Luis'),
('CARARA',@PAIS,1,16,5,'Carara'),
('SANTA MARÍA',@PAIS,1,17,1,'Santa María'),
('JARDÍN',@PAIS,1,17,2,'Jardín'),
('COPEY',@PAIS,1,17,3,'Copey'),
('CURRIDABAT',@PAIS,1,18,1,'Curridabat'),
('GRANADILLA',@PAIS,1,18,2,'Granadilla'),
('SÁNCHEZ',@PAIS,1,18,3,'Sánchez'),
('TIRRASES',@PAIS,1,18,4,'Tirrases'),
('SAN ISIDRO DE EL GENERAL',@PAIS,1,19,1,'San Isidro del General'),
('GENERAL',@PAIS,1,19,2,'General'),
('DANIEL FLORES',@PAIS,1,19,3,'Daniel Flores'),
('RIVAS',@PAIS,1,19,4,'Rivas'),
('SAN PEDRO',@PAIS,1,19,5,'San Pedro'),
('PLATANARES',@PAIS,1,19,6,'Platanares'),
('PEJIBAYE',@PAIS,1,19,7,'Pejibaye'),
('CAJÓN',@PAIS,1,19,8,'Cajón'),
('BARÚ',@PAIS,1,19,9,'Barú'),
('RÍO NUEVO',@PAIS,1,19,10,'Río Nuevo'),
('PÁRAMO',@PAIS,1,19,11,'Páramo'),
('LA AMISTAD',@PAIS,1,19,12,'La Amistad'),
('SAN PABLO',@PAIS,1,20,1,'San Pablo'),
('SAN ANDRÉS',@PAIS,1,20,2,'San Andrés'),
('LLANO BONITO',@PAIS,1,20,3,'Llano Bonito'),
('SAN ISIDRO',@PAIS,1,20,4,'San Isidro'),
('SANTA CRUZ',@PAIS,1,20,5,'Santa Cruz'),
('SAN ANTONIO',@PAIS,1,20,6,'San Antonio'),
('ALAJUELA',@PAIS,2,1,1,'Alajuela'),
('SAN JOSE',@PAIS,2,1,2,'San José'),
('CARRIZAL',@PAIS,2,1,3,'Carrizal'),
('SAN ANTONIO',@PAIS,2,1,4,'San Antonio'),
('GUACIMA',@PAIS,2,1,5,'Guácima'),
('SAN ISIDRO',@PAIS,2,1,6,'San Isidro'),
('SABANILLA',@PAIS,2,1,7,'Sabanilla'),
('SAN RAFAEL',@PAIS,2,1,8,'San Rafael'),
('RIO SEGUNDO',@PAIS,2,1,9,'Río Segundo'),
('DESAMPARADOS',@PAIS,2,1,10,'Desamparados'),
('TURRUCARES',@PAIS,2,1,11,'Turrúcares'),
('TAMBOR',@PAIS,2,1,12,'Tambor'),
('GARITA',@PAIS,2,1,13,'Garita'),
('SARAPIQUÍ',@PAIS,2,1,14,'Sarapiquí'),
('SAN RAMON',@PAIS,2,2,1,'San Ramón'),
('SANTIAGO',@PAIS,2,2,2,'Santiago'),
('SAN JUAN',@PAIS,2,2,3,'San Juan'),
('PIEDADES NORTE',@PAIS,2,2,4,'Piedades Norte'),
('PIEDADES SUR',@PAIS,2,2,5,'Piedades Sur'),
('SAN RAFAEL',@PAIS,2,2,6,'San Rafael'),
('SAN ISIDRO',@PAIS,2,2,7,'San Isidro'),
('ANGELES',@PAIS,2,2,8,'Ángeles'),
('ALFARO',@PAIS,2,2,9,'Alfaro'),
('VOLIO',@PAIS,2,2,10,'Volio'),
('CONCEPCION',@PAIS,2,2,11,'Concepción'),
('ZAPOTAL',@PAIS,2,2,12,'Zapotal'),
('SAN ISIDRO DE PEÑAS BLANCAS',@PAIS,2,2,13,'San Isidro'),
('SAN LORENZO',@PAIS,2,2,14,'San Lorenzo'),
('GRECIA',@PAIS,2,3,1,'Grecia'),
('SAN ISIDRO',@PAIS,2,3,2,'San Isidro'),
('SAN JOSE',@PAIS,2,3,3,'San Jose'),
('SAN ROQUE',@PAIS,2,3,4,'San Roque'),
('TACARES',@PAIS,2,3,5,'Tacares'),
('PUENTE DE PIEDRA',@PAIS,2,3,6,'Puente de Piedra'),
('BOLIVAR',@PAIS,2,3,7,'Bolivar'),
('SAN MATEO',@PAIS,2,4,1,'San Mateo'),
('DESMONTE',@PAIS,2,4,2,'Desmonte'),
('JESUS MARIA',@PAIS,2,4,3,'Jesús María'),
('LABRADOR',@PAIS,2,4,4,'Labrador'),
('ATENAS',@PAIS,2,5,1,'Atenas'),
('JESUS',@PAIS,2,5,2,'Jesús'),
('MERCEDES',@PAIS,2,5,3,'Mercedes'),
('SAN ISIDRO',@PAIS,2,5,4,'San Isidro'),
('CONCEPCION',@PAIS,2,5,5,'Concepción'),
('SAN JOSE',@PAIS,2,5,6,'San José'),
('SANTA EULALIA',@PAIS,2,5,7,'Santa Eulalia'),
('ESCOBAL',@PAIS,2,5,8,'Escobal'),
('NARANJO',@PAIS,2,6,1,'Naranjo'),
('SAN MIGUEL',@PAIS,2,6,2,'San Miguel'),
('SAN JOSE',@PAIS,2,6,3,'San José'),
('CIRRI SUR',@PAIS,2,6,4,'Cirrí Sur'),
('SAN JERONIMO',@PAIS,2,6,5,'San Jerónimo'),
('SAN JUAN',@PAIS,2,6,6,'San Juan'),
('ROSARIO',@PAIS,2,6,7,'Rosario'),
('PALMITOS',@PAIS,2,6,8,'Palmitos'),
('PALMARES',@PAIS,2,7,1,'Palmares'),
('ZARAGOZA',@PAIS,2,7,2,'Zaragoza'),
('BUENOS AIRES',@PAIS,2,7,3,'Buenos Aires'),
('SANTIAGO',@PAIS,2,7,4,'Santiago'),
('CANDELARIA',@PAIS,2,7,5,'Candelaria'),
('ESQUIPULAS',@PAIS,2,7,6,'Esquipulas'),
('GRANJA',@PAIS,2,7,7,'Granja'),
('SAN PEDRO',@PAIS,2,8,1,'San Pedro'),
('SAN JUAN',@PAIS,2,8,2,'San Juan'),
('SAN RAFAEL',@PAIS,2,8,3,'San Rafael'),
('CARRILLOS',@PAIS,2,8,4,'Carrillos'),
('SABANA REDONDA',@PAIS,2,8,5,'Sabana Redonda'),
('OROTINA',@PAIS,2,9,1,'Orotina'),
('MASTATE',@PAIS,2,9,2,'Mastate'),
('HACIENDA VIEJA',@PAIS,2,9,3,'Hacienda Vieja'),
('COYOLAR',@PAIS,2,9,4,'Coyolar'),
('CEIBA',@PAIS,2,9,5,'Ceiba'),
('QUESADA',@PAIS,2,10,1,'Quesada'),
('FLORENCIA',@PAIS,2,10,2,'Florencia'),
('BUENAVISTA',@PAIS,2,10,3,'Buenavista'),
('AGUAS ZARCAS',@PAIS,2,10,4,'Aguas Zarcas'),
('VENECIA',@PAIS,2,10,5,'Venecia'),
('PITAL',@PAIS,2,10,6,'Pital'),
('FORTUNA',@PAIS,2,10,7,'Fortuna'),
('TIGRA',@PAIS,2,10,8,'Tigra'),
('PALMERA',@PAIS,2,10,9,'Palmera'),
('VENADO',@PAIS,2,10,10,'Venado'),
('CUTRIS',@PAIS,2,10,11,'Cutris'),
('MONTERREY',@PAIS,2,10,12,'Monterrey'),
('POCOSOL',@PAIS,2,10,13,'Pocosol'),
('ZARCERO',@PAIS,2,11,1,'Zarcero'),
('LAGUNA',@PAIS,2,11,2,'Laguna'),
('TAPEZCO',@PAIS,2,11,3,'Tapezco'),
('GUADALUPE',@PAIS,2,11,4,'Guadalupe'),
('PALMIRA',@PAIS,2,11,5,'Palmira'),
('ZAPOTE',@PAIS,2,11,6,'Zapote'),
('BRISAS',@PAIS,2,11,7,'Brisas'),
('SARCHI NORTE',@PAIS,2,12,1,'Sarchi Norte'),
('SARCHI SUR',@PAIS,2,12,2,'Sarchi Sur'),
('TORO AMARILLO',@PAIS,2,12,3,'Toro Amarillo'),
('SAN PEDRO',@PAIS,2,12,4,'San Pedro'),
('RODRIGUEZ',@PAIS,2,12,5,'Rodriguez'),
('UPALA',@PAIS,2,13,1,'Upala'),
('AGUAS CLARAS',@PAIS,2,13,2,'Aguas Claras'),
('SAN JOSE O PIZOTE',@PAIS,2,13,3,'San Jose (Pizote)'),
('BIJAGUA',@PAIS,2,13,4,'Bijagua'),
('DELICIAS',@PAIS,2,13,5,'Delicias'),
('DOS RIOS',@PAIS,2,13,6,'Dos Rios'),
('YOLILLAL',@PAIS,2,13,7,'Yoliyllal'),
('CANALETE',@PAIS,2,13,8,'Canalete'),
('LOS CHILES',@PAIS,2,14,1,'Los Chiles'),
('CAÑO NEGRO',@PAIS,2,14,2,'Caño Negro'),
('EL AMPARO',@PAIS,2,14,3,'El Amparo'),
('SAN JORGE',@PAIS,2,14,4,'San Jorge'),
('SAN RAFAEL',@PAIS,2,15,1,'San Rafael'),
('BUENA VISTA',@PAIS,2,15,2,'Buenavista'),
('COTE',@PAIS,2,15,3,'Cote'),
('KATIRA',@PAIS,2,15,4,'Katira'),
('RIO CUARTO',@PAIS,2,16,1,'Rio Cuarto'),
('SANTA RITA',@PAIS,2,16,2,'Santa Rita'),
('SANTA ISABEL',@PAIS,2,16,3,'Santa Isabel'),
('ORIENTAL (CIUDAD DE CARTAGO PARTE)',@PAIS,3,1,1,'Oriental'),
('OCCIDENTAL (CIUDAD DE CARTAGO PARTE)',@PAIS,3,1,2,'Occidental'),
('CARMEN',@PAIS,3,1,3,'Carmen'),
('SAN NICOLAS',@PAIS,3,1,4,'San Nicolas'),
('AGUACALIENTE O SAN FRANCISCO',@PAIS,3,1,5,'Aguacaliente (San Francisco)'),
('GUADALUPE',@PAIS,3,1,6,'Guadalupe (Arenilla)'),
('CORRALILLO',@PAIS,3,1,7,'Corralillo'),
('TIERRA BLANCA',@PAIS,3,1,8,'Tierra Blanca'),
('DULCE NOMBRE',@PAIS,3,1,9,'Dulce Nombre'),
('LLANO GRANDE',@PAIS,3,1,10,'Llano Grande'),
('QUEBRADILLA',@PAIS,3,1,11,'Quebradilla'),
('PARAISO',@PAIS,3,2,1,'Paraiso'),
('SANTIAGO',@PAIS,3,2,2,'Santiago'),
('OROSI',@PAIS,3,2,3,'Orosi'),
('CACHI',@PAIS,3,2,4,'Cachi'),
('LLANOS DE SANTA LUCIA',@PAIS,3,2,5,'Llanos de Santa Lucia'),
('TRES RIOS',@PAIS,3,3,1,'Tres Rios'),
('SAN DIEGO',@PAIS,3,3,2,'San Diego'),
('SAN JUAN',@PAIS,3,3,3,'San Juan'),
('SAN RAFAEL',@PAIS,3,3,4,'San Rafael'),
('CONCEPCION',@PAIS,3,3,5,'Concepcion'),
('DULCE NOMBRE',@PAIS,3,3,6,'Dulce Nombre'),
('SAN RAMON',@PAIS,3,3,7,'San Ramon'),
('RIO AZUL',@PAIS,3,3,8,'Rio Azul'),
('JUAN VIÑAS',@PAIS,3,4,1,'Juan Viñas'),
('TUCURRIQUE',@PAIS,3,4,2,'Tucurrique'),
('PEJIBAYE',@PAIS,3,4,3,'Pejibaye'),
('TURRIALBA',@PAIS,3,5,1,'Turrialba'),
('LA SUIZA',@PAIS,3,5,2,'La Suiza'),
('PERALTA',@PAIS,3,5,3,'Peralta'),
('SANTA CRUZ',@PAIS,3,5,4,'Santa Cruz'),
('SANTA TERESITA',@PAIS,3,5,5,'Santa Teresita'),
('PAVONES',@PAIS,3,5,6,'Pavones'),
('TUIS',@PAIS,3,5,7,'Tuis'),
('TAYUTIC',@PAIS,3,5,8,'Tayutic'),
('SANTA ROSA',@PAIS,3,5,9,'Santa Rosa'),
('TRES EQUIS',@PAIS,3,5,10,'Tres Equis'),
('ISABEL',@PAIS,3,5,11,'La Isabel'),
('CHIRRIPO',@PAIS,3,5,12,'Chirripo'),
('PACAYAS',@PAIS,3,6,1,'Pacayas'),
('CERVANTES',@PAIS,3,6,2,'Cervantes'),
('CAPELLADES',@PAIS,3,6,3,'Capellades'),
('SAN RAFAEL',@PAIS,3,7,1,'San Rafael'),
('COT',@PAIS,3,7,2,'Cote'),
('POTRERO CERRADO',@PAIS,3,7,3,'Potrero Cerrado'),
('CIPRESES',@PAIS,3,7,4,'Cipreses'),
('SANTA ROSA',@PAIS,3,7,5,'Santa Rosa'),
('TEJAR',@PAIS,3,8,1,'Tejar'),
('SAN ISIDRO',@PAIS,3,8,2,'San Isidro'),
('TOBOSI',@PAIS,3,8,3,'Tobosi'),
('PATIO DE AGUA',@PAIS,3,8,4,'Patio de Agua'),
('HEREDIA',@PAIS,4,1,1,'Heredia'),
('MERCEDES',@PAIS,4,1,2,'Mercedes'),
('SAN FRANCISCO',@PAIS,4,1,3,'San Francisco'),
('ULLOA O BARREAL',@PAIS,4,1,4,'Ulloa'),
('VARA BLANCA',@PAIS,4,1,5,'Varablanca'),
('BARVA',@PAIS,4,2,1,'Barva'),
('SAN PEDRO',@PAIS,4,2,2,'San Pedro'),
('SAN PABLO',@PAIS,4,2,3,'San Pablo'),
('SAN ROQUE',@PAIS,4,2,4,'San Roque'),
('SANTA LUCIA',@PAIS,4,2,5,'Santa Lucia'),
('SAN JOSE DE LA MONTAÑA',@PAIS,4,2,6,'San Jose de la Montaña'),
('SANTO DOMINGO',@PAIS,4,3,1,'Santo Domingo'),
('SAN VICENTE',@PAIS,4,3,2,'San Vicente'),
('SAN MIGUEL',@PAIS,4,3,3,'San Miguel'),
('PARACITO',@PAIS,4,3,4,'Paracito'),
('SANTO TOMAS',@PAIS,4,3,5,'Santo Tomas'),
('SANTA ROSA',@PAIS,4,3,6,'Santa Rosa'),
('TURES',@PAIS,4,3,7,'Tures'),
('PARA',@PAIS,4,3,8,'Para'),
('SANTA BARBARA',@PAIS,4,4,1,'Santa Barbara'),
('SAN PEDRO',@PAIS,4,4,2,'San Pedro'),
('SAN JUAN',@PAIS,4,4,3,'San Juan'),
('JESUS',@PAIS,4,4,4,'Jesus'),
('SANTO DOMINGO',@PAIS,4,4,5,'Santo Domingo'),
('PURABA',@PAIS,4,4,6,'Puraba'),
('SAN RAFAEL',@PAIS,4,5,1,'San Rafael'),
('SAN JOSECITO',@PAIS,4,5,2,'San Josecito'),
('SANTIAGO',@PAIS,4,5,3,'Santiago'),
('ANGELES',@PAIS,4,5,4,'angeles'),
('CONCEPCION',@PAIS,4,5,5,'Concepcion'),
('SAN ISIDRO',@PAIS,4,6,1,'San Isidro'),
('SAN JOSE',@PAIS,4,6,2,'San Jose'),
('CONCEPCION',@PAIS,4,6,3,'Concepcion'),
('SAN FRANCISO',@PAIS,4,6,4,'San Francisco'),
('SAN ANTONIO',@PAIS,4,7,1,'San Antonio'),
('LA RIBERA',@PAIS,4,7,2,'Ribera'),
('ASUNCION',@PAIS,4,7,3,'Asuncion'),
('SAN JOAQUIN',@PAIS,4,8,1,'San Joaquin de Flores'),
('BARRANTES',@PAIS,4,8,2,'Barrantes'),
('LLORENTE',@PAIS,4,8,3,'Llorente'),
('SAN PABLO',@PAIS,4,9,1,'San Pablo'),
('RINCON DE SABANILLA',@PAIS,4,9,2,'Rincon de Sabanilla'),
('PUERTO VIEJO',@PAIS,4,10,1,'Puerto Viejo'),
('LA VIRGEN',@PAIS,4,10,2,'La Virgen'),
('HORQUETAS',@PAIS,4,10,3,'Horquetas'),
('LLANURAS DEL GASPAR',@PAIS,4,10,4,'Llanuras del Gaspar'),
('CUREÑA',@PAIS,4,10,5,'Cureña'),
('LIBERIA',@PAIS,5,1,1,'Liberia'),
('CAÑAS DULCES',@PAIS,5,1,2,'Cañas Dulces'),
('MAYORGA',@PAIS,5,1,3,'Mayorga'),
('NACASCOLO',@PAIS,5,1,4,'Nacascolo'),
('CURUBANDE',@PAIS,5,1,5,'Curubande'),
('NICOYA',@PAIS,5,2,1,'Nicoya'),
('MANSION',@PAIS,5,2,2,'Mansion'),
('SAN ANTONIO',@PAIS,5,2,3,'San Antonio'),
('QUEBRADA HONDA',@PAIS,5,2,4,'Quebrada Honda'),
('SAMARA',@PAIS,5,2,5,'Samara'),
('NOSARA',@PAIS,5,2,6,'Nosara'),
('BELEN DE NOSARITA',@PAIS,5,2,7,'Belen de Nosarita'),
('SANTA CRUZ',@PAIS,5,3,1,'Santa Cruz'),
('BOLSON',@PAIS,5,3,2,'Bolson'),
('VEINTISIETE DE ABRIL',@PAIS,5,3,3,'Veintisiete de Abril'),
('TEMPATE',@PAIS,5,3,4,'Tempate'),
('CARTAGENA',@PAIS,5,3,5,'Cartagena'),
('CUAJINIQUIL',@PAIS,5,3,6,'Cuajiniquil'),
('DIRIA',@PAIS,5,3,7,'Diria'),
('CABO VELAS',@PAIS,5,3,8,'Cabo Velas'),
('TAMARINDO',@PAIS,5,3,9,'Tamarindo'),
('BAGACES',@PAIS,5,4,1,'Bagaces'),
('LA FORTUNA',@PAIS,5,4,2,'Fortuna'),
('MOGOTE',@PAIS,5,4,3,'Mogote'),
('RIO NARANJO',@PAIS,5,4,4,'Rio Naranjo'),
('FILADELFIA',@PAIS,5,5,1,'Filadelfia'),
('PALMIRA',@PAIS,5,5,2,'Palmira'),
('SARDINAL',@PAIS,5,5,3,'Sardinal'),
('BELEN',@PAIS,5,5,4,'Belen'),
('CAÑAS',@PAIS,5,6,1,'Cañas'),
('PALMIRA',@PAIS,5,6,2,'Palmira'),
('SAN MIGUEL',@PAIS,5,6,3,'San Miguel'),
('BEBEDERO',@PAIS,5,6,4,'Bebedero'),
('POROZAL',@PAIS,5,6,5,'Porozal'),
('LAS JUNTAS',@PAIS,5,7,1,'Juntas'),
('SIERRA',@PAIS,5,7,2,'Sierra'),
('SAN JUAN',@PAIS,5,7,3,'San Juan'),
('COLORADO',@PAIS,5,7,4,'Colorado'),
('TILARAN',@PAIS,5,8,1,'Tilaran'),
('QUEBRADA GRANDE',@PAIS,5,8,2,'Quebrada Grande'),
('TRONADORA',@PAIS,5,8,3,'Tronadora'),
('SANTA ROSA',@PAIS,5,8,4,'Santa Rosa'),
('LIBANO',@PAIS,5,8,5,'Libano'),
('TIERRAS MORENAS',@PAIS,5,8,6,'Tierras Morenas'),
('ARENAL',@PAIS,5,8,7,'Arenal'),
('CABECERAS',@PAIS,5,8,8,'Cabeceras'),
('CARMONA',@PAIS,5,9,1,'Carmona'),
('SANTA RITA',@PAIS,5,9,2,'Santa Rita'),
('ZAPOTAL',@PAIS,5,9,3,'Zapotal'),
('SAN PABLO',@PAIS,5,9,4,'San Pablo'),
('PROVENIR',@PAIS,5,9,5,'Porvenir'),
('BEJUCO',@PAIS,5,9,6,'Bejuco'),
('LA CRUZ',@PAIS,5,10,1,'La Cruz'),
('SANTA CECILIA',@PAIS,5,10,2,'Santa Cecilia'),
('LA GARITA',@PAIS,5,10,3,'Garita'),
('SANTA ELENA',@PAIS,5,10,4,'Santa Elena'),
('HOJANCHA',@PAIS,5,11,1,'Hojancha'),
('MONTE ROMO',@PAIS,5,11,2,'Monte Romo'),
('PUERTO CARRILLO',@PAIS,5,11,3,'Puerto Carrillo'),
('HUACAS',@PAIS,5,11,4,'Huacas'),
('MATAMBÚ',@PAIS,5,11,5,'Matambú'),
('PUNTARENAS',@PAIS,6,1,1,'Puntarenas'),
('PITAHAYA',@PAIS,6,1,2,'Pitahaya'),
('CHOMES',@PAIS,6,1,3,'Chomes'),
('LEPANTO',@PAIS,6,1,4,'Lepanto'),
('PAQUERA',@PAIS,6,1,5,'Paquera'),
('MANZANILLO',@PAIS,6,1,6,'Manzanillo'),
('GUACIMAL',@PAIS,6,1,7,'Guacimal'),
('BARRANCA',@PAIS,6,1,8,'Barranca'),
('MONTE VERDE',@PAIS,6,1,9,'Monte Verde'),
('ISLA DEL COCO',@PAIS,6,1,10,'Isla del Coco'),
('COBANO',@PAIS,6,1,11,'Cobano'),
('CHACARITA',@PAIS,6,1,12,'Chacarita'),
('CHIRA',@PAIS,6,1,13,'Chira'),
('ACAPULCO',@PAIS,6,1,14,'Acapulco'),
('EL ROBLE',@PAIS,6,1,15,'El Roble'),
('ARANCIBIA',@PAIS,6,1,16,'Arancibia'),
('ESPIRITU SANTO',@PAIS,6,2,1,'Espiritu Santo'),
('SAN JUAN GRANDE',@PAIS,6,2,2,'San Juan Grande'),
('MACACONA',@PAIS,6,2,3,'Macacona'),
('SAN RAFAEL',@PAIS,6,2,4,'San Rafael'),
('SAN JERONIMO',@PAIS,6,2,5,'San Jeronimo'),
('CALDERA',@PAIS,6,2,6,'Caldera'),
('BUENOS AIRES',@PAIS,6,3,1,'Buenos Aires'),
('VOLCAN',@PAIS,6,3,2,'Volcan'),
('POTRERO GRANDE',@PAIS,6,3,3,'Potrero Grande'),
('BORUCA',@PAIS,6,3,4,'Boruca'),
('PILAS',@PAIS,6,3,5,'Pilas'),
('COLINAS',@PAIS,6,3,6,'Colinas'),
('CHANGUENA',@PAIS,6,3,7,'Changena'),
('BIOLLEY',@PAIS,6,3,8,'Briolley'),
('BRUNKA',@PAIS,6,3,9,'Brunka'),
('MIRAMAR',@PAIS,6,4,1,'Miramar'),
('LA UNIÓN ',@PAIS,6,4,2,'Union'),
('SAN ISIDRO',@PAIS,6,4,3,'San Isidro'),
('PUERTO CORTES',@PAIS,6,5,1,'Puerto Cortes'),
('PALMAR',@PAIS,6,5,2,'Palmar'),
('SIERPE',@PAIS,6,5,3,'Sierpe'),
('BAHIA BALLENA',@PAIS,6,5,4,'Bahia Ballena'),
('PIEDRAS BLANCAS',@PAIS,6,5,5,'Piedras Blancas'),
('BAHÍA DRAKE',@PAIS,6,5,6,'Bahía Drake'),
('QUEPOS',@PAIS,6,6,1,'Quepos'),
('SAVEGRE',@PAIS,6,6,2,'Savegre'),
('NARANJITO',@PAIS,6,6,3,'Naranjito'),
('GOLFITO',@PAIS,6,7,1,'Golfito'),
('PUERTO JIMENEZ',@PAIS,6,7,2,'Puerto Jimenez'),
('GUAYCARA',@PAIS,6,7,3,'Guaycara'),
('PAVON',@PAIS,6,7,4,'Pavon'),
('SAN VITO',@PAIS,6,8,1,'San Vito'),
('SABALITO',@PAIS,6,8,2,'Sabalito'),
('AGUABUENA',@PAIS,6,8,3,'Aguabuena'),
('LIMONCITO',@PAIS,6,8,4,'Limoncito'),
('PITTIER',@PAIS,6,8,5,'Pittier'),
('GUTIERREZ BRAUN',@PAIS,6,8,6,'Gutierrez Braun'),
('PARRITA',@PAIS,6,9,1,'Parrita'),
('CORREDOR',@PAIS,6,10,1,'Corredor'),
('LA CUESTA',@PAIS,6,10,2,'La Cuesta'),
('CANOAS',@PAIS,6,10,3,'Canoas'),
('LAUREL',@PAIS,6,10,4,'Laurel'),
('JACO',@PAIS,6,11,1,'Jaco'),
('TARCOLES',@PAIS,6,11,2,'Tarcoles'),
('LIMON',@PAIS,7,1,1,'Limon'),
('VALLE LA ESTRELLA',@PAIS,7,1,2,'Valle La Estrella'),
('RIO BLANCO',@PAIS,7,1,3,'Rio Blanco'),
('MATAMA',@PAIS,7,1,4,'Matama'),
('GUAPILES',@PAIS,7,2,1,'Guapiles'),
('JIMENEZ',@PAIS,7,2,2,'Jimenez'),
('RITA',@PAIS,7,2,3,'Rita'),
('ROXANA',@PAIS,7,2,4,'Roxana'),
('CARIARI',@PAIS,7,2,5,'Cariari'),
('COLORADO',@PAIS,7,2,6,'Colorado'),
('LA COLONIA',@PAIS,7,2,7,'La Colonia'),
('SIQUIRRES',@PAIS,7,3,1,'Siquirres'),
('PACUARITO',@PAIS,7,3,2,'Pacuarito'),
('FLORIDA',@PAIS,7,3,3,'Florida'),
('GERMANIA',@PAIS,7,3,4,'Germania'),
('CAIRO',@PAIS,7,3,5,'Cairo'),
('ALEGRIA',@PAIS,7,3,6,'Alegria'),
('REVENTAZÓN',@PAIS,7,3,7,'Reventazón'),
('BRATSI',@PAIS,7,4,1,'Bratsi'),
('SIXAOLA',@PAIS,7,4,2,'Sixaola'),
('CAHUITA',@PAIS,7,4,3,'Cahuita'),
('TELIRE',@PAIS,7,4,4,'Telire'),
('MATINA',@PAIS,7,5,1,'Matina'),
('BATAAN',@PAIS,7,5,2,'Battan'),
('CARRANDI',@PAIS,7,5,3,'Carrandi'),
('GUACIMO',@PAIS,7,6,1,'Guacimo'),
('MERCEDES',@PAIS,7,6,2,'Mercedes'),
('POCORA',@PAIS,7,6,3,'Pocora'),
('RIO JIMENEZ',@PAIS,7,6,4,'Rio Jimenez'),
('DUACARÍ',@PAIS,7,6,5,'Duacari')

--- INGRESANDO PROVINCIAS, CANTONES Y DISTRITOS EN TABLAS DE SAINT ---
IF (ISNULL(LEN(@PAIS),0) > 0)
	BEGIN
		SET @CONTPROV = NULL;
		SET @LINEAP = 1;

		SELECT @CONTPROV = COUNT(*)
		FROM @TABLA_PROVINCIAS
		WHERE Pais = @PAIS;

		IF (ISNULL(LEN(@CONTPROV),0) > 0)
			BEGIN
				WHILE (@LINEAP <= @CONTPROV)
					BEGIN
						SET @PROVINCIAFE = NULL;
						SET @PROVINCIA = NULL;
						SET @DESCRIP = NULL;
						SET @ProvinciaTV = NULL;

						SELECT	@PROVINCIAFE = A.IDESTADOFE,
								@DESCRIP = A.Descrip,
								@ProvinciaTV = A.ProvinciaTV
						FROM
						(SELECT ROW_NUMBER () OVER (ORDER BY Pais, IDESTADOFE) AS Linea,
								IDESTADOFE,
								Descrip,
								ProvinciaTV
						FROM @TABLA_PROVINCIAS
						WHERE Pais = @PAIS) AS A
						WHERE A.Linea = @LINEAP;

						--- VALIDANDO SI PROVINCIA EXISTE EN TABLA DE SAINT---
						IF (SELECT Descrip FROM SAESTADO WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE) IS NULL
							BEGIN	
								INSERT INTO SAESTADO (Descrip, Pais, IDESTADOFE, ProvinciaTV) 
								VALUES (@DESCRIP,@PAIS,@PROVINCIAFE,@ProvinciaTV);
							END;
						ELSE
							BEGIN
								UPDATE SAESTADO SET Descrip = @DESCRIP, Pais= @PAIS, ProvinciaTV = @ProvinciaTV
								WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE;
							END;

						SELECT @PROVINCIA = Estado
						FROM SAESTADO WITH (NOLOCK)
						WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE;

						SET @LINEAC = 1;

						SELECT @CONTCANTON = COUNT(*)
						FROM @TABLA_CANTONES
						WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE;

						IF (ISNULL(LEN(@CONTCANTON),0) > 0)
							BEGIN
								WHILE (@LINEAC <= @CONTCANTON)
									BEGIN
										SET @CANTONFE = NULL;
										SET @CANTON = NULL;
										SET @DESCRIP = NULL;
										SET @CantonTV = NULL;

										SELECT	@CANTONFE = A.IDCIUDADFE,
												@DESCRIP = A.Descrip,
												@CantonTV = A.CantonTV 
										FROM
										(SELECT ROW_NUMBER () OVER (ORDER BY Pais,IDESTADOFE,IDCIUDADFE) AS Linea,
												IDCIUDADFE,
												Descrip,
												CantonTV
										FROM @TABLA_CANTONES
										WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE) AS A
										WHERE A.Linea = @LINEAC;

										IF (SELECT Descrip FROM SACIUDAD WHERE Pais = @PAIS AND Estado = @PROVINCIA AND IDCIUDADFE = @CANTONFE) IS NULL
											BEGIN
												INSERT INTO SACIUDAD (Descrip, Estado, Pais, IDCIUDADFE, CantonTV) VALUES (@DESCRIP,@PROVINCIA,@PAIS,@CANTONFE,@CantonTV);
											END;
										ELSE
											BEGIN
												UPDATE SACIUDAD SET Descrip = @DESCRIP, Pais= @PAIS, Estado = @PROVINCIA, CantonTV = @CantonTV
												WHERE Pais = @PAIS AND Estado = @PROVINCIA AND IDCIUDADFE = @CANTONFE;
											END;

										SELECT @CANTON = Ciudad
										FROM SACIUDAD WITH (NOLOCK)
										WHERE Pais = @PAIS AND Estado = @PROVINCIA AND IDCIUDADFE = @CANTONFE;

										SET @LINEAD = 1;

										SELECT @CONTDIST = COUNT(*)
										FROM @TABLA_DISTRITOS
										WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE AND IDCIUDADFE = @CANTONFE;

										IF (ISNULL(LEN(@CONTDIST),0) > 0)
											BEGIN
												WHILE (@LINEAD <= @CONTDIST)
													BEGIN
														SET @DISTRITOFE = NULL;
														SET @DISTRITO = NULL;
														SET @DESCRIP = NULL;
														SET @DistritoTV = NULL;

														SELECT	@DISTRITOFE = A.IDMUNICIPIOFE,
																@DESCRIP = A.Descrip,
																@DistritoTV = A.DistritoTV
														FROM
														(SELECT ROW_NUMBER () OVER (ORDER BY Pais,IDESTADOFE,IDCIUDADFE,IDMUNICIPIOFE) AS Linea,
																IDMUNICIPIOFE,
																Descrip,
																DistritoTV
														FROM @TABLA_DISTRITOS
														WHERE Pais = @PAIS AND IDESTADOFE = @PROVINCIAFE AND IDCIUDADFE = @CANTONFE) AS A
														WHERE A.Linea = @LINEAD;

														IF (SELECT Descrip FROM SAMUNICIPIO WHERE Pais = @PAIS AND Estado = @PROVINCIA AND Ciudad = @CANTON AND IDMUNICIPIOFE = @DISTRITOFE) IS NULL
															BEGIN
																INSERT INTO SAMUNICIPIO (Descrip, Ciudad, Estado, Pais, IDMUNICIPIOFE, DistritoTV) VALUES (@DESCRIP,@CANTON,@PROVINCIA,@PAIS,@DISTRITOFE,@DistritoTV);
															END;
														ELSE
															BEGIN
																UPDATE SAMUNICIPIO SET Descrip = @DESCRIP, Pais = @PAIS, Estado = @PROVINCIA, Ciudad = @CANTON, DistritoTV = @DistritoTV
																WHERE Pais = @PAIS AND Estado = @PROVINCIA AND Ciudad = @CANTON AND IDMUNICIPIOFE = @DISTRITOFE;
															END;

														SET @LINEAD +=1;
													END;
											END;
										SET @LINEAC +=1;
									END;		
							END;							
						SET @LINEAP +=1;
					END;
			END;
	END;

DELETE FROM @TABLA_PROVINCIAS;
DELETE FROM @TABLA_CANTONES;
DELETE FROM @TABLA_DISTRITOS;
GO
/*CARGANDO IMPUESTOS*/
IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA') IS NOT NULL
	BEGIN
		UPDATE SATAXES SET Descrip = 'NO USAR ESTE IMPUESTO', MtoTax= 0, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1 
		WHERE CodTaxs = 'IVA';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVI') IS NOT NULL
	BEGIN
		UPDATE SATAXES SET TipoIVA = 0
		WHERE CodTaxs = 'IVI';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA0E') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra)
		VALUES	('IVA0E','IVA Tarifa 0% (Exento)',0,1,1,1,2,1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Tarifa 0% (Exento)', MtoTax= 0, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1 
		WHERE CodTaxs = 'IVA0E';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA1R') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra, Orden)
		VALUES	('IVA1R','IVA Tarifa reducida 1%',1,1,1,1,2,1,1,1,2);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Tarifa reducida 1%', MtoTax= 1, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1, Orden = 2
		WHERE CodTaxs = 'IVA1R';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA2R') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra, Orden)
		VALUES	('IVA2R','IVA Tarifa reducida 2%',2,1,1,1,2,1,1,1,3);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Tarifa reducida 2%', MtoTax= 2, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1, Orden = 3
		WHERE CodTaxs = 'IVA2R';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA4R') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra, Orden)
		VALUES	('IVA4R','IVA Tarifa reducida 4%',4,1,1,1,2,1,1,1,4);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Tarifa reducida 4%', MtoTax= 4, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1, Orden = 4
		WHERE CodTaxs = 'IVA4R';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA0T') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra)
		VALUES	('IVA0T','IVA Transitorio 0%',0,1,1,1,2,1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Transitorio 0%', MtoTax= 0, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1
		WHERE CodTaxs = 'IVA0T';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA4T') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra, Orden)
		VALUES	('IVA4T','IVA Transitorio 4%',4,1,1,1,2,1,1,1,5);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Transitorio 4%', MtoTax= 4, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1, Orden = 5
		WHERE CodTaxs = 'IVA4T';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA8T') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra, Orden)
		VALUES	('IVA8T','IVA Transitorio 8%',8,1,1,1,2,1,1,1,6);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Transitorio 8%', MtoTax= 8, Activo = 1, EsFijo = 1, TipoIVA = 2, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1, Orden = 6
		WHERE CodTaxs = 'IVA8T';
	END;

IF (SELECT CodTaxs FROM SATAXES WITH (NOLOCK) WHERE CodTaxs = 'IVA13') IS NULL
	BEGIN
		INSERT INTO SATAXES (CodTaxs, Descrip, MtoTax, Activo, EsFijo, EsPorct, TipoIVA, EsLibroI, EsTaxVenta, EsTaxCompra)
		VALUES	('IVA13','IVA Tarifa general 13%',13,1,1,1,1,1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SATAXES SET Descrip = 'IVA Tarifa general 13%', MtoTax= 13, Activo = 1, EsFijo = 1, TipoIVA = 1, EsLibroI = 1, EsTaxVenta = 1, EsTaxCompra = 1 
		WHERE CodTaxs = 'IVA13';
	END;

DECLARE
@Orden INT

UPDATE SATAXES SET Orden = NULL
WHERE	(Descrip LIKE '%IVA1R%' AND CodTaxs <> 'IVA1R') OR 
		(Descrip LIKE '%IVA2R%' AND CodTaxs <> 'IVA2R') OR 
		(Descrip LIKE '%IVA4R%' AND CodTaxs <> 'IVA4R') OR 
		(Descrip LIKE '%IVA4T%' AND CodTaxs <> 'IVA4T') OR 
		(Descrip LIKE '%IVA8T%' AND CodTaxs <> 'IVA8T');

UPDATE SATAXES SET Orden = NULL
WHERE Orden < 2;

SELECT @Orden = ISNULL(MAX(Orden),1)
FROM SATAXES WITH (NOLOCK)
WHERE Orden IS NOT NULL;

UPDATE A SET Orden = @Orden + A.Linea
FROM
(SELECT	ROW_NUMBER() OVER(ORDER BY CodTaxs) Linea,
		Codtaxs,
		orden
FROM SATAXES
WHERE	(Orden IS NULL) AND	
		((Descrip LIKE '%IVA1R%' AND CodTaxs <> 'IVA1R') OR 
		(Descrip LIKE '%IVA2R%' AND CodTaxs <> 'IVA2R') OR 
		(Descrip LIKE '%IVA4R%' AND CodTaxs <> 'IVA4R') OR 
		(Descrip LIKE '%IVA4T%' AND CodTaxs <> 'IVA4T') OR 
		(Descrip LIKE '%IVA8T%' AND CodTaxs <> 'IVA8T'))) A;
GO
/*CARGANDO MONEDAS*/
DELETE FROM FECodigoMoneda
WHERE ID = 0;

DECLARE 
@TOTAL AS INT,
@LINEA AS INT,
@CODMONEDA AS VARCHAR(5), 
@PAIS AS VARCHAR(100), 
@NOMBRE_MONEDA AS VARCHAR(100)

DECLARE @MONEDAS TABLE (CODMONEDA VARCHAR(5), PAIS VARCHAR(100), NOMBRE_MONEDA VARCHAR(100), ID INT)

INSERT INTO @MONEDAS (CODMONEDA, PAIS, NOMBRE_MONEDA, ID)
VALUES	('CRC','COSTA RICA','Colón costarricense',1),
		('USD','ESTADOS UNIDOS DE AMÉRICA','Dólar Americano',2),
		('EUR','UNIÓN EUROPEA','Euro',3),
		('CNY','CHINA','Yuan',4),
		('RUB','RUSIA','Rublo ruso',5),
		('JPY','JAPÓN','Yen',6),
		('AFN','AFGANISTÁN','Afghani',7),
		('ALL','ALBANIA','Lek',8),
		('EUR','ALEMANIA','Euro',9),
		('DZD','ALGERIA','Dinar argelino',10),
		('EUR','ANDORRA','Euro',11),
		('AOA','ANGOLA','Kwanza',12),
		('XCD','ANGUILA','Dólar del Caribe Oriental',13),
		('XCD','ANTIGUA Y BARBUDA','Dólar del Caribe Oriental',14),
		('','ANTÁRTIDA','Sin moneda universal',15),
		('SAR','ARABIA SAUDITA','Riyal saudí',16),
		('ARS','ARGENTINA','Peso Argentino',17),
		('AMD','ARMENIA','Dram armenio',18),
		('AWG','ARUBA','Florín arubeño',19),
		('AUD','AUSTRALIA','Dólar australiano',20),
		('EUR','AUSTRIA','Euro',21),
		('AZN','AZERBAIYÁN','Manat azerbaiyano',22),
		('BSD','BAHAMAS (LAS)','Dólar de las Bahamas',23),
		('BHD','BAHRAIN','Dinar de Bahrein',24),
		('BDT','BANGLADESH','Taka',25),
		('BBD','BARBADOS','Dólar de Barbados',26),
		('BZD','BELICE','Dólar de Belice',27),
		('XOF','BENIN','Franco CFA BCEAO',28),
		('BMD','BERMUDA','Dólar de Bermudas',29),
		('BYR','BIELORRUSIA','Rublo bielorruso',30),
		('BOB','BOLIVIA (ESTADO PLURINACIONAL DE)','Boliviano',31),
		('BOV','BOLIVIA (ESTADO PLURINACIONAL DE)','Mvdol',32),
		('USD','BONAIRE SAN EUSTAQUIO Y SABA','Dólar Americano',33),
		('BAM','BOSNIA Y HERZEGOVINA','Marco bosnioherzegovino',34),
		('BWP','BOTSWANA','Pula',35),
		('BRL','BRASIL','Real Brasileño',36),
		('BND','BRUNEI DARUSSALAM','Dólar de Brunei',37),
		('BGN','BULGARIA','Lev búlgaro',38),
		('XOF','BURKINA FASO','Franco CFA BCEAO',39),
		('BIF','BURUNDI','Franco Burundi',40),
		('BTN','BUTÁN','Ngultrum',41),
		('INR','BUTÁN','Rupia india',42),
		('EUR','BÉLGICA','Euro',43),
		('CVE','CABO VERDE','Cabo Verde Escudo',44),
		('KHR','CAMBOYA','Riel',45),
		('XAF','CAMERÚN','Franco CFA BEAC',46),
		('CAD','CANADÁ','Dólar canadiense',47),
		('XAF','CHAD','Franco CFA BEAC',48),
		('CLF','CHILE','Unidad de Fomento',49),
		('CLP','CHILE','Peso chileno',50),
		('EUR','CHIPRE','Euro',51),
		('COP','COLOMBIA','Peso ColombianO',52),
		('COU','COLOMBIA','Unidad de Valor Real',53),
		('KMF','COMOROS','Franco Comoro',54),
		('XAF','CONGO','Franco CFA BEAC',55),
		('CDF','CONGO (LA REPÚBLICA DEMOCRÁTICA DEL)','Franco congoleño',56),
		('KRW','COREA (LA REPÚBLICA DE)','Won',57),
		('KPW','COREA (LA REPÚBLICA DEMOCRÁTICA POPULAR)','Won norcoreano',58),
		('XOF','COSTA DE MARFIL','Franco CFA BCEAO',59),
		('HRK','CROAcIA','Kuna',60),
		('CUC','CUBA','Peso Convertible',61),
		('CUP','CUBA','Peso Cubano',62),
		('ANG','CURAZAO','Florín antillano neerlandés',63),
		('DKK','DINAMARCA','Corona danesa',64),
		('DJF','DJIBOUTI','Franco de Djibouti',65),
		('XCD','DOMINICA','Dólar del Caribe Oriental',66),
		('USD','ECUADOR','DÓlar americano',67),
		('EGP','EGIPTO','Libra egipcia',68),
		('SVC','EL SALVADOR','Colón',69),
		('USD','EL SALVADOR','Dólar americano',70),
		('AED','EMIRATOS ÁRABES UNIDOS','Dírham de los Emiratos Árabes Unidos',71),
		('ERN','ERITREA','Nakfa',72),
		('EUR','ESLOVAQUIA','Euro',73),
		('EUR','ESLOVENIA','Euro',74),
		('EUR','ESPAÑA','Euro',75),
		('USN','ESTADOS UNIDOS DE AMÉRICA','Dólar Americano (Next day)',76),
		('USD','ESTADOS UNIDOS E ISLAS MENORES','Dólar Americano',77),
		('EUR','ESTONIA','Euro',78),
		('ETB','ETIOPÍA','Birr etíope',79),
		('FJD','FIJI','Dólar de Fiji',80),
		('PHP','FILIPINAS (LAS)','Peso filipino',81),
		('EUR','FINLANDIA','Euro',82),
		('XDR','FONDO MONETARIO INTERNACIONAL (IMF)','SDR (Derechos Especiales de Giro)',83),
		('EUR','FRANCIA','Euro',84),
		('XAF','GABÓN','Franco CFA BEAC',85),
		('GMD','GAMBIA','Dalasi',86),
		('GEL','GEORGIA','Lari',87),
		('GHS','GHANA','Cedi de Ghana',88),
		('GIP','GIBRALTAR','Libra de Gibraltar',89),
		('XCD','GRANADA','Dólar del Caribe Oriental',90),
		('EUR','GRECIA','Euro',91),
		('DKK','GROENLANDIA','Corona danesa',92),
		('EUR','GUADALUPE','Euro',93),
		('USD','GUAM','Dólar americano',94),
		('GTQ','GUATEMALA','Quetzal',95),
		('EUR','GUAYANA FRANCESA','Euro',96),
		('GBP','GUERNESEY','Libra esterlina',97),
		('GNF','GUINEA','Franco guineano',98),
		('XAF','GUINEA ECUATORIAL','Franco CFA BEAC',99),
		('XOF','GUINEA-BISSAU','Franco CFA BCEAO',100),
		('GYD','GUYANA','Dólar guyanés',101),
		('HTG','HAITÍ','Gourde',102),
		('USD','HAITÓ','Dólar americano',103),
		('HNL','HONDURAS','Lempira',104),
		('HKD','HONG KONG','Dolar de Hong Kong',105),
		('HUF','HUNGRÍA','Florín',106),
		('INR','INDIA','Rupia india',107),
		('IDR','INDONESIA','Rupia',108),
		('IQD','IRAQ','Dinar iraquí',109),
		('EUR','IRLANDA','Euro',110),
		('IRR','IRÁN (REPÚBLICA ISLÁMICA DE)','Rial iraní',111),
		('NOK','ISLA BOUVET','Corona noruega',112),
		('GBP','ISLA DE MAN','Libra esterlina',113),
		('AUD','ISLA DE NAVIDAD','Dólar australiano',114),
		('AUD','ISLA HEARD E ISLAS McDONALD','Dólar australiano',115),
		('AUD','ISLA NORFOLK','Dólar australiano',116),
		('ISK','ISLANDIA','Corona islandesa',117),
		('EUR','ISLAS ALAND','Euro',118),
		('KYD','ISLAS CAIMÁN','Dólar de las Islas Caimán',119),
		('AUD','ISLAS COCO','Dólar australiano',120),
		('NZD','ISLAS COOK','Dólar neozelandés',121),
		('DKK','ISLAS FEROE','Corona danesa',122),
		('','ISLAS GEORGIAS DEL SUR Y SANDWICH DEL SUR','Sin moneda universal',123),
		('FKP','ISLAS MALVINAS','Libra malvinense',124),
		('USD','ISLAS MARIANAS DEL NORTE','Dólar Americano',125),
		('USD','ISLAS MARSHALL','Dólar americano',126),
		('SBD','ISLAS SALOMÓN','Dólar de las Islas Salomón',127),
		('USD','ISLAS VIRGENES (EE.UU.)','Dólar Americano',128),
		('USD','ISLAS VÍRGENES (BRITÁNICA)','Dólar Americano',129),
		('ILS','ISRAEL','Nuevo Shekel Israelí',130),
		('EUR','ITALIA','Euro',131),
		('JMD','JAMAICA','Dólar jamaiquino',132),
		('GBP','JERSEY','Libra esterlina',133),
		('JOD','JORDANIA','Dinar jordano',134),
		('KZT','KAZAJSTÁN','Tenge',135),
		('KES','KENIA','Chelín keniano',136),
		('KGS','KIRGUISTÁN','Som',137),
		('AUD','KIRIBATI','Dólar australiano',138),
		('KWD','KUWAIT','Dinar kuwaití',139),
		('LAK','LAO REPÚBLICA DEMOCRÁTICA POPULAR','Kip',140),
		('LSL','LESOTHO','Loti',141),
		('ZAR','LESOTHO','Rand',142),
		('EUR','LETONIA','Euro',143),
		('LRD','LIBERIA','Dólar liberiano',144),
		('LYD','LIBIA','Dinar libio',145),
		('CHF','LIECHTENSTEIN','Franco suizo',146),
		('EUR','LITUANIA','Euro',147),
		('EUR','LUXEMBOURG','Euro',148),
		('LBP','LÍBANO','Libra libanesa',149),
		('MOP','MACAO','Pataca',150),
		('MKD','MACEDONIA (Ex República Yugoslava)','Denar',151),
		('MGA','MADAGASCAR','Ariary malgache',152),
		('MYR','MALASIA','Ringgit malayo',153),
		('MWK','MALAWI','Kwacha',154),
		('MVR','MALDIVAS','Rufiyaa',155),
		('EUR','MALTA','Euro',156),
		('XOF','MALÍ','Franco CFA BCEAO',157),
		('MAD','MARRUECOS','Dirham marroquí',158),
		('EUR','MARTINICA','Euro',159),
		('MUR','MAURICIO','Rupia de Mauricio',160),
		('MRO','MAURITANIA','Ouguiya',161),
		('EUR','MAYOTTE','Euro',162),
		('USD','MICRONESIA (ESTADOS FEDERADOS DE)','Dólar americano',163),
		('MDL','MOLDAVIA','Leu moldavo',164),
		('MNT','MONGOLIA','Tugrik',165),
		('EUR','MONTENEGRO','Euro',166),
		('XCD','MONTSERRAT','Dólar del Caribe Oriental',167),
		('MZN','MOZAMBIQUE','Metical mozambiqueño',168),
		('MMK','MYANMAR','Kyat',169),
		('MXN','MÉXICO','Peso MexicanO',170),
		('MXV','MÉXICO','Unidad de Inversion Mexicana (UDI)',171),
		('EUR','MÓNACO','Euro',172),
		('NAD','NAMIBIA','Dólar de Namibia',173),
		('ZAR','NAMIBIA','Rand',174),
		('AUD','NAURU','Dólar australiano',175),
		('NPR','NEPAL','Rupia nepalí',176),
		('NIO','NICARAGUA','Cordoba Oro',177),
		('XOF','NIGER','Franco CFA BCEAO',178),
		('NGN','NIGERIA','Naira',179),
		('NZD','NIUE','Dólar neozelandés',180),
		('NOK','NORUEGA','Corona noruega',181),
		('XPF','NUEVA CALEDONIA','Franco CFP',182),
		('NZD','NUEVA ZELANDA','Dólar neozelandés',183),
		('OMR','OMÁN','Rial omaní',184),
		('PKR','PAKISTÁN','Rupia pakistaní',185),
		('USD','PALAU','Dólar Americano',186),
		('','PALESTINA, ESTADO DE','Sin moneda universal',187),
		('PAB','PANAMÁ','Balboa',188),
		('USD','PANAMÁ','Dólar Americano',189),
		('PGK','PAPUA NUEVA GUINEA','Kina',190),
		('PYG','PARAGUAY','Guaraní',191),
		('EUR','PAÍSES BAJOS','Euro',192),
		('XUA','PAÍSES MIEMBROS DEL GRUPO BANCO AFRICANO DEL DESARROLLO','Unidad de cuenta del BAD',193),
		('PEN','PERú','Nuevo Sol',194),
		('NZD','PITCAIRN','Dólar neozelandés',195),
		('XPF','POLINESIA FRANCESA','CFP Franc',196),
		('PLN','POLONIA','Zloty',197),
		('EUR','PORTUGAL','Euro',198),
		('USD','PUERTO RICO','Dólar Americano',199),
		('QAR','QATAR','Riyal catarí',200),
		('GBP','REINO UNIDO DE GRAN BRETAÑA E IRLANDA DEL NORTE','Libra esterlina',201),
		('XAF','REPÚBLICA CENTROAFRICANA','Franco CFA BEAC',202),
		('CZK','REPÚBLICA CHECA','Corona checa',203),
		('DOP','REPÚBLICA DOMINICANA','Peso Dominicano',204),
		('SYP','REPÚBLICA ÁRABE SIRIA','Libra Siria',205),
		('RWF','RUANDA','Franco ruandés',206),
		('RON','RUMANIA','Leu rumano',207),
		('EUR','RÉUNION','Euro',208),
		('MAD','SAHARA OCCIDENTAL','Dirham marroquí',209),
		('WST','SAMOA','Tala',210),
		('USD','SAMOA AMERICANA','Dólar Americano',211),
		('EUR','SAN BARTOLOMÉ','Euro',212),
		('XCD','SAN CRISTÓBAL Y NIEVES','Dólar del Caribe Oriental',213),
		('EUR','SAN MARINO','Euro',214),
		('EUR','SAN MARTIN (PARTE FRANCESA)','Euro',215),
		('EUR','SAN PEDRO Y MIQUELÓN','Euro',216),
		('XCD','SAN VICENTE Y LAS GRANADINAS','Dólar del Caribe Oriental',217),
		('SHP','SANTA ELENA, ASCENSIÓN Y TRISTÁN DE ACUÑA','Libra de Santa Helena',218),
		('XCD','SANTA LUCÍA','Dólar del Caribe Oriental',219),
		('EUR','SANTA SEDE','Euro',220),
		('STD','SANTO TOMÉ Y PRÍNCIPE','Dobra',221),
		('XOF','SENEGAL','Franco CFA BCEAO',222),
		('RSD','SERBIA','Dinar serbio',223),
		('SCR','SEYCHELLES','Rupia seychelense',224),
		('SLL','SIERRA LEONA','Leone',225),
		('SGD','SINGAPUR','Dolar de Singapur',226),
		('ANG','SINT MAARTEN (PARTE HOLANDESA)','Florín antillano neerlandés',227),
		('XSU','SISTEMA UNITARIO DE COMPENSACION REGIONAL DE PAGOS "SUCRE"','Sucre',228),
		('SOS','SOMALIA','Chelín somalí',229),
		('LKR','SRI LANKA','Rupia de Sri Lanka',230),
		('ZAR','SUDÁFRICA','Rand',231),
		('SDG','SUDÁN','Libra sudanesa',232),
		('SSP','SUDÁN DEL SUR','Libra sursudanesa',233),
		('SEK','SUECIA','Corona sueca',234),
		('CHE','SUIZA','Euro WIR',235),
		('CHF','SUIZA','Franco suizo',236),
		('CHW','SUIZA','Franco WIR',237),
		('SRD','SURINAME','Dólar surinamés',238),
		('NOK','SVALBARD AND JAN MAYEN','Corona noruega',239),
		('SZL','SWAZILANDIA','Lilangeni',240),
		('THB','TAILANDIA','Baht',241),
		('TWD','TAIWÁN (PROVINCIA DE CHINA)','Nuevo dólar taiwanés',242),
		('TZS','TANZANIA, REPÚBLICA UNIDA DE','Chelín tanzano',243),
		('TJS','TAYIKISTÁN','Somoni',244),
		('USD','TERRITORIO BRITÁNICO DEL OCÉANO ÍNDICO','Dólar Americano',245),
		('EUR','TERRITORIOS AUSTRALES FRANCESES','Euro',246),
		('USD','TIMOR ORIENTAL','Dólar Americano',247),
		('XOF','TOGO','Franco CFA BCEAO',248),
		('NZD','TOKELAU','Dólar neozelandés',249),
		('TOP','TONGA','Pa’anga',250),
		('TTD','TRINIDAD Y TOBAGO','Dólar trinitense',251),
		('USD','TURCOS Y CAICOS','Dólar Americano',252),
		('TMT','TURKMENISTÁN','Manat turcomano',253),
		('TRY','TURQUÍA','Lira turca',254),
		('AUD','TUVALU','Dólar australiano',255),
		('TND','TÚNEZ','Dinar tunecino',256),
		('UAH','UCRANIA','Hryvnia',257),
		('UGX','UGANDA','Chelín ugandés',258),
		('UYI','URUGUAY','Uruguay Peso en Unidades Indexadas (URUIURUI)',259),
		('UYU','URUGUAY','Peso Uruguayo',260),
		('UZS','UZBEKISTÁN','Som uzbeko',261),
		('VUV','VANUATU','Vatu',262),
		('VEF','VENEZUELA (REPÚBLICA BOLIVARIANA DE)','Bolívar',263),
		('VND','VIETNAM','Dong',264),
		('XPF','WALLIS Y FUTUNA','Franco CFP',265),
		('YER','YEMEN','Rial yemení',266),
		('ZMW','ZAMBIA','Kwacha zambiano',267),
		('ZWL','ZIMBABWE','Dólar zimbabuense',268);

SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @MONEDAS;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @CODMONEDA = NULL;
		SET @PAIS = NULL;
		SET @NOMBRE_MONEDA = NULL;

		SELECT 	@CODMONEDA = CODMONEDA,
				@PAIS = PAIS,
				@NOMBRE_MONEDA = NOMBRE_MONEDA
		FROM @MONEDAS
		WHERE ID = @LINEA;

		IF (SELECT ID FROM FECodigoMoneda  WHERE ID = @LINEA) IS NULL
			BEGIN
				INSERT INTO FECodigoMoneda (CODMONEDA, PAIS, NOMBRE_MONEDA, ID)
				VALUES	(@CODMONEDA,@PAIS,@NOMBRE_MONEDA,@LINEA);
			END;
		ELSE
			BEGIN
				UPDATE FECodigoMoneda SET CODMONEDA = @CODMONEDA, PAIS = @PAIS, NOMBRE_MONEDA = @NOMBRE_MONEDA
				WHERE ID = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @MONEDAS;
GO
/*CARGANDO UNIDADES*/
DELETE FROM FEunidades
WHERE ID = 0;

DECLARE
@TOTAL AS INT,
@LINEA AS INT,
@CodHacienda VARCHAR(20), 
@CodSaint VARCHAR(3), 
@Descrip VARCHAR(100)

DECLARE @UNIDADES TABLE (CodHacienda VARCHAR(20), CodSaint VARCHAR(3), Descrip VARCHAR(100), ID INT)

INSERT INTO @UNIDADES (CodHacienda, CodSaint, Descrip, ID)
VALUES  ('Sp','Sp','Servicios Profesionales',1),
		('m','m','Metro',2),
		('kg','kg','Kilogramo',3),
		('s','seg','Segundo',4),
		('A','A','Ampere',5),
		('K','K','Kelvin',6),
		('mol','mol','Mol',7),
		('cd','cd','Candela',8),
		('m²','m2','metro cuadrado',9),
		('m³','m3','metro cúbico',10),
		('m/s','m/s','metro por segundo',11),
		('m/s²','ms2','metro por segundo cuadrado',12),
		('1/m','1/m','1 por metro',13),
		('kg/m³','km3','kilogramo por metro cúbico',14),
		('A/m²','Am2','ampere por metro cuadrado',15),
		('A/m','A/m','ampere por metro',16),
		('mol/m³','mm3','mol por metro cúbico',17),
		('cd/m²','dm2','candela por metro cuadrado',18),
		('1','1','uno (indice de refracción)',19),
		('rad','rad','radián',20),
		('sr','sr','estereorradián',21),
		('Hz','Hz','hertz',22),
		('N','N','newton',23),
		('Pa','Pa','pascal',24),
		('J','J','Joule',25),
		('W','W','Watt',26),
		('C','C','coulomb',27),
		('V','V','volt',28),
		('F','F','farad',29),
		('Ω','ohm','ohm',30),
		('S','S','siemens',31),
		('Wb','Wb','weber',32),
		('T','Tes','tesla',33),
		('H','Hen','henry',34),
		('°C','°C','grado Celsius',35),
		('lm','lm','lumen',36),
		('lx','lx','lux',37),
		('Bq','Bq','Becquere',38),
		('Gy','Gy','gray',39),
		('Sv','Sv','sievert',40),
		('kat','kat','katal',41),
		('Pa·s','P·s','pascal segundo',42),
		('N·m','N·m','newton metro',43),
		('N/m','N/m','newton por metro',44),
		('rad/s','r/s','radián por segundo',45),
		('rad/s²','rs2','radián por segundo cuadrado',46),
		('W/m²','Wm2','watt por metro cuadrado',47),
		('J/K','J/K','joule por kelvin',48),
		('J/(kg·K)','JkK','joule por kilogramo kelvin',49),
		('J/kg','Jkg','joule por kilogramo',50),
		('W/(m·K)','WmK','watt por metro kevin',51),
		('J/m³','Jm3','joule por metro cúbico',52),
		('V/m','V/m','volt por metro',53),
		('C/m³','Cm3','coulomb por metro cúbico',54),
		('C/m²','Cm2','coulomb por metro cuadrado',55),
		('F/m','F/m','farad por metro',56),
		('H/m','H/m','henry por metro',57),
		('J/mol','J/m','joule por mol',58),
		('J/(mol·K)','JmK','joule por mol kelvin',59),
		('C/kg','C/k','coulomb por kilogramo',60),
		('Gy/s','G/s','gray por segundo',61),
		('W/sr','W/s','watt por estereorradián',62),
		('W/(m²·sr)','Wms','watt por metro cuadrado estereo rradián',63),
		('kat/m³','kx3','katal por metro cúbico',64),
		('min','min','minuto',65),
		('h','hr','hora',66),
		('d','d','día',67),
		('º','º','grado',68),
		('´','´','minuto',69),
		('´´','´´','segundo',70),
		('L','Lts','litro',71),
		('t','Ton','tonelada',72),
		('Np','Np','neper',73),
		('B','B','bel',74),
		('eV','eV','electronvolt',75),
		('u','u','unidad de masa atómica unificada',76),
		('ua','ua','unidad astronómica',77),
		('Unid','UND','Unidad',78),
		('Gal','Gal','Galón',79),
		('g','gr','Gramo',80),
		('Km','Km','Kilometro',81),
		('ln','ln','pulgada',82),
		('cm','cm','centímetro',83),
		('mL','ml','mililitro',84),
		('mm','mm','Milímetro',85),
		('Oz','Oz','Onzas',86),
		('Cm','Cms','Comisiones',87),
		('Al','Al','Alquiler de uso habitacional',88),
		('Alc','Alc','Alquiler de uso comercial',89),
		('I','I','Intereses',90),
		('Os','Os','Otro tipo de servicio',91),
		('Spe','Spe','Servicios personales',92),
		('St','St','Servicios técnicos',93),
		('Kw','Kw','kilovatios',94),
		('Otros','OTR','Se debe indicar la descripción de la medida a utilizar',95);

SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @UNIDADES;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @CodHacienda = NULL;
		SET @CodSaint = NULL;
		SET @Descrip = NULL;

		SELECT 	@CodHacienda = CodHacienda,
				@CodSaint = CodSaint,
				@Descrip = Descrip
		FROM @UNIDADES
		WHERE ID = @LINEA;

		IF (SELECT ID FROM FEunidades  WHERE ID = @LINEA) IS NULL
			BEGIN
				INSERT INTO FEunidades (CodHacienda, CodSaint, Descrip, ID)
				VALUES	(@CodHacienda,@CodSaint,@Descrip,@LINEA);
			END;
		ELSE
			BEGIN
				UPDATE FEunidades SET 	CodHacienda = @CodHacienda, CodSaint = @CodSaint, Descrip = @Descrip
				WHERE ID = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @UNIDADES;
GO
/*CARGANDO TIPO DE IDENTIFICACION*/
DELETE FROM FETipoIdentificacion
WHERE ID = 0;

DECLARE
@TOTAL AS INT,
@LINEA AS INT,
@CodTipo VARCHAR(2),
@Descrip varchar(60)

DECLARE @IDENTIFICACION TABLE (CodTipo varchar(2), Descrip varchar(60), ID INT)

INSERT INTO @IDENTIFICACION (CodTipo, Descrip, ID)
VALUES	('01','Cédula Física',1),
		('02','Cédula Jurídica',2),
		('03','DIMEX',3),
		('04','NITE',4);
	
SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @IDENTIFICACION;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @CodTipo = NULL;
		SET @Descrip = NULL;

		SELECT 	@CodTipo = CodTipo,
				@Descrip = Descrip
		FROM @IDENTIFICACION
		WHERE ID = @LINEA;

		IF (SELECT ID FROM FETipoIdentificacion  WHERE ID = @LINEA) IS NULL
			BEGIN
				INSERT INTO FETipoIdentificacion (CodTipo, Descrip, ID)
				VALUES	(@CodTipo,@Descrip,@LINEA);
			END;
		ELSE
			BEGIN
				UPDATE FETipoIdentificacion SET CodTipo = @CodTipo, Descrip = @Descrip
				WHERE ID = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @IDENTIFICACION;
GO
/*CARGANDO CODIGO DE EXONERACION*/
DELETE FROM FECodigoExoneracion
WHERE ID = 0;

DECLARE
@TOTAL AS INT,
@LINEA AS INT,
@Descrip varchar(100),
@CodTipo VARCHAR(2)

DECLARE @EXONERACION TABLE (CodExoneracion varchar(2), Descrip varchar(100), ID INT)

INSERT INTO @EXONERACION (CodExoneracion, Descrip, ID)
VALUES 	('01','Compras Autorizadas',1),
		('02','Ventas exentas a diplomáticos',2),
		('03','Autorizado por Ley especial',3),
		('04','Exenciones Dirección General de Hacienda',4),
		('05','Transitorio V',5),
		('06','Transitorio IX',6),
		('07','Transitorio XVII',7),
		('99','Otros',8);
		
SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @EXONERACION;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @CodTipo = NULL;
		SET @Descrip = NULL;

		SELECT 	@CodTipo = CodExoneracion,
				@Descrip = Descrip
		FROM @EXONERACION
		WHERE ID = @LINEA

		IF (SELECT ID FROM FECodigoExoneracion WHERE ID = @LINEA) IS NULL
			BEGIN
				INSERT INTO FECodigoExoneracion (CodExoneracion, Descrip, ID)
				VALUES	(@CodTipo,@Descrip,@LINEA);
			END;
		ELSE
			BEGIN
				UPDATE FECodigoExoneracion SET CodExoneracion = @CodTipo, Descrip = @Descrip
				WHERE ID = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @EXONERACION;
GO		
/*CARGANDO CODIGOS DE AREAS DE PAIS*/
DELETE FROM FECodAreaPais
WHERE ID = 0;

DECLARE
@TOTAL AS INT,
@LINEA AS INT,
@Prefijo VARCHAR(5),
@Descrip VARCHAR(50),
@Pais VARCHAR(50)

DECLARE @CODIGOAREA TABLE (Id INT, Descrip VARCHAR(50), Prefijo	VARCHAR(5), Pais VARCHAR(50))

INSERT INTO @CODIGOAREA (Id, Descrip, Prefijo, Pais)
VALUES	(1,'+93 Afghanistan','93','Afghanistan'),
		(2,'+355 Albania','355','Albania'),
		(3,'+49 Alemania','49','Alemania'),
		(4,'+213 Algeria','213','Algeria'),
		(5,'+376 Andorra','376','Andorra'),
		(6,'+244 Angola','244','Angola'),
		(7,'+1 Anguila','1','Anguila'),
		(8,'+1 Antigua & Barbuda','1','Antigua & Barbuda'),
		(9,'+966 Arabia Saudí','966','Arabia Saudí'),
		(10,'+54 Argentina','54','Argentina'),
		(11,'+374 Armenia','374','Armenia'),
		(12,'+297 Aruba','297','Aruba'),
		(13,'+247 Ascension Island','247','Ascension Island'),
		(14,'+61 Australia','61','Australia'),
		(15,'+43 Austria','43','Austria'),
		(16,'+994 Azerbaijan','994','Azerbaijan'),
		(17,'+1 Bahamas','1','Bahamas'),
		(18,'+973 Bahrain','973','Bahrain'),
		(19,'+880 Bangladesh','880','Bangladesh'),
		(20,'+1 Barbados','1','Barbados'),
		(21,'+375 Belarus','375','Belarus'),
		(22,'+32 Belgium','32','Belgium'),
		(23,'+501 Belize','501','Belize'),
		(24,'+229 Benin','229','Benin'),
		(25,'+1 Bermuda','1','Bermuda'),
		(26,'+591 Bolivia','591','Bolivia'),
		(27,'+387 Bosnia','387','Bosnia'),
		(28,'+267 Botswana','267','Botswana'),
		(29,'+55 Brasil','55','Brasil'),
		(30,'+673 Brunei','673','Brunei'),
		(31,'+359 Bulgaria','359','Bulgaria'),
		(32,'+226 Burkina Faso','226','Burkina Faso'),
		(33,'+257 Burundi','257','Burundi'),
		(34,'+975 Butan','975','Butan'),
		(35,'+855 Camboya','855','Camboya'),
		(36,'+237 Camerun','237','Camerun'),
		(37,'+1 Canada','1','Canada'),
		(38,'+235 Chad','235','Chad'),
		(39,'+56 Chile','56','Chile'),
		(40,'+86 China','86','China'),
		(41,'+357 Chipre','357','Chipre'),
		(42,'+57 Colombia','57','Colombia'),
		(43,'+850 Corea del Norte','850','Corea del Norte'),
		(44,'+82 Corea del Sur','82','Corea del Sur'),
		(45,'+225 Costa de Ivory','225','Costa de Ivory'),
		(46,'+506 Costa Rica','506','Costa Rica'),
		(47,'+385 Croacia','385','Croacia'),
		(48,'+53 Cuba','53','Cuba'),
		(49,'+246 Diego Garcia','246','Diego Garcia'),
		(50,'+45 Dinamarca','45','Dinamarca'),
		(51,'+1 Dominica','1','Dominica'),
		(52,'+593 Ecuador','593','Ecuador'),
		(53,'+20 Egipto','20','Egipto'),
		(54,'+503 El Salvador','503','El Salvador'),
		(55,'+971 Emiratos arabes unidos','971','Emiratos arabes unidos'),
		(56,'+291 Eritrea','291','Eritrea'),
		(57,'+421 Eslovaquia','421','Eslovaquia'),
		(58,'+386 Eslovenia','386','Eslovenia'),
		(59,'+34 España','34','España'),
		(60,'+372 Estonia','372','Estonia'),
		(61,'+251 Etiopia','251','Etiopia'),
		(62,'+63 Filipinas','63','Filipinas'),
		(63,'+358 Finlandia','358','Finlandia'),
		(64,'+33 Francia','33','Francia'),
		(65,'+241 Gabón','241','Gabón'),
		(66,'+220 Gambia','220','Gambia'),
		(67,'+995 Georgia','995','Georgia'),
		(68,'+233 Ghana','233','Ghana'),
		(69,'+350 Gibraltar','350','Gibraltar'),
		(70,'+1 Granada','1','Granada'),
		(71,'+30 Grecia','30','Grecia'),
		(72,'+299 Groenlandia','299','Groenlandia'),
		(73,'+298 Islas Feroe','298','Islas Feroe'),
		(74,'+679 Islas Fiyi','679','Islas Fiyi'),
		(75,'+1 Islas Mariana','1','Islas Mariana'),
		(76,'+692 Islas Marshall','692','Islas Marshall'),
		(77,'+672 Islas Norfolk','672','Islas Norfolk'),
		(78,'+677 Islas Salomón','677','Islas Salomón'),
		(79,'+1 Islas Vírgenes Británicas','1','Islas Vírgenes Británicas'),
		(80,'+972 Israel','972','Israel'),
		(81,'+39 Italia','39','Italia'),
		(82,'+1 Jamaica','1','Jamaica'),
		(83,'+81 Japón','81','Japón'),
		(84,'+962 Jordania','962','Jordania'),
		(85,'+7 Kazajistán','7','Kazajistán'),
		(86,'+254 Kenia','254','Kenia'),
		(87,'+996 Kirguistán','996','Kirguistán'),
		(88,'+686 Kiribati','686','Kiribati'),
		(89,'+965 Kuwait','965','Kuwait'),
		(90,'+856 Laos','856','Laos'),
		(91,'+266 Lesoto','266','Lesoto'),
		(92,'+371 Letonia','371','Letonia'),
		(93,'+961 Líbano','961','Líbano'),
		(94,'+231 Liberia','231','Liberia'),
		(95,'+218 Libia','218','Libia'),
		(96,'+423 Liechtenstein','423','Liechtenstein'),
		(97,'+674 Nauru','674','Nauru'),
		(98,'+977 Nepal','977','Nepal'),
		(99,'+1 Nevis & St. Kitts','1','Nevis & St. Kitts'),
		(100,'+505 Nicaragua','505','Nicaragua'),
		(101,'+227 Niger','227','Niger'),
		(102,'+234 Nigeria','234','Nigeria'),
		(103,'+683 Niue','683','Niue'),
		(104,'+47 Noruega','47','Noruega'),
		(105,'+687 Nueva Caledonia','687','Nueva Caledonia'),
		(106,'+64 Nueva Zelanda','64','Nueva Zelanda'),
		(107,'+968 Oman','968','Oman'),
		(108,'+92 Pakistan','92','Pakistan'),
		(109,'+680 Palau','680','Palau'),
		(110,'+970 Palestina','970','Palestina'),
		(111,'+507 Panama','507','Panama'),
		(112,'+675 Papua Nueva Guinea','675','Papua Nueva Guinea'),
		(113,'+595 Paraguay','595','Paraguay'),
		(114,'+51 Peru','51','Peru'),
		(115,'+689 Polinesia francesa','689','Polinesia francesa'),
		(116,'+48 Polonia','48','Polonia'),
		(117,'+351 Portugal','351','Portugal'),
		(118,'+974 Qatar','974','Qatar'),
		(119,'+44 Reino Unido','44','Reino Unido'),
		(120,'+236 Rep. Centro Africana','236','Rep. Centro Africana'),
		(121,'+420 Rep. Checa','420','Rep. Checa'),
		(122,'+243 Rep. Dem. del Congo','243','Rep. Dem. del Congo'),
		(123,'+1 Rep. Dominicana','1','Rep. Dominicana'),
		(124,'+242 República del Congo','242','República del Congo'),
		(125,'+250 Ruanda','250','Ruanda'),
		(126,'+40 Rumania','40','Rumania'),
		(127,'+7 Rusia','7','Rusia'),
		(128,'+685 Samoa oriental','685','Samoa oriental'),
		(129,'+378 San Marino','378','San Marino'),
		(130,'+239 San Tome','239','San Tome'),
		(131,'+221 Senegal','221','Senegal'),
		(132,'+381 Serbia','381','Serbia'),
		(133,'+248 Seychelles','248','Seychelles'),
		(134,'+232 Sierra Leone','232','Sierra Leone'),
		(135,'+65 Singapur','65','Singapur'),
		(136,'+963 Siria','963','Siria'),
		(137,'+252 Somalia','252','Somalia'),
		(138,'+94 Sri Lanka','94','Sri Lanka'),
		(139,'+290 St. Helena','290','St. Helena'),
		(140,'+1 St. Lucia','1','St. Lucia'),
		(141,'+508 St. Pierre & Miquelon','508','St. Pierre & Miquelon'),
		(142,'+1 St. Vicente & Granadinas','1','St. Vicente & Granadinas'),
		(143,'+249 Sudan','249','Sudan'),
		(144,'+46 Suecia','46','Suecia'),
		(145,'+41 Suiza','41','Suiza'),
		(146,'+27 Surafrica','27','Surafrica'),
		(147,'+597 Surinam','597','Surinam'),
		(148,'+268 Swazilandia','268','Swazilandia'),
		(149,'+66 Tailandia','66','Tailandia'),
		(150,'+886 Taiwan','886','Taiwan'),
		(151,'+992 Tajikistan','992','Tajikistan'),
		(152,'+255 Tanzania','255','Tanzania'),
		(153,'+670 Timor del Este','670','Timor del Este'),
		(154,'+228 Togo','228','Togo'),
		(155,'+690 Tokelau','690','Tokelau'),
		(156,'+676 Tonga','676','Tonga'),
		(157,'+1 Trinidad & Tobago','1','Trinidad & Tobago'),
		(158,'+216 Tunez','216','Tunez'),
		(159,'+90 Turquia','90','Turquia'),
		(160,'+688 Tuvalu','688','Tuvalu'),
		(161,'+380 Ucrania','380','Ucrania'),
		(162,'+256 Uganda','256','Uganda'),
		(163,'+598 Uruguay','598','Uruguay'),
		(164,'+1 USA','1','USA'),
		(165,'+998 Uzbekistan','998','Uzbekistan'),
		(166,'+678 Vanuatu','678','Vanuatu'),
		(167,'+58 Venezuela','58','Venezuela'),
		(168,'+84 Vietnam','84','Vietnam'),
		(169,'+681 Wallis & Futuna','681','Wallis & Futuna'),
		(170,'+967 Yemen','967','Yemen'),
		(171,'+253 Yibouti','253','Yibouti'),
		(172,'+260 Zambia','260','Zambia'),
		(173,'+263 Zimbawe','263','Zimbawe');
	
SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @CODIGOAREA;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @Prefijo = NULL;
		SET @Descrip = NULL;
		SET @Pais = NULL;

		SELECT 	@Prefijo = Prefijo,
				@Descrip = Descrip,
				@Pais = Pais
		FROM @CODIGOAREA
		WHERE ID = @LINEA;

		IF (SELECT ID FROM FECodAreaPais  WHERE ID = @LINEA) IS NULL
			BEGIN
				INSERT INTO FECodAreaPais (Id, Descrip, Prefijo, Pais)
				VALUES	(@LINEA,@Descrip,@Prefijo,LEFT(@Pais,50));
			END;
		ELSE
			BEGIN
				UPDATE FECodAreaPais SET Prefijo = @Prefijo, Descrip = @Descrip, Pais = LEFT(@Pais,50)
				WHERE ID = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @CODIGOAREA;
GO
/*CARGANDO ACTIVIDAD ECONOMICA*/
DELETE FROM FEActividades_Eco
WHERE NroUnico = 0;

DECLARE
@TOTAL AS INT,
@LINEA AS INT,
@Codigo_Actividad VARCHAR(6),
@Nombre varchar(250)

DECLARE @ACTIVIDADES TABLE (NroUnico int, Codigo_Actividad varchar(6), Nombre varchar(250))

INSERT INTO @ACTIVIDADES (NroUnico, Codigo_Actividad, Nombre) 
VALUES 	(1,'011101','CULTIVO Y VENTA DE CEREALES, LEGUMBRES Y GRANOS BÁSICOS NO INCLUIDAS EN CANASTA BÁSICA'),
		(2,'011102','CULTIVO DE PALMA AFRICANA Y OTROS FRUTOS OLEAGINOSOS'),
		(3,'011103','CULTIVO Y COMERCIALIZACIÓN DE CESPED'),
		(4,'011218','PRODUCCIÓN DE MINIVEGETALES'),
		(5,'011231','CULTIVO HORTALIZAS LEGUMBRES ESPECIALIDADES HORTÍCOLAS PRODUCTOS VIVERO GRAVADOS IVA'),
		(6,'011301','CULTIVO DE ESPECIAS DE TODO TIPO'),
		(7,'011302','CULTIVO DE FRUTAS'),
		(8,'011322','CULTIVO DE SEMILLAS COMESTIBLES Y GERMINACION DE SEMILLAS OLEAGINOSAS'),
		(9,'011329','CULTIVO DE PLANTAS PARA PREPARAR BEBIDAS Y MEDICINAS'),
		(10,'011349','CULTIVO DE CACAO'),
		(11,'011401','CULTIVO DE CAFE'),
		(12,'011701','CULTIVO DE  CAñA DE AZÚCAR'),
		(13,'011802','CULTIVO Y VENTA DE CEREALES, LEGUMBRES Y GRANOS BÁSICOS INCLUIDOS  EN CANASTA BÁSICA'),
		(14,'011901','CULTIVO DE FLORES DE TODO TIPO'),
		(15,'011903','VIVEROS'),
		(16,'011907','PEQUEÑOS PRODUCTORES AGRICOLAS (FERIAS DEL AGRICULTOR)'),
		(17,'012101','CRíA DE CABALLOS Y OTROS EQUINOS'),
		(18,'012102','PRODUCCION DE SEMEN BOVINO, VENTA DE SEMEN CONGELADO Y DILUYENTE PARA SEMEN'),
		(19,'012103','CRÍA DE ANIMALES DOMÉSTICOS,COMO: GANADO VACUNO, OVEJAS Y CABRAS'),
		(20,'012201','CRIA DE CERDOS'),
		(21,'012202','CRIA DE ANIMALES DOMESTICADOS (AVES DE CORRAL)'),
		(22,'012203','CRíA DE MARIPOSAS'),
		(23,'012204','CRIA Y VENTA DE OTROS ANIMALES SEMIDOMESTICADOS O SALVAJES'),
		(24,'012401','PRODUCCION Y VENTA DE HUEVOS DE CUALQUIER TIPO, EXCEPTO  LOS DE GALLINA'),
		(25,'012402','CRÍA DE ANIMALES Y OBTENCIÓN DE PRODUCTOS DE ANIMALES VIVOS'),
		(26,'012403','PRODUCCION DE HUEVOS DE GALLINA INCLUIDOS EN LA CANASTA BASICA'),
		(27,'013001','CULTIVO DE PRODUCTOS AGRICOLAS EN COMBINACION CON LA CRIA DE ANIMALES'),
		(28,'014001','SERVICIO DE JARDINERIA Y/O DISEÑO PAISAJISTA'),
		(29,'014002','RECOLECCION DE COSECHAS Y ACTIVIDADES CONEXAS'),
		(30,'014003','FUMIGACION DE CULTIVOS'),
		(31,'014004','SERVICIO DE PLANTACION DE ARBOLES Y SIMILARES'),
		(32,'014005','MANEJO E INSTALACION DE SISTEMAS DE RIEGO'),
		(33,'015001','CAZA ORDINARIA MEDIANTE TRAMPAS, Y REPOBLACION DE ANIMALES DE CAZA'),
		(34,'020001','VENTA DE ARBOLES EN PIE (ARBOLES DE REFORESTACION)'),
		(35,'020002','EXTRACCION Y/O VENTA DE MADERA'),
		(36,'020005','ACTIVIDADES DE CONSERVACIÓN DE BOSQUES. (SERVICIOS AMBIENTALES, VENTA DE OXÍGENO)'),
		(37,'050001','PESCADORES ARTESANALES EN PEQ. ESCALA'),
		(38,'050002','PESCADORES ARTESANALES MEDIOS'),
		(39,'050003','CRIADERO DE PECES, CRUSTACEOS O MOLUSCOS. COMERCIALIZACION DE LARVAS DE ESPECIES'),
		(40,'050005','PESCADORES EN GRAN ESCALA'),
		(41,'101001','EXTRACCION Y AGLOMERACION DE CARBON DE PIEDRA'),
		(42,'111001','EXTRACCION DE PETROLEO CRUDO Y GAS NATURAL'),
		(43,'120001','EXTRACCIÓN DE MINERALES DE URANIO Y TORIO'),
		(44,'141001','EXTRACCION DE PIEDRA, ARENA Y ARCILLA'),
		(45,'142101','EXTRACCION DE MINERALES Y SUSTANCIAS PARA LA FABRICACION DEABONOS'),
		(46,'142201','EXTRACCION DE SAL'),
		(47,'142901','EXPLOTACION DE OTRAS MINAS Y CANTERAS N.C.P.'),
		(48,'142902','VENTA DE ASFALTO/MEZCLA ASFALTICA'),
		(49,'151101','ELABORACION Y CONSERVACIÓN DE CARNE  Y EMBUTIDOS DE GANADO VACUNO, PORCINO  Y DE AVES'),
		(50,'151102','OPERACIÓN DE MATADEROS QUE REALIZAN ACTIVIDADES DE MATANZA DE ANIMALES'),
		(51,'151103','PRODUCCION DE CUEROS Y PIELES SIN CURTIR'),
		(52,'151105','PRODUCCION Y VENTA DE EMBUTIDOS EMPACADOS, ENVASADO Y ENLATADO'),
		(53,'151201','ELABORACIóN Y CONSERVACIóN DE PESCADO, CRUSTÁCEOS Y MOLUSCOS'),
		(54,'151301','ELABORACION Y CONSERVACION DE FRUTAS, LEGUMBRES Y HORTALIZAS'),
		(55,'151304','PRODUCCIóN DE CONCENTRADOS PARA JUGOS (CITRICULTURA)'),
		(56,'151401','ELABORACION Y VENTA DE ACEITES  Y GRASAS DE ORIGEN VEGETAL Y ANIMAL'),
		(57,'152001','PRODUCCIóN DE HELADOS Y OTROS PRODUCTOS SIMILARES'),
		(58,'152002','ELABORACIÓN Y VENTA DE PRODUCTOS LÁCTEOS NO INCLUIDOS EN CANASTA BÁSICA'),
		(59,'152003','ELABORACIÓN DE PRODUCTOS DE MOLINERÍA NO INCLUIDOS CANASTA BÁSICA'),
		(60,'153101','SERVICIO DE MOLIENDA'),
		(61,'153102','ELABORACIÓN DE PRODUCTOS DE MOLINERÍA NO INCLUIDOS CANASTA BÁSICA'),
		(62,'153103','BENEFICIO DE ARROZ'),
		(63,'153104','ELABORACIÓN DE PRODUCTOS DE MOLINERÍA INCLUIDOS EN CANASTA BÁSICA'),
		(64,'153201','ELABORACION DE PRODUCTOS DERIVADOS DE ALMIDON'),
		(65,'153301','ELABORACION DE ALIMENTOS PARA ANIMALES DESTINADOS AL CONSUMO HUMANO'),
		(66,'153302','ELABORACION DE PACAS DE HENO'),
		(67,'154101','VENTA DE PAN Y OTROS PRODUCTOS SIMILARES GRAVADOS CON IVA'),
		(68,'154102','ELABORACIÓN Y VENTA DE PRODUCTOS DE PANADERIA NO INCLUIDOS EN CANASTA BÁSICA '),
		(69,'154104','ELABORACIÓN Y VENTA DE PRODUCTOS DE PANADERIA INCLUIDOS EN CANASTA BÁSICA'),
		(70,'154201','ELABORACIÓN  Y VENTA DE AZÚCAR Y PRODUCTOS DERIVADOS DE LA CAÑA DE AZÚCAR'),
		(71,'154301','ELABORACION DE CACAO'),
		(72,'154302','ELABORACIÓN DE CHOCOLATE'),
		(73,'154303','ELABORACION DE DULCES, GOLOSINAS Y CONSERVAS EN AZUCAR'),
		(74,'154401','ELABORACIÓN DE MACARRONES, FIDEOS, ALCUZCUZ Y PRODUCTOS FARINÁCEOS SIMILARES'),
		(75,'154402','ELABORACIÓN DE MACARRONES, FIDEOS, ALCUZCUZ Y PRODUCTOS FARINÁCEOS SIMILARES INCLUIDOS EN CANASTA BÁSICA'),
		(76,'154902','FABRICACION DE PRODUCTOS ALIMENTICIOS PREPARADOS N.C.P. (NO CONTEMPLADOS EN OTRA'),
		(77,'154903','FABRICACIóN DE HIELO'),
		(78,'154904','FABRICACIóN DE CAFÉ (EXCEPTO EL ENVASADO, ENLATADO, SOLUBLEY EL DESCAFEINADO)'),
		(79,'154905','ELABORACION DE PRODUCTOS DE MAIZ EXENTOS DE IVA'),
		(80,'154908','ELABORACION DE PRODUCTOS DE MAIZ GRAVADOS CON IVA'),
		(81,'155101','ELABORAC. RECTIFICAC MEZCLA BEBIDAS ALCOHOLICAS/PROD. DE ALCOHOL ETÍLICO (SUSTAN'),
		(82,'155103','ELABORACION DE BEBIDAS CON PORCENTAJE DE ALCOHOL POR VOLUMEN MENOR AL 15%.'),
		(83,'155301','ELABORACION DE BEBIDAS MALTEADAS Y DE MALTA NO ARTESANALES'),
		(84,'155302','ELABORACION ARTESANAL DE BEBIDAS MALTEADAS Y DE MALTA.'),
		(85,'155403','ELABORACION DE BEBIDAS NO ALCOHOLICAS / GASEOSAS / AGUA MINERAL Y DE MANANTIAL'),
		(86,'155404','ELABORACION DE CONCENTRADO PARA BEBIDAS NATURALES Y GASEOSAS'),
		(87,'160001','ELABORACION DE PRODUCTOS DE TABACO'),
		(88,'171101','FABRICACION DE TODO TIPO DE TELAS Y/O HILOS'),
		(89,'171201','MAQUILA DE PRODUCTOS TEXTILES'),
		(90,'171202','HIDROFUGADO/IMPERMEABILIZADO'),
		(91,'172101','FABRICACION DE ARTICULOS CONFECCIONADOS DE MATERIALES TEXTILES, EXEPTO PRENDAS D'),
		(92,'172103','DISEÑO ARTISTICO PARA COSTURA (MOLDES)'),
		(93,'172201','FABRICACION DE TAPICES Y ALFOMBRAS'),
		(94,'172301','FABRICACION DE CUERDAS, CORDELES,BRAMANTES Y REDES'),
		(95,'172302','FABRICACION DE HILOS Y CUERDAS PARA USO AGRICOLA Y PESCA'),
		(96,'172902','SERVICIO DE BORDADO A MANO O A MAQUINA'),
		(97,'173001','FABRICACION DE TEJDOS Y ARTICULOS DE PUNTO Y GANCHILLO'),
		(98,'181001','SERVICIOS DE COSTURA Y SASTRERIA (COSTURERAS Y SASTRES)'),
		(99,'181002','FABRICACION DE PRENDAS DE VESTIR (ROPA DE TODO TIPO)'),
		(100,'182001','ADOBO Y TEÑIDO DE PIELES; FABRICACIÓN DE ARTÍCULOS DE PIEL NATURAL O ARTIFICIAL'),
		(101,'191101','FABRICACIóN DE MALETAS, BOLSOS DE MANO Y ARTICULOS SIMILARES'),
		(102,'192001','FABRICACION DE  TODO TIPO DE ZAPATOS EXEPTO EL ORTOPEDICO'),
		(103,'201002','ASERRADO Y ACEPILLADURA DE MADERA'),
		(104,'202101','FABRICACIÓN DE HOJAS DE MADERA PARA ENCHAPADO Y TABLEROS COMO PLYWOOD, DURPANEL'),
		(105,'202201','FABRICACION DE PARTES Y PIEZAS DE CARPINTERIA PARA EDIFICIOS Y CONSTRUCCIONES'),
		(106,'202301','FABRICACIóN DE TARIMAS'),
		(107,'202902','FABRICACIóN  Y/O VENTA DE ATAUDES (CAJAS MORTUORIAS, FERETROS).'),
		(108,'210101','FABRICACION DE PAPEL Y CARTON Y ENVASES DE PAPEL Y CARTON'),
		(109,'210201','FABRICACION DE OTROS ARTICULOS DE PAPEL Y CARTON'),
		(110,'221101','EDICION DE LIBROS DE TEXTOS'),
		(111,'221201','EDICION DE PERIODICOS, REVISTAS Y OTRAS PUBLICACIONES PERIODICAS'),
		(112,'221901','ARTES GRáFICAS'),
		(113,'222102','TIPOGRAFIA  Y/O LITOGRAFIA (IMPRENTA)'),
		(114,'222104','IMPRESIÓN DIGITAL'),
		(115,'222105','SERIGRAFIA'),
		(116,'223001','REPRODUCCION DE GRABACIONES'),
		(117,'232001','REFINERIAS DE PETROLEO'),
		(118,'232002','FABRICACIÓN DE PRODUCTOS DIVERSOS DERIVADOS DEL PETRÓLEO'),
		(119,'232005','ELABORACION DE BIOCOMBUSTIBLES'),
		(120,'241101','FABRICACION DE SUSTANCIAS QUIMICAS Y GASES INDUSTRIALES EXCEPTO ABONOS'),
		(121,'241201','FABRICACION DE ABONOS Y COMPUESTOS DE NITROGENO (ORGANICOS Y QUIMICOS)'),
		(122,'241301','FABRICACION DE PLASTICOS Y CAUCHO SINTETICO EN FORMAS PRIMARIAS'),
		(123,'241302','FABRICACIÓN DE RESINAS'),
		(124,'242101','FABRICACION DE PLAGUICIDAS Y OTROS PRODUCTOS QUIMICOS DE USO AGROPECUARIO'),
		(125,'242201','FABRICACIÓN DE PINTURAS, BARNICES Y PRODUCTOS DE REVESTIMI-TO SIMILARES'),
		(126,'242202','FABRICACION DE TINTAS DE IMPRENTAS'),
		(127,'242301','FABRICACIÓN PRODUCTOS FARMACÉUTICOS, SUSTANCIAS QUÍMICAS  Y PRODUCTOS BOTANICOS'),
		(128,'242401','FABRICACION JABONES, DETERGENTES, PREPARADOS DE TOCADOR, PARA LIMPIAR PULIR '),
		(129,'242404','FABRICACION DE JABONES DE TOCADOR'),
		(130,'242405','FABRICACION DE JABONES PARA LAVAR (EXCEPTO PREPARACIONES TENSO ACTIVAS USADAS'),
		(131,'242406','FABRICACION DE CERAS, ABRILLANTADORES, LUSTRADORES Y PREPARACIONES SIMILARES'),
		(132,'242901','FABRICACION DE OTROS PRODUCTOS QUIMICOS.'),
		(133,'242902','FABRICACION DE PEGAMENTOS Y ADHESIVOS'),
		(134,'242903','REPARACION DE ARMAS DE FUEGO'),
		(135,'243001','FABRICACION DE FIBRAS SINTETICAS'),
		(136,'251101','FABRICACION DE LLANTAS Y CUBIERTAS PARA EQUIPO Y MAQUINARIAMOVIL'),
		(137,'251102','REENCAUCHADORA DE LLANTAS'),
		(138,'251901','FABRICACIÓN DE OTROS PRODUCTOS DE CAUCHO'),
		(139,'252001','FABRICACION DE ARTICULOS DE PLASTICO'),
		(140,'261001','FABRICACION DE VIDRIO Y PRODUCTOS DE VIDRIO'),
		(141,'269101','FABRICACIÓN PRODUCTOS DE CERÁMICA BARRO LOZA Y/O PORCELANANO REFRACTARIA USO NO ESTRUCTURAL'),
		(142,'269201','FABRICACION DE PRODUCTOS DE CERAMICA REFRACTARIA'),
		(143,'269301','FABRICACIÓN DE PRODUCTOS DE ARCILLA Y CERÁMICA NO REFRACTARIAS PARA USO ESTRUCTURAL'),
		(144,'269401','FABRICACIÓN Y/O VENTA DE CEMENTO, CAL Y YESO'),
		(145,'269501','FABRICACION DE ARTÍCULOS DE CEMENTO, YESO Y HORMIGON PARA LA CONSTRUCCION'),
		(146,'269601','CORTE, TALLADO Y ACABADO DE LA PIEDRA'),
		(147,'269901','FABRICACIÓN DE OTROS PRODUCTOS MINERALES NO METÁLICOS N.C.P.'),
		(148,'271001','INDUSTRIAS BÁSICAS DE HIERRO Y ACERO'),
		(149,'272001','FABRICACIÓN DE PRODUCTOS PRIMARIOS DE METALES PRECIOSOS Y METALES NO FERROSOS'),
		(150,'273101','FUNDICION DE HIERRO Y ACERO'),
		(151,'273201','FUNDICIÓN DE METALES NO FERROSOS'),
		(152,'281101','FABRICACION DE PRODUCTOS METALICOS PARA USO ESTRUCTURAL'),
		(153,'281201','FABRICACIÓN DE TANQUES, DEPÓSITOS Y RECIPIENTES DE METAL'),
		(154,'281301','FABRICACIÓN GENERADORES DE VAPOR, EXCEPTO CALDERAS DE AGUA CALIENTE PARA CALEFACCIÓN'),
		(155,'289201','SERVICIOS DE SOLDADURA'),
		(156,'289202','SERVICIO DE TRATAMIENTO Y REVESTIMIENTO DE TODO TIPO DE MATERIALES'),
		(157,'289302','HOJALATERIA'),
		(158,'289304','TALLER DE MECáNICA DE PRECISIÓN'),
		(159,'289305','FABRICACION DE PIEZAS, ARTICULOS Y ACCESORIOS DE METAL INCLUYE LAS CERRAJERIAS.'),
		(160,'289306','SERVICIOS DE TORNO (MECANICA DE PRECISION)'),
		(161,'289902','FABRICACIÓN DE OTROS PRODUCTOS ELABORADOS DE METAL N.C.P.'),
		(162,'290001','SERVICIO DE REPARACION DE MAQUINARIA Y EQUIPO.'),
		(163,'290003','SERVICIO DE MANTENIMIENTO DE MAQUINARIA Y EQUIPO.'),
		(164,'291101','FABRICACIÓN DE MOTORES Y TURBINAS, EXCEPTO MOTORES PARA AERONAVES, VEHÍCULOS AUT'),
		(165,'291201','FABRICACIÓN DE BOMBAS, COMPRESORES, GRIFOS Y VÁLVULAS'),
		(166,'291202','VENTA DE BOMBAS DE AGUA'),
		(167,'291401','FABRICACIÓN DE HORNOS Y QUEMADORES'),
		(168,'291901','REPARACION DE EQUIPO DE REFRIGERACION Y CONGELACION'),
		(169,'291903','INSTALACION Y/O MANTENIMIENTO DE EQUIPO DE REFRIGERACION Y CONGELACION'),
		(170,'292101','FABRICACION DE MAQUINARIA AGRICOLA'),
		(171,'292201','FABRICACIÓN DE MÁQUINAS HERRAMIENTA'),
		(172,'292301','FABRICACIÓN DE MAQUINARIA METALÚRGICA'),
		(173,'292401','FABRICACIÓN DE MAQUINARIA PARA LA EXPLOTACIÓN DE MINAS Y CANTERAS '),
		(174,'292402','PERFORACION DE POZOS'),
		(175,'292501','FABRICACIÓN DE MAQUINARIA PARA LA ELABORACIÓN DE ALIMENTOS,BEBIDAS Y TABACO'),
		(176,'292601','FABRICACIÓN MAQUINARIA PARA LA ELABORACIÓN DE PRODUCTOS TEXTILES, PRENDAS DE VES'),
		(177,'292701','FABRICACIÓN DE ARMAS Y MUNICIONES'),
		(178,'292901','FABRICACIÓN DE OTROS TIPOS DE MAQUINARIA DE USO ESPECIAL'),
		(179,'293001','FABRICACIÓN DE APARATOS DE USO DOMÉSTICO N.C.P.'),
		(180,'300001','FABRICACIÓN DE MAQUINARIA DE OFICINA, CONTABILIDAD E INFORMÁTICA'),
		(181,'311001','FABRICACION DE MOTORES, GENERADORES Y TRANSFORMADORES ELECTRICOS, PARTES Y ACCESORIOS'),
		(182,'313001','FABRICACIÓN DE HILOS Y CABLES AISLADOS'),
		(183,'314001','FABRICACION DE PILAS, BATERIAS Y ACUMULADORES'),
		(184,'315001','FABRICACION DE LAMPARAS ELECTRICAS Y EQUIPO DE ILUMINACION'),
		(185,'319001','FABRICACIÓN DE OTROS TIPOS DE EQUIPO ELÉCTRICO N.C.P.'),
		(186,'321001','MANUFACTURA O FABRICACION DE OTROS COMPONENTES ELECTRONICOS'),
		(187,'321002','DISEÑO DE COMPONENTES ELECTRONICOS'),
		(188,'322001','FABRICACION DE TRANSMISORES Y RECEPTORES DE RADIO Y TELEVISION'),
		(189,'331101','FABRICACION  DE PROTESIS EN GENERAL'),
		(190,'331102','FABRICACION Y COMERCIALIZACION DE EQUIPO MEDICO'),
		(191,'331103','FABRICACION Y COMERCIALIZACION DE ZAPATOS ORTOPEDICOS'),
		(192,'331201','FABRICACIÓN DE EQUIPOS PARA MEDIR, VERIFICAR, Y NAVEGAR Y DE EQUIPOS DE CONTROL'),
		(193,'332001','FABRICACIÓN DE INSTRUMENTOS OPTICOS Y EQUIPO FOTOGRÁFICO'),
		(194,'333001','FABRICACION DE RELOJES DE TODO TIPO'),
		(195,'341001','FABRICACIÓN DE VEHÍCULOS AUTOMOTORES'),
		(196,'342001','FABRICACIÓN DE CARROCERÍAS PARA VEHÍCULOS AUTOMOTORES, REMOLQUES Y SEMIREMOLQUES'),
		(197,'342002','FABRICACION DE CARROCERIAS PARA CAMIONES DE LAS PARTIDAS ARANCELARIAS 870790002'),
		(198,'343001','FABRICACIÓN DE PARTES, PIEZAS Y ACCESORIOS PARA VEHÍCULOS AUTOMOTORES'),
		(199,'351101','SERVICIO DE REPARACION DE EMBARCACIONES Y SUS PARTES Y/O ESTRUCTURAS FLOTANTES'),
		(200,'351102','CONSTRUCCION DE EMBARCACIONES Y ESTRUCTURAS FLOTANTES'),
		(201,'351103','VENTA DE EMBARCACIONES DE MOTOR Y VELA'),
		(202,'352001','FABRICACIÓN DE LOCOMOTORAS Y  MATERIAL RODANTE PARA FERROCARRILES Y TRANVÍAS'),
		(203,'353001','FABRICACIÓN DE AERONAVES Y NAVES ESPACIALES Y MAQUINARIA CONEXA'),
		(204,'359101','FABRICACIÓN DE MOTOCICLETAS, PARTES, PIEZAS Y SUS ACCESORIOS'),
		(205,'359201','FABRICACION DE BICICLETAS Y SILLAS DE RUEDAS, PARTES, PIEZAS Y SUS ACCESORIOS'),
		(206,'359901','FABRICACIÓN DE OTROS TIPOS DE EQUIPO DE TRANSPORTE N.C.P.'),
		(207,'361001','FABRICACION Y/O REPARACION DE MUEBLES Y ACCESORIOS (INCLUYECOLCHONES)'),
		(208,'361002','REPARACIóN DE TAPICERíA'),
		(209,'361004','MAQUILA DE MUEBLES'),
		(210,'369101','FABRICACIÓN DE JOYAS, BISUTERIA Y ARTÍCULOS CONEXOS'),
		(211,'369201','FABRICACION DE INSTRUMENTOS MUSICALES, PARTES Y PIEZAS Y SUS ACCESORIOS'),
		(212,'369301','FABRICACIÓN DE ARTÍCULOS DE DEPORTE'),
		(213,'369401','FABRICACION DE JUEGOS Y JUGUETES'),
		(214,'369901','FABRICACIÓN DE  ESCOBAS'),
		(215,'369902','FABRICACIóN DE VELAS (CANDELAS) EXCEPTO LAS PERFUMADAS, COLOREADAS Y DECORADAS.'),
		(216,'369903','FABRICACIóN DE VELAS COLOREADAS, PERFUMADAS Y DECORADAS'),
		(217,'369904','FABRICACION DE SELLOS DE MANO, METAL O HULE (CAUCHO)'),
		(218,'371001','RECICLAJE DE DESPERDICIOS Y DESECHOS METáLICOS'),
		(219,'372001','RECICLAJE DE OTRO TIPO DE MATERIALES N.C.P.'),
		(220,'372003','RECICLAJE DE PAPEL Y PLASTICO Y MATERIALES RELACIONADOS'),
		(221,'401002','GENERACIÓN Y/O DISTRIBUCION DE ENERGIA ELECTRICA (HIDRAULICA,CONVENCIONAL, TERMI'),
		(222,'401004','FABRICACION, ENSAMBLE Y VENTA DE SISTEMAS PARA EL APROVECHAMIENTO DE ENERGIAS RE'),
		(223,'402001','FABRICACIÓN DE GAS; DISTRIBUCIÓN DE COMBUSTIBLES GASEOSOS POR TUBERÍAS'),
		(224,'402002','INSTALACION Y VENTA DE TANQUES PARA GAS'),
		(225,'403001','SUMINISTRO DE VAPOR Y AIRE ACONDICIONADO'),
		(226,'410001','CAPTACION, TRATAMIENTO Y DISTRIBUCION DE AGUA'),
		(227,'451001','DEMOLICION DE EDIFICIOS Y OTRAS ESTRUCTURAS'),
		(228,'451002','PREPARACIóN DE TERRENOS'),
		(229,'452002','CONSTRUCCION DE EDIFICIOS, APARTAMENTOS, CONDOMINIOS  Y CASAS DE HABITACION'),
		(230,'452003','MANTENIMIENTO, REPARACION Y AMPLIACIONES DE EDIFICIOS, APARTAMENTOS, CONDOMINIOS'),
		(231,'452005','CONSTRUCCION Y MANTENIMIENTO DE CARRETERAS Y OTRAS VIAS'),
		(232,'452006','ACTIVIDADES DE CONSTRUCCIÓN ESPECIALES'),
		(233,'453001','REPARACION DE AIRE ACONDICIONADO'),
		(234,'453002','REPARACIóN  DE ASCENSORES(ELEVADORES)'),
		(235,'453003','VENTA E INSTALACION DE ALARMAS Y OTROS SISTEMAS ELECTRICOS'),
		(236,'453004','REPARACION DE CABLEADO DE COMUNICACIONES'),
		(237,'453005','INSTALACIÓN DE ALARMAS Y OTROS SISTEMAS DE SEGURIDAD'),
		(238,'453006','INSTALACION Y MANTENIMIENTO DE AIRE ACONDICIONADO'),
		(239,'453007','INSTALACION Y MANTENIMIENTO DE ASCENSORES (ELEVADORES)'),
		(240,'453008','INSTALACION Y MANTENIMIENTO DE CABLEADO DE COMUNICACIONES Y/O ENERGIA ELECTRICA'),
		(241,'453009','REPARACION DE PORTONES ELECTRICOS'),
		(242,'453010','INSTALACION Y MANTENIMIENTO DE PORTONES ELECTRICOS'),
		(243,'453011','INSTALACION Y MANTENIMIENTO DE CONMUTADORES Y OTROS SISTEMAS PARA TELECOMUNICACI'),
		(244,'454001','SERVICIOS DE TERMINACIÓN Y ACABADO DE EDIFICIOS'),
		(245,'455001','ALQUILER DE EQUIPO DE CONSTRUCCION O DEMOLICION CON OPERADORES'),
		(246,'501001','VENTA AL POR MAYOR Y MENOR DE VEHICULOS NUEVOS Y/O USADOS (PARTIDAS ARANCELARIAS 8702 Y 8704)'),
		(247,'501002','VENTA AL POR MAYOR Y MENOR DE VEHICULOS NUEVOS Y/O USADOS, EXCEPTO PARTIDAS ARANCELARIAS 8702 Y 8704'),
		(248,'502001','LAVADO, ENCERADO Y PULIDO DE AUTOMÓVILES (LAVA CAR)'),
		(249,'502002','SERVICIOS DE  ALINEAMIENTO Y REPARACIÓN DE LLANTAS'),
		(250,'502003','SERVICIO DE REPARACION DE TODA CLASE DE VEHICULOS Y SUS PARTES'),
		(251,'502004','SERVICIO DE ENDEREZADO Y PINTURA PARA TODA CLASE DE VEHICULO'),
		(252,'502007','AUTODECORACIóN'),
		(253,'502008','SERVICIO DE REVISION TECNICA VEHICULAR(DIAGNOSTICOS POR ESCANER)'),
		(254,'503002','VENTA DE REPUESTOS USADOS PARA AUTOMOVILES'),
		(255,'503004','VENTA DE REPUESTOS NUEVOS PARA AUTOMOVILES'),
		(256,'503005','COMERCIALIZACION DE LLANTAS (NEUMATICAS) PARA VEHICULOS AUTOMOTORES'),
		(257,'503006','ACTIVIDADES DE DESARME DE VEHICULOS Y VENTA DE REPUESTOS'),
		(258,'504001','VENTA DE MOTOCICLETAS'),
		(259,'504003','VENTA DE PARTES O ACCESORIOS DE MOTOCICLETAS'),
		(260,'504004','SERVICIO DE REPARACION DE TODA CLASE DE MOTOCICLETAS Y SUS PARTES'),
		(261,'505001','VENTA DE COMBUSTIBLES (CONOCIDAS COMO GASOLINERAS O BOMBAS)'),
		(262,'505002','VENTA DE LUBRICANTES, ACEITES, GRASAS Y PRODUCTOS DE LIMPIEZA PARA AUTOMOTORES'),
		(263,'505003','COMERCIO DE COMBUSTIBLE SIN PUNTO FIJO (PEDDLER)'),
		(264,'511001','COMISIONISTAS, AGENTES DE VENTAS, ORGANIZADORES DE SUBASTAS,TIQUETERAS, ETC.'),
		(265,'512102','VENTA AL POR MAYOR DE FLORES Y PLANTAS DE TODO TIPO'),
		(266,'512201','VENTA AL POR MAYOR DE OTROS ALIMENTOS INCLUIDOS EN CANASTA BÁSICA'),
		(267,'512202','VENTA AL POR MAYOR DE FRUTAS Y VERDURAS FRESCAS INCLUIDAS EN CANASTA BÁSICA'),
		(268,'512204','COMERCIO AL POR MAYOR DE PRODUCTOS LáCTEOS'),
		(269,'512208','COMERCIO AL POR MAYOR DE PRODUCTOS DE CONFITERíA'),
		(270,'512209','COMERCIO AL POR MAYOR DE PRODUCTOS DE TABACO'),
		(271,'512210','COMERCIO AL POR MAYOR DE BEBIDAS CON CONTENIDO ALCOHOLICO (IMPORTADORES)'),
		(272,'512211','COMERCIO AL POR MAYOR DE OTROS ALIMENTOS N.C.P. GRAVADOS CON IVA'),
		(273,'512213','COMERCIO AL POR MAYOR DE BEBIDAS GASEOSAS Y CARBONATADAS'),
		(274,'512214','VENTA AL POR MAYOR DE CAFE (EXCEPTO EL ENVASADO, ENLATADO, SOLUBLE, DESCAFEINADO'),
		(275,'512217','COMERCIO AL POR MAYOR DE VINOS. BEBIDAS FERMENTADAS Y NO FERMENTADAS'),
		(276,'512225','COMERCIO AL POR MAYOR DE BEBIDAS NO ALCOHOLICAS (JUGOS DEFRUTAS, VEGETALES) Y AG'),
		(277,'512232','COMERCIO AL POR MAYOR DE PRODUCTOS SUSTITUTOS DEL AZUCAR'),
		(278,'512233','VENTA AL POR MAYOR DE CARNES Y PRODUCTOS CÁRNICOS'),
		(279,'512234','COMERCIO AL POR MAYOR DE CERVEZA IMPORTADA'),
		(280,'512235','VENTA AL POR MAYOR DE FRUTAS Y VERDURAS FRESCAS NO INCLUIDAS EN CANASTA BÁSICA'),
		(281,'513101','VENTA AL POR MAYOR DE  CALZADO, PRODUCTOS TEXTíLES; PRENDAS DE VESTIR'),
		(282,'513501','VENTA AL POR MAYOR DE PREPARADOS Y/O ARTICULOS PARA LA LIMPIEZA DE USO GENERAL'),
		(283,'513602','VENTA AL POR MAYOR DE PRODUCTOS VETERINARIOS GRAVADOS CON IVA'),
		(284,'513604','VENTA AL POR MAYOR EQUIPO MÉDICO ACCESORIO MEDICAMENTO PRODUC FARMACEUTICO GRAVADOS IVA'),
		(285,'513710','VENTA AL POR MAYOR DE LIBROS (EXCEPTO EN MEDIOS ELECTRÓNICOS)'),
		(286,'513901','VENTA AL POR MENOR DE ARTICULOS DEPORTIVOS'),
		(287,'513904','VENTA AL POR MAYOR DE SUMINISTROS Y ARTíCULOS DE LIBRERíA'),
		(288,'513906','VENTA AL POR MAYOR DE ARTICULOS, ARTEFACTOS, DISCOS Y MUEBLES PARA EL HOGAR'),
		(289,'513907','VENTA AL POR MAYOR DE DISCOS COMPACTOS Y OTROS DISPOSITIVOSDE GRABACION'),
		(290,'513910','VENTA AL POR MAYOR DE TODO TIPO DE ARTICULOS POR CATALOGO'),
		(291,'514101','VENTA AL POR MAYOR DE COMBUSTIBLES SOLIDOS (LEÑA Y SIMILARES)'),
		(292,'514103','VENTA AL POR MAYOR DE COMBUSTIBLES SOLIDOS (CARBON)'),
		(293,'514201','VENTA AL POR MAYOR DE METALES Y MINERALES METALÍFEROS'),
		(294,'514304','VENTA AL POR MAYOR DE EQUIPO DE AIRE ACONDICIONADO Y CALENTADORES (ELECTRICOS)'),
		(295,'514308','VENTA AL POR MAYOR DE  MATERIALES PARA LA CONTRUCCION, ARTICULOS DE FERRETERIA,'),
		(296,'514701','VENTA AL POR MAYOR DE PRODUCTOS,SUSTANCIAS O REACTIVOS QUIMICOS Y SOLVENTES'),
		(297,'514901','VENTA AL POR MAYOR  Y AL POR MENOR DE CHATARRA'),
		(298,'514903','VENTA AL POR MAYOR DE PRODUCTOS PARA USO AGROPECUARIO Y VENTA DESECHOS ORGANICOS'),
		(299,'515001','VENTA AL POR MAYOR DE MAQUINARIA Y EQUIPO INDUSTRIAL, DE CONSTRUCCION, INGENIERIA'),
		(300,'515002','VENTA AL POR MAYOR DE REPUESTOS Y/O ACCESORIOS PARA MAQUINARIA Y EQUIPO AGROPECUARIO'),
		(301,'515003','VENTA AL POR MAYOR DE MAQUINARIA Y EQUIPO AGROPECUARIO'),
		(302,'515004','VENTA AL POR MAYOR DE EXTINTORES Y EQUIPO SIMILAR'),
		(303,'515201','VENTA AL  POR MAYOR DE EQUIPO DE COMPUTO, SUS PARTES Y ACCESORIOS'),
		(304,'515203','VENTA AL POR MAYOR DE EQUIPO Y SUMINISTROS DE OFICINA'),
		(305,'519003','VENTA AL POR MAYOR DE EQUIPO, ARTICULOS Y ACCESORIOS DE BELLEZA, COSMETICOS E HIGIENE'),
		(306,'519005','VENTA AL POR MAYOR DE EQUIPO PARA CAMPO DE JUEGOS (PLAY)'),
		(307,'519006','DISTRIBUCION Y COMERCIALIZACION AL POR MAYOR DE MATERIAL DEEMPAQUE'),
		(308,'519010','COMERCIALIZACION Y DISTRIBUCION AL POR MAYOR DE ALIMENTOS PREPARADOS PARA ANIMAL'),
		(309,'519011','COMERCIO AL POR MAYOR DE EQUIPO Y ACCESORIOS PARA PESCA DEPORTIVA O ARTESANAL'),
		(310,'519013','COMERCIALIZACION AL POR MAYOR DE SUPLEMENTOS ALIMENTICIOS'),
		(311,'519014','VENTA AL POR MAYOR DE POLEN Y/O  SEMILLAS PARA USO AGRICOLA'),
		(312,'519015','VENTA AL POR MAYOR DE OTROS PRODUCTOS NO ESPECIALIZADOS'),
		(313,'519016','VENTA AL POR MAYOR DE ARTICULOS Y ACCESORIOS ELECTRONICOS'),
		(314,'521101','VENTA AL POR MENOR EN SUPERMERCADOS, ALMACENES Y SIMILARES DE PRODUCTOS ALIMENTICIOS'),
		(315,'521102','VENTA AL POR MENOR DE ESPECIAS, SALSAS Y CONDIMENTOS'),
		(316,'521201','VENTA AL POR MENOR EN TIENDAS POR DEPARTAMENTOS'),
		(317,'521202','PULPERíAS ( MINI-SUPER)(SIN CANTINA)'),
		(318,'521901','VENTA AL POR MENOR EN TIENDAS POR DEPARTAMENTOS'),
		(319,'522001','COMERCIO AL POR MENOR DE CONFITES Y OTROS PRODUCTOS RELACIONADOS (CONFITERIA)'),
		(320,'522002','VENTA AL POR MENOR DE CARNE DE BOVINO, PORCINO Y AVES (ADEMÁS EMBUTIDOS), INCLUIDOS EN CANASTA BÁSICA'),
		(321,'522003','MACROBIóTICAS'),
		(322,'522004','VENTA AL POR MENOR DE PRUTAS Y VERDURAS FRESCAS INCLUIDAS EN CANASTA BÁSICA'),
		(323,'522005','VENTA AL POR MENOR DE PESCADOS INCLUIDOS EN CANASTA BÁSICA'),
		(324,'522007','LICORERíAS Y/O DEPOSITO DE LICORES (VENTA AL POR MENOR)'),
		(325,'522008','VENTA AL POR MENOR DE HUEVOS DE GALLINA INCLUIDOS EN LA CANASTA BASICA'),
		(326,'522009','VENTA AL POR MENOR DE PRUTAS Y VERDURAS FRESCAS, NO INCLUIDAS EN CANASTA BÁSICA'),
		(327,'522011','PREPARACIÓN, SERVICIO Y VENTA DE FRUTAS PICADAS Y BEBIDAS DE FRUTAS Y/O LEGUMBRE'),
		(328,'522012','COMERCIO AL POR MENOR DE BEBIDAS GASEOSAS Y CARBONATADAS'),
		(329,'522013','COMERCIO AL POR MENOR DE AGUA EMBOTELLADA'),
		(330,'522016','COMERCIO AL POR MENOR DE PRODUCTOS DE TABACO'),
		(331,'522017','VENTA AL POR MENOR DE CARNE DE BOVINO, PORCINO Y AVES (ADEMÁS EMBUTIDOS) NO INCLUIDOS EN CANASTA BÁSICA'),
		(332,'522018','VENTA AL POR MENOR  DE PESCADOS Y MARISCOS , ASI COMO OTROS PRODUCTOS DE ORIGEN MARINO'),
		(333,'522020','VENTA AL POR MENOR DE OTROS ALIMENTOS INCLUIDOS EN CANASTA BÁSICA'),
		(334,'522021','VENTA AL POR MENOR DE OTROS ALIMENTOS NO INCLUIDOS EN CANASTA BÁSICA'),
		(335,'523101','FARMACIAS'),
		(336,'523102','VENTA AL POR MENOR DE COSMETICOS Y PERFUMERIA'),
		(337,'523103','VENTA AL POR MENOR DE PRÓTESIS EN GENERAL'),
		(338,'523201','BAZARES'),
		(339,'523202','VENTA AL POR MENOR DE ROPA (BOUTIQUE)'),
		(340,'523203','VENTA AL POR MENOR DE CALZADO (ZAPATAERIAS)'),
		(341,'523204','PASAMANERíAS'),
		(342,'523205','VENTA AL POR MENOR DE PRENDAS DE VESTIR, ROPA Y ZAPATOS (TIENDAS)'),
		(343,'523206','SERVICIO DE FOTOCOPIADORA'),
		(344,'523207','VENTA AL POR MENOR DE ARTíCULOS DE CUERO (EXCEPTO CALZADO)'),
		(345,'523208','VENTA AL POR MENOR DE PRODUCTOS TEXTILES (TELAS)'),
		(346,'523209','VENTA AL POR MENOR DE MATERIALES PARA CALZADO'),
		(347,'523301','COMERCIO AL POR MENOR DE OBJETOS DE CERAMICA  Y PORCELANA'),
		(348,'523302','VENTA AL POR MENOR DE CRISTALERíA'),
		(349,'523303','VENTA AL POR MENOR DE DISCOS, CD(S) Y OTROS SIMILARES'),
		(350,'523304','VENTA AL POR MENOR DE ELECTRODOMESTICOS, MUEBLES Y ARTICULOS PARA EL HOGAR'),
		(351,'523306','VENTA AL POR MENOR DE ANTIGUEDADES'),
		(352,'523308','VENTA AL POR MENOR DE INSTRUMENTOS MUSICALES, PARTES Y ACCESORIOS'),
		(353,'523309','VENTA DE CUADROS PINTURAS HECHAS POR PINTORES NACIONALES Y EXTRANJEROS PRODUCIDO'),
		(354,'523401','VENTA AL POR MENOR DE DEPóSITO DE MADERA'),
		(355,'523402','VENTA AL POR MENOR ARTÍCULOS DE FERRETERÍA PINTURAS MADERA Y MATERIALES PARA LA CONSTRUCCIÓN'),
		(356,'523403','VENTA DE PINTURAS'),
		(357,'523404','VENTA AL POR MENOR DE VIDRIO PARA LA CONSTRUCCION'),
		(358,'523405','VENTA AL POR MENOR DE MATERIALES PARA LA CONSTRUCCION'),
		(359,'523406','VENTA AL POR MENOR DE ARTICULOS ELECTRONICOS, ELECTRICOS Y SIMILARES'),
		(360,'523407','VENTA AL POR MENOR DE PLYWOOD'),
		(361,'523501','VENTA AL POR MENOR REALIZADA DENTRO DEL DEPÓSITO LIBRE COMERCIAL DE GOLFITO.'),
		(362,'523601','COMERCIO AL POR MENOR DE COMPUTADORAS, ACCESORIOS, MICROCOMPONENTES Y PAQUETES '),
		(363,'523701','LIBRERÍAS'),
		(364,'523702','VENTA AL POR MENOR DE REVISTAS/PERIODICOS EN PUESTOS DE VENTAS O MERCADOS'),
		(365,'523703','VENTA AL POR MENOR DE LIBROS (EXCEPTO EN MEDIOS ELECTRÓNICOS)'),
		(366,'523801','VENTA AL POR MENOR DE ARMAS (ARMERIAS)'),
		(367,'523803','VENTA AL POR MENOR DE MAQUINARIA Y EQUIPO DE TODO TIPO Y ARTICULOS CONEXOS'),
		(368,'523805','VENTA AL POR MENOR DE PURIFICADORES DE AGUA, SUS PARTES Y REPUESTOS'),
		(369,'523806','VENTA AL POR MENOR DE MAQUINARIA INDUSTRIAL USADA'),
		(370,'523807','VENTA AL POR MENOR DE REPUESTOS NUEVOS PARA MAQUINARIA, EQUIPO Y OTROS'),
		(371,'523808','VENTA AL POR MENOR DE REPUESTOS USADOS PARA MAQUINARIA, EQUIPO Y OTROS'),
		(372,'523901','VENTA AL POR MENOR DE OTROS PRODUCTOS EN ALMACENES ESPECIALIZADOS'),
		(373,'523902','VENTA AL POR MENOR DE SUMINISTROS Y/0 EQUIPO DE OFICINA'),
		(374,'523903','VENTA AL POR MENOR DE FLORES'),
		(375,'523904','SERVICIOS DE REPARACION DE JOYERIA Y RELOJERIA EN GENERAL'),
		(376,'523905','VENTA AL POR MENOR DE JOYERíA, RELOJERIA Y BISUTERIA'),
		(377,'523906','VENTA AL POR MENOR CELULARES ACCESORIOS EQUIPO Y ART PARA COMUNICACIONES '),
		(378,'523907','FLORISTERIA'),
		(379,'523908','COMERCIO AL POR MENOR DE ANIMALES DOMÉSTICOS PARA CONSUMO HUMANO'),
		(380,'523909','VENTA DE PRODUCTOS DE ARTESANIA Y SOUVENIR'),
		(381,'523910','VENTA AL POR MENOR DE JUGUETES Y/O ARTICULOS DE ESPARCIMIENTO'),
		(382,'523911','VENTA AL POR MENOR DE OTROS PRODUCTOS NUEVOS EN COMERCIOS ESPECIALIZADOS NO INCLUIDOS EN CANASTA BÁSICA'),
		(383,'523912','VENTA AL POR MENOR Y MAYOR DE PRODUCTOS E INSUMOS AGROPECUARIOS'),
		(384,'523913','VENTA AL POR MENOR DE ANIMALES DOMESTICOS PARA MASCOTAS.'),
		(385,'523915','DISTRIBUCION Y VENTA DE GAS EN CILINDRO'),
		(386,'523916','COMERCIO AL POR MENOR DE ALIMENTOS Y PRODUCTOS N.C.P. EXENTOS DE IVA'),
		(387,'523917','VENTA AL POR MENOR DE BICICLETAS Y SUS ACCESORIOS'),
		(388,'523918','VENTA AL POR MENOR DE EQUIPO DE AUDIO Y VIDEO'),
		(389,'523919','VENTA AL POR MENOR DE ARTICULOS Y ACCESORIOS ORTOPEDICOS'),
		(390,'523920','VENTA AL POR MENOR DE ANTEOJOS Y ARTICULOS OPTICOS (OPTICA)'),
		(391,'523921','VENTA AL POR MENOR DE CAJAS REGISTRADORAS, CALCULADORAS O MAQUINAS DE CONTABILIDAD'),
		(392,'523922','VENTA AL POR MENOR DE POLEN Y SEMILLAS'),
		(393,'523923','VENTA AL POR MENOR DE REPUESTOS PARA ELECTRODOMESTICOS'),
		(394,'523925','VENTA AL POR MENOR DE EXTINTORES'),
		(395,'523926','MANTENIMIENTO DE EXTINTORES'),
		(396,'523927','REPARACION EQUIPO DE AUDIO Y VIDEO'),
		(397,'523929','VENTA DE PERROS ENTRENADOS PARA SEGURIDAD'),
		(398,'523930','VENTA AL POR MENOR DE PIEZAS DE BAMBU'),
		(399,'523935','VENTA AL POR MENOR DE OTROS PRODUCTOS NUEVOS EN COMERCIOS ESPECIALIZADOS NO INCLUIDOS EN CANASTA BÁSICA'),
		(400,'524001','VENTA DE TODO TIPO DE ARTICULOS USADOS'),
		(401,'524002','CASA DE EMPEñO Y AFíN'),
		(402,'524004','VENTA DE LIBROS USADOS'),
		(403,'524005','VENTA DE MONEDAS, BILLETES,ESTAMPILLAS (NUEVAS Y USADAS) PARA COLECCION '),
		(404,'525101','VENTA AL POR MENOR DE TODO TIPO DE ARTICULOS POR CATALOGO'),
		(405,'525201','VENTA DE REVISTAS/PERIÓDICOS (PUESTOS CALLEJEROS)'),
		(406,'525301','VENTA AL POR MENOR DE LOTERIA Y SIMILARES'),
		(407,'525303','EMISIÓN DE LA LOTERIA NACIONAL Y SIMILARES'),
		(408,'525901','VENTA DE ALFOMBRAS Y TAPICES'),
		(409,'525902','VENTA AMBULANTE DE ARTICULOS PARA EL HOGAR'),
		(410,'525903','VENTA DE LIBROS A DOMICILIO'),
		(411,'525904','TRABAJOS DE  MANUALIDADES'),
		(412,'525905','VENTA AL POR MENOR DE TARJETAS TELEFóNICAS, PINES, TIEMPO AIRE Y SIMILARES'),
		(413,'526001','SERVICIOS DE REPARACIÓN DE ZAPATOS'),
		(414,'526002','REPARACION DE MUEBLES Y ACCESORIOS DOMESTICOS'),
		(415,'526003','REPARACION DE BICICLETAS'),
		(416,'526005','ELECTRICISTA, SERVICIOS'),
		(417,'526006','REPARACION DE ARTICULOS ELECTRICOS'),
		(418,'526008','REPARACION Y MANTENIMIENTO DE PERSIANAS Y CORTINAS'),
		(419,'526010','INSTALACION Y MANTENIMIENTO DE PERSIANAS Y CORTINAS'),
		(420,'526011','REPARACION Y MANTENIMIENTO DE EQUIPO PARA TELECOMUNICACIONES'),
		(421,'526012','INSTALACION Y MANTENIMIENTO DE PISCINAS,JACUZZIS Y SIMILARES'),
		(422,'551002','HOTEL'),
		(423,'551004','MOTEL Y/O SERVICIO DE HABITACION OCASIONAL, ALBERGUES, POSADAS Y SIMILARES'),
		(424,'551005','SERVICIO DE SALAS VIP Y PREMIUN EN AEROPUERTOS'),
		(425,'552001','BARES, CANTINAS O TABERNAS'),
		(426,'552002','CAFETERíAS'),
		(427,'552003','SERVICIOS DE CATERING SERVICE'),
		(428,'552004','SERVICIO DE RESTAURANTE, CAFETERIAS, SODAS Y OTROS EXPENDIOS DE COMIDA'),
		(429,'552005','SODAS'),
		(430,'552007','OTROS EXPENDIOS DE COMIDAS'),
		(431,'601001','SERVICIO DE TRANSPORTE POR VÍA FÉRREA'),
		(432,'602001','SERVICIO DE TRANSPORTE DE CARGA POR VÍA TERRESTRE'),
		(433,'602002','TRANSPORTE DE  PRODUCTOS DERIVADOS DEL PETROLEO'),
		(434,'602101','TRANSPORTE REGULAR DE ESTUDIANTES Y EMPLEADOS POR VIA TERRESTRE'),
		(435,'602102','TRANSPORTE REGULAR DE PASAJEROS POR VIA TERRESTRE'),
		(436,'602201','TRANSPORTE NO REGULAR DE PASAJEROS POR VIA TERRESTRE (EXCURSIONES)'),
		(437,'602202','SERVICIO DE TAXI'),
		(438,'602203','SERVICIO ESPECIAL ESTABLE DE TAXI (SEETAXI)'),
		(439,'602301','SERVICIO DE ACARREO Y DISTRIBUCION DE TODO TIPO DE MERCANCIA (INCLUYE LA MUDANZA'),
		(440,'602302','SERVICIOS DE GRUA'),
		(441,'602304','SERVICIO DE MUDANZA INTERNACIONAL'),
		(442,'603001','SERVICIO DE TRANSPORTE POR TUBERÍAS'),
		(443,'611001','TRANSPORTE DE CARGA POR  VIA ACUATICA'),
		(444,'611002','TRANSPORTE DE CABOTAJE DE PASAJEROS POR  VIA ACUATICA'),
		(445,'621001','SERVICIO DE TRANSPORTE DE CARGA POR VIA AEREA'),
		(446,'621002','SERVICIO DE TRANSPORTE AEREO REGULADO DE PASAJEROS (LINEAS AEREAS)'),
		(447,'621003','SERVICIO DE PILOTAJE'),
		(448,'622001','SERVICIO DE TRANSPORTE AEREO NO REGULAR DE PASAJEROS'),
		(449,'630001','PRESTACION DE SERVICIOS DE TRANSITO AEREO'),
		(450,'630101','SERVICIO DE CONSOLIDACION DE CARGA Y DESCARGA'),
		(451,'630201','SERVICIOS DE ALMACENAJE'),
		(452,'630301','PARQUEOS/ESTACIONAMIENTO DE VEHíCULOS'),
		(453,'630302','FUNCIONAMIENTO DE INSTALACIONES TERMINALES COMO PUERTOS, MUELLES Y AEROPUERTOS'),
		(454,'630303','PERMISO DE PASO POR PROPIEDADES PRIVADAS (PEAJE)'),
		(455,'630401','ACTIVIDADES DE AGENCIAS DE VIAJES '),
		(456,'630403','ACTIVIDADES DE OPERADORES TURÍSTICOS (GUIA  DE TURISMO)'),
		(457,'630405','EXPLOTACIóN DE ACTIVIDADES TURíSTICAS(CUYA PRESTACION NO SEA REALIZADA POR UN CE'),
		(458,'630901','ALMACENES FISCALES (DEPOSITOS ADUANEROS) Y ESTACIONAMIENTOSTRANSITORIOS'),
		(459,'630903','AGENCIA DE TRANSPORTE (NAVIERA)'),
		(460,'630904','AGENTE ADUANERO INDEPENDIENTE Y AGENCIA DE ADUANAS'),
		(461,'641101','ACTIVIDADES POSTALES Y DE CORREO'),
		(462,'642001','SERVICIOS DE RADIO-MENSAJES, RADIOLOCALIZADORES Y SIMILARES'),
		(463,'642002','SERVICIO DE TELEVISION POR CABLE, SATELITE U OTROS SISTEMA S SIMILARES'),
		(464,'642003','MANTENIMIENTO DE REDES DE TELECOMUNICACION'),
		(465,'642004','SERVICIO DE RADIO FRECUENCIA'),
		(466,'642005','SERVICIOS TELEFONICOS, TELEGRAFICOS Y POR TELEX'),
		(467,'642007','VENTA DE ESPACIO EN EL CABLE SUBMARINO'),
		(468,'642008','SERVICIO DE TRANSMISION  DE  DATOS, TEXTO, SONIDO, VOZ  Y VIDEO POR MEDIO DE LA'),
		(469,'642009','SERVICIOS DIGITALES O DE TELECOMUNICACIONES E INTANGIBLES PRESTADOS DESDE EL EXTERIOR'),
		(470,'651101','BANCA CENTRAL'),
		(471,'651901','COOPERATIVAS DE AHORRO Y CREDITO'),
		(472,'651903','BANCOS ESTATALES (EXCEPTO EL BANCO CENTRAL)'),
		(473,'651904','ENTIDADES FINANCIERAS PRIVADAS (BANCOS)'),
		(474,'651905','SERVICIO DE ENVIO Y RECIBO DE DINERO'),
		(475,'651906','INSTITUCIONES DE AHORRO Y CREDITO PARA VIVIENDA'),
		(476,'659101','ARRENDAMIENTO OPERATIVO EN FUNCION FINANCIERA CON OPCION DECOMPRA O RENOVACION ('),
		(477,'659201','ENTIDADES FINANCIERAS DISTINTAS AL SISTEMA BANCARIO NACIONAL'),
		(478,'659202','EMISORAS Y PROCESADORAS DE TARJETAS DE CREDITO'),
		(479,'659901','SERVICIO DE PRESTAMO (PRESTAMISTAS)'),
		(480,'659902','FIDEICOMISOS Y/O ADMINISTRADORES DE FONDOS DE INVERSION'),
		(481,'659903','SOCIEDADES DE INVERSION MOBILIARIA'),
		(482,'659904','SERVICIO RECUPERACION DE DEUDAS (FACTORE0)'),
		(483,'659905','FONDOS DE INVERSION'),
		(484,'659906','ACTIVIDADES DE SOCIEDADES DE CARTERA (HOLDING)'),
		(485,'659907','INGRESOS POR INTERESES DIFERENTES AL COMERCIO DEL PRESTAMO'),
		(486,'659908','FONDOS DE LA LEY DEL SISTEMA DE BANCA PARA EL DESARROLLO'),
		(487,'659909','FIDEICOMISOS DE LA LEY DEL SISTEMA DE BANCA PARA EL DESARROLLO'),
		(488,'660101','SEGUROS DE VIDA'),
		(489,'660201','OPERADORAS DE PENSIONES'),
		(490,'660202','FONDOS DE OPERADORAS DE PENSIONES EXENTOS DEL 8%'),
		(491,'660301','AGENTES DE SEGUROS'),
		(492,'660302','COMERCIALIZADORES DE SEGUROS'),
		(493,'660303','SEGUROS GENERALES'),
		(494,'671101','ADMINISTRACIÓN DE MERCADOS FINANCIEROS'),
		(495,'671201','CORREDORES DE BOLSA'),
		(496,'671202','PUESTOS DE BOLSA'),
		(497,'671901','PUESTOS Y/O CASAS DE CAMBIO DE MONEDA EXTRANJERA'),
		(498,'671902','ASESORES FINANCIEROS Y ACTIVIDADES AUXILIARES DE LA INTERMEDICIACION FINANCIERA'),
		(499,'672001','ACTIVIDADES AUXILIARES DE LA FINANCIACIÓN DE PLANES DE SEGUROS Y DE PENSIONES'),
		(500,'701001','ACTIVIDADES INMOBILIARIAS (ALQUILER DE VIVIENDA SUPERIOR A 1.5 SALARIO BASE)'),
		(501,'701002','ALQUILER DE LOCALES COMERCIALES Y CENTROS COMERCIALES'),
		(502,'701003','COMPRA Y VENTA DE PROPIEDADES (INVERSIONISTAS)'),
		(503,'701004','ALQUILER DE EDIFICIOS Y PROPIEDADES DIFERENTES A CASAS DE HABITACION'),
		(504,'701005','ALQUILER DE MARCAS REGISTRADAS'),
		(505,'701006','ALQUILER DE PATENTES (DE LICORES, UNICAMENTE)'),
		(506,'701007','EXPLOTACION DE FRANQUICIAS'),
		(507,'701008','ACTIVIDADES INMOBILIARIAS (ALQUILER DE VIVIENDA INFERIOR A 1.5 SALARIO BASE)'),
		(508,'702001','AGENTES O CORREDORES DE BIENES RAICES'),
		(509,'702003','ADMINISTRACIóN, MANTENIMIENTO, REPARACIóN Y LIMPIEZA DE LOS SERVICIOS Y BIENES COMUNES DE LA PROPIEDAD EN CONDOMINIO'),
		(510,'711101','ALQUILER DE AUTOMOVILES  DE TODO TIPO'),
		(511,'711102','ALQUILER DE MOTOCICLETA/SERVICIO'),
		(512,'711103','ALQUILER DE EQUIPO DE TRANSPORTE POR VIA TERRESTRE, ACUATICA O AEREA'),
		(513,'711105','ALQUILER DE CARRITOS DE GOLF Y OTROS'),
		(514,'712101','ALQUILER DE MAQUINARIA Y EQUIPO PARA USO AGRICOLA'),
		(515,'712201','ALQUILER DE MAQUINARIA Y EQUIPO DE CONSTRUCCIÓN E INGENIERÍA CIVIL'),
		(516,'712301','SERVICIOS DE INTERNET EN LOCALES PUBLICOS ( CAFE INTERNET )'),
		(517,'712302','ALQUILER DE MAQUINARIA Y EQUIPO DE OFICINA'),
		(518,'712901','ALQUILER DE MáQUINAS EXPENDEDORAS DE ALIMENTOS Y OTROS'),
		(519,'712902','ALQUILER DE MAQUINAS DE ENTRETENIMIENTO (CON MONEDAS)'),
		(520,'712904','ALQUILER DE PELICULAS CINEMATOGRAFICAS (VIDEO CLUB) Y/O VIDEO JUEGOS'),
		(521,'712905','ALQUILER DE EQUIPO PARA RADIO, TELEVISIóN Y COMUNICACIONES'),
		(522,'712906','ALQUILER DE EQUIPO PARA DISPENSAR BILLETES (CAJERO AUTOMá-TICO)'),
		(523,'712907','ALQUILER MAQUINARIA Y EQUIPO PARA LA ELABORACION Y/O MANTENIMIENTO DE PRODUCTOS'),
		(524,'712909','ALQUILER DE OTROS TIPOS DE MAQUINARIA Y EQUIPO PARA USO COMERCIAL'),
		(525,'712910','ALQUILER DE MAQUINARIA Y/O EQUIPO PARA USO INDUSTRIAL'),
		(526,'712911','ALQUILER DE RAMPAS Y SIMILARES'),
		(527,'713001','ALQUILER DE MENAJE'),
		(528,'713002','ALQUILER DE EQUIPO RECREATIVO Y DEPORTIVO'),
		(529,'713003','ALQUILER DE EQUIPO MEDICO Y ARTICULOS CONEXOS'),
		(530,'713012','ALQUILER DE TRAJES DE TODO TIPO'),
		(531,'713013','ALQUILER DE EQUIPO Y UTENSILIOS PARA EVENTOS ESPECIALES'),
		(532,'713053','ALQUILER DE ANIMALES (CABALLOS, SERPIENTES, ETC)'),
		(533,'721001','CONSULTORES INFORMÁTICOS'),
		(534,'722003','DISEÑADOR GRAFICO, DE SOFWARE Y PAGINAS WEB'),
		(535,'723001','PROCESAMIENTO DE DATOS'),
		(536,'724001','ACTIVIDADES RELACIONADAS CON BASES DE DATOS'),
		(537,'725001','REPARACION DE EQUIPO DE COMPUTO'),
		(538,'725002','MANTENIMIENTO DE EQUIPO DE COMPUTO'),
		(539,'725003','REPARACIÓN CAJAS REGISTRADORAS, CALCULADORAS, MAQUINAS DE CONTABILIDAD Y EQUIPO'),
		(540,'725004','MANTENIMIENTO DE CAJAS REGISTRADORAS, CALCULADORAS, MAQUINAS DE CONTABILIDAD'),
		(541,'729001','OTRAS ACTIVIDADES DE INFORMÁTICA'),
		(542,'731001','INVESTIGACION Y DESARROLLO EXPERIMENTAL EN EL CAMPO DE LAS CIENCIAS NATURALES '),
		(543,'731003','METEOROLOGO POR CUENTA PROPIA'),
		(544,'732001','INVESTIGACIONES Y DESARROLLO EXPERIMENTAL EN EL CAMPO DE LAS CIENCIAS SOCIALES '),
		(545,'741101','BUFFETE DE ABOGADO, NOTARIO, ASESOR LEGAL'),
		(546,'741203','ACTVIDAD DE CONTABILIDAD (CONTADORES), TENEDURIA DE LIBROS,AUDITORIA Y ASESOR FINANCIERO'),
		(547,'741301','ASESORES EN MERCADEO Y VENTAS'),
		(548,'741302','ENCUESTAS DE OPINIóN PúBLICA'),
		(549,'741401','ECONOMISTAS'),
		(550,'741402','ASESORAMIENTO EMPRESARIAL Y EN MATERIA DE GESTIÓN (HANDLING)'),
		(551,'741407','ASESOR ADUANERO'),
		(552,'741408','CORRESPONSAL DE RADIO, TELEVISION Y PRENSA ESCRITA'),
		(553,'742102','ACTIVIDADES DE ARQUITECTURA E INGENIERÍA'),
		(554,'742105','SERVICIOS DE CONSULTORIA EN MANTENIMIENTO INDUSTRIAL Y MECANICO'),
		(555,'742107','QUIMICOS'),
		(556,'742108','ACTIVIDADES DE GEOGRAFIA Y/O GEOLOGIA'),
		(557,'742110','BIOLOGO POR CUENTA PROPIA'),
		(558,'742112','DIBUJANTE ARQUITECTONICO Y/O PLANOS DE CONSTRUCCION'),
		(559,'742201','ENSAYOS Y ANÁLISIS TÉCNICOS'),
		(560,'742202','PROFESIONALES EN TECNOLOGÍA DE ALIMENTOS'),
		(561,'743001','PUBLICIDAD'),
		(562,'743004','SERVICIOS DE PUBLICIDAD'),
		(563,'743005','ALQUILER DE ESPACIOS PUBLICITARIOS'),
		(564,'743006','PUBLICIDAD A TRAVES DE MEDIOS ELECTRONICOS'),
		(565,'749101','SERVICIOS DE ADMINISTRACIÓN DE PERSONAL'),
		(566,'749102','SERVICIOS DE BIBLIOTECOLOGíA'),
		(567,'749201','SERVICIOS DE INVESTIGACION, SEGURIDAD PRIVADA, AGENCIAS Y CONSULTORES'),
		(568,'749204','INSTRUCTOR DE TIRO (MANEJO DE ARMAS).'),
		(569,'749301','SERVICIOS DE FUMIGACIóN (NO AGRíCOLA)'),
		(570,'749302','SERVICIOS DE LIMPIEZA (INTERIORES Y EXTERIORES)'),
		(571,'749401','ESTUDIOS FOTOGRAFICOS'),
		(572,'749402','SERVICIO DE FOTOGRAFIA (FOTOGRAFO)'),
		(573,'749501','SERVICIOS DE ENVASE Y EMPAQUE'),
		(574,'749901','SERVICIO DE FOTOCOPIADO Y OTROS'),
		(575,'749902','SERVICIOS DE LEVANTADO DE TEXTO Y/O CORRECCION DE TEXTOS Y OTROS'),
		(576,'749903','SERVICIOS SECRETARIALES Y/O OFICINISTA'),
		(577,'749904','SERVICIO DE DISEÑO O DECORACION DE INTERIORES (POR CUENTA PROPIA).'),
		(578,'749905','SERVICIO DE CONTESTACIóN DE TELéFONOS (CALL CENTER)'),
		(579,'749906','SERVICIOS DE TRADUCTOR'),
		(580,'749907','SERVICIO DE INSPECCION DE TODO TIPO DE MERCADERIAS Y ACTIVOS(INCLUYE DROGAS)'),
		(581,'749908','SERVICIO DE RECOLECCION DE MONEDAS DE LOS TELEFONOS PUBLICOS'),
		(582,'749911','AGENCIAS FOTOGRAFICAS POR CATALOGO'),
		(583,'749914','SERVCICIO DE COBRANZA DE RECIBOS PUBLICOS Y OTROS'),
		(584,'749917','MODELAJE PROFESIONAL (MODELO)'),
		(585,'749918','AGENCIAS DE COBRO Y CALIFICACION CREDITICIA'),
		(586,'751101','ACTIVIDADES DE LA ADMINISTRACION PUBLICA EN GENERAL'),
		(587,'751102','ACTIVIDADES DE LA ADMINISTRACIÓN PÚBLICA EN GENERAL, NO SUJETAS AL IMPUESTO SOBR'),
		(588,'751103','MUNICIPALIDADES Y CONCEJOS DE DISTRITO'),
		(589,'751106','ACTIVIDADES DE LA ADMINISTRACION PUBLICA EN GENERAL GRAVADAS CON IVA'),
		(590,'751201','JUNTAS DE EDUCACIÓN, COMEDORES ESCOLARES, PATRONATOS, COOPERATIVAS ESCOLARES, CO'),
		(591,'751202','COMITES CANTONALES DE DEPORTES Y RECREACION'),
		(592,'751301','ACTIVIDADES DEL SECTOR PúBLICO RELACIONADAS CON LA INFRAESTRUCTURA'),
		(593,'751302','FONDOS PUBLICOS (ACTIVIDADES DEL SECTOR PUBLICO)'),
		(594,'751401','ACTIVIDADES DE SERVICIOS AUXILIARES PARA LA ADMINISTRACIÓN PÚBLICA EN GENERAL'),
		(595,'752101','RELACIONES EXTERIORES'),
		(596,'752201','ACTIVIDADES DE DEFENSA'),
		(597,'752301','ACTIVIDADES DE MANTENIMIENTO DEL ORDEN PÚBLICO Y DE SEGURIDAD'),
		(598,'752302','SERVICIOS DE VIGILANCIA O CONTROL PORTUARIA, COSTERA, AEREA,Y FRONTERIZA'),
		(599,'753001','ACTIVIDADES DE PLANES DE SEGURIDAD SOCIAL DE AFILIACIÓN OBLIGATORIA'),
		(600,'801001','ENSEÑANZA PREESCOLAR Y PRIMARIA PRIVADA'),
		(601,'801002','ENSEÑANZA PREESCOLAR Y PRIMARIA PRIVADA NO AUTORIZADOS POR EL MEP'),
		(602,'802101','ENSEÑANZA SECUNDARIA PRIVADA'),
		(603,'802102','ENSEÑANZA SECUNDARIA PRIVADA NO AUTORIZADA POR EL MEP'),
		(604,'802201','ENSEÑANZA DE FORMACIÓN TÉCNICA Y PARAUNIVERSITARIA'),
		(605,'802202','ENSEÑANZA SECUNDARIA DE FORMACIÓN TÉCNICA Y PROFESIONAL NO AUTORIZADA POR EL MEP'),
		(606,'803002','ENSEÑANZA SUPERIOR PRIVADA (UNIVERSIDADES)'),
		(607,'803003','ENSEÑANZA SUPERIOR PUBLICA (UNIVERSIDADES)'),
		(608,'809001','ESCUELAS COMERCIALES (NO ESTATALES)'),
		(609,'809004','PROFESOR POR CUENTA PROPIA'),
		(610,'809005','ENSEÑANZA CULTURAL'),
		(611,'809006','ENSEÑANZA DE LA DE SEGURIDAD PRIVADA'),
		(612,'809007','ESCUELA Y AGENCIA DE MODELOS'),
		(613,'851101','ACTIVIDADES DE HOSPITALES '),
		(614,'851201','GINECÓLOGO'),
		(615,'851202','SERVICOS DE MÉDICO GENERAL'),
		(616,'851203','NEURÓLOGOS'),
		(617,'851204','SERVICIOS DE OFTALMOLOGO U OCULISTA, OPTOMETRISTA Y/O OPTICO'),
		(618,'851206','ORTOPEDISTA (CONSULTA PRIVADA)'),
		(619,'851207','SERVICIOS DE ODONTOLOGO Y CONEXOS'),
		(620,'851208','OTORRINOLARINGOLOGIA, AUDIOLOGíA Y SERVICIOS CONEXOS.'),
		(621,'851209','FARMACEUTICO O BOTICARIO'),
		(622,'851210','CARDIOLOGOS'),
		(623,'851212','ONCOLOGOS'),
		(624,'851213','REUMATOLOGO'),
		(625,'851901','MEDICINA ALTERNATIVA'),
		(626,'851902','FÍSICO TERAPISTA'),
		(627,'851905','PSICOLOGO'),
		(628,'851906','PSIQUIATRÍA'),
		(629,'851907','SERVICIOS DE ENFERMERÍA'),
		(630,'851908','LABORATORIOS MÉDICOS - CLÍNICOS'),
		(631,'851909','SERVICIOS DE RADIOLOGÍA, ANESTESIOLOGIA Y OTROS'),
		(632,'851910','TRANSPORTE EN AMBULANCIA TERRESTRE Y AEREO (SERVICIO PRIVADO)'),
		(633,'851911','PROFESIONALES EN SALUD OCUPACIONAL'),
		(634,'851912','NUTRICIONISTA'),
		(635,'851913','PROFESIONALES EN EDUCACION ESPECIAL'),
		(636,'851914','SERVICIOS DE PARAMEDICOS'),
		(637,'851915','SERVICIOS DE ESTERILIZACIÓN DE PRODUCTOS MÉDICOS Y FARMACÉUTICOS'),
		(638,'851916','OTRAS ACTIVIDADES RELACIONADAS CON LA SALUD HUMANA(BANCO DE SANGRE…)'),
		(639,'851917','CIRUJANO PLÁSTICO'),
		(640,'853101','FUNDACIONES DE BIEN SOCIAL'),
		(641,'853102','FUNDACIONES DE SERVICIO SOCIAL (PRIVADAS)'),
		(642,'853103','RESIDENCIAS UNIVERSITARIAS CON SERVICIOS INTEGRADOS'),
		(643,'853201','GUARDERIAS/CENTROS INFANTILES Y SERVICIOS SOCIALES'),
		(644,'853202','SERVICIOS SOCIALES DE REDES DE CUIDO  Y CENTROS DE ATENCIÓN DE ADULTOS MAYORES'),
		(645,'853301','ASOCIACIONES DECLARADAS DE UTILIDAD PUBLICA POR EL PODER EJECUTIVO'),
		(646,'900001','ASESORAMIENTO Y ELIMINACION DE DESPERDICIOS, SANEAMIENTO (EXCEPTO LIMPIEZA DE TANQUES)'),
		(647,'900002','LIMPIEZA DE TANQUES SEPTICOS Y AGUAS REDISUALES'),
		(648,'911101','ACTIVIDADES DE ORGANIZACIONES EMPRESARIALES Y DE EMPLEADORES'),
		(649,'911102','CAMARAS DE COMERCIO'),
		(650,'911201','ACTIVIDADES DE ORGANIZACIONES PROFESIONALES'),
		(651,'912001','ASOCIACIONES SOLIDARISTAS'),
		(652,'912002','ACTIVIDADES DE SINDICATOS'),
		(653,'919101','ACTIVIDADES DE ORGANIZACIONES RELIGIOSAS'),
		(654,'919201','ACTIVIDADES DE ORGANIZACIONES POLÍTICAS'),
		(655,'919901','ASOCIACION DE DESARROLLO COMUNAL Y/O SERVICIOS COMUNITARIOS'),
		(656,'919903','ASOCIACIONES O ENTIDADES CON FINES CULTURALES, SOCIALES, RECREATIVOS, ARTESANALE'),
		(657,'919906','ASOCIACIONES ADMINISTRADORAS DE LOS SISTEMAS DE ACUEDUCTOS Y ALCANTARILLADOS (ASADAS)'),
		(658,'921101','ACTIVIDADES PRODUCCION POSTPRODUCCION Y DISTRIBUCION DE PELICULAS CINEMATOGRAFIC'),
		(659,'921201','EXHIBICIÓN DE FILMES Y VIDEOCINTAS  (SALAS DE CINE)'),
		(660,'921301','TRANSMISIONES DE RADIO'),
		(661,'921302','PROGRAMACIÓN Y TRANSMISIONES DE TELEVISIÓN'),
		(662,'921303','TRANSMISIONES DE RADIO CULTURALES SEGÚN LEY GENERAL DE TELECOMUNICACIONES'),
		(663,'921401','TEATROS (EXPLOTACION)'),
		(664,'921402','ACTIVIDADES MUSICALES Y ARTÍSTICAS (SERVICIOS)'),
		(665,'921403','PERIODISTA POR CUENTA PROPIA'),
		(666,'921501','CANALES DE TELEVISIóN'),
		(667,'921902','VENTA AL POR MENOR DE DISCOS, GRABACIONES DE MUSICA Y DE VIDEO'),
		(668,'921903','SALóN DE BAILE Y DISCOTECA'),
		(669,'921904','SALON DE PATINES'),
		(670,'922001','ACTIVIDADES DE AGENCIAS DE NOTICIAS'),
		(671,'923101','ACTIVIDADES DE BIBLIOTECAS Y ARCHIVOS'),
		(672,'923201','GALERIAS DE ARTE'),
		(673,'923202','SERVICIOS DE RESTAURACION DE OBRAS DE ARTE'),
		(674,'923301','ACTIVIDADES DE JARDINES BOTANICOS, ZOOLOGICOS, PARQUES NACIONALES Y RESERVAS NACIONALES'),
		(675,'924101','GIMNASIOS'),
		(676,'924102','EXPLOTACIÓN DE PISCINAS O ALBERCAS DE BAÑO'),
		(677,'924103','ENTRENADOR, INSTRUCTORES Y/O PREPARADORES FISICOS POR CUENTA PROPIA'),
		(678,'924104','ACTIVIDADES DE ESCUELAS Y CLUBES DEPORTIVOS'),
		(679,'924105','EXPLOTACION DE INSTALACIONES Y CAMPOS DEPORTIVOS'),
		(680,'924106','ACTIVIDADES DEPORTIVAS Y OTRAS POR CUENTA PROPIA'),
		(681,'924107','ESPECTÁCULOS DEPORTIVOS'),
		(682,'924901','NIGHT CLUB/CABARETTE'),
		(683,'924902','EXPLOTACIÓN DE CASINOS (INCLUYE MESAS DE JUEGOS Y MAQUINAS TRAGAMONEDAS)'),
		(684,'924903','PESCA DEPORTIVA'),
		(685,'924904','ACTIVIDADES DE JUEGOS DE BILLAR, POOL Y OTROS SIMILARES'),
		(686,'924905','SALA DE VIDEO JUEGOS'),
		(687,'924906','GRABACIÓN DE SONIDO (MUSICA, ETC.) EN DISCOS GRAMOFONICOS YEN CINTA MAGNETOFONIC'),
		(688,'924907','SERVICIO DE ENLACE DE LLAMADAS Y CASAS DE APUESTAS ELECTRONICAS (SPORTBOOKS)'),
		(689,'924908','ESPECTACULOS PUBLICOS EN GENERAL EXCEPTO LOS DEPORTIVOS Y EL TEATRO'),
		(690,'924909','OTRAS ACTIVIDADES DE ESPARCIMIENTO'),
		(691,'924910','EXPLOTACIÓN DE JUEGOS DE ÁZAR NO ELECTRÓNICOS (INCLUYE MÁQUINAS TRAGAMONEDAS'),
		(692,'930101','SERVICIOS DE LAVANDERíA DE TODO TIPO'),
		(693,'930102','SERVICIO DE TEÑIDO DE PRENDAS DE VESTIR'),
		(694,'930103','SERVICIO DE LIMPIEZA Y LAVADO DE MUEBLES'),
		(695,'930202','SALONES DE BELLEZA, PELUQUERIA Y BARBERIA'),
		(696,'930301','SERVICIOS FUNEBRES Y ACTIVIDADES CONEXAS'),
		(697,'930302','CEMENTERIO PUBLICO (JUNTA ADMINISTRATIVA)'),
		(698,'930303','CEMENTERIO O CAMPOSANTO PRIVADO'),
		(699,'930901','SALAS DE MASAJES'),
		(700,'930903','OTRAS ACTIVIDADES DE SERVICIOS PERSONALES N.C.P.'),
		(701,'930904','CENTRO O SALA DE BRONCEADO'),
		(702,'930905','PELUQUERíA Y SALA DE ESTÉTICA PARA ANIMALES'),
		(703,'930906','SERVICIO DE TATUAJE Y PIERCING'),
		(704,'930907','SERVICIO DE DISCO MOVIL'),
		(705,'930908','SERVICIO DE HERRADURA'),
		(706,'930915','ALBERGUE Y CUIDO DE ANIMALES A DOMICILIO'),
		(707,'950001','HOGARES PRIVADOS CON SERVICIO DOMÉSTICO'),
		(708,'960104','IMPUESTO A LAS PERSONAS JURIDICAS'),
		(709,'960105','ACTIVIDADES PREOPERATIVAS O DE ORGANIZACION'),
		(710,'960106','EXPORTADORES DE MERCANCIAS EXENTAS DEL IMPUESTO DE VENTAS'),
		(711,'960107','PRODUCTORES DE MERCANCIAS EXENTAS DEL IMPUESTO DE VENTAS'),
		(712,'960108','COMERCIALIZADORES DE MERCANCIAS EXENTAS DEL IMPUESTO DE VENTAS'),
		(713,'960109','DISTRIBUIDORES DE MERCANCIAS EXENTAS DEL IMPUESTO DE VENTAS'),
		(714,'990001','ACTVIDADES DE ORGANIZACIONES Y ORGANOS EXTRATERRITORIALES, INCLUYE LAS EMBAJADAS'),
		(715,'522010','VENTA DE EMBUTIDOS Y CARNES (RES, POLLO, CERDO)INCLUIDAS ENLA CANASTA BASICA'),
		(716,'551001','ALQUILER DE BIENES INMUEBLES DE USO HABITACIONAL POR PERIODOS INFERIORES A UN MES (CASAS DE ESTANCIA TRANSITORIA, CASA DE HUESPEDES, CABINAS, CAMPAMENTOS, ENTRE OTROS)'),
		(717,'702002','ARRENDAMIENTO O ALQUILER DE BIENES INMUEBLES MEDIANTE CONTRATO VERBAL O ESCRITO.'),
		(718,'852001','SERVICIOS VETERINARIOS CON VENTA DE PRODUCTOS GRAVADOS CON VENTAS'),
		(719,'852002','SERVICIOS MEDICOS VETERINARIOS'),
		(720,'960102','ACTIVIDAD NO ESPECIFICADA (RENTA Y VENTAS PARA EFECTOS DE MI GRACION)');

SET @TOTAL = 0;

SELECT @TOTAL = COUNT(*)
FROM @ACTIVIDADES;

SET @LINEA = 1;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @Codigo_Actividad = NULL;
		SET @Nombre = NULL;

		SELECT 	@Codigo_Actividad = Codigo_Actividad,
				@Nombre = Nombre
		FROM @ACTIVIDADES
		WHERE NroUnico = @LINEA;

		IF (SELECT NroUnico FROM FEActividades_Eco  WHERE NroUnico = @LINEA) IS NULL
			BEGIN
				INSERT INTO FEActividades_Eco (NroUnico, Codigo_Actividad, Nombre)
				VALUES	(@LINEA,@Codigo_Actividad,@Nombre);
			END;
		ELSE
			BEGIN
				UPDATE FEActividades_Eco SET Codigo_Actividad = @Codigo_Actividad, Nombre = @Nombre									     
				WHERE NroUnico = @LINEA;
			END;

		SET @LINEA +=1;
	END;

DELETE FROM @ACTIVIDADES;
GO
/***TABLAS ADICIONALES SAINT***/
DECLARE
@NUMGRP_1 AS INT,  --- TABLA Datos_para_facturacion_electronica ---
@NUMGRP_2 AS INT,  --- TABLA Exoneración_y_Datos_Adicionales ---
@NUMGRP_3 AS INT,  --- TABLA Correos_y_Datos_Adiconales ---
@NUMGRP_4 AS INT,  --- TABLA Corrige_datos_de_Cliente_FE ---
@NUMGRP_5 AS INT,  --- TABLA Exoneracion ---
@NUMGRP_6 AS INT,  --- Datos_Adicionales (Productos) ---
@NUMGRP_7 AS INT,  --- Datos_Adicionales (Servicios) ---
@NUMGRP_8 AS INT,  --- Otros Cargos (Servicios Ventas) ---
@NUMGRP_9 AS INT,  --- Adicionales (Productos Ventas) ---
@NUMGRP_10 AS INT,  --- Otros Datos (Compras) ---
@NUMGRP_11 AS INT,  --- Otros Cargos (Servicios Ventas) ---
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_2_M AS VARCHAR(50),
@NOM_TABLE_3_M AS VARCHAR(50),
@NOM_TABLE_4_M AS VARCHAR(50),
@NOM_TABLE_5_M AS VARCHAR(50),
@NOM_TABLE_6_M AS VARCHAR(50),
@NOM_TABLE_7_M AS VARCHAR(50),
@NOM_TABLE_8_M AS VARCHAR(50),
@NOM_TABLE_9_M AS VARCHAR(50),
@NOM_TABLE_10_M AS VARCHAR(50),
@NOM_TABLE_11_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NOM_TABLE_2 AS VARCHAR(50),
@NOM_TABLE_3 AS VARCHAR(50),
@NOM_TABLE_4 AS VARCHAR(50),
@NOM_TABLE_5 AS VARCHAR(50),
@NOM_TABLE_6 AS VARCHAR(50),
@NOM_TABLE_7 AS VARCHAR(50),
@NOM_TABLE_8 AS VARCHAR(50),
@NOM_TABLE_9 AS VARCHAR(50),
@NOM_TABLE_10 AS VARCHAR(50),
@NOM_TABLE_11 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@NombreGrp_2 AS VARCHAR(50),
@NombreGrp_3 AS VARCHAR(50),
@NombreGrp_4 AS VARCHAR(50),
@NombreGrp_5 AS VARCHAR(50),
@NombreGrp_6 AS VARCHAR(50),
@NombreGrp_7 AS VARCHAR(50),
@NombreGrp_8 AS VARCHAR(50),
@NombreGrp_9 AS VARCHAR(50),
@NombreGrp_10 AS VARCHAR(50),
@NombreGrp_11 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX),
@Nombre_Actividad_1 AS VARCHAR(260),
@Nombre_Actividad_2 AS VARCHAR(260),
@Nombre_Actividad_3 AS VARCHAR(260),
@Nombre_Actividad_4 AS VARCHAR(260),
@Nombre_Actividad_5 AS VARCHAR(260),
@Nombre_Actividad_6 AS VARCHAR(260),
@Nombre_Actividad_7 AS VARCHAR(260),
@Nombre_Actividad_8 AS VARCHAR(260),
@Nombre_Actividad_9 AS VARCHAR(260),
@Nombre_Actividad_10 AS VARCHAR(260),
@CodInst AS INT,
@TOTAL AS INT,
@LINEA AS INT,
@LISTADO AS NVARCHAR(MAX),
@tablename varchar(60),
@fieldname varchar(60),
@datatype varchar(60),
@contador int

DECLARE @FIELS TABLE (id int, fieldname varchar(60), datatype varchar(60))

/*CREANDO TABLA ADICIONAL Datos_para_facturacion_electronica*/
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SAOPER';
SET @NombreGrp_1 = 'Datos_para_facturacion_electronica';

IF NOT EXISTS (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1)
	BEGIN
		SELECT @NUMGRP_1 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M;

		SET @NUMGRP_1 = @NUMGRP_1 + 1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, @NombreGrp_1,	'Datos para facturación electrónica', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NroOper = '800') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, 800, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_1);
		Delete SAFIEL Where (TableName=@NOM_TABLE_1);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_1, CONCAT('Operaciones:',@NombreGrp_1));

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_1,'CodOper','CodOper','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN
		SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Operaciones:',@NombreGrp_1)
		WHERE (TableName=@NOM_TABLE_1);

		UPDATE SAFIEL SET fieldalias = 'CodOper'
		WHERE (TableName=@NOM_TABLE_1) AND (fieldname='CodOper');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NroOper = '800') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, 800, 0);
			END;
	END;

IF OBJECT_ID(@NOM_TABLE_1) IS NULL
	BEGIN		
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_1,'(
		CodOper VARCHAR(10) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_1,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_1,'_IX0
		PRIMARY KEY CLUSTERED  (CodOper) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

/*CREANDO TABLA ADICIONAL Exoneración_y_Datos_Adicionales*/
SET @NUMGRP_2 = 0;
SET @NOM_TABLE_2_M = 'SAFACT';
SET @NombreGrp_2 = 'Exoneración_y_Datos_Adicionales';

UPDATE SAAGRUPOS SET 	NombreGrp = 'Exoneración_y_Datos_Adicionales',
						AliasGrp = 'Exoneración y Datos Adicionales'
WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = 'Datos_Exoneracion_Especial';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = @NombreGrp_2) IS NULL
	BEGIN
		SELECT @NUMGRP_2 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_2_M;

		SET @NUMGRP_2 = @NUMGRP_2 + 1;

		SET @NOM_TABLE_2 = CONCAT(@NOM_TABLE_2_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_2 AS VARCHAR(2)))),CAST(@NUMGRP_2 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_2_M,	@NUMGRP_2, @NombreGrp_2, 'Exoneración y Datos Adicionales', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NroOper = '330') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_2_M,	@NUMGRP_2, 330, 0);
			END;

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NroOper = '331') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_2_M,	@NUMGRP_2, 331, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_2); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_2);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_2, CONCAT('Ventas:',@NombreGrp_2));

		INSERT INTO SAFIEL
		VALUES	(@NOM_TABLE_2,'CodSucu','CodSucu','dtString','T','T','T','F','F'),
				(@NOM_TABLE_2,'TipoFac','TipoFac','dtString','T','T','T','F','F'),
				(@NOM_TABLE_2,'NumeroD','NumeroD','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_2 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = @NombreGrp_2;

		SET @NOM_TABLE_2 = CONCAT(@NOM_TABLE_2_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_2 AS VARCHAR(2)))),CAST(@NUMGRP_2 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Ventas:',@NombreGrp_2)
		WHERE (TableName=@NOM_TABLE_2);

		UPDATE SAFIEL SET fieldalias = 'CodSucu'
		WHERE (TableName=@NOM_TABLE_2) AND (fieldname='CodSucu');

		UPDATE SAFIEL SET fieldalias = 'TipoFac'
		WHERE (TableName=@NOM_TABLE_2) AND (fieldname='TipoFac');

		UPDATE SAFIEL SET fieldalias = 'NumeroD'
		WHERE (TableName=@NOM_TABLE_2) AND (fieldname='NumeroD');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NroOper = '330') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_2_M,	@NUMGRP_2, 330, 0);
			END;

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NroOper = '331') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_2_M,	@NUMGRP_2, 331, 0);
			END;

		DELETE FROM SAAOPER
		WHERE CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NroOper BETWEEN 332 AND 335; 
	END;

IF OBJECT_ID(@NOM_TABLE_2) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_2,'(
		CodSucu VARCHAR(5) NOT NULL,
		TipoFac VARCHAR(1) NOT NULL,
		NumeroD VARCHAR(20) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_2,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_2,'_IX0
		PRIMARY KEY CLUSTERED  (CodSucu, TipoFac, NumeroD) ON [PRIMARY]')

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Correos_y_Datos_Adiconales*/
SET @NUMGRP_3 = 0
SET @NOM_TABLE_3_M = 'SACLIE'
SET @NombreGrp_3 = 'Correos_y_Datos_Adiconales';

UPDATE SAAGRUPOS SET 	NombreGrp = 'Correos_y_Datos_Adiconales',
						AliasGrp = 'Correos y Datos Adiconales'
WHERE CodTbl = @NOM_TABLE_3_M AND NombreGrp = 'Correos_Adiconales';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_3_M AND NombreGrp = @NombreGrp_3) IS NULL
	BEGIN
		SELECT @NUMGRP_3 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_3_M;

		SET @NUMGRP_3 = @NUMGRP_3 + 1;

		SET @NOM_TABLE_3 = CONCAT(@NOM_TABLE_3_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_3 AS VARCHAR(2)))),CAST(@NUMGRP_3 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_3_M,	@NUMGRP_3, @NombreGrp_3,	'Correos y Datos Adiconales', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_3_M AND NumGrp = @NUMGRP_3 AND NroOper = '600') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_3_M,	@NUMGRP_3, 600, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_3); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_3);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_3, CONCAT('Clientes:',@NombreGrp_3));

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_3,'CodClie','CodClie','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_3 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_3_M AND NombreGrp = @NombreGrp_3;

		SET @NOM_TABLE_3 = CONCAT(@NOM_TABLE_3_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_3 AS VARCHAR(2)))),CAST(@NUMGRP_3 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Clientes:',@NombreGrp_3)
		WHERE (TableName=@NOM_TABLE_3);

		UPDATE SAFIEL SET fieldalias = 'CodClie'
		WHERE (TableName=@NOM_TABLE_3) AND (fieldname='CodClie');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_3_M AND NumGrp = @NUMGRP_3 AND NroOper = '600') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_3_M,	@NUMGRP_3, 600, 0);
			END;
	END;

IF OBJECT_ID(@NOM_TABLE_3) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_3,'(
		CodClie VARCHAR(15) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_3,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_3,'_IX0
		PRIMARY KEY CLUSTERED  (CodClie) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Corrige_datos_de_Cliente_FE*/
SET @NUMGRP_4 = 0;
SET @NOM_TABLE_4_M = 'SAOPER';
SET @NombreGrp_4 = 'Corrige_datos_de_Cliente_FE';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_4_M AND NombreGrp = @NombreGrp_4) IS NULL
	BEGIN
		SELECT @NUMGRP_4 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_4_M;

		SET @NUMGRP_4 = @NUMGRP_4 + 1;

		SET @NOM_TABLE_4 = CONCAT(@NOM_TABLE_4_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_4 AS VARCHAR(2)))),CAST(@NUMGRP_4 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_4_M,	@NUMGRP_4, @NombreGrp_4, 'Reenvio Comprobantes Rechazados', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_4_M AND NumGrp = @NUMGRP_4 AND NroOper = '800') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_4_M,	@NUMGRP_4, 800, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_4); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_4);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_4, CONCAT('Operaciones:',@NombreGrp_4));

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_4,'CodOper','CodOper','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_4 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_4_M AND NombreGrp = @NombreGrp_4;

		SET @NOM_TABLE_4 = CONCAT(@NOM_TABLE_4_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_4 AS VARCHAR(2)))),CAST(@NUMGRP_4 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Operaciones:',@NombreGrp_4)
		WHERE (TableName=@NOM_TABLE_4);

		UPDATE SAFIEL SET fieldalias = 'CodOper'
		WHERE (TableName=@NOM_TABLE_4) AND (fieldname='CodOper');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_4_M AND NumGrp = @NUMGRP_4 AND NroOper = '800') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_4_M,	@NUMGRP_4, 800, 0);
			END;

		UPDATE SAAGRUPOS SET AliasGrp = 'Reenvio Comprobantes Rechazados'
		WHERE CodTbl = @NOM_TABLE_4_M AND NombreGrp = @NombreGrp_4 AND NumGrp = @NUMGRP_4;
	END;

IF OBJECT_ID(@NOM_TABLE_4) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_4,'(
		CodOper VARCHAR(10) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_4,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_4,'_IX0
		PRIMARY KEY CLUSTERED  (CodOper) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Exoneración Ficha Cliente*/
SET @NUMGRP_5 = 0;
SET @NOM_TABLE_5_M = 'SACLIE';
SET @NombreGrp_5 = 'Exoneracion';

UPDATE SAAGRUPOS SET 	NombreGrp = 'Exoneracion',
						AliasGrp = 'Exoneración'
WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = 'Exoneracion';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5) IS NULL
	BEGIN
		SELECT @NUMGRP_5 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M;

		SET @NUMGRP_5 = @NUMGRP_5 + 1;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_5_M,	@NUMGRP_5, @NombreGrp_5, 'Exoneración', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_5_M AND NumGrp = @NUMGRP_5 AND NroOper = '600') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_5_M,	@NUMGRP_5, 600, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_5); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_5);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_5, CONCAT('Clientes:',@NombreGrp_5));

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_5,'CodClie','CodClie','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_5 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Clientes:',@NombreGrp_5)
		WHERE (TableName=@NOM_TABLE_5);

		UPDATE SAFIEL SET fieldalias = 'CodClie'
		WHERE (TableName=@NOM_TABLE_5) AND (fieldname='CodClie');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_5_M AND NumGrp = @NUMGRP_5 AND NroOper = '600') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_5_M,	@NUMGRP_5, 600, 0);
			END;
	END;

IF OBJECT_ID(@NOM_TABLE_5) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_5,'(
		CodClie VARCHAR(15) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_5,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_5,'_IX0
		PRIMARY KEY CLUSTERED  (CodClie) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Datos_Adicionales Productos*/
SET @NUMGRP_6 = 0;
SET @NOM_TABLE_6_M = 'SAPROD';
SET @NombreGrp_6 = 'Datos_Adicionales';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_6_M AND NombreGrp = @NombreGrp_6) IS NULL
	BEGIN
		SELECT @NUMGRP_6 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_6_M;

		SET @NUMGRP_6 = @NUMGRP_6 + 1;

		SET @NOM_TABLE_6 = CONCAT(@NOM_TABLE_6_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_6 AS VARCHAR(2)))),CAST(@NUMGRP_6 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_6_M,	@NUMGRP_6, @NombreGrp_6,	'Datos Adicionales', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_6_M AND NumGrp = @NUMGRP_6 AND NroOper = '300') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_6_M,	@NUMGRP_6, 300, 0);
			END;		

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_6); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_6);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_6, @NombreGrp_6);

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_6,'CodProd','CodProd','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_6 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_6_M AND NombreGrp = @NombreGrp_6;

		SET @NOM_TABLE_6 = CONCAT(@NOM_TABLE_6_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_6 AS VARCHAR(2)))),CAST(@NUMGRP_6 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Productos:',@NombreGrp_6)
		WHERE (TableName=@NOM_TABLE_6);

		UPDATE SAFIEL SET fieldalias = 'CodProd'
		WHERE (TableName=@NOM_TABLE_6) AND (fieldname='CodProd');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_6_M AND NumGrp = @NUMGRP_6 AND NroOper = '300') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_6_M,	@NUMGRP_6, 300, 0);
			END;
	END;

IF OBJECT_ID(@NOM_TABLE_6) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_6,'(
		CodProd VARCHAR(15) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_6,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_6,'_IX0
		PRIMARY KEY CLUSTERED  (CodProd) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Datos_Adicionales Servicios*/
SET @NUMGRP_7 = 0;
SET @NOM_TABLE_7_M = 'SASERV';
SET @NombreGrp_7 = 'Datos_Adicionales';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_7_M AND NombreGrp = @NombreGrp_7) IS NULL
	BEGIN
		SELECT @NUMGRP_7 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_7_M;

		SET @NUMGRP_7 = @NUMGRP_7 + 1;

		SET @NOM_TABLE_7 = CONCAT(@NOM_TABLE_7_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_7 AS VARCHAR(2)))),CAST(@NUMGRP_7 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_7_M,	@NUMGRP_7, @NombreGrp_7,	'Datos Adicionales', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_7_M AND NumGrp = @NUMGRP_7 AND NroOper = '500') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_7_M,	@NUMGRP_7, 500, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_7); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_7);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_7, @NombreGrp_7);

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_7,'CodServ','CodServ','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_7 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_7_M AND NombreGrp = @NombreGrp_7;

		SET @NOM_TABLE_7 = CONCAT(@NOM_TABLE_7_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_7 AS VARCHAR(2)))),CAST(@NUMGRP_7 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Servicios:',@NombreGrp_7)
		WHERE (TableName=@NOM_TABLE_7);

		UPDATE SAFIEL SET fieldalias = 'CodServ'
		WHERE (TableName=@NOM_TABLE_7) AND (fieldname='CodServ');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_7_M AND NumGrp = @NUMGRP_7 AND NroOper = '500') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_7_M,	@NUMGRP_7, 500, 0);
			END;
	END;

IF OBJECT_ID(@NOM_TABLE_7) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_7,'(
		CodServ VARCHAR(10) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_7,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_7,'_IX0
		PRIMARY KEY CLUSTERED  (CodServ) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*CREANDO TABLA ADICIONAL Otros Datos (Compras)*/
SET @NUMGRP_10 = 0;
SET @NOM_TABLE_10_M = 'SACOMP';
SET @NombreGrp_10 = 'Otros_Datos';

UPDATE SAAGRUPOS SET 	NombreGrp = 'Otros_Datos',
						AliasGrp = 'Otros Datos'
WHERE CodTbl = @NOM_TABLE_10_M AND NombreGrp = 'Otros_Datos';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_10_M AND NombreGrp = @NombreGrp_10) IS NULL
	BEGIN
		SELECT @NUMGRP_10 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_10_M;

		SET @NUMGRP_10 = @NUMGRP_10 + 1;

		SET @NOM_TABLE_10 = CONCAT(@NOM_TABLE_10_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_10 AS VARCHAR(2)))),CAST(@NUMGRP_10 AS VARCHAR(2)));

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_10_M, @NUMGRP_10, @NombreGrp_10, 'Otros Datos', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_10_M AND NumGrp = @NUMGRP_10 AND NroOper = '340') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_10_M, @NUMGRP_10, 340, 0);
			END;

		--- ACTUALIZANDO DICCIONARIO DE REPORTES ---
		Delete SATABL Where (TableName=@NOM_TABLE_10); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_10);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_10, CONCAT('Compras:',@NombreGrp_10));

		INSERT INTO SAFIEL
		VALUES	(@NOM_TABLE_10,'CodSucu','CodSucu','dtString','T','T','T','F','F'),
				(@NOM_TABLE_10,'TipoCom','TipoCom','dtString','T','T','T','F','F'),
				(@NOM_TABLE_10,'NumeroD','NumeroD','dtString','T','T','T','F','F'),
				(@NOM_TABLE_10,'CodProv','CodProv','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_10 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_10_M AND NombreGrp = @NombreGrp_10;

		SET @NOM_TABLE_10 = CONCAT(@NOM_TABLE_10_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_10 AS VARCHAR(2)))),CAST(@NUMGRP_10 AS VARCHAR(2)));
	
		UPDATE SATABL SET tablealias = CONCAT('Compras:',@NombreGrp_10)
		WHERE (TableName=@NOM_TABLE_10);

		UPDATE SAFIEL SET fieldalias = 'CodSucu'
		WHERE (TableName=@NOM_TABLE_10) AND (fieldname='CodSucu');

		UPDATE SAFIEL SET fieldalias = 'TipoCom'
		WHERE (TableName=@NOM_TABLE_10) AND (fieldname='TipoCom');

		UPDATE SAFIEL SET fieldalias = 'NumeroD'
		WHERE (TableName=@NOM_TABLE_10) AND (fieldname='NumeroD');

		UPDATE SAFIEL SET fieldalias = 'CodProv'
		WHERE (TableName=@NOM_TABLE_10) AND (fieldname='CodProv');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_10_M AND NumGrp = @NUMGRP_10 AND NroOper = '340') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES	(@NOM_TABLE_10_M,	@NUMGRP_10, 340, 0);
			END;

		DELETE FROM SAAOPER
		WHERE CodTbl = @NOM_TABLE_10_M AND NumGrp = @NUMGRP_10 AND NroOper BETWEEN 341 AND 345; 
	END;

IF OBJECT_ID(@NOM_TABLE_10) IS NULL
	BEGIN
		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_10,'(
		CodSucu VARCHAR(5) NOT NULL,
		TipoCom VARCHAR(1) NOT NULL,
		NumeroD VARCHAR(20) NOT NULL,
		CodProv VARCHAR(15) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_10,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_10,'_IX0
		PRIMARY KEY CLUSTERED  (CodSucu, TipoCom, NumeroD, CodProv) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;
	END;
/*CAMPOS ADICIONALES DE TABLAS ADICIONALES*/
/*CAMPOS TABLA ADICIONAL EN OPERACIONES LLAMADA Datos para facturación electrónica*/
DECLARE @TIPO_DATO AS VARCHAR(50)

IF COL_LENGTH(@NOM_TABLE_1, 'Usuario_FactElect') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Usuario_FactElect varchar(90) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Contrasena_FactElec') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Contrasena_FactElec	varchar(90)	NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Cod_Moneda_Prinicpal') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Cod_Moneda_Prinicpal INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('SELECT @TIPO_DATO = DATA_TYPE from information_schema.columns 
							  WHERE TABLE_NAME=', '''', @NOM_TABLE_1, '''',' AND COLUMN_NAME=','''','Cod_Moneda_Prinicpal','''');

		EXECUTE sp_executesql @STRING, N'
		@TIPO_DATO AS VARCHAR(20) OUTPUT',
		@TIPO_DATO = @TIPO_DATO OUTPUT;
				  
		SET @STRING = NULL;
		
		IF @TIPO_DATO = 'varchar'
			BEGIN
				IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_DATOS_FE') IS NOT NULL
					BEGIN
						SET @STRING = CONCAT('Disable TRIGGER SCR_DATOS_FE ON ', @NOM_TABLE_1);
						EXEC (@STRING);
						SET @STRING = NULL;
					END;

				SET @STRING = CONCAT('UPDATE ', @NOM_TABLE_1,' SET Cod_Moneda_Prinicpal = CASE WHEN Cod_Moneda_Prinicpal = ', '''', 'CRC', '''', ' THEN ', '''', '1', '''',
																					   		 ' WHEN Cod_Moneda_Prinicpal = ', '''', 'USD', '''', ' THEN ', '''', '2', '''',
																					   		 ' WHEN Cod_Moneda_Prinicpal = ', '''', 'EUR', '''', ' THEN ', '''', '3', '''',
																					   		 ' WHEN Cod_Moneda_Prinicpal = ', '''', 'CNY', '''', ' THEN ', '''', '4', '''',
																					   		 ' WHEN Cod_Moneda_Prinicpal = ', '''', 'RUB', '''', ' THEN ', '''', '5', '''',
																					   		 ' WHEN Cod_Moneda_Prinicpal = ', '''', 'JPY', '''', ' THEN ', '''', '6', '''', ' ELSE ', '''', '0', '''',' END',
									' WHERE CodOper = ', '''', 'FACTELEC', '''');
				EXEC (@STRING);
				SET @STRING = NULL;

				IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_DATOS_FE') IS NOT NULL
					BEGIN	
						SET @STRING = CONCAT('Enable TRIGGER SCR_DATOS_FE ON ', @NOM_TABLE_1);
						EXEC (@STRING);
						SET @STRING = NULL;
					END;
			END;

		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Cod_Moneda_Prinicpal INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
		SET @TIPO_DATO = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Cod_Moneda_Referencial') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Cod_Moneda_Referencial INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('SELECT @TIPO_DATO = DATA_TYPE from information_schema.columns 
							  WHERE TABLE_NAME=', '''', @NOM_TABLE_1, '''',' AND COLUMN_NAME=','''','Cod_Moneda_Referencial','''');

		EXECUTE sp_executesql @STRING, N'
				@TIPO_DATO AS VARCHAR(20) OUTPUT',
				@TIPO_DATO = @TIPO_DATO OUTPUT;
				  
		SET @STRING = NULL;
		
		IF (@TIPO_DATO = 'varchar')
			BEGIN
				IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_DATOS_FE') IS NOT NULL
					BEGIN
						SET @STRING = CONCAT('Disable TRIGGER SCR_DATOS_FE ON ', @NOM_TABLE_1);
						EXEC (@STRING);
						SET @STRING = NULL;
					END;

				SET @STRING = CONCAT('UPDATE ', @NOM_TABLE_1,' SET Cod_Moneda_Referencial = CASE WHEN Cod_Moneda_Referencial = ', '''', 'CRC', '''', ' THEN ', '''', '1', '''',
																					   		   ' WHEN Cod_Moneda_Referencial = ', '''', 'USD', '''', ' THEN ', '''', '2', '''',
																					   		   ' WHEN Cod_Moneda_Referencial = ', '''', 'EUR', '''', ' THEN ', '''', '3', '''',
																					   		   ' WHEN Cod_Moneda_Referencial = ', '''', 'CNY', '''', ' THEN ', '''', '4', '''',
																					   		   ' WHEN Cod_Moneda_Referencial = ', '''', 'RUB', '''', ' THEN ', '''', '5', '''',
																					   		   ' WHEN Cod_Moneda_Referencial = ', '''', 'JPY', '''', ' THEN ', '''', '6', '''', ' ELSE ', '''', '0', '''',' END',
									' WHERE CodOper = ', '''', 'FACTELEC', '''');
				EXEC (@STRING);
				SET @STRING = NULL;

				IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_DATOS_FE') IS NOT NULL
					BEGIN	
						SET @STRING = CONCAT('Enable TRIGGER SCR_DATOS_FE ON ', @NOM_TABLE_1);
						EXEC (@STRING);
						SET @STRING = NULL;
					END	;
			END;

		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Cod_Moneda_Referencial	INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
		SET @TIPO_DATO = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Tipo_Emisor') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Tipo_Emisor	INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Tipo_Emisor INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Descripcion_Fletes') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Descripcion_Fletes varchar(60) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Descripcion_Fletes varchar(60) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Cabys_Fletes') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Cabys_Fletes varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Cabys_Fletes varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Email_Emisor') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Email_Emisor varchar(60) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Email_Emisor varchar(60) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Nombre_Comercial') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Nombre_Comercial varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Nombre_Comercial varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'correos_adicionales_1') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD correos_adicionales_1 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN correos_adicionales_1 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'correos_adicionales_2') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD correos_adicionales_2 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN correos_adicionales_2 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'correos_adicionales_3') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD correos_adicionales_3 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN correos_adicionales_3 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'correos_adicionales_4') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD correos_adicionales_4 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN correos_adicionales_4 varchar(99) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Sucursal') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Sucursal int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Sucursal int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Llave_Sucursal') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Llave_Sucursal varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Llave_Sucursal varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Si_FC') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Si_FC smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Si_FC smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Incluir_DSPDF') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Incluir_DSPDF smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Incluir_DSPDF smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_1') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_1 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_1 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_2') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_2 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_2 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_3') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_3 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_3 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_4') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_4 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_4 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_5') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_5 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_5 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_6') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_6 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_6 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_7') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_7 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_7 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_8') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_8 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_8 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_9') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_9 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_9 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_10') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_10 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_10 INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Incluir_Receptor') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Incluir_Receptor smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Incluir_Receptor smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Unidad_Servicios') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Unidad_Servicios smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Unidad_Servicios smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Regalias') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Regalias smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Regalias smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

/*EDITANDO TABLA SAACAMPOS*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodigoMoneda WITH (NOLOCK);

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), CODMONEDA, '-', NOMBRE_MONEDA, '-', PAIS) 
		FROM FECodigoMoneda WITH (NOLOCK)
		WHERE ID = @LINEA;

		SET @LINEA +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Usuario_FactElect') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Usuario_FactElect';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Contrasena_FactElec') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Contrasena_FactElec';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cod_Moneda_Prinicpal') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Cod_Moneda_Prinicpal', 'Cod Moneda Prinicpal', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Cod Moneda Prinicpal', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cod_Moneda_Prinicpal';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cod_Moneda_Referencial') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Cod_Moneda_Referencial', 'Cod Moneda Referencial', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Cod Moneda Referencial',TipoCpo = 56,Longitud = 4,Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cod_Moneda_Referencial';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Tipo_Emisor') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Tipo_Emisor', 'Tipo Emisor', 56, 4, 0, 0, 'Seleccionar
Física
Jurídica
DIMEX
NITE
Pasaporte');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Tipo Emisor', TipoCpo = 56, Longitud = 4, Value = 'Seleccionar
Física
Jurídica
DIMEX
NITE
Pasaporte'
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Tipo_Emisor';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Descripcion_Fletes') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Descripcion_Fletes', 'Descripcion Fletes', 167, 60, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Descripcion Fletes', TipoCpo = 167, Longitud = 60, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Descripcion_Fletes';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cabys_Fletes') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Cabys_Fletes', 'Cabys Fletes', 167, 13, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Cabys Fletes', TipoCpo = 167, Longitud = 13, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Cabys_Fletes';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Email_Emisor') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Email_Emisor', 'Email Emisor', 167, 60, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Email Emisor', TipoCpo = 167, Longitud = 60, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Email_Emisor';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Nombre_Comercial') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Nombre_Comercial', 'Nombre Comercial', 167, 80, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Nombre Comercial', TipoCpo = 167, Longitud = 80, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Nombre_Comercial';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_1') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'correos_adicionales_1', 'correos adicionales 1', 167, 99, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'correos adicionales 1', TipoCpo = 167, Longitud = 99, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_1';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_2') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'correos_adicionales_2', 'correos adicionales 2', 167, 99, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'correos adicionales 2', TipoCpo = 167, Longitud = 99, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_2';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_3') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'correos_adicionales_3', 'correos adicionales 3', 167, 99, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'correos adicionales 3', TipoCpo = 167, Longitud = 99, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_3';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_4') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'correos_adicionales_4', 'correos adicionales 4', 167, 99, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'correos adicionales 4', TipoCpo = 167, Longitud = 99, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'correos_adicionales_4';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Sucursal') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Sucursal', 'Sucursal Principal', 56, 4, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Sucursal Principal', TipoCpo = 56, Longitud = 4, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Sucursal';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Llave_Sucursal') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Llave_Sucursal', 'Llave Sucursal Princ.', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Llave Sucursal Princ.', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Llave_Sucursal';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Si_FC') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Si_FC', 'Mostrar Factor Cambio', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Mostrar Factor Cambio', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Si_FC';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Incluir_DSPDF') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Incluir_DSPDF', 'Incluir Datos Suc PDF', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Incluir Datos Suc PDF', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Incluir_DSPDF';
	END;

/*UBICANDO LISTADO ACTIVIDADES*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FEActividades_Eco WITH (NOLOCK);

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), Codigo_Actividad, '-', Nombre) 
		FROM FEActividades_Eco WITH (NOLOCK)
		WHERE NroUnico = @LINEA;

		SET @LINEA +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_1') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_1', 'Código Actividad 1', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 1', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_1';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_2') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_2', 'Código Actividad 2', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 2', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_2';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_3') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_3', 'Código Actividad 3', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 3', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_3';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_4') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_4', 'Código Actividad 4', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 4', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_4';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_5') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_5', 'Código Actividad 5', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 5', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_5';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_6') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_6', 'Código Actividad 6', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 6', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_6';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_7') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_7', 'Código Actividad 7', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 7', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_7';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_8') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_8', 'Código Actividad 8', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 8', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_8';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_9') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_9', 'Código Actividad 9', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 9', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_9';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_10') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Codigo_Actividad_10', 'Código Actividad 10', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad 10', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_10';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Incluir_Receptor') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Incluir_Receptor';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Unidad_Servicios') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Unidad_Servicios', 'Liberar Uni. Servicios', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Liberar Uni. Servicios', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Unidad_Servicios';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Regalias') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_1, 'Regalias', 'Usar Regalias', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Usar Regalias', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Regalias';
	END;

/*EDITANDO TABLA SAFIEL @NOM_TABLE_1 (SAOPER Dats facturacion electronica)*/
SET @tablename = @NOM_TABLE_1;

INSERT INTO @FIELS 
VALUES	
(1,'Cod_Moneda_Prinicpal','dtString'),
(2,'Cod_Moneda_Referencial','dtString'),
(3,'Email_Emisor','dtString'),
(4,'Nombre_Comercial','dtString'),
(5,'correos_adicionales_1','dtString'),
(6,'correos_adicionales_2','dtString'),
(7,'correos_adicionales_3','dtString'),
(8,'correos_adicionales_4','dtString'),
(9,'Sucursal','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE (@linea <= @contador)
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
/*CAMPOS TABLA ADICIONAL EN VENTAS Exoneración_y_Datos_Adicionales*/
IF COL_LENGTH(@NOM_TABLE_2, 'Tipo_documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Tipo_documento INT DEFAULT(0) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Tipo_documento INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Numero_Documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Numero_Documento varchar(40) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Numero_Documento varchar(40) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Nombre_Institucion') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Nombre_Institucion varchar(160) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Nombre_Institucion varchar(160) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Fecha_Documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Fecha_Documento	datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Fecha_Documento datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Porcentaje') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Porcentaje int DEFAULT(0) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Porcentaje	int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Factor_Cambio') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Factor_Cambio smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Factor_Cambio smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'WM_GLN') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD WM_GLN varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN WM_GLN varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Guardar_Datos_Exoneracion') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Guardar_Datos_Exoneracion smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Guardar_Datos_Exoneracion smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'No_Exonerar') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD No_Exonerar smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN No_Exonerar smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Codigo_Actividad_S') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Codigo_Actividad_S INT DEFAULT (0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Codigo_Actividad_S INT NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Pedido_ICE') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Pedido_ICE varchar(50) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Pedido_ICE varchar(50) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Moneda') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Moneda INT DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Moneda INT NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Nro_Recepcion') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Nro_Recepcion varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Nro_Recepcion varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Fecha_Orden') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Fecha_Orden datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Fecha_Orden datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Nro_Reclamo') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Nro_Reclamo varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Nro_Reclamo varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_2, 'Fecha_Reclamo') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ADD Fecha_Reclamo datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_2,' ALTER COLUMN Fecha_Reclamo datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

/*UBICANDO LISTADO DE EXONERACIONES*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodigoExoneracion WITH (NOLOCK);

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), Descrip) 
		FROM FECodigoExoneracion WITH (NOLOCK)
		WHERE ID = @LINEA;

		SET @LINEA +=1;
	END;

/*MODIFICANDO TABLA SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Tipo_documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Tipo_documento', 'Tipo documento', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Tipo documento', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Tipo_documento';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Numero_Documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Numero_Documento', 'Numero Documento', 167, 40, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Numero Documento', TipoCpo = 167, Longitud = 40, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Numero_Documento';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nombre_Institucion') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Nombre_Institucion', 'Nombre Institucion', 167, 160, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Nombre Institucion', TipoCpo = 167, Longitud = 160, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nombre_Institucion';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Fecha_Documento', 'Fecha Documento', 61, 8, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Fecha Documento', TipoCpo = 61, Longitud = 8, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Documento';
	END;

/*UBICANDO LISTADO DE PORCENTAJES*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;
SET @TOTAL = 13;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), @LINEA); 
		SET @LINEA +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Porcentaje') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Porcentaje', 'Porcentaje Exoneración', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Porcentaje Exoneración', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Porcentaje';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Factor_Cambio') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Factor_Cambio', 'Factor de Cambio', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Factor de Cambio', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Factor_Cambio';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'WM_GLN') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'WM_GLN', 'Enviar GLN', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Enviar GLN', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'WM_GLN';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Guardar_Datos_Exoneracion') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Guardar_Datos_Exoneracion', 'Guardar Datos Exoneración', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Guardar Datos Exoneracion', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Guardar_Datos_Exoneracion';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'No_Exonerar') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'No_Exonerar', 'No Exonerar', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'No Exonerar', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'No_Exonerar';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Codigo_Actividad_S') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Codigo_Actividad_S', 'Código Actividad Alt', 56, 4, 0, 0, 'Seleccionar');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad Alt', TipoCpo = 56, Longitud = 4, Value = 'Seleccionar'
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Codigo_Actividad_S';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Pedido_ICE') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Pedido_ICE', 'Nro Pedido ICE', 167, 50, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Nro Pedido ICE', TipoCpo = 167, Longitud = 50, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Pedido_ICE';
	END;

/*UBICANDO LISTADO DE MONEDAS*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodigoMoneda WITH (NOLOCK)
WHERE Nivel > 0;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), CODMONEDA, '-', NOMBRE_MONEDA, '-', PAIS) 
		FROM FECodigoMoneda WITH (NOLOCK)
		WHERE Nivel = @LINEA;

		SET @LINEA +=1;
	END;

/*MODIFICANDO TABLA SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Moneda') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Moneda', 'Moneda', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Moneda', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Moneda';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nro_Recepcion') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Nro_Recepcion', 'Número Recepción', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Número Recepción', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nro_Recepcion';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Orden') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Fecha_Orden', 'Fecha Orden', 61, 8, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Fecha Orden', TipoCpo = 61, Longitud = 8, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Orden';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nro_Reclamo') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Nro_Reclamo', 'Número Reclamo', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Número Reclamo', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Nro_Reclamo';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Reclamo') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAFACT', @NUMGRP_2, 'Fecha_Reclamo', 'Fecha Reclamo', 61, 8, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Fecha Reclamo', TipoCpo = 61, Longitud = 8, Value = ''
		WHERE	CodTbl = 'SAFACT' AND NumGrp = @NUMGRP_2 AND NombCpo = 'Fecha_Reclamo';
	END;

/*MODIFICANDO TABLA SAFIEL NOM_TABLE_2 (Datos adicionales y de exoneracion Ventas)*/
SET @tablename = @NOM_TABLE_2;

INSERT INTO @FIELS 
VALUES	
(1,'Tipo_documento','dtInteger'),
(2,'Numero_Documento','dtString'),
(3,'Nombre_Institucion','dtString'),
(4,'Fecha_Documento','dtDateTime'),
(5,'Porcentaje','dtInteger'),
(6,'WM_GLN','dtString'),
(7,'Pedido_ICE','dtString'),
(8,'Moneda','dtInteger'),
(9,'Nro_Recepcion','dtString'),
(10,'Fecha_Orden','dtDateTime'),
(11,'Nro_Reclamo','dtString'),
(12,'Fecha_Reclamo','dtDateTime')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE (@linea <= @contador)
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
/*CAMPOS TABLA ADICIONAL PARA CORREOS ADICIONALES RECEPTOR Correos y Datos Adiconales*/
IF COL_LENGTH(@NOM_TABLE_3, 'Correo_Adicional_1') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Correo_Adicional_1 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Correo_Adicional_1 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Correo_Adicional_2') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Correo_Adicional_2 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Correo_Adicional_2 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Correo_Adicional_3') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Correo_Adicional_3 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Correo_Adicional_3 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Correo_Adicional_4') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Correo_Adicional_4 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Correo_Adicional_4 varchar(80) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'No_Ubicacion') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD No_Ubicacion smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN No_Ubicacion smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Codigo_Pais') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Codigo_Pais int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Codigo_Pais int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'WM_Vendedor') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD WM_Vendedor varchar(30) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN WM_Vendedor varchar(30) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'receptor_direccion_extranjero') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD receptor_direccion_extranjero varchar(300) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN receptor_direccion_extranjero varchar(300) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'WM_GLN') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD WM_GLN varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN WM_GLN varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Receptor_Incluir') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Receptor_Incluir smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Receptor_Incluir smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Ubicacion_Incluir') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Ubicacion_Incluir smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Ubicacion_Incluir smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'GS1') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD GS1 smallint DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN GS1 smallint NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Impuesto') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Impuesto int DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Impuesto int NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Usa_Representante') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Usa_Representante int NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_3, 'Usa_Descrip_Ampli') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ADD Usa_Descrip_Ampli int DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_3,' ALTER COLUMN Usa_Descrip_Ampli int NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

/*MODIFICANDO SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_1') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Correo_Adicional_1', 'Correo Adicional 1', 167, 80, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Correo Adicional 1', TipoCpo = 167, Longitud = 80, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_1';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_2') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Correo_Adicional_2', 'Correo Adicional 2', 167, 80, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Correo Adicional 2', TipoCpo = 167, Longitud = 80, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_2';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_3') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Correo_Adicional_3', 'Correo Adicional 3', 167, 80, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Correo Adicional 3', TipoCpo = 167, Longitud = 80, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_3';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_4') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Correo_Adicional_4', 'Correo Adicional 4', 167, 80, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Correo Adicional 4', TipoCpo = 167, Longitud = 80, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Correo_Adicional_4';
	END;

/*UBICANDO LISTADO DE CODIGOS DE AREA*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodAreaPais WITH (NOLOCK);

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), Descrip) 
		FROM FECodAreaPais WITH (NOLOCK)
		WHERE ID = @LINEA;

		SET @LINEA +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Codigo_Pais') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Codigo_Pais', 'Codigo Pais', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Codigo Pais', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Codigo_Pais';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'No_Ubicacion') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'No_Ubicacion', 'No Mostrar Ubicacion', 52, 35, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'No Mostrar Ubicacion', TipoCpo = 52, Longitud = 35, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'No_Ubicacion';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'WM_Vendedor') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'WM_Vendedor', 'Número Vendedor', 167, 30, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Número Vendedor', TipoCpo = 167, Longitud = 30, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'WM_Vendedor';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'receptor_direccion_extranjero') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'receptor_direccion_extranjero', 'Dirección Extranjero', 167, 300, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Dirección Extranjero', TipoCpo = 167, Longitud = 300, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'receptor_direccion_extranjero';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'WM_GLN') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'WM_GLN', 'Enviar GLN', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Enviar GLN', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'WM_GLN';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Receptor_Incluir') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Receptor_Incluir', 'Incluir Receptor Tiq.', 52, 35, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Incluir Receptor Tiq.', TipoCpo = 52, Longitud = 35, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Receptor_Incluir';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Ubicacion_Incluir') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Ubicacion_Incluir';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'GS1') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'GS1', 'Estandar GS1', 52, 35, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Estandar GS1', TipoCpo = 52, Longitud = 35, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'GS1';
	END;

/*UBICANDO LISTADO DE CODIGOS DE AREA*/
SET @LISTADO = CONCAT('Seleccionar',CHAR(13), CHAR(10),'Es Exento');
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM SATAXES WITH (NOLOCK)
WHERE Orden >= 2;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), A.Descrip) 
		FROM
		(SELECT ROW_NUMBER() OVER(ORDER BY Orden) Linea,
				Descrip
		FROM SATAXES WITH (NOLOCK)
		WHERE Orden >= 2) A
		WHERE A.Linea = @LINEA;

		SET @LINEA +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Impuesto') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Impuesto', 'Impuesto', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Impuesto', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Impuesto';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Usa_Representante') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Usa_Representante';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Usa_Descrip_Ampli') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_3, 'Usa_Descrip_Ampli', 'Usa Descrip. Ampli.', 52, 35, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Usa Descrip. Ampli.', TipoCpo = 52, Longitud = 35, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_3 AND NombCpo = 'Usa_Descrip_Ampli';
	END;

/*MODIFICANDO SAFIEL NOM_TABLE_3*/
SET @tablename = @NOM_TABLE_3;

INSERT INTO @FIELS 
VALUES	
(1,'Correo_Adicional_1','dtString'),
(2,'Correo_Adicional_2','dtString'),
(3,'Correo_Adicional_3','dtString'),
(4,'Correo_Adicional_4','dtString'),
(5,'Codigo_Pais','dtInteger'),
(6,'WM_Vendedor','dtString'),
(7,'receptor_direccion_extranjero','dtString'),
(8,'WM_GLN','dtString'),
(9,'Receptor_Incluir','dtInteger'),
(10,'Ubicacion_Incluir','dtInteger'),
(11,'No_Ubicacion','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE (@linea <= @contador)
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
/*CAMPOS TABLA ADICIONAL LLAMADA 'Corrige datos de Cliente FE'*/
IF COL_LENGTH(@NOM_TABLE_4, 'Nro_Factura') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' ADD Nro_Factura varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' ALTER COLUMN Nro_Factura varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Nombre') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Nombre');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Cedula') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Cedula');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Tipo_Cedula') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Tipo_Cedula');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Direccion_1') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Direccion_1');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Direccion_2') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Direccion_2');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Telefono') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Telefono');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Email') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Email');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Fax') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Fax');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Direccion_Extranjero') IS NOT NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' DROP COLUMN Direccion_Extranjero');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_4, 'Mensaje') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' ADD Mensaje text NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_4,' ALTER COLUMN Mensaje text NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

/*MODIFICANDO SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Nro_Factura') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_4, 'Nro_Factura', 'Consecutivo Hacienda', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Consecutivo Hacienda', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Nro_Factura';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Nombre') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Nombre';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Cedula') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Cedula';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Tipo_Cedula') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Tipo_Cedula';
	END;
		
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_1') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_1';
	END;
	
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_2') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_2';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Telefono') IS NOT NULL
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Telefono';
	END;	
		
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Email') IS NOT NULL 
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Email';
	END;			

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Fax') IS NOT NULL 
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Fax';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_Extranjero') IS NOT NULL 
	BEGIN
		DELETE FROM SAACAMPOS 
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Direccion_Extranjero';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Mensaje') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAOPER', @NUMGRP_4, 'Mensaje', 'Mensaje', 35, 16, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Mensaje', TipoCpo = 35, Longitud = 16, Value = ''
		WHERE	CodTbl = 'SAOPER' AND NumGrp = @NUMGRP_4 AND NombCpo = 'Mensaje';
	END;

/*MODIFICANDO SAFIEL NOM_TABLE_4*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Nro_Factura') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES (@NOM_TABLE_4,'Nro_Factura','Nro_Factura','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Nro_Factura', datatype = 'dtString'
		WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Nro_Factura';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Nombre') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Nombre';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Cedula') IS NOT NULL 
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Cedula';
	END	;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Tipo_Cedula') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Tipo_Cedula';
	END;			
	
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_1') IS NOT NULL 
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_1';
	END;
	
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_2') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_2';
	END;		

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Telefono') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Telefono';
	END;		

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Email') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Email';
	END;
	
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Fax') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Fax';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_Extranjero') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Direccion_Extranjero';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Mensaje') IS NOT NULL
	BEGIN
		DELETE FROM SAFIEL WHERE tablename = @NOM_TABLE_4 AND fieldname = 'Mensaje';
	END;

/*CAMPOS TABLA ADICIONAL EN CLIENTES Exoneración*/
IF COL_LENGTH(@NOM_TABLE_5, 'Tipo_documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ADD Tipo_documento INT DEFAULT(0) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ALTER COLUMN Tipo_documento INT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_5, 'Numero_Documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ADD Numero_Documento varchar(40) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ALTER COLUMN Numero_Documento varchar(40) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_5, 'Nombre_Institucion') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ADD Nombre_Institucion varchar(160) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ALTER COLUMN Nombre_Institucion varchar(160) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END; 							

IF COL_LENGTH(@NOM_TABLE_5, 'Fecha_Documento') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ADD Fecha_Documento	datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ALTER COLUMN Fecha_Documento datetime NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_5, 'Porcentaje') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ADD Porcentaje int DEFAULT(0) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_5,' ALTER COLUMN Porcentaje	int NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

/*UBICANDO LISTADO DE EXONERACIONES*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodigoExoneracion WITH (NOLOCK);

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), Descrip) 
		FROM FECodigoExoneracion WITH (NOLOCK)
		WHERE ID = @LINEA;

		SET @LINEA +=1;
	END;

/*MODIFICANDO TABLA SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Tipo_documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_5, 'Tipo_documento', 'Tipo documento', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Tipo documento', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Tipo_documento';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Numero_Documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_5, 'Numero_Documento', 'Numero Documento', 167, 40, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Numero Documento', TipoCpo = 167, Longitud = 40, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Numero_Documento';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Nombre_Institucion') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_5, 'Nombre_Institucion', 'Nombre Institucion', 167, 160, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Nombre Institucion', TipoCpo = 167, Longitud = 160, Value = ''
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Nombre_Institucion';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Fecha_Documento') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_5, 'Fecha_Documento', 'Fecha Documento', 61, 8, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Fecha Documento', TipoCpo = 61, Longitud = 8, Value = ''
		WHERE	CodTbl = 'SACLIE' AND
				NumGrp = @NUMGRP_5 AND 
				NombCpo	= 'Fecha_Documento';
	END;

/*UBICANDO LISTADO DE PORCENTAJES*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;
SET @TOTAL = 13;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SET @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), @LINEA);

		SET @LINEA +=1;
	END

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Porcentaje') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACLIE', @NUMGRP_5, 'Porcentaje', 'Porcentaje Exoneración', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Porcentaje Exoneración', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_5 AND NombCpo = 'Porcentaje';
	END;

/*MODIFICANDO TABLA SAFIEL NOM_TABLE_5*/
SET @tablename = @NOM_TABLE_5;

INSERT INTO @FIELS 
VALUES	
(1,'Tipo_documento','dtInteger'),
(2,'Numero_Documento','dtString'),
(3,'Nombre_Institucion','dtString'),
(4,'Fecha_Documento','dtDateTime'),
(5,'Porcentaje','dtInteger')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE (@linea <= @contador)
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
/*CAMPOS TABLA ADICIONAL EN PRODUCTOS Datos_Adicionales*/
IF COL_LENGTH(@NOM_TABLE_6, 'codigo_hacienda') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ADD codigo_hacienda varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ALTER COLUMN codigo_hacienda varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_6, 'partida_arancelaria') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ADD partida_arancelaria varchar(15) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ALTER COLUMN partida_arancelaria varchar(15) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_6, 'Descrip_Larga') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ADD Descrip_Larga text NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_6,' ALTER COLUMN Descrip_Larga text NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;

/*MODIFICANDO TABLA SAACAMPOS NOM_TABLE_6*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'codigo_hacienda') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAPROD', @NUMGRP_6, 'codigo_hacienda', 'Código CABYS', 167, 13, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código CABYS', TipoCpo = 167, Longitud = 13, Value = ''
		WHERE	CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'codigo_hacienda';
	END;

/*MODIFICANDO TABLA SAACAMPOS NOM_TABLE_6*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'partida_arancelaria') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAPROD', @NUMGRP_6, 'partida_arancelaria', 'Partida Arancelaria', 167, 15, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Partida Arancelaria', TipoCpo = 167, Longitud = 15, Value = ''
		WHERE	CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'partida_arancelaria';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'Descrip_Larga') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SAPROD', @NUMGRP_6, 'Descrip_Larga', 'Descrip larga max 1000', 35, 16, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Descrip larga max 1000', TipoCpo = 35, Longitud = 16, Value = ''
		WHERE	CodTbl = 'SAPROD' AND NumGrp = @NUMGRP_6 AND NombCpo = 'Descrip_Larga';
	END;

/*MODIFICANDO SAFIEL NOM_TABLE_6*/
SET @tablename = @NOM_TABLE_6;

INSERT INTO @FIELS 
VALUES	
(1,'codigo_hacienda','dtString'),
(2,'partida_arancelaria','dtString'),
(3,'Descrip_Larga','dtMemo')

SET @linea = 1;

SELECT @contador = COUNT(*)
FROM @FIELS;

SET @contador = ISNULL(@contador,0);

IF (@contador > 0)
	BEGIN
		WHILE (@linea <= @contador)
			BEGIN
				SET @fieldname = NULL;
				SET @datatype = NULL;

				SELECT	@fieldname = A.fieldname,
						@datatype = A.datatype
				FROM
				(SELECT ROW_NUMBER () OVER (ORDER BY id) AS Linea,
						fieldname,
						datatype
				FROM @FIELS) AS A
				WHERE A.Linea = @linea;

				IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @tablename AND fieldname = @fieldname) IS NULL
					BEGIN
						INSERT INTO SAFIEL 
						VALUES (@tablename,@fieldname,@fieldname,@datatype,'T','T','T','F','F');
					END;
				ELSE 
					BEGIN
						UPDATE SAFIEL SET 	fieldalias = @fieldname, datatype = @datatype
						WHERE tablename = @tablename AND fieldname = @fieldname;
					END;

				SET @linea +=1;
			END;
	END;

DELETE FROM @FIELS;
SET @tablename = NULL;
SET @fieldname = NULL;
SET @datatype = NULL;
SET @contador = 0;
SET @linea = 1;
/*CAMPOS TABLA ADICIONAL EN Servicios Datos_Adicionales*/
IF COL_LENGTH(@NOM_TABLE_7, 'codigo_hacienda') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_7,' ADD codigo_hacienda varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_7,' ALTER COLUMN codigo_hacienda varchar(13) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_7, 'Descrip_Larga') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_7,' ADD Descrip_Larga text NULL');
		EXEC (@STRING)
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_7,' ALTER COLUMN Descrip_Larga text NULL');
		EXEC (@STRING)
		SET @STRING = NULL;		
	END;

/*MODIFICANDO TABLA SAACAMPOS NOM_TABLE_7*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SASERV' AND NumGrp = @NUMGRP_7 AND NombCpo = 'codigo_hacienda') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SASERV', @NUMGRP_7, 'codigo_hacienda', 'Código CABYS', 167, 13, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código CABYS', TipoCpo = 167, Longitud = 13, Value = ''
		WHERE	CodTbl = 'SASERV' AND NumGrp = @NUMGRP_7 AND NombCpo = 'codigo_hacienda';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SASERV' AND NumGrp = @NUMGRP_7 AND NombCpo = 'Descrip_Larga') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SASERV', @NUMGRP_7, 'Descrip_Larga', 'Descrip larga max 1000', 35, 16, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Descrip larga max 1000', TipoCpo = 35, Longitud = 16, Value = ''
		WHERE	CodTbl = 'SASERV' AND NumGrp = @NUMGRP_7 AND NombCpo = 'Descrip_Larga';
	END;

/*MODIFICANDO SAFIEL NOM_TABLE_7*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_7 AND fieldname = 'codigo_hacienda') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES (@NOM_TABLE_7,'codigo_hacienda','codigo_hacienda','dtString','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'codigo_hacienda', datatype = 'dtString'
		WHERE tablename = @NOM_TABLE_7 AND fieldname = 'codigo_hacienda';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_7 AND fieldname = 'Descrip_Larga') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES (@NOM_TABLE_7,'Descrip_Larga','Descrip_Larga','dtMemo','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Descrip_Larga', datatype = 'dtMemo'
		WHERE tablename = @NOM_TABLE_7 AND fieldname = 'Descrip_Larga';
	END;

/*CAMPOS TABLA ADICIONAL EN Otros Datos (Compras)*/
IF COL_LENGTH(@NOM_TABLE_10, 'Emitir_FactE') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_10,' ADD Emitir_FactE smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_10,' ALTER COLUMN Emitir_FactE smallint NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF COL_LENGTH(@NOM_TABLE_10, 'Moneda') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_10,' ADD Moneda INT DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_10,' ALTER COLUMN Moneda INT NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

/*MODIFICANDO TABLA SAACAMPOS NOM_TABLE_10*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACOMP' AND NumGrp = @NUMGRP_10 AND NombCpo = 'Emitir_FactE') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACOMP', @NUMGRP_10, 'Emitir_FactE', 'No Emitir Fact Electrónica', 52, 2, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'No Emitir Fact Electrónica', TipoCpo = 52, Longitud = 2, Value = ''
		WHERE	CodTbl = 'SACOMP' AND NumGrp = @NUMGRP_10 AND NombCpo = 'Emitir_FactE';
	END;

/*UBICANDO LISTADO DE MONEDAS*/
SET @LISTADO = 'Seleccionar';
SET @TOTAL = 0;
SET @LINEA = 1;

SELECT @TOTAL = COUNT(*)
FROM FECodigoMoneda WITH (NOLOCK)
WHERE Nivel > 0;

WHILE (@LINEA <= @TOTAL)
	BEGIN
		SELECT @LISTADO = CONCAT(@LISTADO, CHAR(13), CHAR(10), CODMONEDA, '-', NOMBRE_MONEDA, '-', PAIS) 
		FROM FECodigoMoneda WITH (NOLOCK)
		WHERE Nivel = @LINEA;

		SET @LINEA +=1;
	END;

/*MODIFICANDO TABLA SAACAMPOS*/
IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACOMP' AND NumGrp = @NUMGRP_10 AND NombCpo = 'Moneda') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SACOMP', @NUMGRP_10, 'Moneda', 'Moneda', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Moneda', TipoCpo = 56, Longitud = 4, Value = @LISTADO
		WHERE	CodTbl = 'SACOMP' AND NumGrp = @NUMGRP_10 AND NombCpo = 'Moneda';
	END;

/*MODIFICANDO SAFIEL NOM_TABLE_10*/
IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_10 AND fieldname = 'Emitir_FactE') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES (@NOM_TABLE_10,'Emitir_FactE','Emitir_FactE','dtInteger','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Emitir_FactE', datatype = 'dtInteger'
		WHERE tablename = @NOM_TABLE_10 AND fieldname = 'Emitir_FactE';
	END;

IF (SELECT fieldname FROM SAFIEL WITH (NOLOCK) WHERE tablename = @NOM_TABLE_10 AND fieldname = 'Moneda') IS NULL
	BEGIN
		INSERT INTO SAFIEL 
		VALUES (@NOM_TABLE_10,'Moneda','Moneda','dtInteger','T','T','T','F','F');
	END;
ELSE 
	BEGIN
		UPDATE SAFIEL SET 	fieldalias = 'Moneda', datatype = 'dtInteger'
		WHERE tablename = @NOM_TABLE_10 AND fieldname = 'Moneda';
	END;

/*TABLA SADEPO*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NOM_TABLE_1 = NULL;
SET @NombreGrp_1 = NULL;
SET @STRING = NULL;

/*CREANDO TABLA ADICIONAL Atributos (SADEPO)*/
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SADEPO';
SET @NombreGrp_1 = 'Factura_Electronica';

IF (SELECT CodTbl FROM SAAGRUPOS WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1) IS NULL
	BEGIN
		SELECT @NUMGRP_1 = ISNULL(MAX(NumGrp),0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M;

		SET @NUMGRP_1 = @NUMGRP_1 + 1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

		SET @STRING =  CONCAT('CREATE TABLE [dbo].', @NOM_TABLE_1,'(
		CodUbic VARCHAR(10) NOT NULL)

		ALTER TABLE ', @NOM_TABLE_1,' WITH NOCHECK 
		ADD CONSTRAINT ',@NOM_TABLE_1,'_IX0
		PRIMARY KEY CLUSTERED  (CodUbic) ON [PRIMARY]');

		EXEC (@STRING);
		SET @STRING = NULL;

		INSERT INTO SAAGRUPOS
		VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, @NombreGrp_1, 'Factura Electrónica', 0, 0, 0);
		
		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NroOper = '200') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, 200, 0);
			END;

		/*ACTUALIZANDO DICCIONARIO DE REPORTES*/
		Delete SATABL Where (TableName=@NOM_TABLE_1); 
		Delete SAFIEL Where (TableName=@NOM_TABLE_1);

		INSERT INTO SATABL
		VALUES (@NOM_TABLE_1, CONCAT('Deposito:',@NombreGrp_1));

		INSERT INTO SAFIEL
		VALUES (@NOM_TABLE_1,'CodUbic','CodUbic','dtString','T','T','T','F','F');
	END;
ELSE
	BEGIN 
		SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));
	
		UPDATE SAAGRUPOS SET AliasGrp = 'Factura Electrónica'
		WHERE CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NombreGrp = @NombreGrp_1;

		UPDATE SATABL SET tablealias = CONCAT('Deposito:',@NombreGrp_1)
		WHERE (TableName=@NOM_TABLE_1);

		UPDATE SAFIEL SET fieldalias = 'CodUbic'
		WHERE (TableName=@NOM_TABLE_1) AND (fieldname='CodUbic');

		IF (SELECT CodTbl FROM SAAOPER WITH (NOLOCK) WHERE CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NroOper = '200') IS NULL
			BEGIN
				INSERT INTO SAAOPER 
				VALUES (@NOM_TABLE_1_M,	@NUMGRP_1, 200, 0);
			END;
	END;

/*CREANDO CAMPOS TABLA Atributos (SADEPO)*/
IF COL_LENGTH(@NOM_TABLE_1, 'Sucursal') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Sucursal int DEFAULT(0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Sucursal int NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Llave_Sucursal') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Llave_Sucursal varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;
ELSE
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Llave_Sucursal varchar(20) NULL');
		EXEC (@STRING);
		SET @STRING = NULL;		
	END;

IF COL_LENGTH(@NOM_TABLE_1, 'Codigo_Actividad_S') IS NULL
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ADD Codigo_Actividad_S INT DEFAULT (0) NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;
ELSE 
	BEGIN
		SET @STRING = CONCAT('ALTER TABLE ', @NOM_TABLE_1,' ALTER COLUMN Codigo_Actividad_S INT NOT NULL');
		EXEC (@STRING);
		SET @STRING = NULL;	
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Sucursal') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SADEPO', @NUMGRP_1, 'Sucursal', 'Nro Sucursal', 56, 4, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Nro Sucursal', TipoCpo = 56, Longitud = 4, Value = ''
		WHERE	CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Sucursal';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Llave_Sucursal') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SADEPO', @NUMGRP_1, 'Llave_Sucursal', 'Llave Sucursal', 167, 20, 0, 0, '');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Llave Sucursal', TipoCpo = 167, Longitud = 20, Value = ''
		WHERE	CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Llave_Sucursal';
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_S') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl, NumGrp, NombCpo, AliasCpo, TipoCpo, Longitud, Requerido,CBusqueda, Value)
		VALUES	('SADEPO', @NUMGRP_1, 'Codigo_Actividad_S', 'Código Actividad Eco.', 56, 4, 0, 0, 'Seleccionar');
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET AliasCpo = 'Código Actividad Eco.', TipoCpo = 56, Longitud = 4, Value = 'Seleccionar'
		WHERE	CodTbl = 'SADEPO' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_S';
	END;

/***** FIN CREAR CODOPER PARA MODIFICAR FE BLOQUEADAS *****/
/***** INICIO ACTUALIZACION DE ACTIVIDADES ECONOMICAS EN SAACAMPOS *****/
SET @STRING = NULL;
SET @Nombre_Actividad_1 = 'ACTIVIDAD 1 NO ASIGNADA';

SET @STRING = CONCAT('SELECT @Nombre_Actividad_1 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 1');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_1 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_1 = @Nombre_Actividad_1 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_2 = 'ACTIVIDAD 2 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_2 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 2');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_2 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_2 = @Nombre_Actividad_2 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_3 = 'ACTIVIDAD 3 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_3 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 3');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_3 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_3 = @Nombre_Actividad_3 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_4 = 'ACTIVIDAD 4 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_4 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 4');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_4 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_4 = @Nombre_Actividad_4 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_5 = 'ACTIVIDAD 5 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_5 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE Nivel = 5');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_5 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_5 = @Nombre_Actividad_5 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_6 = 'ACTIVIDAD 6 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_6 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 6');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_6 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_6 = @Nombre_Actividad_6 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_7 = 'ACTIVIDAD 7 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_7 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 7');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_7 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_7 = @Nombre_Actividad_7 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_8 = 'ACTIVIDAD 8 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_8 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 8');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_8 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_8 = @Nombre_Actividad_8 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_9 = 'ACTIVIDAD 9 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_9 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 9');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_9 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_9 = @Nombre_Actividad_9 OUTPUT;

SET @STRING = NULL;
SET @Nombre_Actividad_10 = 'ACTIVIDAD 10 NO ASIGNADA';

SET @STRING = 	CONCAT('SELECT @Nombre_Actividad_10 = CONCAT(Codigo_Actividad,', ' '' '' ', ', Nombre)
FROM FEActividades_Eco WITH (NOLOCK)
WHERE Nivel = 10');

EXECUTE sp_executesql @STRING, N'
@Nombre_Actividad_10 AS VARCHAR(260) OUTPUT',
@Nombre_Actividad_10 = @Nombre_Actividad_10 OUTPUT;

SET @STRING = NULL;

---- UBICANDO DATOS DE TABLA Exoneración_y_Datos_Adicionales ---
SET @NUMGRP_2 = 0;
SET @NOM_TABLE_2_M = 'SAFACT';
SET @NombreGrp_2 = 'Exoneración_y_Datos_Adicionales';

SELECT @NUMGRP_2 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = @NombreGrp_2;

--- ACTUALIZANDO Codigo_Actividad_S en SAFACT_0X --- 
UPDATE SAACAMPOS SET Value = CONCAT('Seleccionar', CHAR(13), CHAR(10), 
@Nombre_Actividad_1, CHAR(13), CHAR(10),	
@Nombre_Actividad_2, CHAR(13), CHAR(10),
@Nombre_Actividad_3, CHAR(13), CHAR(10),
@Nombre_Actividad_4, CHAR(13), CHAR(10),
@Nombre_Actividad_5, CHAR(13), CHAR(10),
@Nombre_Actividad_6, CHAR(13), CHAR(10),
@Nombre_Actividad_7, CHAR(13), CHAR(10),
@Nombre_Actividad_8, CHAR(13), CHAR(10),
@Nombre_Actividad_9, CHAR(13), CHAR(10),
@Nombre_Actividad_10)
WHERE	CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NombCpo = 'Codigo_Actividad_S';

SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NOM_TABLE_1 = NULL;
SET @NombreGrp_1 = NULL;
SET @STRING = NULL;

--- CREANDO TABLA ADICIONAL Atributos (SADEPO)
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SADEPO';
SET @NombreGrp_1 = 'Factura_Electronica';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

--- ACTUALIZANDO Codigo_Actividad_S en SADEPO_0X --- 
UPDATE SAACAMPOS SET Value = CONCAT('Seleccionar', CHAR(13), CHAR(10), 
@Nombre_Actividad_1, CHAR(13), CHAR(10),	
@Nombre_Actividad_2, CHAR(13), CHAR(10),
@Nombre_Actividad_3, CHAR(13), CHAR(10),
@Nombre_Actividad_4, CHAR(13), CHAR(10),
@Nombre_Actividad_5, CHAR(13), CHAR(10),
@Nombre_Actividad_6, CHAR(13), CHAR(10),
@Nombre_Actividad_7, CHAR(13), CHAR(10),
@Nombre_Actividad_8, CHAR(13), CHAR(10),
@Nombre_Actividad_9, CHAR(13), CHAR(10),
@Nombre_Actividad_10)
WHERE	CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NombCpo = 'Codigo_Actividad_S';

/***** FIN ACTUALIZACION DE ACTIVIDADES ECONOMICAS EN SAACAMPOS *****/
/***** INICIO DE CREACION Y ACTUALIZACION DE SERVICIOS DE OTROS COSTOS *****/
	--- CREANDO PARA OTROS CARGOS ---
IF NOT EXISTS (SELECT Descrip FROM SAINSTA WITH (NOLOCK) WHERE Descrip = 'OTROS CARGOS')
	BEGIN
		INSERT INTO SAINSTA (InsPadre, Nivel, TipoIns, Descrip)
		VALUES	(0,0,1,'OTROS CARGOS');
	END;

SELECT TOP(1) @CodInst = CodInst
FROM SAINSTA WITH (NOLOCK)
WHERE Descrip = 'OTROS CARGOS'
ORDER BY CodInst;

	--- SERVICIOS ---
IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS01') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS01', @CodInst,'?Contribución parafiscal','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Contribución parafiscal', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS01';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS02') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS02', @CodInst,'?Timbre de la Cruz Roja','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Timbre de la Cruz Roja', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS02';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS03') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS03', @CodInst,'?Timbre de Benemérito Cuerpo de Bomberos','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Timbre de Benemérito Cuerpo de Bomberos', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS03';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS04') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra, UsaServ)
		VALUES	('OCARGOS04', @CodInst,'?Cobro de un tercero','INTERNO',1,'Car',1,1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Cobro de un tercero', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 1, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS04';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS05') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS05', @CodInst,'?Costos de Exportación','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Costos de Exportación', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS05';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS06') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS06', @CodInst,'?Impuesto de servicio 10%','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Impuesto de servicio 10%', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS06';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS07') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS07', @CodInst,'?Timbre de Colegios Profesionales','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Timbre de Colegios Profesionales', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS07';
	END;

IF (SELECT CodServ FROM SASERV WITH (NOLOCK) WHERE CodServ = 'OCARGOS99') IS NULL
	BEGIN
		INSERT INTO SASERV (CodServ, CodInst, Descrip, Clase, Activo, Unidad, EsExento, EsVenta, EsCompra)
		VALUES	('OCARGOS99', @CodInst,'?Otros Cargos','INTERNO',1,'Car',1,1,1);
	END;
ELSE
	BEGIN
		UPDATE SASERV SET CodInst = @CodInst, Descrip = '?Otros Cargos', Descrip2 = NULL, Descrip3 = NULL, Clase = 'INTERNO', Activo = 1, Unidad = 'Car', Precio1 = 0, PrecioI1 = 0, Precio2 = 0, PrecioI2 = 0, Precio3 = 0, PrecioI3 = 0, Costo = 0, EsExento = 1, EsReten = 0, EsPorCost = 0, UsaServ = 0, Comision = 0, EsPorComi = 0, EsImport = 0, EsVenta = 1, EsCompra =1
		WHERE CodServ = 'OCARGOS99';
	END;
--- FIN DE LOTE 4---
GO
/*ACTUALIZANDO TAMAÑO DE NUMEROD*/
ALTER TABLE SAFACT_01 ALTER COLUMN NumeroD varchar(20) NOT NULL;

/*COLOCANDO LENCORREL EN 10*/
UPDATE SACORRELSIS SET ValueInt = 10 WHERE FieldName = 'LenCorrel';

/*BORRANDO CODIGOS DE MONEDA COMO CODOPERACION*/
DELETE FROM SAOPER WHERE Clase = 'MONEDA';

/*CREAR CODOPER PARA DATOS DE FACTURACION ELECTRONICA*/
IF (SELECT CodOper FROM SAOPER WITH (NOLOCK) WHERE CodOper = 'FACTELEC') IS NULL
	BEGIN
		INSERT INTO SAOPER (CodOper, Descrip, Clase, TipoOpe, Rango, Activo)
		VALUES ('FACTELEC','DATOS PARA FACTURACION ELECTRONICA','TRIGGER', 0, '0000000000',1);
	END;

/*CREAR CODOPER PARA MODIFICAR FE BLOQUEADAS*/
IF (SELECT CodOper FROM SAOPER WITH (NOLOCK) WHERE CodOper = 'CLIENTEFE') IS NOT NULL
	BEGIN
		DELETE FROM SAOPER WHERE CodOper = 'CLIENTEFE';
	END;

IF (SELECT CodOper FROM SAOPER WITH (NOLOCK) WHERE CodOper = 'REENVIAR') IS NULL
	BEGIN
		INSERT INTO SAOPER (CodOper, Descrip, Clase, TipoOpe, Rango, Activo)
		VALUES	('REENVIAR', 'REENVIAR COMPROBANTE RECHAZADO FE', 'TRIGGER', 0, '0000000000', 1);
	END;
GO
/*CAMBIO DE IVI A IVA Y DE IMAD A IVA*/
-- EN PRODUCTOS
UPDATE SATAXPRD SET CodTaxs = 'IVA13', Monto = 13				
WHERE Monto IN (13,10);
-- EN SERVICIOS
UPDATE SATAXSRV SET CodTaxs = 'IVA13', Monto = 13
WHERE Monto IN (13,10);
GO
/*CAMBIO DE ETIQUETAS ADMINISTRATIVO Y VENTAS*/
-- Servidores
UPDATE SSOPMN SET Nombre = 'Terceros'
WHERE (Nombre = 'Servidores');
-- Depósitos
UPDATE SSOPMN SET Nombre = 'Bodegas'
WHERE (Nombre = 'Depósitos');
-- Presupuestos
UPDATE SSOPMN SET Nombre = REPLACE(Nombre, 'Presupuestos', 'Cotizaciones')
WHERE (Nombre LIKE '%Presupuestos%');
-- Presupuesto
UPDATE SSOPMN SET Nombre = REPLACE(Nombre, 'Presupuesto', 'Cotización')
WHERE (Nombre LIKE '%Presupuesto%');
-- Instancias de inventario
UPDATE SSOPMN SET Nombre = 'Familias de inventario'
WHERE (Nombre LIKE 'Instancias de inventario');
-- Instancias de Servicios
UPDATE SSOPMN SET Nombre = 'Familias de Servicios'
WHERE (Nombre LIKE 'Instancias de Servicios');
-- FIN DE LOTE 6
GO 
/*ACTUALIZACION DE INSTRUMENTOS DE PAGO*/
UPDATE SATARJ SET Clase = CASE 	WHEN Descrip LIKE '%Tarj%Credito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Crédito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Debito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Débito%' THEN 'DF'
								WHEN Descrip LIKE '%datafono%' THEN 'DF'
								WHEN Descrip LIKE '%Transf%' THEN 'TD'
								WHEN Descrip LIKE '%Deposito%' THEN 'TD'
								WHEN Descrip LIKE '%Depósito%' THEN 'TD'
								ELSE Clase END;
GO 
/*ACTUALIZACION DE TIPODOC Y NUMERO EN ENCABEZADO*/
UPDATE A SET	A.tipodoc = B.TipoDoc,
				A.numerod = B.NumeroD
FROM encabezado AS A INNER JOIN
(SELECT	CLAVE,
		ConsecutivoDE,
		TipoFac AS TipoDoc,
		NumeroD
FROM SAFACT
WHERE CLAVE IS NOT NULL AND ConsecutivoDE IS NOT NULL
UNION ALL
SELECT	CLAVE,
		ConsecutivoDE,
		TipoCxc AS TipoDoc,
		NumeroD
FROM SAACXC
WHERE CLAVE IS NOT NULL AND ConsecutivoDE IS NOT NULL
UNION ALL
SELECT	CLAVE,
		ConsecutivoDE,
		TipoCom AS TipoDoc,
		NumeroD
FROM SACOMP
WHERE CLAVE IS NOT NULL AND ConsecutivoDE IS NOT NULL) AS B
ON A.clave = B.Clave
WHERE (A.tipodoc IS NULL) AND (A.numerod IS NULL);
GO
/*ACTUALIZA CONSECUTIVODE EN DOCUMENT DE SAACXC Y MONEDA EN SAFACT_01*/
UPDATE A SET A.Document = B.ConsecutivoDE
FROM SAACXC AS A INNER JOIN SAFACT AS B
ON A.NumeroD = B.NumeroD
WHERE A.TipoCxc = '10' AND B.TipoFac = 'A' AND A.FromTran = 0 AND B.ConsecutivoDE IS NOT NULL OR A.TipoCxc = '10' AND B.TipoFac = 'A' AND A.FromTran = 0 AND B.ConsecutivoDE <> '';

UPDATE SAACXC SET Document = ConsecutivoDE
WHERE TipoCxc = '10' AND FromTran = 1 AND ConsecutivoDE IS NOT NULL OR TipoCxc = '10' AND FromTran = 1 AND ConsecutivoDE <> '';

DECLARE
@MONEDAP VARCHAR(5),
@MONEDAR VARCHAR(5),
@NUMGRP_1 AS INT,
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX)

SELECT	@MONEDAP = CodMoneda,@MONEDAR = CodMonedaR
FROM SACONF WITH (NOLOCK);

SET @MONEDAP = ISNULL(@MONEDAP,'CRC');

/*UBICANDO DATOS DE TABLA Atributos (SAINST)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NOM_TABLE_1 = NULL;
SET @NombreGrp_1 = NULL;
SET @STRING = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SAFACT';
SET @NombreGrp_1 = 'Exoneración_y_Datos_Adicionales';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('UPDATE A SET A.Moneda = (CASE WHEN B.CodOper = ', '''',@MONEDAP,'''', ' THEN 1 WHEN B.CodOper = ', '''',@MONEDAR, '''',' THEN 2 ELSE 1 END)',
' FROM ', QUOTENAME(@NOM_TABLE_1),' AS A INNER JOIN SAFACT AS B',
' ON A.TipoFac = B.TipoFac AND A.NumeroD = B.NumeroD',
' WHERE A.TipoFac  IN (','''','A','''',',','''','B','''',')', ' AND A.Moneda = 0');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;
GO
/*CARGANDO TABLAS ADICIONALES SAINT*/
DECLARE
@NUMGRP_1 INT,  -- TABLA Atributos (SADEPO)
@NOM_TABLE_1_M VARCHAR(50),
@NOM_TABLE_1 VARCHAR(50),
@NombreGrp_1 VARCHAR(50),
@STRING NVARCHAR(MAX),
@VersionFE varchar(20)

/*UBICANDO DATOS DE TABLA Correos_y_Datos_Adiconales (SACLIE)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SACLIE';
SET @NombreGrp_1 = 'Correos_y_Datos_Adiconales';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('INSERT INTO ', QUOTENAME(@NOM_TABLE_1), ' (CodClie, No_Ubicacion, Codigo_Pais)
SELECT A.CodClie, 0, 0
FROM SACLIE A WITH (NOLOCK) FULL OUTER JOIN ', QUOTENAME(@NOM_TABLE_1), ' B WITH (NOLOCK)
ON A.CodClie = B.CodClie
WHERE (B.CodClie IS NULL)');

EXECUTE sp_executesql @STRING; 
SET @STRING = NULL;

/*UBICANDO DATOS DE TABLA Exoneracion (SACLIE)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SACLIE';
SET @NombreGrp_1 = 'Exoneracion';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('INSERT INTO ', QUOTENAME(@NOM_TABLE_1), ' (CodClie, Tipo_documento, Porcentaje)
SELECT A.CodClie, 0, 0
FROM SACLIE A WITH (NOLOCK) FULL OUTER JOIN ', QUOTENAME(@NOM_TABLE_1), ' B WITH (NOLOCK)
ON A.CodClie = B.CodClie
WHERE (B.CodClie IS NULL)');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;

SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_1), ' SET [Porcentaje] = 13', 
' WHERE [Porcentaje] > 13 ');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;

/*UBICANDO DATOS DE TABLA Datos_Adicionales (SAPROD)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SAPROD';
SET @NombreGrp_1 = 'Datos_Adicionales';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('INSERT INTO ', QUOTENAME(@NOM_TABLE_1), ' (CodProd)
SELECT A.CodProd
FROM SAPROD A WITH (NOLOCK) FULL OUTER JOIN ', QUOTENAME(@NOM_TABLE_1), ' B WITH (NOLOCK)
ON A.CodProd = B.CodProd
WHERE (B.CodProd IS NULL)');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;

/*UBICANDO DATOS DE TABLA Datos_Adicionales (SASERV)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SASERV';
SET @NombreGrp_1 = 'Datos_Adicionales';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('INSERT INTO ', QUOTENAME(@NOM_TABLE_1), ' (CodServ)
SELECT A.CodServ
FROM SASERV A WITH (NOLOCK) FULL OUTER JOIN ', QUOTENAME(@NOM_TABLE_1), ' B WITH (NOLOCK)
ON A.CodServ = B.CodServ
WHERE (B.CodServ IS NULL)');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;

/*COLOCANDO COMO Clase T a los clientes CONNTADO*/
UPDATE [dbo].[SACLIE] SET [Clase] = 'T'
WHERE [Descrip] like '?%CONTADO%' AND [Clase] <> 'P';

/*CONFIGURANDO CLASE T Y P TABLA Correos_y_Datos_Adiconales (SACLIE)*/
SELECT TOP(1) @VersionFE = DBO.RemoveChars(VersionFE)
FROM SACONF WITH (NOLOCK);

SET @VersionFE = ISNULL(@VersionFE,'0');

IF (CAST(@VersionFE AS int) <= 4355)
	BEGIN
		SET @NUMGRP_1 = NULL;
		SET @NOM_TABLE_1_M = NULL;
		SET @NombreGrp_1 = NULL;
		SET @NOM_TABLE_1 = NULL;

		SET @NUMGRP_1 = 0;
		SET @NOM_TABLE_1_M = 'SACLIE';
		SET @NombreGrp_1 = 'Correos_y_Datos_Adiconales';

		SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

		SET @STRING = CONCAT('UPDATE B SET B.[Receptor_Incluir] = 0,',
		' B.[Ubicacion_Incluir] = 0',
		' FROM [SACLIE] AS A INNER JOIN ', QUOTENAME(@NOM_TABLE_1), ' AS B',
		' ON A.[CodClie] = B.[CodClie]');

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;
	END;

/*CREANDO REGISTROS A DEPOSITOS EN TABLA ADICIONAL FE (SADEPO)*/
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SADEPO';
SET @NombreGrp_1 = 'Factura_Electronica';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('INSERT INTO ', QUOTENAME(@NOM_TABLE_1), ' (CodUbic)
SELECT A.CodUbic
FROM SADEPO A WITH (NOLOCK) FULL OUTER JOIN ', QUOTENAME(@NOM_TABLE_1), ' B WITH (NOLOCK)
ON A.CodUbic = B.CodUbic
WHERE (B.CodUbic IS NULL)');

EXECUTE sp_executesql @STRING;
SET @STRING = NULL;
GO