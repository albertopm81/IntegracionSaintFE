/***FUNCIONES***/
/*REGALIAS VENTAS*/
IF EXISTS (SELECT OBJECT_NAME(sm.object_id) AS object_name FROM sys.sql_modules AS sm  INNER JOIN sys.objects AS o ON sm.object_id = o.object_id WHERE sm.object_id = OBJECT_ID('[dbo].[FN_ADM_TAXPRICE]'))
	BEGIN
		DROP FUNCTION [dbo].[FN_ADM_TAXPRICE];
	END;
GO
SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [DBO].FN_ADM_TAXPRICE(@CODIGOI VARCHAR(20), @PRECIO DECIMAL(28,5), @CANTIDAD DECIMAL(28,5),  @ESSERV INT, @ESUNID INT) RETURNS DECIMAL(28,5) 
WITH ENCRYPTION AS 
BEGIN 
  DECLARE 
  @TOTALTAX DECIMAL(28,5),
  @REGALIAS SMALLINT; 
  SET @TOTALTAX=0; 

  SELECT TOP(1) @REGALIAS = Regalias
  FROM SACONF WITH (NOLOCK);

  SET @REGALIAS = ISNULL(@REGALIAS,0);

  If @ESSERV=0 
	BEGIN
		IF (@REGALIAS = 1)
			BEGIN
				IF (@PRECIO = 0)
					BEGIN
						IF (@CODIGOI NOT IN ('OCARGOS01','OCARGOS02','OCARGOS03','OCARGOS04','OCARGOS05','OCARGOS06','OCARGOS07','OCARGOS99'))
							BEGIN
								SELECT @PRECIO = IIF(@ESUNID = 1,CostAct/IIF(CantEmpaq=0,1,CantEmpaq),CostAct)
								FROM SAPROD WITH (NOLOCK)
								WHERE CodProd = @CODIGOI;
							END;
					END;
			END;

		SELECT @TOTALTAX= dbo.FN_ADM_PRICETAXPRD(@CODIGOI,@PRECIO,@CANTIDAD,@ESUNID)-@PRECIO 
	END;
  ELSE 
	BEGIN
		IF (@REGALIAS = 1)
			BEGIN
				IF (@PRECIO = 0)
					BEGIN
						IF (@CODIGOI NOT IN ('OCARGOS01','OCARGOS02','OCARGOS03','OCARGOS04','OCARGOS05','OCARGOS06','OCARGOS07','OCARGOS99'))
							BEGIN
								SELECT @PRECIO = Costo
								FROM SASERV WITH (NOLOCK)
								WHERE CodServ = @CODIGOI;
							END;
					END;
			END;

		SELECT @TOTALTAX= dbo.FN_ADM_PRICETAXSRV(@CODIGOI,@PRECIO,@CANTIDAD)-@PRECIO 
	END;
  RETURN @TOTALTAX;
END
GO
/*FUNCION PARA REMOVER CARACTERES*/
DECLARE 
@TEXTO AS VARCHAR(20),
@FUNCION AS NVARCHAR(MAX)

IF OBJECT_ID('RemoveChars') IS NULL
	SET @TEXTO = 'CREATE';
ELSE
	SET @TEXTO = 'ALTER';

SET @FUNCION = CONCAT(@TEXTO, ' FUNCTION dbo.RemoveChars(@Input varchar(1000))
RETURNS VARCHAR(1000)
with encryption
BEGIN
  DECLARE @pos INT
  SET @Pos = PATINDEX(''%[^0-9]%'',@Input)
  WHILE @Pos > 0
   BEGIN
    SET @Input = STUFF(@Input,@pos,1,'''')
    SET @Pos = PATINDEX(''%[^0-9]%'',@Input)
   END
  RETURN @Input
END');

EXEC (@FUNCION);
GO
/***ELIMINANDO TRIGGER VIEJOS***/
-- TRIGGER SAFACT_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_ENCABEZADO_01') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_ENCABEZADO_01];
	END;
GO
-- TRIGGER SAITEMFAC
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_LINEA') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_LINEA];
	END;
GO
-- TRIGGER SAACXC
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_ENCABEZADO_02') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_ENCABEZADO_02];
	END;
GO
-- TRIGGER SAPAGCXC
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_ENCABEZADO_03') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_ENCABEZADO_03];
	END;
GO
-- TRIGGER SATAXVTA
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_TAXVTA') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_TAXVTA];
	END;
GO
-- TRIGGER SATAXITF
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_TAXITF') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_TAXITF];
	END;
GO
-- TRIGGER SATAXCXC
IF (SELECT name FROM SYS.triggers WHERE name = 'FACT_ELECTRONICA_TAXCXC') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FACT_ELECTRONICA_TAXCXC];
	END;
GO
-- TRIGGER SACLIE
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_CEDULA_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_CEDULA_FE];
	END;
GO
-- TRIGGER SACONF
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_EMISOR_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_EMISOR_FE];
	END;
GO
-- TRIGGER SAOPER_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_DATOS_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_DATOS_FE];
	END;
GO
-- TRIGGER SAOPER_0X+1
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_CORRIGE_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_CORRIGE_FE];
	END;
GO
-- TRIGGER SAEVEN
IF (SELECT name FROM SYS.triggers WHERE name = 'CIERRE_VEN_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[CIERRE_VEN_FE];
	END;
GO
-- TRIGGER SAECLI
IF (SELECT name FROM SYS.triggers WHERE name = 'CIERRE_CLI_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[CIERRE_CLI_FE];
	END;
GO
-- TRIGGER SAITEPRDFAC_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_AdicionalesPRD_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_AdicionalesPRD_FE];
	END;
GO
-- TRIGGER SAITESERVFAC_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_Otros_Cargos_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_Otros_Cargos_FE];
	END;
GO
-- TRIGGER SAPROV
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_CED_PROV_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_CED_PROV_FE];
	END;
GO
-- TRIGGER SACOMP_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_COMP_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_COMP_FE];
	END;
GO
-- TRIGGER SACLIE_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_CLIENTES') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_CLIENTES];
	END;
GO
-- TRIGGER SATARJ
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_TARJ_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_TARJ_FE];
	END;
GO
-- TRIGGER SAMECA
IF (SELECT name FROM SYS.triggers WHERE name = 'CORRIJE_TERC_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[CORRIJE_TERC_FE];
	END;
GO
-- TRIGGER SAPROD_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_PART_ARAN') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_PART_ARAN];
	END;
GO
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_PART_ARAN_CABYS') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_PART_ARAN_CABYS];
	END;
GO
-- TRIGGER SASERV_0X
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_SERV_CABYS') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_SERV_CABYS];
	END;
GO
-- TRIGGER SAACXP
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_SAACXP') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_SAACXP];
	END;
GO
-- TRIGGER SAPAGCXP
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_SAPAGCXP') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_SAPAGCXP];
	END;
GO
-- TRIGGER SADEPO
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_SUCURSAL') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_SUCURSAL];
	END;
GO
-- TRIGGER SATAXES
IF (SELECT name FROM SYS.triggers WHERE name = 'FE_TAX_CLIENTES') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[FE_TAX_CLIENTES];
	END;
GO
/***TRIGGERS NUEVOS***/
/*CLAVE Y CONSECUTIVO FACTURAS Y DEVOLUCIONES*/
/*INICIO TRIGGER SAFACT_0X*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_CLAVE_VTA_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_CLAVE_VTA_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)
-- UBICANDO DATOS DE TABLA Datos_para_facturacion_electronica
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SAFACT';
SET @NombreGrp = 'Exoneración_y_Datos_Adicionales';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_CLAVE_VTA_FE] ON [dbo].['+@NOM_TABLE+'] with encryption 
AFTER INSERT
AS 
BEGIN
DECLARE 
@Tipofac varchar(2), 
@Numerod varchar(20), 
@Numeror varchar(20),
@Nrounico int, 
@Codclie varchar(15), 
@Clave varchar(50),
@Fecha datetime, 
@User varchar(50),
@Dia varchar(2),
@Mes varchar(2), 
@Ano varchar(2), 
@Sucursal int,
@Sucursal_principal int, 
@Estacion int,
@Numgrp_1 int,
@Nom_table_1_m varchar(50), 
@Nom_table_1 varchar(50), 
@NombreGrp_1 varchar(50),
@String nvarchar(MAX), 
@Consecutivo_incompleto varchar(8), 
@Consecutivo_completo varchar(20),
@Idclave varchar(12), 
@Tipo_documento varchar(2), 
@Consecutivo_origen varchar(10),
@Llave_sucursal varchar(20), 
@Llave_sucursal_principal varchar(20), 
@Ref_tipo_doc varchar(2),
@Ref_clave varchar(50), 
@Ref_fecha datetime, 
@CodUbic varchar(10),
@Notas10 varchar(60), 
@LenCorrelFE int,
@PrxFactFE int,
@PrxNDFE int, 
@PrxNCFE int, 
@PrxTickEFE int,
@PrxCompCFE int, 
@PrxFExpFE int, 
@TipoCli smallint,
@Validador2 varchar(1), 
@Clase varchar(10), 
@Emisor_identificacion varchar(12),
@Tipo_documentoAux_Fac varchar(2),
@Numero_DocumentoAux_Fac varchar(17),
@Nombre_InstitucionAux_Fac varchar(99),
@Fecha_DocumentoAux_Fac datetime,
@Porcentaje_Fac int,
@Porcentaje_Cli int,
@Tipo_documentoAux_Cli AS VARCHAR(2),
@Numero_DocumentoAux_Cli varchar(17),
@Nombre_InstitucionAux_Cli varchar(99),
@Fecha_DocumentoAux_Cli datetime,
@codclie2 varchar(17),
@codclie3 varchar(15),
@table_1 SYSNAME,
@tipofac2 varchar(3),
@numerod2 varchar(22),
@vale01 smallint,
@vale02 smallint,
@vale03 smallint,
@vale04 smallint,
@Tipo_documentoAux varchar(2),
@Numero_DocumentoAux varchar(17),
@Nombre_InstitucionAux varchar(99),
@Fecha_DocumentoAux datetime,
@Porcentaje int,
@Tipo_Doc_Aux int,
@Guardar_Exoneracion smallint,
@Ref_razon varchar(180),
@Ref_codigo varchar(2),
@Pedido_ICE varchar(50),
@NroUnicoAC int,
@Nro_Recepcion varchar(20),
@Fecha_Orden datetime,
@WMGLN_FAC varchar(20),
@IDMonedaR int
	
SET NOCOUNT ON;
SET @User = SYSTEM_USER;	-- Usuario de conexion sql

SELECT	@Tipofac = TipoFac, 
				@Numerod = NumeroD,
				@Tipo_documentoAux_Fac = CASE Tipo_documento 	WHEN 1 THEN ''01'' WHEN 2 THEN ''02'' WHEN 3 THEN ''03'' WHEN 4 THEN ''04'' WHEN 5 THEN ''05'' WHEN 6 THEN ''06'' WHEN 7 THEN ''07'' WHEN 8 THEN ''99'' ELSE NULL END,
				@Numero_DocumentoAux_Fac = Numero_Documento,
				@Nombre_InstitucionAux_Fac = Nombre_Institucion,
				@Fecha_DocumentoAux_Fac = Fecha_Documento,
				@Porcentaje_Fac = ISNULL(IIF(Porcentaje > 100, 100, IIF(Porcentaje < 0, 0, Porcentaje)),0),
				@Pedido_ICE = Pedido_ICE,
				@Guardar_Exoneracion = Guardar_Datos_Exoneracion
FROM inserted;

-- RESTRICCION
IF (ISNULL(LEN(@NUMEROD),0) > 0)
	BEGIN
		-- DETERMINANDO QUE DATOS DE EXONERACION USAR
		IF (@Tipofac = ''A'')
			BEGIN
				SET @Tipo_documentoAux_Cli = NULL;
				SET @Numero_DocumentoAux_Cli = NULL;
				SET @Nombre_InstitucionAux_Cli = NULL;
				SET @Fecha_DocumentoAux_Cli = NULL;
				SET @codclie3 = NULL;
				SET @codclie2 = CONCAT('''''''', @codclie,'''''''');

				SET @Numgrp_1 = NULL;
				SET @Nom_table_1_m = NULL;
				SET @NombreGrp_1 = NULL;
				SET @Nom_table_1 = NULL;
				SET @Table_1 = NULL;			

				SET @NUMGRP_1 = 0;
				SET @NOM_TABLE_1_M = ''SACLIE'';
				SET @NombreGrp_1 = ''Exoneracion'';

				SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
				FROM SAAGRUPOS WITH (NOLOCK)
				WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

				SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, ''_'',REPLICATE(''0'',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

				SET @TABLE_1 = @NOM_TABLE_1;

				SET @Tipofac2 = CONCAT('''''''',@Tipofac,'''''''');
				SET @Numerod2 = CONCAT('''''''',@Numerod,'''''''');

				SET @String = CONCAT(
				''SELECT 	@Codclie3 = CodClie,
							@Tipo_documentoAux_Cli = CASE Tipo_documento 	WHEN 1 THEN ''''01'''' 
																			WHEN 2 THEN ''''02'''' 
																			WHEN 3 THEN ''''03''''
																			WHEN 4 THEN ''''04''''
																			WHEN 5 THEN ''''05''''
																			WHEN 6 THEN ''''06''''
																			WHEN 7 THEN ''''07''''
																			WHEN 8 THEN ''''99''''
													 ELSE NULL END,
							@Numero_DocumentoAux_Cli = Numero_Documento,
							@Nombre_InstitucionAux_Cli = Nombre_Institucion,
							@Fecha_DocumentoAux_Cli = Fecha_Documento,
							@Porcentaje_Cli = ISNULL(IIF(Porcentaje > 100, 100, IIF(Porcentaje < 0, 0, Porcentaje)),0)
				FROM '', QUOTENAME(@TABLE_1),'' WITH (NOLOCK)
				WHERE CodClie = '', @Codclie2);

				EXECUTE sp_executesql @STRING, N''
				@CODCLIE3 AS VARCHAR(15) OUTPUT,
				@Tipo_documentoAux_Cli AS VARCHAR(2) OUTPUT,
				@Numero_DocumentoAux_Cli varchar(17) OUTPUT,
				@Nombre_InstitucionAux_Cli varchar(99) OUTPUT,
				@Fecha_DocumentoAux_Cli datetime OUTPUT,
				@Porcentaje_Cli AS INT OUTPUT'',
				@CODCLIE3 = @CODCLIE3 OUTPUT,
				@Tipo_documentoAux_Cli = @Tipo_documentoAux_Cli OUTPUT,
				@Numero_DocumentoAux_Cli = @Numero_DocumentoAux_Cli OUTPUT,
				@Nombre_InstitucionAux_Cli = @Nombre_InstitucionAux_Cli OUTPUT,
				@Fecha_DocumentoAux_Cli = @Fecha_DocumentoAux_Cli OUTPUT,
				@Porcentaje_Cli = @Porcentaje_Cli OUTPUT;

				SET @STRING = NULL;

				IF (ISNULL(@Tipo_documentoAux_Fac,0) = 0)
					BEGIN					
						SET @Vale01 = 1;
					END;
				ELSE
					BEGIN
						SET @Vale01 = 0;					
					END;
				IF (ISNULL(LEN(@Numero_DocumentoAux_Fac),0) = 0)
					BEGIN					
						SET @Vale02 = 1;
					END;
				ELSE
					BEGIN						
						SET @Vale02 = 0;					
					END;
				IF (ISNULL(LEN(@Nombre_InstitucionAux_Fac),0) = 0)
					BEGIN					
						SET @Vale03 = 1;
					END;
				ELSE
					BEGIN						
						SET @Vale03 = 0;					
					END;
				IF (ISNULL(@Porcentaje_Fac,0) = 0)
					BEGIN						
						SET @Vale04 = 1;
					END;
				ELSE
					BEGIN
						SET @Vale04 = 0;
					END;

				-- Validando si aplicar exoneracion de la factura o de la ficha del cliente
				IF (@Vale01 = 0) AND (@Vale02 = 0) AND (@Vale03 = 0) AND (@Vale04 = 0)
					BEGIN
						SET @Tipo_documentoAux = @Tipo_documentoAux_Fac;
						SET @Numero_DocumentoAux = @Numero_DocumentoAux_Fac;
						SET @Nombre_InstitucionAux = @Nombre_InstitucionAux_Fac;
						SET @Porcentaje = @Porcentaje_Fac;
						SET @Fecha_DocumentoAux = @Fecha_DocumentoAux_Fac;								
					END;
				ELSE
					BEGIN
						SET @Tipo_documentoAux = @Tipo_documentoAux_Cli;
						SET @Numero_DocumentoAux = @Numero_DocumentoAux_Cli;
						SET @Nombre_InstitucionAux = @Nombre_InstitucionAux_Cli;
						SET @Porcentaje = @Porcentaje_Cli;
						SET @Fecha_DocumentoAux = @Fecha_DocumentoAux_Cli;
					END;

				SET @Tipo_Doc_Aux = CASE @Tipo_documentoAux WHEN ''01'' THEN 1 WHEN ''02'' THEN 2 WHEN ''03'' THEN 3 WHEN ''04'' THEN 4 WHEN ''05'' THEN 5 WHEN ''06'' THEN 6 WHEN ''07'' THEN 7 WHEN ''99'' THEN 8 ELSE NULL END;

				-- Guardando datos de exoneracion en ficha de cliente
				IF (@Guardar_Exoneracion = 1)
					BEGIN
						IF (@CODCLIE3 IS NOT NULL)
							BEGIN
								SET @String = CONCAT(''UPDATE '',QUOTENAME(@Table_1),'' SET  Tipo_documento = '',IIF((@Tipo_Doc_Aux IS NULL) OR (@Tipo_Doc_Aux = ''''), ''Tipo_documento'', CAST(@Tipo_Doc_Aux AS VARCHAR(10))),'', 
								 Numero_Documento = '',IIF((@Numero_DocumentoAux IS NULL) OR (@Numero_DocumentoAux = ''''), ''Numero_Documento'',CONCAT('''''''',@Numero_DocumentoAux,'''''''')),'', 
								 Nombre_Institucion = '',IIF((@Nombre_InstitucionAux IS NULL) OR (@Nombre_InstitucionAux = ''''), ''Nombre_Institucion'',CONCAT('''''''',@Nombre_InstitucionAux,'''''''')),'', 
								 Fecha_Documento = '',IIF((@Fecha_DocumentoAux IS NULL) OR (@Fecha_DocumentoAux = ''''), ''Fecha_Documento'',CONCAT('''''''',CONVERT(VARCHAR(30),@Fecha_DocumentoAux,126),'''''''')),'', 
								 Porcentaje = '',IIF((@Porcentaje IS NULL) OR (@Porcentaje = ''''), ''Porcentaje'',CAST(@Porcentaje AS VARCHAR(10))),''
								 WHERE CodClie = '',@CODCLIE2);

								EXECUTE sp_executesql @String;
								SET @String = NULL;

								IF (@Porcentaje = 100)
									BEGIN
										SET @STRING = CONCAT(''UPDATE SACLIE SET TipoCli = 3 WHERE CodClie = '',@Codclie2);
									
										EXECUTE sp_executesql @String;
										SET @String = NULL;
									END;
								ELSE
									BEGIN
										SET @STRING = CONCAT(''UPDATE SACLIE SET TipoCli = 0 WHERE CodClie = '',@Codclie2);	
									
										EXECUTE sp_executesql @String;
										SET @String = NULL;
									END;
							END;
						ELSE
							BEGIN
								SET @String = CONCAT(''INSERT INTO '',QUOTENAME(@Table_1),'' (CodClie, Tipo_documento, Numero_Documento, Nombre_Institucion, Fecha_Documento, Porcentaje)
														VALUES ('',CONCAT('''''''',@CODCLIE,''''''''),'',
																'',IIF((@Tipo_Doc_Aux IS NULL) OR (@Tipo_Doc_Aux = ''''), ''NULL'', CAST(@Tipo_Doc_Aux AS VARCHAR(10))),'', 
																'',IIF((@Numero_DocumentoAux IS NULL) OR (@Numero_DocumentoAux = ''''), ''NULL'',CONCAT('''''''',@Numero_DocumentoAux,'''''''')),'', 
																'',IIF((@Nombre_InstitucionAux IS NULL) OR (@Nombre_InstitucionAux = ''''), ''NULL'',CONCAT('''''''',@Nombre_InstitucionAux,'''''''')),'', 
																'',IIF((@Fecha_DocumentoAux IS NULL) OR (@Fecha_DocumentoAux = ''''), ''NULL'',CONCAT('''''''',CONVERT(VARCHAR(30),@Fecha_DocumentoAux,126),'''''''')),'', 
																'',IIF((@Porcentaje IS NULL) OR (@Porcentaje = ''''), ''NULL'',CAST(@Porcentaje AS VARCHAR(10))),'')'');

								EXECUTE sp_executesql @String;
								SET @String = NULL;		

								IF (@Porcentaje = 100)
									BEGIN
										SET @String = CONCAT(''UPDATE SACLIE SET TipoCli = 3 WHERE CodClie = '',@Codclie2);
									
										EXECUTE sp_executesql @STRING;
										SET @String = NULL;
									END;
								ELSE
									BEGIN
										SET @String = CONCAT(''UPDATE SACLIE SET TipoCli = 0 WHERE CodClie = '',@Codclie2);	
									
										EXECUTE sp_executesql @String;
										SET @String = NULL;
									END;			
							END;
					END;

				SET @Numgrp_1 = NULL;
				SET @Nom_table_1_m = NULL;
				SET @NombreGrp_1 = NULL;
				SET @Nom_table_1 = NULL;
				SET @Table_1 = NULL;

				SET @Numgrp_1 = 0;
				SET @Nom_table_1_m = ''SAFACT'';
				SET @NombreGrp_1 = ''Exoneración_y_Datos_Adicionales'';

				SELECT @Numgrp_1 = ISNULL(NumGrp,0)
				FROM SAAGRUPOS WITH (NOLOCK)
				WHERE CodTbl = @Nom_table_1_m AND NombreGrp = @NombreGrp_1;

				SET @Nom_table_1 = CONCAT(@Nom_table_1_m, ''_'',REPLICATE(''0'',2 - LEN(CAST(@Numgrp_1 AS Varchar(2)))),CAST(@Numgrp_1 AS Varchar(2)));

				SET @TABLE_1 = @NOM_TABLE_1;

				--- ACTUALIZANDO TABLA ADICIONAL EXONERACION ESPECIAL EN FACTURAS ---
				SET @String = CONCAT(''UPDATE '',QUOTENAME(@Table_1),'' SET  Tipo_documento = '',IIF((@Tipo_Doc_Aux IS NULL) OR (@Tipo_Doc_Aux = ''''), ''Tipo_documento'', CAST(@Tipo_Doc_Aux AS VARCHAR(10))),'', 
																			 Numero_Documento = '',IIF((@Numero_DocumentoAux IS NULL) OR (@Numero_DocumentoAux = ''''), ''Numero_Documento'',CONCAT('''''''',@Numero_DocumentoAux,'''''''')),'', 
																			 Nombre_Institucion = '',IIF((@Nombre_InstitucionAux IS NULL) OR (@Nombre_InstitucionAux = ''''), ''Nombre_Institucion'',CONCAT('''''''',@Nombre_InstitucionAux,'''''''')),'', 
																			 Fecha_Documento = '',IIF((@Fecha_DocumentoAux IS NULL) OR (@Fecha_DocumentoAux = ''''), ''Fecha_Documento'',CONCAT('''''''',CONVERT(VARCHAR(30),@Fecha_DocumentoAux,126),'''''''')),'', 
																			 Porcentaje = '',IIF((@Porcentaje IS NULL) OR (@Porcentaje = ''''), ''Porcentaje'',CAST(@Porcentaje AS VARCHAR(10))),''
																			 WHERE Tipofac = '',@Tipofac2,'' AND NumeroD = '',@Numerod2);

				EXECUTE sp_executesql @STRING;
				SET @String = NULL;
				SET @Codclie2 = NULL;
				SET @Numgrp_1 = NULL;
				SET @Nom_table_1_m = NULL;
				SET @NombreGrp_1 = NULL;
				SET @Nom_table_1 = NULL;
				SET @Table_1 = NULL;
			END;

		IF (@Tipofac = ''B'')
			BEGIN
				SET @Numgrp_1 = 0;
				SET @Nom_table_1_m = ''SAFACT'';
				SET @Nombregrp_1 = ''Exoneración_y_Datos_Adicionales'';

				SELECT @Numgrp_1 = ISNULL(NumGrp,0)
				FROM SAAGRUPOS WITH (NOLOCK)
				WHERE CodTbl = @Nom_table_1_m AND NombreGrp = @NombreGrp_1;

				SET @Nom_table_1 = CONCAT(@Nom_table_1_m, ''_'',REPLICATE(''0'',2 - LEN(CAST(@Numgrp_1 AS VARCHAR(2)))),CAST(@Numgrp_1 AS VARCHAR(2)));

				SET @Table_1 = @Nom_table_1;
				SET @Tipofac2 = CONCAT('''''''',''A'','''''''');
				SET @Numerod2 = CONCAT('''''''',@Numeror,'''''''');

				SET @NroUnicoAC = NULL;
				SET @Nro_Recepcion = NULL;
				SET @Fecha_Orden = NULL;
				SET @WMGLN_FAC = NULL;

				SET @String = CONCAT(
				''SELECT 	@Tipo_documentoAux_Fac = CASE Tipo_documento 	WHEN 1 THEN ''''01'''' 
																			WHEN 2 THEN ''''02'''' 
																			WHEN 3 THEN ''''03''''
																			WHEN 4 THEN ''''04''''
																			WHEN 5 THEN ''''05''''
																			WHEN 6 THEN ''''06''''
																			WHEN 7 THEN ''''07''''
																			WHEN 8 THEN ''''99''''
																			ELSE NULL END,
							@Numero_DocumentoAux_Fac = Numero_Documento,
							@Nombre_InstitucionAux_Fac = Nombre_Institucion,
							@Fecha_DocumentoAux_Fac = Fecha_Documento,
							@NroUnicoAC = Codigo_Actividad_S,
							@IDMonedaR = Moneda,
							@Porcentaje_Fac = ISNULL(IIF(Porcentaje > 100, 100, IIF(Porcentaje < 0, 0, Porcentaje)),0),
							@Nro_Recepcion = Nro_Recepcion, 
							@Fecha_Orden = Fecha_Orden,							
							@WMGLN_FAC = WM_GLN
				FROM '', QUOTENAME(@Table_1),'' WITH (NOLOCK)
				WHERE TipoFac = '', @Tipofac2, '' AND NumeroD = '',@Numerod2);

				EXECUTE sp_executesql @STRING, N''
				@Tipo_documentoAux_Fac AS VARCHAR(2) OUTPUT,
				@Numero_DocumentoAux_Fac varchar(17) OUTPUT,
				@Nombre_InstitucionAux_Fac varchar(99) OUTPUT,
				@Fecha_DocumentoAux_Fac datetime OUTPUT,
				@NroUnicoAC AS INT OUTPUT,
				@IDMonedaR AS INT OUTPUT,
				@Porcentaje_Fac AS INT OUTPUT,
				@Nro_Recepcion AS varchar(20) OUTPUT,
				@Fecha_Orden datetime OUTPUT,
				@WMGLN_FAC AS varchar(20) OUTPUT'',
				@Tipo_documentoAux_Fac = @Tipo_documentoAux_Fac OUTPUT,
				@Numero_DocumentoAux_Fac = @Numero_DocumentoAux_Fac OUTPUT,
				@Nombre_InstitucionAux_Fac = @Nombre_InstitucionAux_Fac OUTPUT,
				@Fecha_DocumentoAux_Fac = @Fecha_DocumentoAux_Fac OUTPUT,
				@NroUnicoAC = @NroUnicoAC OUTPUT,
				@IDMonedaR = @IDMonedaR OUTPUT,
				@Porcentaje_Fac = @Porcentaje_Fac OUTPUT,
				@Nro_Recepcion = @Nro_Recepcion OUTPUT,
				@Fecha_Orden = @Fecha_Orden OUTPUT,
				@WMGLN_FAC = @WMGLN_FAC OUTPUT;
				
				SET @String = NULL;

				SET @Tipo_documentoAux = @Tipo_documentoAux_Fac;
				SET @Numero_DocumentoAux = @Numero_DocumentoAux_Fac;
				SET @Nombre_InstitucionAux = @Nombre_InstitucionAux_Fac;
				SET @Fecha_DocumentoAux = @Fecha_DocumentoAux_Fac;
				SET @Porcentaje = @Porcentaje_Fac;

				SET @Tipo_Doc_Aux = CASE @Tipo_documentoAux WHEN ''01'' THEN 1  
															WHEN ''02'' THEN 2 
															WHEN ''03'' THEN 3
															WHEN ''04'' THEN 4
															WHEN ''05'' THEN 5
															WHEN ''06'' THEN 6
															WHEN ''07'' THEN 7
															WHEN ''99'' THEN 8													
															ELSE NULL END;

				SET @Tipofac2 = NULL;
				SET @Numerod2 = NULL;
				SET @Tipofac2 = CONCAT('''''''',@Tipofac,'''''''');
				SET @Numerod2 = CONCAT('''''''',@Numerod,'''''''');

				--- ACTUALIZANDO TABLA ADICIONAL EXONERACION ESPECIAL EN FACTURAS ---
				SET @String = CONCAT(''UPDATE '',QUOTENAME(@Table_1),'' SET  Tipo_documento = '',IIF((@Tipo_Doc_Aux IS NULL) OR (@Tipo_Doc_Aux = ''''), ''Tipo_documento'', CAST(@Tipo_Doc_Aux AS VARCHAR(10))),'', 
																			 Numero_Documento = '',IIF((@Numero_DocumentoAux IS NULL) OR (@Numero_DocumentoAux = ''''), ''Numero_Documento'',CONCAT('''''''',@Numero_DocumentoAux,'''''''')),'', 
																			 Nombre_Institucion = '',IIF((@Nombre_InstitucionAux IS NULL) OR (@Nombre_InstitucionAux = ''''), ''Nombre_Institucion'',CONCAT('''''''',@Nombre_InstitucionAux,'''''''')),'', 
																			 Fecha_Documento = '',IIF((@Fecha_DocumentoAux IS NULL) OR (@Fecha_DocumentoAux = ''''), ''Fecha_Documento'',CONCAT('''''''',CONVERT(VARCHAR(30),@Fecha_DocumentoAux,126),'''''''')),'', 
																			 Porcentaje = '',IIF((@Porcentaje IS NULL) OR (@Porcentaje = ''''), ''Porcentaje'',CAST(@Porcentaje AS VARCHAR(10))),'',
																			 Moneda = '',IIF((@IDMonedaR IS NULL) OR (@IDMonedaR = ''''), ''Moneda'',CAST(@IDMonedaR AS VARCHAR(10))),'',
																			 Nro_Recepcion = '', IIF((@Nro_Recepcion IS NULL) OR (@Nro_Recepcion = ''''), ''Nro_Recepcion'',CONCAT('''''''',CAST(@Nro_Recepcion AS VARCHAR(20)),'''''''')),'',
																			 Fecha_Orden = '',IIF((@Fecha_Orden IS NULL) OR (@Fecha_Orden = ''''), ''Fecha_Orden'',CONCAT('''''''',CONVERT(VARCHAR(30),@Fecha_Orden,126),'''''''')),'', 
																			 WM_GLN = '', IIF((@WMGLN_FAC IS NULL) OR (@WMGLN_FAC = ''''), ''WM_GLN'',CONCAT('''''''',CAST(@WMGLN_FAC AS VARCHAR(20)),'''''''')),'',
																			 Codigo_Actividad_S = '',IIF((@NroUnicoAC IS NULL) OR (@NroUnicoAC = ''''), ''Codigo_Actividad_S'',CAST(@NroUnicoAC AS VARCHAR(10))),''
															WHERE Tipofac = '',@Tipofac2,'' AND NumeroD = '',@Numerod2);

				EXECUTE sp_executesql @String;
				SET @String = NULL;
			END;

		IF (@TipoFac IN (''A'',''B''))
			BEGIN
				SELECT	@Nrounico = NroUnico, 		
								@Codclie = CodClie, 
								@Numeror = NumeroR,
								@CodUbic = CodUbic, 
								@Notas10 = Notas10, 
								@Fecha = FechaE
				FROM [dbo].[SAFACT]
				WHERE ([TipoFac] = @Tipofac) AND ([NumeroD] = @Numerod)

				IF (ISNULL(LEN(@Numerod),0) > 0)
					BEGIN
						SET @Ref_clave = NULL;
						SET @Ref_fecha = NULL;
						SET @Ref_tipo_doc = NULL;
						SET	@Ref_codigo = NULL;
						SET	@Ref_razon = NULL;

						-- UBICANDO CLAVEREF Y FECHA_REF PARA DEVOLUCIONES
						IF (@Tipofac = ''B'')
							BEGIN
								SELECT	@Ref_clave = Clave, @Ref_fecha = FechaE, @Ref_tipo_doc = TIPODOCFE
								FROM SAFACT WITH (NOLOCK)
								WHERE Tipofac = ''A'' AND Numerod = @Numeror;
								
								SET @Ref_clave = IIF(@Ref_clave IS NOT NULL, @Ref_clave, IIF((SELECT TOP (1) Clave FROM SAFACT WHERE TIPODOCFE IN (''01'',''04'',''09'') AND CodClie = @CODCLIE ORDER BY NroUnico DESC) IS NOT NULL, (SELECT TOP (1) Clave FROM SAFACT WHERE TIPODOCFE IN (''01'',''04'',''09'') AND CodClie = @Codclie ORDER BY NroUnico DESC), ''00000000000000000000000000000000000000000000000000''));
								SET @Ref_fecha = IIF(@Ref_fecha IS NOT NULL, @Ref_fecha, IIF(@Ref_clave = ''00000000000000000000000000000000000000000000000000'', GETDATE(), (SELECT FechaE FROM SAFACT WHERE CLAVE = @REF_CLAVE)));
								SET @Ref_tipo_doc = IIF(@Ref_tipo_doc IS NOT NULL, @Ref_tipo_doc, IIF(@Ref_clave = ''00000000000000000000000000000000000000000000000000'', ''01'', IIF((SELECT MAX(CLAVE) FROM SAFACT WHERE TIPODOCFE = ''01'') IS NOT NULL, ''01'', ''04'')));

								SET	@Ref_codigo = ''01'';
								SET	@Ref_razon = ''Devolución de Factura'';
							END;
												
						-- UBICANDO SUCURSAL
						SET @Sucursal_principal = 1;

						SELECT TOP(1) 	@Sucursal_principal = Sucursal, @llave_sucursal_principal = Llave_Sucursal, @Emisor_identificacion = REPLACE(REPLACE(RIF, ''-'',''''),'' '','''')
						FROM SACONF WITH (NOLOCK)
						WHERE CodSucu = ''00000'';

						SET @Sucursal_principal = ISNULL(@Sucursal_principal,0);
						SET @Estacion = 0;

						-- Ubicando sucursal de bodega
						SET @Numgrp_1 = NULL;
						SET @Nom_table_1_m = NULL;
						SET @NombreGrp_1 = NULL;
						SET @Nom_table_1 = NULL;

						SET @Numgrp_1 = 0;
						SET @Nom_table_1_m = ''SADEPO'';
						SET @NombreGrp_1 = ''Factura_Electronica'';

						SELECT @Numgrp_1 = ISNULL(NumGrp,0)
						FROM SAAGRUPOS WITH (NOLOCK)
						WHERE CodTbl = @Nom_table_1_m AND NombreGrp = @NombreGrp_1;

						SET @Nom_table_1 = CONCAT(@Nom_table_1_m, ''_'',REPLICATE(''0'',2 - LEN(CAST(@Numgrp_1 AS VARCHAR(2)))),CAST(@Numgrp_1 AS VARCHAR(2)));

						SET @STRING = CONCAT(''SELECT @Sucursal = [Sucursal], @llave_sucursal = [Llave_Sucursal]
						FROM '', QUOTENAME(@Nom_table_1), '' WITH (NOLOCK)
						WHERE CodUbic = '', '''''''', @CodUbic, '''''''');

						EXECUTE sp_executesql @STRING, N''
						@Sucursal int OUTPUT,
						@llave_sucursal varchar(20) OUTPUT'',					
						@Sucursal = @Sucursal OUTPUT,
						@llave_sucursal = @llave_sucursal OUTPUT;

						SET @Sucursal = ISNULL(@Sucursal,0);
						SET @String = NULL;

						IF (@Sucursal = 0)
							BEGIN
								SET @Sucursal = @Sucursal_principal;
								SET @llave_Sucursal = @llave_sucursal_principal;
							END;
						ELSE
							BEGIN
								IF (ISNULL(LEN(@llave_sucursal),0) = 0)
									BEGIN
										SET @Sucursal = @Sucursal_principal;
										SET @llave_Sucursal = @llave_sucursal_principal;										
									END;
							END;

						IF (@Tipofac = ''B'')
							BEGIN
								IF (@Ref_clave <> ''00000000000000000000000000000000000000000000000000'')
									BEGIN
										SET @Sucursal = CAST(SUBSTRING(@Ref_clave,22,3) AS INT);

										SET @String = CONCAT(''SELECT TOP (1) @llave_sucursal = [Llave_Sucursal]
															FROM '', QUOTENAME(@NOM_TABLE_1), '' WITH (NOLOCK)
															WHERE [Sucursal] = '', '''''''', @Sucursal, '''''''');

										EXECUTE sp_executesql @STRING, N''
										@llave_sucursal varchar(20) OUTPUT'',
										@llave_sucursal = @llave_sucursal OUTPUT;

										SET @String = NULL;
										SET @Sucursal = ISNULL(@Sucursal,0);

										IF (@Sucursal = 0)
											BEGIN
												SET @Sucursal = @Sucursal_principal;
												SET @llave_Sucursal = @llave_sucursal_principal;
											END;
									END;
							END;

						-- UBICANDO ESTACION
						IF (@User NOT LIKE ''%[^0-9]%'')
							BEGIN
								SET @Estacion = CAST(@User AS INT);					
							END;
						ELSE
							BEGIN
								SET @Estacion = 1;
							END;

						-- DETERMINANDO SI CREAR NUEVO REGISTRO EN TABLA CONSECUTIVOSFE
						IF NOT EXISTS (SELECT [Estacion] FROM ConsecutivosFE WITH (NOLOCK) WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion)
							BEGIN			
								INSERT INTO ConsecutivosFE 	(Sucursal,Estacion,LenCorrelFE,PrxFactFE,PrxNDFE,PrxNCFE,PrxTickEFE,PrxCompCFE)
								VALUES	(@Sucursal,@Estacion,10,1,1,1,1,1);
							END;

						SET @Consecutivo_incompleto = CONCAT(REPLICATE(''0'',3 - DATALENGTH(CAST(@Sucursal AS VARCHAR(3)))), CAST(@Sucursal AS VARCHAR(3)), REPLICATE(''0'',5 - DATALENGTH(CAST(@Estacion AS VARCHAR(5)))), CAST(@Estacion AS VARCHAR(5)));

						-- UBICANDO DATOS DEL RECEPTOR
						SELECT	@Validador2 = IIF(Descrip LIKE ''?%CONTADO%'', 1, 0),
								@TipoCli = TipoCli,
								@Clase = ISNULL(Clase,'''')
						FROM SACLIE WITH (NOLOCK)
						WHERE CodClie = @Codclie;

						-- UBICANDO TIPO DE DOCUMENTO
						IF (@TipoCli = 2)
							BEGIN
								SET @Tipo_documento = CASE @Tipofac WHEN ''A'' THEN ''09'' WHEN ''B'' THEN ''03'' END;		
							END;
						ELSE
							BEGIN
								IF (@Clase NOT IN (''P'',''T''))
									BEGIN
										SET @Tipo_documento = CASE @Tipofac WHEN ''A'' THEN IIF(@Validador2 = 1, ''04'',''01'') WHEN ''B'' THEN ''03'' END;
									END;
								ELSE
									BEGIN
										SET @Tipo_documento = CASE @Tipofac WHEN ''A'' THEN ''04'' WHEN ''B'' THEN ''03'' END;
									END;
							END;

						-- ASIGNADO CONSECUTIVO EN NUMERODE (FACTURACION ELECTRONICA 10 DIGITOS SEGUN TIPO DE DOCUMENTO)
						SELECT 	@LenCorrelFE = [LenCorrelFE], 
								@PrxFactFE = [PrxFactFE],
								@PrxNDFE = [PrxNDFE],
								@PrxNCFE = [PrxNCFE],
								@PrxTickEFE = [PrxTickEFE],
								@PrxCompCFE = [PrxCompCFE],
								@PrxFExpFE = [PrxFExpFE]
						FROM [dbo].[ConsecutivosFE] WITH (NOLOCK)
						WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;

						IF (@TIPOFAC = ''A'')
							BEGIN
								-- DETERMINANDO SI ES FACTRA O TIQUETE ELECTRONICO
								IF (@Tipo_documento = ''04'')
									BEGIN
										SET @Consecutivo_origen	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxTickEFE AS VARCHAR(10)))) + CAST(@PrxTickEFE AS VARCHAR(10)) AS VARCHAR(10))
										UPDATE ConsecutivosFE SET [PrxTickEFE] = [PrxTickEFE] + 1
										WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;							
									END;
					
								IF (@Tipo_documento = ''01'')
									BEGIN
										SET @Consecutivo_origen	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxFactFE AS VARCHAR(10)))) + CAST(@PrxFactFE AS VARCHAR(10)) AS VARCHAR(10))
										UPDATE ConsecutivosFE SET [PrxFactFE] = [PrxFactFE] + 1
										WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;
									END;

								IF (@Tipo_documento = ''09'')
									BEGIN
										SET @Consecutivo_origen	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxFExpFE AS VARCHAR(10)))) + CAST(@PrxFExpFE AS VARCHAR(10)) AS VARCHAR(10))
										UPDATE ConsecutivosFE SET [PrxFExpFE] = [PrxFExpFE] + 1
										WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;
									END;
							END;

						IF (@Tipofac = ''B'')
							BEGIN
								SET @Consecutivo_origen	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxNCFE AS VARCHAR(10)))) + CAST(@PrxNCFE AS VARCHAR(10)) AS VARCHAR(10))
								UPDATE ConsecutivosFE SET [PrxNCFE] = [PrxNCFE] + 1
								WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;
							END;

						-- CREANDO CONSECUTIVO COMPLETO
						SET @Consecutivo_completo = CONCAT(@Consecutivo_incompleto, @Tipo_documento, @Consecutivo_origen);

						-- CREANDO CLAVE
						SET @Dia = IIF(LEN(CAST(DAY(@Fecha) AS VARCHAR(2))) = 1, CONCAT(''0'', CAST(DAY(@Fecha) AS VARCHAR(2))), CAST(DAY(@Fecha) AS VARCHAR(2)));
						SET @Mes = IIF(LEN(CAST(MONTH(@Fecha) AS VARCHAR(2))) = 1, CONCAT(''0'', CAST(MONTH(@Fecha) AS VARCHAR(2))), CAST(MONTH(@Fecha) AS VARCHAR(2)));
						SET @Ano = RIGHT(CAST(YEAR(@FECHA) AS VARCHAR(4)), 2);
						SET @Idclave = RIGHT(CONCAT(''00000000000'', @Emisor_identificacion), 12);
						SET @Clave = CONCAT(''506'', @Dia, @Mes, @Ano, @Idclave, @Consecutivo_completo, ''1'', ''44332211'');

						--- ASIGNANDO DATOS PARA PEDIDO ICE ---
						IF (@Tipo_documento = ''01'')
							BEGIN
								IF (ISNULL(LEN(@Pedido_ICE),0) <> 0)
									BEGIN
										SET @Ref_tipo_doc = ''99'';
										SET @Ref_clave = @Pedido_ICE;
										SET @Ref_fecha = @Fecha;
										SET @Ref_codigo = ''99'';
										SET @Ref_razon = @Pedido_ICE;
									END;
								ELSE
									BEGIN
										SET @Ref_tipo_doc = NULL;
										SET @Ref_clave = NULL;
										SET @Ref_fecha = ''1900-01-01'';
										SET @Ref_codigo = NULL;
										SET @Ref_razon = NULL;
									END;
							END;
						ELSE
							BEGIN
								IF (@Tipo_documento IN (''04'',''09''))
									BEGIN
										SET @Ref_tipo_doc = NULL;
										SET @Ref_clave = NULL;
										SET @Ref_fecha = ''1900-01-01'';
										SET @Ref_codigo = NULL;
										SET @Ref_razon = NULL;
									END;
							END;

						UPDATE SAFACT SET	NUMERODE = @Consecutivo_origen, 
											Clave = @Clave,
											TIPODOCFE = @Tipo_documento,
											ConsecutivoDE = @Consecutivo_completo,
											ClaveRef = @Ref_clave
						WHERE (NroUnico = @Nrounico);

						INSERT INTO [dbo].[ComprobantesFE]
								([NroUnico]
								,[TipoDoc]
								,[NumeroD]
								,[NumeroDE]
								,[Clave]
								,[TipoDocFE]
								,[ConsecutivoDE]
								,[ClaveRef]
								,[RefTipoDoc]
								,[RefFecha]
								,[RefCodigo]
								,[RefRazon]
								,[ExoneracionE]
								,[NroItemT]
								,[Iva_Devuelto]
								,[Monto_Exonerado]
								,[CodTaxsClie]
								,[EstadoFE]
								,[MensajeFE])
						VALUES	(@NroUnico
									,@Tipofac
									,@numeroD
									,@Consecutivo_origen
									,@Clave
									,@Tipo_documento
									,@Consecutivo_completo
									,@Ref_clave
									,@Ref_tipo_doc
									,@Ref_fecha
									,@Ref_codigo
									,@Ref_razon
									,0
									,0
									,0
									,0
									,NULL
									,0
									,''Pendiente por enviar a tablas intermedias'');
					END;
			END;
	END;
END;');
GO
/*CLAVE Y CONSECUTIVO NOTAS DE DEBITO*/
/*TRIGGER SAACXC*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_CLAVE_ND_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_CLAVE_ND_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_CLAVE_ND_FE]
   ON  [dbo].[SAACXC] with encryption
   AFTER INSERT, UPDATE
AS 
BEGIN
DECLARE 
@Tipofac varchar(2),
@Numerod varchar(20),
@Numeror varchar(20),
@Nrounico int,
@Codclie varchar(15),
@Clave varchar(50),
@Fecha datetime,
@User varchar(50),
@Dia varchar(2),
@Mes varchar(2),
@Ano varchar(2),
@Sucursal Int,
@Sucursal_principal Int,
@Estacion Int,
@Numgrp_1 Int,  -- TABLA Atributos (SADEPO)
@Nom_table_1_m varchar(50),
@Nom_table_1 varchar(50),
@NombreGrp_1 varchar(50),
@String Nvarchar(MAX),
@Consecutivo_incompleto varchar(8),
@Consecutivo_completo varchar(20),
@Idclave varchar(12),
@Tipo_documento varchar(2),
@Consecutivo_origen varchar(10),
@llave_sucursal varchar(20),
@llave_sucursal_principal varchar(20),
@Ref_tipo_doc varchar(2),
@Ref_clave varchar(50),
@Ref_fecha datetime,
@CodUbic varchar(10),
@Notas10 varchar(60),
@LenCorrelFE Int,
@PrxFactFE Int,
@PrxNDFE Int,
@PrxNCFE Int,
@PrxTickEFE Int,
@PrxCompCFE Int,
@PrxFExpFE Int,
@TipoCli smallint,
@Validador2 varchar(1),
@Clase varchar(10),
@Emisor_identificacion varchar(12),
@Tipocxc varchar(2),
@EstadoFE tinyint,
@Fromtran smallint,
@EsLibroI smallint,
@Document varchar(40),
@ClaveI varchar(50),
@Ref_razon varchar(180),
@Ref_codigo varchar(2)

SET NOCOUNT ON;
SET @User = SYSTEM_USER;	-- Usuario de conexion sql
SET @Estacion = 1;

SELECT	@Nrounico = NroUnico,
		@Tipocxc = TipoCxc,
		@Numerod = NumeroD,
		@Codclie = CodClie,
		@Fecha = FechaE,
		@Fromtran = FromTran,
		@EsLibroI = EsLibroI,
		@Document = IIF(CHARINDEX(' ', LTRIM(Document)) = 0, LTRIM(LEFT(Document, LEN(Document))),LEFT(Document, CHARINDEX(' ', LTRIM(Document)) - 1)),
		@EstadoFE = EstadoFE,
		@ClaveI = Clave
FROM inserted;

IF (@Tipocxc = '20') 
	BEGIN
		IF (@EsLibroI = 1)
			BEGIN
				IF (@Fromtran = 1)
					BEGIN
						IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
							BEGIN
								-- UBICANDO DATOS DEL RECEPTOR
								SELECT	@TipoCli = TipoCli,
										@Clase = ISNULL(Clase,'')
								FROM SACLIE WITH (NOLOCK)
								WHERE CodClie = @Codclie;

								-- UBICANDO ESTACION
								IF (@User NOT LIKE '%[^0-9]%')
									BEGIN
										SET @Estacion = CAST(@User AS INT);						
									END;
								ELSE
									BEGIN
										SET @Estacion = 1;
									END;

								SET @Ref_clave = NULL;
								SET @Ref_fecha = NULL;
								set @Ref_tipo_doc = NULL;
								SET @Validador2 = NULL;

								-- VALIDANDO QUE EL COMPROBANTE DE REF ESTE CREADO EN SAFACT									
								SELECT TOP(1)	@Validador2 = NumeroD,
												@Ref_clave = Clave,
												@Ref_fecha = FechaE,
												@Ref_tipo_doc = TIPODOCFE
								FROM SAFACT WITH (NOLOCK)
								WHERE ConsecutivoDE = @Document AND CodClie = @Codclie;

								-- VALIDANDO QUE EL COMPROBANTE DE REF ESTE CREADO EN SAACXC
								IF (@Validador2 IS NULL)
									BEGIN
										-- VALIDANDO QUE ESTE CREADO EN SAACXC
										SET @Validador2 = NULL;

										SELECT	@Validador2 = NumeroD,
												@Ref_clave = Clave,
												@Ref_fecha = FechaE,
												@Ref_tipo_doc = TIPODOCFE
										FROM SAACXC WITH (NOLOCK)
										WHERE ConsecutivoDE = @Document AND CodClie = @CODCLIE;
									END;

								IF (ISNULL(LEN(@Validador2),0) > 0)
									BEGIN
										SET	@Ref_codigo = '02';
										SET	@Ref_razon = 'Corrige Monto Doc de referencia';

										-- UBICANDO SUCURSAL
										SET @Sucursal_principal = 1;

										SELECT TOP(1) 	@Sucursal_principal = Sucursal,
														@llave_sucursal_principal = Llave_Sucursal,
														@Emisor_identificacion = REPLACE(REPLACE(RIF, '-',''),' ','')
										FROM SACONF WITH (NOLOCK)
										WHERE CodSucu = '00000';

										SET @Sucursal_principal = ISNULL(@Sucursal_principal,0);											

										-- UBICANDO NUMERO DE SUCURSAL EN CLAVE DE DOCDE REREFERENCIA
										SET @Sucursal = CAST(SUBSTRING(@Ref_clave,22,3) AS INT);

										-- UBICANDO SUCURSAL DE BODEGA
										SET @Numgrp_1 = NULL;
										SET @Nom_table_1_m = NULL;
										SET @Nombregrp_1 = NULL;
										SET @Nom_table_1 = NULL;

										SET @Numgrp_1 = 0;
										SET @Nom_table_1_m = 'SADEPO';
										SET @Nombregrp_1 = 'Factura_Electronica';

										SELECT @Numgrp_1 = ISNULL(NumGrp,0)
										FROM SAAGRUPOS WITH (NOLOCK)
										WHERE CodTbl = @Nom_table_1_m AND NombreGrp = @NombreGrp_1;

										SET @Nom_table_1 = CONCAT(@Nom_table_1_m, '_',REPLICATE('0',2 - LEN(CAST(@Numgrp_1 AS VARCHAR(2)))),CAST(@Numgrp_1 AS VARCHAR(2)));

										SET @STRING = CONCAT('SELECT TOP (1) @llave_sucursal = [Llave_Sucursal]
										FROM ', QUOTENAME(@Nom_table_1), ' WITH (NOLOCK)
										WHERE [Sucursal] = ', '''', @Sucursal, '''');

										EXECUTE sp_executesql @STRING, N'
										@llave_sucursal varchar(20) OUTPUT',					
										@llave_sucursal = @llave_sucursal OUTPUT;

										SET @String = NULL;
										SET @Sucursal = ISNULL(@Sucursal,0);

										IF (@Sucursal = 0)
											BEGIN
												SET @Sucursal = @Sucursal_principal;
												SET @llave_Sucursal = @llave_sucursal_principal;
											END;											

										-- DETERMINANDO SI CREAR NUEVO REGISTRO EN TABLA CONSECUTIVOSFE
										IF NOT EXISTS (SELECT [Estacion] FROM ConsecutivosFE WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion)
											BEGIN			
												INSERT INTO ConsecutivosFE 	(Sucursal,Estacion,LenCorrelFE,PrxFactFE,PrxNDFE,PrxNCFE,PrxTickEFE,PrxCompCFE)
												VALUES	(@Sucursal,@Estacion,10,1,1,1,1,1);
											END;

										SET @Consecutivo_incompleto = CONCAT(REPLICATE('0',3 - DATALENGTH(CAST(@Sucursal AS VARCHAR(3)))), CAST(@Sucursal AS VARCHAR(3)), REPLICATE('0',5 - DATALENGTH(CAST(@Estacion AS VARCHAR(5)))), CAST(@Estacion AS VARCHAR(5)));

										-- UBICANDO TIPO DE DOCUMENTO
										SET @Tipo_documento = '02';

										-- ASIGNADO CONSECUTIVO EN NUMERODE (FACTURACION ELECTRONICA 10 DIGITOS SEGUN TIPO DE DOCUMENTO)
										SELECT 	@LenCorrelFE = LenCorrelFE, 
												@PrxNDFE = PrxNDFE
										FROM ConsecutivosFE WITH (NOLOCK)
										WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;

										SET @Consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxNDFE AS VARCHAR(10)))) + CAST(@PrxNDFE AS VARCHAR(10)) AS VARCHAR(10));

										UPDATE ConsecutivosFE SET PrxNDFE = PrxNDFE + 1
										WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;

										-- CREANDO CONSECUTIVO COMPLETO
										SET @Consecutivo_completo = CONCAT(@Consecutivo_incompleto, @Tipo_documento, @Consecutivo_origen);											
							
										-- CREANDO CLAVE
										SET @Dia = IIF(LEN(CAST(DAY(@Fecha) AS VARCHAR(2))) = 1, CONCAT('0', CAST(DAY(@Fecha) AS VARCHAR(2))), CAST(DAY(@Fecha) AS VARCHAR(2)));
										SET @Mes = IIF(LEN(CAST(MONTH(@Fecha) AS VARCHAR(2))) = 1, CONCAT('0', CAST(MONTH(@Fecha) AS VARCHAR(2))), CAST(MONTH(@Fecha) AS VARCHAR(2)));
										SET @Ano = RIGHT(CAST(YEAR(@Fecha) AS VARCHAR(4)), 2);
										SET @Idclave = RIGHT(CONCAT('00000000000', @Emisor_identificacion), 12);
										SET @Clave = CONCAT('506', @Dia, @Mes, @Ano, @Idclave, @Consecutivo_completo, '1', '44332211');

										UPDATE SAACXC SET	Clave = @Clave,
															ConsecutivoDE = @Consecutivo_completo,
															NUMERODE = @Consecutivo_origen,
															TIPODOCFE = @Tipo_documento,
															ClaveRef = @Ref_clave
										WHERE NroUnico = @Nrounico;

										INSERT INTO [dbo].[ComprobantesFE]
												([NroUnico]
												,[TipoDoc]
												,[NumeroD]
												,[NumeroDE]
												,[Clave]
												,[TipoDocFE]
												,[ConsecutivoDE]
												,[ClaveRef]
												,[RefTipoDoc]
												,[RefFecha]
												,[RefCodigo]
												,[RefRazon]
												,[ExoneracionE]
												,[NroItemT]
												,[Iva_Devuelto]
												,[Monto_Exonerado]
												,[CodTaxsClie]
												,[EstadoFE]
												,[MensajeFE])
										VALUES	(@NroUnico
												,@Tipocxc
												,@numeroD
												,@Consecutivo_origen
												,@Clave
												,@Tipo_documento
												,@Consecutivo_completo
												,@Ref_clave
												,@Ref_tipo_doc
												,@Ref_fecha
												,@Ref_codigo
												,@Ref_razon
												,0
												,0
												,0
												,0
												,NULL
												,0
												,'Pendiente por enviar a tablas intermedias');
									END;	
								ELSE
									BEGIN
										UPDATE SAACXC SET EstadoFE = 3, MensajeFE = CONCAT('El consecutivo -', @Document,'-, colocado en Detalle no es de este cliente, no existe o no es valido')
										WHERE NroUnico = @Nrounico;
									END;
							END;											
					END;
				ELSE
					BEGIN
						IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
							BEGIN
								UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'No aplica para FE porque el campo Fromtran es igual a 0'
								WHERE NroUnico = @Nrounico;
							END;
					END;
			END;
		ELSE
			BEGIN
				IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
					BEGIN
						UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'No aplica para FE porque el campo LibroI es igual a 0'
						WHERE NroUnico = @Nrounico;
					END;
			END;
	END;
END;
GO
/*CLAVE Y CONSECUTIVO NOTAS DE CREDITO*/
/*TRIGGER SAPAGCXC*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_CLAVE_NC_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_CLAVE_NC_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_CLAVE_NC_FE]
   ON  [dbo].[SAPAGCXC] with encryption
   AFTER INSERT
AS 
BEGIN
DECLARE 
@Tipofac varchar(2),
@Numerod varchar(20),
@Numeror varchar(20),
@Nrounico int,
@Codclie varchar(15),
@Clave varchar(50),
@Fecha datetime,
@User varchar(50),
@Dia varchar(2),
@Mes varchar(2),
@Ano varchar(2),
@Sucursal Int,
@Sucursal_principal Int,
@Estacion Int,
@Numgrp_1 Int,  -- TABLA Atributos (SADEPO)
@Nom_table_1_m varchar(50),
@Nom_table_1 varchar(50),
@NombreGrp_1 varchar(50),
@String Nvarchar(MAX),
@Consecutivo_incompleto varchar(8),
@Consecutivo_completo varchar(20),
@Idclave varchar(12),
@Tipo_documento varchar(2),
@Consecutivo_origen varchar(10),
@llave_sucursal varchar(20),
@llave_sucursal_principal varchar(20),
@Ref_tipo_doc varchar(2),
@Ref_clave varchar(50),
@Ref_fecha datetime,
@CodUbic varchar(10),
@Notas10 varchar(60),
@LenCorrelFE Int,
@PrxFactFE Int,
@PrxNDFE Int,
@PrxNCFE Int,
@PrxTickEFE Int,
@PrxCompCFE Int,
@PrxFExpFE Int,
@TipoCli smallint,
@Validador2 varchar(1),
@Clase varchar(10),
@Emisor_identificacion varchar(12),
@Tipocxc varchar(2),
@EstadoFE tinyint,
@Fromtran smallint,
@EsLibroI smallint,
@Document varchar(40),
@ClaveI varchar(50),
@Nroppal AS INT,
@Nroregi AS INT,
@Tipocxc2 varchar(2),
@MtoTotal decimal(28,4),
@MtoTotal2 decimal(28,4),
@Detalle varchar(160),
@NroUnicoCXC int,
@Ref_razon varchar(180),
@Ref_codigo varchar(2)

SET NOCOUNT ON;
SET @User = SYSTEM_USER;	-- Usuario de conexion sql
SET @Estacion = 1;

SELECT	@Nroppal = NroPpal,
		@Nroregi = NroRegi,
		@Tipocxc2 = TipoCxc,
		@MtoTotal2 = Monto,
		@detalle = Descrip,
		@NroUnicoCXC = NroUnico
FROM inserted;

SELECT	@Nrounico = NroUnico,
		@Tipocxc = TipoCxc,
		@Numerod = NumeroD,
		@Codclie = CodClie,
		@Fecha = FechaE,
		@Fromtran = FromTran,
		@EsLibroI = EsLibroI,			
		@EstadoFE = EstadoFE,
		@ClaveI = Clave,
		@MtoTotal = Monto
FROM SAACXC WITH (NOLOCK)
WHERE NroUnico = @NROPPAL;

IF (@Tipocxc = '31') 
	BEGIN
		IF (@EsLibroI = 1)
			BEGIN
				IF (@Fromtran = 1)
					BEGIN
						IF (@Tipocxc2 IN ('10', '20'))	-- DETERMINANDO SI SE APLICO LA NC A UNA FAC O A UNA ND
							BEGIN
								IF (ROUND(@MtoTotal,0) = ROUND(@MtoTotal2,0))
									BEGIN
										IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
											BEGIN
												-- DATOS DE DOC REF
												SELECT	@Document = Numerod
												FROM SAACXC WITH (NOLOCK)
												WHERE NroUnico = @Nroregi;

												-- UBICANDO DATOS DEL RECEPTOR
												SELECT	@TipoCli = TipoCli,
														@Clase = ISNULL(Clase,'')
												FROM SACLIE WITH (NOLOCK)
												WHERE CodClie = @Codclie;

												-- UBICANDO ESTACION
												IF (@User NOT LIKE '%[^0-9]%')
													BEGIN
														SET @Estacion = CAST(@User AS INT);						
													END;
												ELSE
													BEGIN
														SET @Estacion = 1;
													END;

												SET @Ref_clave = NULL;
												SET @Ref_fecha = NULL;
												set @Ref_tipo_doc = NULL;
												SET @Validador2 = NULL;

												-- BUSCANDO DATOS DE LA ND SI SE APLICO A ND
												IF (@Tipocxc2 = '20')
													BEGIN
														SET @Validador2 = NULL;

														SELECT	@Validador2 = NumeroD,
																@Ref_clave = ConsecutivoDE,
																@Ref_fecha = FechaE,
																@Ref_tipo_doc = TIPODOCFE
														FROM SAACXC WITH (NOLOCK)
														WHERE Tipocxc = 20 AND NumeroD = @Document AND CodClie = @Codclie;		

														SET @Ref_clave = IIF(@Ref_clave IS NOT NULL, @Ref_clave, '00000000000000000000');
														SET @Ref_fecha = IIF(@Ref_fecha IS NOT NULL, @Ref_fecha, GETDATE());
														SET @Ref_tipo_doc = IIF(@Ref_tipo_doc IS NOT NULL, @Ref_tipo_doc, '02');			
													END;

												-- BUSCANDO DATOS DE FAC SI SE APLICO A UNA FAC												IF (@Tipocxc2 = '10')
													BEGIN
														-- VALIDANDO QUE LA FACTURA ESTE CREADA EN SAFACT
														SET @Validador2 = NULL;

														SELECT	@Validador2 = NumeroD,
																@Ref_clave = ConsecutivoDE,
																@Ref_fecha = FechaE,
																@Ref_tipo_doc = TIPODOCFE
														FROM SAFACT WITH (NOLOCK)
														WHERE TipoFac = 'A' AND NumeroD = @Document AND CodClie = @Codclie;

														SET @Ref_clave = IIF(@Ref_clave IS NOT NULL, @Ref_clave, '00000000000000000000');
														SET @Ref_fecha = IIF(@Ref_fecha IS NOT NULL, @Ref_fecha, GETDATE());
														SET @Ref_tipo_doc = IIF(@Ref_tipo_doc IS NOT NULL, @Ref_tipo_doc, IIF(@Ref_clave IS NOT NULL, SUBSTRING(@Ref_clave,30,2), '01'));

														-- BUSCANDO FACTURA DE REFERENCIA ESTA EN SAACXC
														IF (ISNULL(LEN(@Validador2),2) = 0)
															BEGIN
																SELECT	@Validador2 = NumeroD,
																		@Ref_clave = ConsecutivoDE,
																		@Ref_fecha = FechaE,
																		@Ref_tipo_doc = TIPODOCFE											
																FROM SAACXC WITH (NOLOCK)
																WHERE Tipocxc = '10' AND NumeroD = @Document AND CodClie = @Codclie;

																SET @Ref_clave = IIF(@Ref_clave IS NOT NULL, @Ref_clave, '00000000000000000000');
																SET @Ref_fecha = IIF(@Ref_fecha IS NOT NULL, @Ref_fecha, GETDATE());
																SET @Ref_tipo_doc = IIF(@Ref_tipo_doc IS NOT NULL, @Ref_tipo_doc, IIF(@Ref_clave IS NOT NULL, SUBSTRING(@Ref_clave,30,2), '01'));
															END;
													END;

												IF (ISNULL(LEN(@Validador2),0) > 0)
													BEGIN
														SET	@Ref_codigo = '02';
														SET	@Ref_razon = CASE WHEN @Tipocxc2 = '10' THEN 'Corrección de Monto Factura' WHEN @Tipocxc2 = '20' THEN 'Corrección de Monto ND' ELSE 'Corrección' END;

														-- UBICANDO SUCURSAL
														SET @Sucursal_principal = 1;

														SELECT TOP(1) 	@Sucursal_principal = Sucursal,
																		@llave_sucursal_principal = Llave_Sucursal,
																		@Emisor_identificacion = REPLACE(REPLACE(RIF, '-',''),' ','')
														FROM SACONF WITH (NOLOCK)
														WHERE CodSucu = '00000';

														SET @Sucursal_principal = ISNULL(@Sucursal_principal,0);											

														-- UBICANDO NUMERO DE SUCURSAL EN CLAVE DE DOCDE REREFERENCIA
														SET @Sucursal = CAST(SUBSTRING(@Ref_clave,22,3) AS INT);

														-- UBICANDO SUCURSAL DE BODEGA
														SET @Numgrp_1 = NULL;
														SET @Nom_table_1_m = NULL;
														SET @Nombregrp_1 = NULL;
														SET @Nom_table_1 = NULL;

														SET @Numgrp_1 = 0;
														SET @Nom_table_1_m = 'SADEPO';
														SET @Nombregrp_1 = 'Factura_Electronica';

														SELECT @Numgrp_1 = ISNULL(NumGrp,0)
														FROM SAAGRUPOS WITH (NOLOCK)
														WHERE CodTbl = @Nom_table_1_m AND NombreGrp = @NombreGrp_1;

														SET @Nom_table_1 = CONCAT(@Nom_table_1_m, '_',REPLICATE('0',2 - LEN(CAST(@Numgrp_1 AS VARCHAR(2)))),CAST(@Numgrp_1 AS VARCHAR(2)));

														SET @STRING = CONCAT('SELECT TOP (1) @llave_sucursal = [Llave_Sucursal]
														FROM ', QUOTENAME(@Nom_table_1), ' WITH (NOLOCK)
														WHERE [Sucursal] = ', '''', @Sucursal, '''');

														EXECUTE sp_executesql @STRING, N'
														@llave_sucursal varchar(20) OUTPUT',					
														@llave_sucursal = @llave_sucursal OUTPUT;

														SET @String = NULL;
														SET @Sucursal = ISNULL(@Sucursal,0);

														IF (@Sucursal = 0)
															BEGIN
																SET @Sucursal = @Sucursal_principal;
																SET @llave_Sucursal = @llave_sucursal_principal;
															END;											

														-- DETERMINANDO SI CREAR NUEVO REGISTRO EN TABLA CONSECUTIVOSFE
														IF NOT EXISTS (SELECT [Estacion] FROM ConsecutivosFE WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion)
															BEGIN			
																INSERT INTO ConsecutivosFE 	(Sucursal,Estacion,LenCorrelFE,PrxFactFE,PrxNDFE,PrxNCFE,PrxTickEFE,PrxCompCFE)
																VALUES	(@Sucursal,@Estacion,10,1,1,1,1,1);
															END;

														SET @Consecutivo_incompleto = CONCAT(REPLICATE('0',3 - DATALENGTH(CAST(@Sucursal AS VARCHAR(3)))), CAST(@Sucursal AS VARCHAR(3)), REPLICATE('0',5 - DATALENGTH(CAST(@Estacion AS VARCHAR(5)))), CAST(@Estacion AS VARCHAR(5)));

														-- UBICANDO TIPO DE DOCUMENTO
														SET @Tipo_documento = '03';

														-- ASIGNADO CONSECUTIVO EN NUMERODE (FACTURACION ELECTRONICA 10 DIGITOS SEGUN TIPO DE DOCUMENTO)
														SELECT 	@LenCorrelFE = LenCorrelFE, 
																@PrxNCFE = PrxNCFE
														FROM ConsecutivosFE WITH (NOLOCK)
														WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;

														SET @Consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxNCFE AS VARCHAR(10)))) + CAST(@PrxNCFE AS VARCHAR(10)) AS VARCHAR(10));

														UPDATE ConsecutivosFE SET PrxNCFE = PrxNCFE + 1
														WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion;

														-- CREANDO CONSECUTIVO COMPLETO
														SET @Consecutivo_completo = CONCAT(@Consecutivo_incompleto, @Tipo_documento, @Consecutivo_origen);											
							
														-- CREANDO CLAVE
														SET @Dia = IIF(LEN(CAST(DAY(@Fecha) AS VARCHAR(2))) = 1, CONCAT('0', CAST(DAY(@Fecha) AS VARCHAR(2))), CAST(DAY(@Fecha) AS VARCHAR(2)));
														SET @Mes = IIF(LEN(CAST(MONTH(@Fecha) AS VARCHAR(2))) = 1, CONCAT('0', CAST(MONTH(@Fecha) AS VARCHAR(2))), CAST(MONTH(@Fecha) AS VARCHAR(2)));
														SET @Ano = RIGHT(CAST(YEAR(@Fecha) AS VARCHAR(4)), 2);
														SET @Idclave = RIGHT(CONCAT('00000000000', @Emisor_identificacion), 12);
														SET @Clave = CONCAT('506', @Dia, @Mes, @Ano, @Idclave, @Consecutivo_completo, '1', '44332211');

														UPDATE SAACXC SET	Clave = @Clave,
																			ConsecutivoDE = @Consecutivo_completo,
																			NUMERODE = @Consecutivo_origen,
																			TIPODOCFE = @Tipo_documento,
																			ClaveRef = @Ref_clave																			
														WHERE NroUnico = @NROPPAL;

														INSERT INTO [dbo].[ComprobantesFE]
																([NroUnico]
																,[TipoDoc]
																,[NumeroD]
																,[NumeroDE]
																,[Clave]
																,[TipoDocFE]
																,[ConsecutivoDE]
																,[ClaveRef]
																,[RefTipoDoc]
																,[RefFecha]
																,[RefCodigo]
																,[RefRazon]
																,[ExoneracionE]
																,[NroItemT]
																,[Iva_Devuelto]
																,[Monto_Exonerado]
																,[CodTaxsClie]
																,[EstadoFE]
																,[MensajeFE])
														VALUES	(@NROPPAL
																,@Tipocxc
																,@numeroD
																,@Consecutivo_origen
																,@Clave
																,@Tipo_documento
																,@Consecutivo_completo
																,@Ref_clave
																,@Ref_tipo_doc
																,@Ref_fecha
																,@Ref_codigo
																,@Ref_razon
																,0
																,0
																,0
																,0
																,NULL
																,0
																,'Pendiente por enviar a tablas intermedias');
													END;	
												ELSE
													BEGIN
														UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'El documento de referencia no tiene un Consecutivo FE valido'
														WHERE NroUnico = @NROPPAL;
													END;
											END;	
									END;
								ELSE
									BEGIN
										IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
											BEGIN
												UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'El Monto del detalle de la NC no coincide con el total General, esto se puede deber a que se aplico NC a más de 1 Documento'
												WHERE NroUnico = @NROPPAL;
											END;
									END;
							END;	
						ELSE
							BEGIN
								IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
									BEGIN
										UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'Esta NC no procede porque no se aplico a una factura o Nota de débito'
										WHERE NroUnico = @NROPPAL;
									END;
							END;									
					END;
				ELSE
					BEGIN
						IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
							BEGIN
								UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'No aplica para FE porque el campo Fromtran es igual a 0'
								WHERE NroUnico = @NROPPAL;
							END;
					END;
			END;
		ELSE
			BEGIN
				IF (@EstadoFE <> 1) AND (ISNULL(LEN(@ClaveI),0) = 0)
					BEGIN
						UPDATE SAACXC SET EstadoFE = 3, MensajeFE = 'No aplica para FE porque el campo LibroI es igual a 0'
						WHERE NroUnico = @NROPPAL;
					END;
			END;
	END;
END;
GO
/*CLAVE Y CONSECUTIVO COMPRAS REGIMEN SIMPLIFICADO*/
/*TRIGGER SACOMP_0X*/
IF (SELECT name FROM SYS.triggers WHERE name = 'SCR_COMP_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[SCR_COMP_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50),
@STRINGTRIGGERS AS NVARCHAR(MAX)
-- UBICANDO DATOS DE TABLA Otros_Datos (Compras)
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SACOMP';
SET @NombreGrp = 'Otros_Datos';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC(N'CREATE TRIGGER [dbo].[SCR_COMP_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER INSERT
AS
BEGIN
DECLARE
-- VARIABLES PARA ENCABEZADO
@TIPO_DOCUMENTO VARCHAR(2),
@CLAVE varchar(50),
@FECHA datetime,
@CONSECUTIVO_ORIGEN varchar(10),
@CONSECUTIVO_INCOMPLETO varchar(8),
@CONSECUTIVO_COMPLETO varchar(20),
@MONEDA varchar(3),
@TIPO_CAMBIO decimal(28, 5),
@REF_TIPO_DOC varchar(2),
@REF_CLAVE varchar(50),
@REF_FECHA datetime,
@REF_CODIGO varchar(2),
@REF_RAZON varchar(180),
@TIPOCOM AS VARCHAR(2),
@CODPROV AS VARCHAR(15),
@ESTADOE AS INT,
@CIUDADE AS INT,
@MUNICIPIOE AS INT,
@ESTADOR AS INT,
@CIUDADR AS INT,
@MUNICIPIOR AS INT,
@VALIDADOR1 AS VARCHAR(10),
@VALIDADOR2 AS VARCHAR(1),
@NUMEROD AS VARCHAR(20),
@NUMERODE AS VARCHAR(10),
@NUMEROR VARCHAR(20),
@LenCorrelFE AS INT,
@PrxComFE AS INT,
@PrxNCFE AS INT,
@NROUNICO AS INT,
@CODMONEDA AS VARCHAR(5),
@CODMONEDAR AS VARCHAR(5),
@CODMONEDAP AS VARCHAR(5),
@VALIDADOR AS VARCHAR(1),
@DIA AS VARCHAR(2),
@MES AS VARCHAR(2),
@ANO AS VARCHAR(2),
@IDCLAVE AS VARCHAR(12),
@Sucursal AS INT,
@USER AS VARCHAR(50),
@Estacion AS INT,
@EstacionV AS INT,
@Cod_Pais AS VARCHAR(5),
@WMVendedor AS VARCHAR(30),
@Id_Prefijo_Pais AS INT,
@Emitir_FactE AS smallint,
@EsReten AS smallint,
@STRING AS NVARCHAR(MAX),
@STRINGTRIGGERS AS NVARCHAR(MAX),
@Pais AS INT,
@IDMoneda AS INT,
@CodUbic varchar(10),
@Sucursal_principal int,
@llave_sucursal varchar(20),
@llave_sucursal_principal varchar(20),
@NUMGRP_1 AS INT,  --- TABLA Atributos (SADEPO)
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@MONTO AS DECIMAL(28,4),
@MtoTotal decimal(28, 4),
@DESCTO1 AS DECIMAL(28,4),
@DESCTO2 AS DECIMAL(28,4),
@Contado decimal(28, 4),
@Credito decimal(28, 4),
@CancelI decimal(28, 4),
@CancelA decimal(28, 4),
@CancelE decimal(28, 4),
@CancelC decimal(28, 4),
@CancelT decimal(28, 4),
@CancelG decimal(28, 4),
@CancelP decimal(28, 4),
@FLETES AS DECIMAL(28,4),
@MTOTAX AS DECIMAL (28,4),
@TOTALPRDSRV AS DECIMAL(28,4),
@DESCTOG AS DECIMAL(28,4),
@Emisor_identificacion varchar(12)

SET NOCOUNT ON;

-- UBICANDO USUARIO DE CONEXION SQL
SET @USER = SYSTEM_USER;

SELECT  @TipoCom = TipoCom,
		  @NumeroD = NumeroD,
		  @CodProv = CodProv, 
		  @Emitir_FactE = Emitir_FactE,
		  @IDMoneda = Moneda
FROM inserted;

-- UBICANDO TIPO PROVEEDOR
SET @EsReten = NULL;

SELECT	@EsReten = EsReten
FROM SAPROV WITH (NOLOCK)
WHERE (CodProv = @CodProv);

IF (@TipoCom IN (''H''))
	BEGIN
		IF (@EsReten = 0) 	-- VALIDANDO TIPO DE PROVEEDOR
			BEGIN
				IF (@Emitir_FactE = 0) OR (@Emitir_FactE IS NULL)	-- VALIDANDO EMITIR FACTURA
					BEGIN
						-- UBICANDO MONTO TOTAL DE FACTURA EXENT0
						SELECT	@Monto = SUM(ISNULL(TotalItem,0))
						FROM SAITEMCOM WITH (NOLOCK)
						WHERE (TipoCom = @TipoCom) AND (NumeroD = @NUMEROD) AND (CodProv = @CodProv) AND (NroLineaC = 0);

						SELECT	@NROUNICO = NroUnico,
								@CodUbic = CodUbic,
								@CODPROV = CodProv,
								@MtoTotal = MtoTotal,
								@DESCTO1 = Descto1,
								@DESCTO2 = Descto2,
								@Contado = Contado,
								@Credito = Credito,
								@CancelI = CancelI,
								@CancelA = CancelA,
								@CancelE = CancelE,
								@CancelC = CancelC,
								@CancelT = CancelT,
								@CancelG = CancelG,
								@FECHA = FechaE,
								@FLETES = Fletes,
								@MTOTAX = Mtotax,
								@TOTALPRDSRV = TotalPrd + TotalSrv,
								@NUMEROR = NumeroN								
						FROM SACOMP WITH (NOLOCK)
						WHERE TipoCom = @TipoCom and NumeroD = @NUMEROD AND CodProv = @CodProv;

						-- RECALCUANDO DESCUENTOS GLOBALES NUEVO EN 4.3.1.1
						IF ((@DESCTO1 + @DESCTO2) <> 0)
							BEGIN
								UPDATE SACOMP SET 	Descto1 = (@TOTALPRDSRV) - (@MTOTOTAL - @MTOTAX - @FLETES), Descto2 = 0
								WHERE NroUnico = @NROUNICO;

								SET @DESCTOG = (@TOTALPRDSRV) - (@MTOTOTAL - @MTOTAX - @FLETES);
							END;
						ELSE
							BEGIN
								SET @DESCTOG = 0;
							END;

						-- UBICANDO CLAVEREF Y FECHA_REF PARA DEVOLUCIONES
						IF (@TipoCom = ''H'')
							BEGIN
								SET @REF_CLAVE = NULL;
								SET @REF_FECHA = ''1900-01-01'';
								SET @REF_TIPO_DOC = NULL;
								SET @REF_CODIGO = NULL;
								SET @REF_RAZON = NULL;
							END;
						ELSE
							BEGIN
								SET @REF_CLAVE = NULL;
								SET @REF_FECHA = NULL;
								SET @REF_TIPO_DOC = NULL;

								SELECT	@REF_CLAVE = CLAVE,
											@REF_FECHA = FechaE,
											@REF_TIPO_DOC = TIPODOCFE
								FROM SACOMP WITH (NOLOCK)
								WHERE (TipoCom = ''H'') AND (NumeroD = @NUMEROR) AND (CodProv = @CodProv);
								
								SET @REF_CLAVE = IIF(@REF_CLAVE IS NOT NULL, @REF_CLAVE, IIF((SELECT MAX(CLAVE) FROM SACOMP WHERE TIPODOCFE = ''08'') IS NOT NULL, (SELECT MAX(CLAVE) FROM SACOMP WHERE TIPODOCFE = ''08''), ''00000000000000000000000000000000000000000000000000''));
								SET @REF_FECHA = IIF(@REF_FECHA IS NOT NULL, @REF_FECHA, IIF(@REF_CLAVE = ''00000000000000000000000000000000000000000000000000'', GETDATE(), (SELECT FechaE FROM SACOMP WHERE CLAVE = @REF_CLAVE)));
								SET @REF_TIPO_DOC = ''99'';
								SET @REF_CODIGO = ''01'';
								SET @REF_RAZON = ''Anulación de Compra'';						
							END;

						-- UBICANDO SUCURSAL
						SET @Sucursal_principal = 1;

						SELECT 	@Sucursal_principal = Sucursal,
									@llave_sucursal_principal = Llave_Sucursal,
									@EMISOR_IDENTIFICACION = REPLACE(REPLACE(RIF, ''-'',''''), '' '','''')
						FROM SACONF WITH (NOLOCK)
						WHERE (CodSucu = ''00000'');

						SET @Sucursal_principal = ISNULL(@Sucursal_principal,0);
						SET @Estacion = 0;

						-- Ubicando sucursal de bodega
						SET @NUMGRP_1 = NULL;
						SET @NOM_TABLE_1_M = NULL;
						SET @NombreGrp_1 = NULL;
						SET @NOM_TABLE_1 = NULL;

						SET @NUMGRP_1 = 0;
						SET @NOM_TABLE_1_M = ''SADEPO'';
						SET @NombreGrp_1 = ''Factura_Electronica'';

						SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
						FROM SAAGRUPOS WITH (NOLOCK)
						WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

						SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, ''_'',REPLICATE(''0'',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

						SET @STRING = CONCAT(''SELECT @Sucursal = [Sucursal],
											@llave_sucursal = [Llave_Sucursal]
											FROM '', QUOTENAME(@NOM_TABLE_1), '' WITH (NOLOCK)
											WHERE CodUbic = '', '''''''', @CodUbic, '''''''');

						EXECUTE sp_executesql @STRING, N''
						@Sucursal int OUTPUT,
						@llave_sucursal varchar(20) OUTPUT'',					
						@Sucursal = @Sucursal OUTPUT,
						@llave_sucursal = @llave_sucursal OUTPUT;

						SET @Sucursal = ISNULL(@Sucursal,0);
						SET @STRING = NULL;

						IF (@Sucursal = 0)
							BEGIN
								SET @Sucursal = @Sucursal_principal;
								SET @llave_Sucursal = @llave_sucursal_principal;
							END;
						ELSE
							BEGIN
								IF (ISNULL(LEN(@llave_sucursal),0) = 0)
									BEGIN
										SET @Sucursal = @Sucursal_principal;
										SET @llave_Sucursal = @llave_sucursal_principal;										
									END;
							END;

						IF (@TipoCom = ''I'')
							BEGIN
								IF (@REF_CLAVE <> ''00000000000000000000000000000000000000000000000000'')
									BEGIN
										SET @Sucursal = CAST(SUBSTRING(@REF_CLAVE,22,3) AS INT);

										SET @STRING = CONCAT(''SELECT TOP (1) @llave_sucursal = [Llave_Sucursal]
															FROM '', QUOTENAME(@NOM_TABLE_1), '' WITH (NOLOCK)
															WHERE [Sucursal] = '', '''''''', @Sucursal, '''''''');

										EXECUTE sp_executesql @STRING, N''
										@llave_sucursal varchar(20) OUTPUT'',					
										@llave_sucursal = @llave_sucursal OUTPUT;

										SET @STRING = NULL;
										SET @Sucursal = ISNULL(@Sucursal,0);

										IF (@Sucursal = 0)
											BEGIN
												SET @Sucursal = @Sucursal_principal;
												SET @llave_Sucursal = @llave_sucursal_principal;
											END;
									END;
							END;

						-- UBICANDO ESTACION
						IF (@USER NOT LIKE ''%[^0-9]%'')
							BEGIN
								SET @Estacion = CAST(@USER AS INT);						
							END;
						ELSE
							BEGIN
								SET @Estacion = 1;
							END;

						-- Determinando si crear nuevo registro en tabla ConsecutivosFE
						IF NOT EXISTS (SELECT [Estacion] FROM ConsecutivosFE WHERE [Sucursal] = @Sucursal AND [Estacion] = @Estacion)
							BEGIN			
								INSERT INTO ConsecutivosFE 	(Sucursal,Estacion,LenCorrelFE,PrxFactFE,PrxNDFE,PrxNCFE,PrxTickEFE,PrxCompCFE)
								VALUES	(@Sucursal,@Estacion,10,1,1,1,1,1);
							END;

						SET @CONSECUTIVO_INCOMPLETO = CONCAT(REPLICATE(''0'',3 - DATALENGTH(CAST(@Sucursal AS VARCHAR(3)))), CAST(@Sucursal AS VARCHAR(3)), REPLICATE(''0'',5 - DATALENGTH(CAST(@Estacion AS VARCHAR(5)))), CAST(@Estacion AS VARCHAR(5)));

						-- UBICANDO TIPO DE DOCUMENTO CAMBIO EN 4.3.1.1 ---
						SET @TIPO_DOCUMENTO = IIF(@TipoCom = ''H'',''08'', ''03'');
						
						--- ASIGNADO CONSECUTIVO EN NUMERODE (FACTURACION ELECTRONICA 10 DIGITOS SEGUN TIPO DE DOCUMENTO) ---
						SELECT 	@LenCorrelFE = LenCorrelFE, 
								@PrxComFE = PrxComFE,
								@PrxNCFE = PrxNCFE
						FROM ConsecutivosFE WITH (NOLOCK)
						WHERE ([Sucursal] = @Sucursal) AND ([Estacion] = @Estacion);

						IF (@TipoCom = ''H'')
							BEGIN
								SET @CONSECUTIVO_ORIGEN	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxComFE AS VARCHAR(10)))) + CAST(@PrxComFE AS VARCHAR(10)) AS VARCHAR(10));
								UPDATE ConsecutivosFE SET PrxComFE = PrxComFE + 1
								WHERE ([Sucursal] = @Sucursal) AND ([Estacion] = @Estacion);
							END;

						IF (@TipoCom = ''I'')
							BEGIN
								SET @CONSECUTIVO_ORIGEN	= CAST(REPLICATE(''0'', @LenCorrelFE - DATALENGTH(CAST(@PrxNCFE AS VARCHAR(10)))) + CAST(@PrxNCFE AS VARCHAR(10)) AS VARCHAR(10));
								UPDATE ConsecutivosFE SET PrxNCFE = PrxNCFE + 1
								WHERE ([Sucursal] = @Sucursal) AND ([Estacion] = @Estacion);
							END;
										
						-- CREANDO CONSECUTIVO COMPLETO
						SET @CONSECUTIVO_COMPLETO = CONCAT(@CONSECUTIVO_INCOMPLETO, @TIPO_DOCUMENTO, @CONSECUTIVO_ORIGEN);
						
						-- CREANDO CLAVE
						SET @DIA = IIF(LEN(CAST(DAY(@FECHA) AS VARCHAR(2))) = 1, CONCAT(''0'', CAST(DAY(@FECHA) AS VARCHAR(2))), CAST(DAY(@FECHA) AS VARCHAR(2)));
						SET @MES = IIF(LEN(CAST(MONTH(@FECHA) AS VARCHAR(2))) = 1, CONCAT(''0'', CAST(MONTH(@FECHA) AS VARCHAR(2))), CAST(MONTH(@FECHA) AS VARCHAR(2)));
						SET @ANO = RIGHT(CAST(YEAR(@FECHA) AS VARCHAR(4)), 2);
						SET @IDCLAVE = RIGHT(CONCAT(''00000000000'', @EMISOR_IDENTIFICACION), 12);
						SET @CLAVE = CONCAT(''506'', @DIA, @MES, @ANO, @IDCLAVE, @CONSECUTIVO_COMPLETO, ''1'', ''44332211'');

						UPDATE SACOMP SET	
								CLAVE = @CLAVE, 
								CONSECUTIVODE = @CONSECUTIVO_COMPLETO,
								NUMERODE = @CONSECUTIVO_ORIGEN, 
								TIPODOCFE = @TIPO_DOCUMENTO
						WHERE (NroUnico = @NROUNICO) AND (TipoCom = ''H'');

						INSERT INTO [dbo].[ComprobantesFE]
								([NroUnico]
								,[TipoDoc]
								,[NumeroD]
								,[NumeroDE]
								,[Clave]
								,[TipoDocFE]
								,[ConsecutivoDE]
								,[ClaveRef]
								,[ExoneracionE]
								,[NroItemT]
								,[Iva_Devuelto]
								,[Monto_Exonerado]
								,[CodTaxsClie]
								,[EstadoFE]
								,[MensajeFE])
						VALUES	(@NroUnico
									,@TipoCom
									,@numeroD
									,@CONSECUTIVO_ORIGEN
									,@CLAVE
									,@TIPO_DOCUMENTO
									,@CONSECUTIVO_COMPLETO
									,@REF_CLAVE
									,0
									,0
									,0
									,0
									,NULL
									,0
									,''Pendiente por enviar a tablas intermedias'');

						-- ACTUALIZANDO SACOMP MONTO DE FACTURA Y PAGO
						UPDATE SACOMP SET 	Monto = ROUND(@Monto - (@Descto1 + @Descto2) + @Fletes,2),
											MtoTax = 0,
											TGravable = 0,
											TExento = ROUND(@Monto - (@Descto1 + @Descto2) + @Fletes,2),
											MtoTotal = ROUND(@Monto - (@Descto1 + @Descto2) + @Fletes,2),
											Contado = ROUND((@Contado/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											Credito = ROUND((@Credito/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelI = ROUND((@CancelI/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelA = ROUND((@CancelA/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelE = ROUND((@CancelE/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelC = ROUND((@CancelC/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelT = ROUND((@CancelT/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2),
											CancelG = ROUND((@CancelG/IIF(ISNULL(@MtoTotal,0) = 0, 1, @MtoTotal)) * (@Monto - (@Descto1 + @Descto2) + @Fletes),2)
						WHERE NroUnico = @NroUnico;			

						-- ELIMINANDO REGISTROS DE TAXES
						DELETE FROM SATAXCOM
						WHERE TipoCom = @TipoCom and NumeroD = @NUMEROD AND CodProv = @CodProv;

						DELETE FROM SATAXITC
						WHERE TipoCom = @TipoCom and NumeroD = @NUMEROD AND CodProv = @CodProv;	
					END;
			END;
	END;
END;');
GO
/*GUARDAR DATOS DE EMISOR FACTURAS ELECTRONICAS*/
/*INICIO TRIGGER SAOPER_0X*/ 
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_DATOS_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_DATOS_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,  -- TABLA Datos_para_facturacion_electronica
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Datos_para_facturacion_electronica
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SAOPER';
SET @NombreGrp = 'Datos_para_facturacion_electronica';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_DATOS_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CODOPER VARCHAR(10),
@CODMONEDAP INT,
@CODMONEDAR INT,
@Codigo_Moneda_P AS VARCHAR(5),
@Codigo_Moneda_R AS VARCHAR(5),
@TIPOEMISOR VARCHAR(2),
@DESCRIPFLETES VARCHAR(60),
@VALIDADOR AS VARCHAR(5),
@EMAILEMISOR AS VARCHAR(60),
@NOMBRECOMERCIAL AS VARCHAR(80),
@correos_adicionales1 as VARCHAR(100),
@correos_adicionales2 as VARCHAR(100),
@correos_adicionales3 as VARCHAR(100),
@correos_adicionales4 as VARCHAR(100),
@correos_adicionales AS VARCHAR(500),
@Sucursal AS INT,
@Si_FC AS SMALLINT,
@NUMGRP_1 AS INT,  -- TABLA Datos_para_facturacion_electronica
@NUMGRP_2 AS INT,  -- TABLA Exoneración_y_Datos_Adicionales
@NUMGRP_10 AS INT,  -- Otros Datos (Compras)
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_2_M AS VARCHAR(50),
@NOM_TABLE_10_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NOM_TABLE_2 AS VARCHAR(50),
@NOM_TABLE_10 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@NombreGrp_2 AS VARCHAR(50),
@NombreGrp_10 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX),
@CODOPER2 AS VARCHAR(12),
@SUCURSAL2 AS VARCHAR(10),
@TABLE_1 AS SYSNAME,
@Incluir_DSPDF AS smallint,
@Codigo_Actividad_1 AS INT,
@Codigo_Actividad_2 AS INT,
@Codigo_Actividad_3 AS INT,
@Codigo_Actividad_4 AS INT,
@Codigo_Actividad_5 AS INT,
@Codigo_Actividad_6 AS INT,
@Codigo_Actividad_7 AS INT,
@Codigo_Actividad_8 AS INT,
@Codigo_Actividad_9 AS INT,
@Codigo_Actividad_10 AS INT,
@Nombre_Actividad_1 AS VARCHAR(260),
@Nombre_Actividad_2 AS VARCHAR(260),
@Nombre_Actividad_3 AS VARCHAR(260),
@Nombre_Actividad_4 AS VARCHAR(260),
@Nombre_Actividad_5 AS VARCHAR(260),
@Nombre_Actividad_6 AS VARCHAR(260),
@Nombre_Actividad_7 AS VARCHAR(260),
@Nombre_Actividad_8 AS VARCHAR(260),
@Nombre_Actividad_9 AS VARCHAR(260),
@Nombre_Actividad_10 AS VARCHAR(260),
@NroUnico AS INT,
@Pos AS INT,
@TOTAL AS INT,
@LINEA AS INT,
@LISTADO AS NVARCHAR(MAX),
@llave_sucursal varchar(20),
@Incluir_Receptor smallint,
@Unidad_Servicios smallint,
@Cabys_Fletes varchar(13),
@Regalias smallint

SET NOCOUNT ON;
SELECT	@CODOPER = CodOper,
		@CODMONEDAP = Cod_Moneda_Prinicpal,
		@CODMONEDAR = Cod_Moneda_Referencial,
		@TIPOEMISOR = CASE Tipo_Emisor 	WHEN 1 THEN ''01''
										WHEN 2 THEN ''02''
										WHEN 3 THEN ''03''
										WHEN 4 THEN ''04''
										WHEN 5 THEN ''05''
										ELSE NULL END,     
		@DESCRIPFLETES = Descripcion_Fletes,
		@EMAILEMISOR = REPLACE(Email_Emisor,'' '',''''),
		@NOMBRECOMERCIAL = Nombre_Comercial,
		@correos_adicionales1 = REPLACE(correos_adicionales_1,'' '',''''),
		@correos_adicionales2 = REPLACE(correos_adicionales_2,'' '',''''),
		@correos_adicionales3 = REPLACE(correos_adicionales_3,'' '',''''),
		@correos_adicionales4 = REPLACE(correos_adicionales_4,'' '',''''),
		@Sucursal = ISNULL(Sucursal,1),
		@Si_FC = ISNULL(Si_FC,0),
		@Incluir_DSPDF = ISNULL(Incluir_DSPDF,0),
		@Codigo_Actividad_1 = ISNULL(Codigo_Actividad_1,0),
		@Codigo_Actividad_2 = ISNULL(Codigo_Actividad_2,0),
		@Codigo_Actividad_3 = ISNULL(Codigo_Actividad_3,0),
		@Codigo_Actividad_4 = ISNULL(Codigo_Actividad_4,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_5,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_6,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_7,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_8,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_9,0),
		@Codigo_Actividad_5 = ISNULL(Codigo_Actividad_10,0),
		@llave_sucursal = Llave_Sucursal,
		@Incluir_Receptor = Incluir_Receptor,
		@Unidad_Servicios = Unidad_Servicios,
		@Cabys_Fletes = Cabys_Fletes,
		@Regalias = Regalias
FROM inserted;

-- UBICANDO DATOS DE TABLA Datos_para_facturacion_electronica
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = ''SAOPER'';
SET @NombreGrp_1 = ''Datos_para_facturacion_electronica'';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, ''_'',REPLICATE(''0'',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @TABLE_1 = @NOM_TABLE_1;
SET @CODOPER2 = CONCAT('''''''', @CODOPER, '''''''');

-- UBICANDO DATOS DE TABLA EXONERACIÓN_Y_DATOS_ADICIONALES
SET @NUMGRP_2 = 0;
SET @NOM_TABLE_2_M = ''SAFACT'';
SET @NombreGrp_2 = ''Exoneración_y_Datos_Adicionales'';

SELECT @NUMGRP_2 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = @NombreGrp_2;

-- UBICANDO DATOS DE TABLA Otros Datos (Compras)
SET @NUMGRP_10 = 0;
SET @NOM_TABLE_10_M = ''SACOMP'';
SET @NombreGrp_10 = ''Otros_Datos'';

SELECT @NUMGRP_10 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_10_M AND NombreGrp = @NombreGrp_10;

-- OTROS DATOS
SET @TIPOEMISOR = IIF(IIF(LEN(@TIPOEMISOR) = 1, CONCAT(''0'',@TIPOEMISOR), @TIPOEMISOR) IN (''01'',''02'',''03'',''04''), IIF(LEN(@TIPOEMISOR) = 1, CONCAT(''0'',@TIPOEMISOR), @TIPOEMISOR), '''');

-- UBICANDO MONEDAS
IF (ISNULL(LEN(@CODMONEDAP),0) <> 0)
	BEGIN
		IF (@CODMONEDAP <> 0)
			BEGIN
				SET @Codigo_Moneda_P = NULL;

				SELECT @Codigo_Moneda_P = CODMONEDA
				FROM FECodigoMoneda WITH (NOLOCK)
				WHERE ID = @CODMONEDAP;		

				UPDATE FECodigoMoneda SET Nivel = 0
				WHERE Nivel = 1;

				UPDATE FECodigoMoneda SET Nivel = 1
				WHERE ID = @CODMONEDAP;				
			END;
		ELSE
			BEGIN
				SET @Codigo_Moneda_P = ''CRC'';

				UPDATE FECodigoMoneda SET Nivel = 0
				WHERE Nivel = 1;

				UPDATE FECodigoMoneda SET Nivel = 1
				WHERE ID = 1;
			END;
	END;
ELSE
	BEGIN
		SET @Codigo_Moneda_P = ''CRC'';

		UPDATE FECodigoMoneda SET Nivel = 0
		WHERE Nivel = 1;

		UPDATE FECodigoMoneda SET Nivel = 1
		WHERE ID = 1;
	END;

IF (ISNULL(LEN(@CODMONEDAR),0) <> 0)
	BEGIN
		IF (@CODMONEDAR <> 0)
			BEGIN
				SET @Codigo_Moneda_R = NULL;

				SELECT	@Codigo_Moneda_R = CODMONEDA
				FROM FECodigoMoneda WITH (NOLOCK)
				WHERE ID = @CODMONEDAR;

				IF (@Codigo_Moneda_P = @Codigo_Moneda_R)
					BEGIN
						SET @Codigo_Moneda_R = NULL;

						UPDATE FECodigoMoneda SET Nivel = 0
						WHERE Nivel = 2;
					END;
				ELSE
					BEGIN
						UPDATE FECodigoMoneda SET Nivel = 0
						WHERE Nivel = 2;

						UPDATE FECodigoMoneda SET Nivel = 2
						WHERE ID = @CODMONEDAR;
					END;
			END;
		ELSE
			BEGIN
				SET @Codigo_Moneda_R = NULL;

				UPDATE FECodigoMoneda SET Nivel = 0
				WHERE Nivel = 2;
			END;
	END;
ELSE
	BEGIN
		SET @Codigo_Moneda_R = NULL; 

		UPDATE FECodigoMoneda SET Nivel = 0
		WHERE Nivel = 2;
	END;

IF (@EMAILEMISOR NOT LIKE ''%@%.%'')
	BEGIN
		SET @EMAILEMISOR = NULL;
	END;
IF (@correos_adicionales1 NOT LIKE ''%@%.%'')
	BEGIN
		SET @correos_adicionales1 = NULL;
	END;
IF (@correos_adicionales2 NOT LIKE ''%@%.%'')
	BEGIN
		SET @correos_adicionales2 = NULL;
	END;
IF (@correos_adicionales3 NOT LIKE ''%@%.%'')
	BEGIN
		SET @correos_adicionales3 = NULL;
	END;
IF (@correos_adicionales4 NOT LIKE ''%@%.%'')
	BEGIN
		SET @correos_adicionales4 = NULL;
	END;

SET @correos_adicionales = 	CONCAT(IIF(@correos_adicionales1 IS NULL, '''', @correos_adicionales1 ), 
							IIF(@correos_adicionales2 IS NULL, '''', IIF(@correos_adicionales1 IS NULL, @correos_adicionales2, CONCAT('';'', @correos_adicionales2))), 
							IIF(@correos_adicionales3 IS NULL, '''', IIF((@correos_adicionales1 IS NULL) AND (@correos_adicionales2 IS NULL), @correos_adicionales3, CONCAT('';'', @correos_adicionales3))),
							IIF(@correos_adicionales4 IS NULL, '''', IIF((@correos_adicionales1 IS NULL) AND (@correos_adicionales2 IS NULL) AND (@correos_adicionales3 IS NULL), @correos_adicionales4, CONCAT('';'', @correos_adicionales4))));

-- ACTIVIDADES ECONOMICAS
SET @STRING = ''UPDATE FEActividades_Eco SET Nivel = NULL'';
EXECUTE sp_executesql @STRING;
SET @STRING = NULL;

IF (@Codigo_Actividad_1 IS NOT NULL)
	IF (@Codigo_Actividad_1 <> 0)
		IF (@Codigo_Actividad_1 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_1 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_1);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_1 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_1 = @Nombre_Actividad_1 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 1 
									   WHERE NroUnico = '',@Codigo_Actividad_1);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_1 = ''ACTIVIDAD 1 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_1 = ''ACTIVIDAD 1 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_1 = ''ACTIVIDAD 1 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_2 IS NOT NULL) 
	IF (@Codigo_Actividad_2 <> 0) 
		IF (@Codigo_Actividad_2 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_2 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_2);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_2 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_2 = @Nombre_Actividad_2 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 2 
										WHERE NroUnico = '',@Codigo_Actividad_2);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_2 = ''ACTIVIDAD 2 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_2 = ''ACTIVIDAD 2 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_2 = ''ACTIVIDAD 2 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_3 IS NOT NULL) 
	IF (@Codigo_Actividad_3 <> 0) 
		IF (@Codigo_Actividad_3 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_3 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_3);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_3 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_3 = @Nombre_Actividad_3 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 3 
										WHERE NroUnico = '',@Codigo_Actividad_3);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_3 = ''ACTIVIDAD 3 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_3 = ''ACTIVIDAD 3 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_3 = ''ACTIVIDAD 3 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_4 IS NOT NULL) 
	IF (@Codigo_Actividad_4 <> 0) 
		IF (@Codigo_Actividad_4 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_4 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_4);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_4 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_4 = @Nombre_Actividad_4 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 4 
										WHERE NroUnico = '',@Codigo_Actividad_4);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END
		ELSE
			BEGIN
				SET @Nombre_Actividad_4 = ''ACTIVIDAD 4 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_4 = ''ACTIVIDAD 4 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_4 = ''ACTIVIDAD 4 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_5 IS NOT NULL) 
	IF (@Codigo_Actividad_5 <> 0) 
		IF (@Codigo_Actividad_5 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_5 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_5);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_5 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_5 = @Nombre_Actividad_5 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 5 
										WHERE NroUnico = '',@Codigo_Actividad_5);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_5 = ''ACTIVIDAD 5 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_5 = ''ACTIVIDAD 5 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_5 = ''ACTIVIDAD 5 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_6 IS NOT NULL) 
	IF (@Codigo_Actividad_6 <> 0) 
		IF (@Codigo_Actividad_6 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_6 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_6);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_6 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_6 = @Nombre_Actividad_6 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 6 
										WHERE NroUnico = '',@Codigo_Actividad_6);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_6 = ''ACTIVIDAD 6 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_6 = ''ACTIVIDAD 6 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_6 = ''ACTIVIDAD 6 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_7 IS NOT NULL) 
	IF (@Codigo_Actividad_7 <> 0) 
		IF (@Codigo_Actividad_7 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_7 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_7);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_7 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_7 = @Nombre_Actividad_7 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 7 
										WHERE NroUnico = '',@Codigo_Actividad_7);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_7 = ''ACTIVIDAD 7 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_7 = ''ACTIVIDAD 7 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_7 = ''ACTIVIDAD 7 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_8 IS NOT NULL) 
	IF (@Codigo_Actividad_8 <> 0) 
		IF (@Codigo_Actividad_8 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_8 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_8);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_8 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_8 = @Nombre_Actividad_8 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 8 
										WHERE NroUnico = '',@Codigo_Actividad_8);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_8 = ''ACTIVIDAD 8 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_8 = ''ACTIVIDAD 8 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_8 = ''ACTIVIDAD 8 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_9 IS NOT NULL) 
	IF (@Codigo_Actividad_9 <> 0) 
		IF (@Codigo_Actividad_9 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_9 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_9);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_9 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_9 = @Nombre_Actividad_9 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 9 
										WHERE NroUnico = '',@Codigo_Actividad_9);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_9 = ''ACTIVIDAD 9 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_9 = ''ACTIVIDAD 9 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_9 = ''ACTIVIDAD 9 NO ASIGNADA'';
	END;

IF (@Codigo_Actividad_10 IS NOT NULL) 
	IF (@Codigo_Actividad_10 <> 0) 
		IF (@Codigo_Actividad_10 <> '''')
			BEGIN
				-- BUSCO NOMBRE DE ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''SELECT @Nombre_Actividad_10 = CONCAT(Codigo_Actividad,'', '' '''' '''' '', '', Nombre)
				FROM FEActividades_Eco WITH (NOLOCK)
				WHERE NroUnico = '', @Codigo_Actividad_10);

				EXECUTE sp_executesql @STRING, N''
				@Nombre_Actividad_10 AS VARCHAR(260) OUTPUT'',
				@Nombre_Actividad_10 = @Nombre_Actividad_10 OUTPUT;

				SET @STRING = NULL;

				-- ASIGNO NIVEL A ACTIVIDAD ECONOMICA
				SET @STRING = CONCAT(''UPDATE FEActividades_Eco SET Nivel = 10 
										WHERE NroUnico = '',@Codigo_Actividad_10);

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;	
			END;
		ELSE
			BEGIN
				SET @Nombre_Actividad_10 = ''ACTIVIDAD 10 NO ASIGNADA'';
			END;
	ELSE
		BEGIN
			SET @Nombre_Actividad_10 = ''ACTIVIDAD 10 NO ASIGNADA'';
		END;
ELSE
	BEGIN
		SET @Nombre_Actividad_10 = ''ACTIVIDAD 10 NO ASIGNADA'';
	END;

IF (@CODOPER = ''FACTELEC'')
	BEGIN 
		UPDATE SACONF SET	CODMONEDA = UPPER(@Codigo_Moneda_P),
							CODMONEDAR = UPPER(@Codigo_Moneda_R),
							EMISORTIPO = @TIPOEMISOR,
							DESCRIPFLETESFE = @DESCRIPFLETES,
							Email = LOWER(@EMAILEMISOR),
							NOMBRECOMERCIAL = @NOMBRECOMERCIAL,
							correos_adicionales = LOWER(IIF(CAST(@correos_adicionales AS VARCHAR(500)) = '''', NULL, CAST(@correos_adicionales AS VARCHAR(500)))),
							Sucursal = IIF(@Sucursal = 0, 1, ISNULL(@Sucursal,1)),
							Si_FC = @Si_FC,
							Incluir_DSPDF = @Incluir_DSPDF,
							Llave_Sucursal = @llave_sucursal,
							Incluir_Receptor = @Incluir_Receptor,
							Unidad_Servicios = @Unidad_Servicios,
							Cabys_Fletes = @Cabys_Fletes,
							Regalias = @Regalias;

		SET @EMAILEMISOR = CONCAT('''''''',LOWER(@EMAILEMISOR),'''''''');
		SET @correos_adicionales1 = IIF(@correos_adicionales1 IS NOT NULL, CONCAT('''''''',LOWER(@correos_adicionales1),''''''''), ''NULL'');
		SET @correos_adicionales2 = IIF(@correos_adicionales2 IS NOT NULL, CONCAT('''''''',LOWER(@correos_adicionales2),''''''''), ''NULL'');
		SET @correos_adicionales3 = IIF(@correos_adicionales3 IS NOT NULL, CONCAT('''''''',LOWER(@correos_adicionales3),''''''''), ''NULL'');
		SET @correos_adicionales4 = IIF(@correos_adicionales4 IS NOT NULL, CONCAT('''''''',LOWER(@correos_adicionales4),''''''''), ''NULL'');
		SET @SUCURSAL2 = CAST(IIF(@Sucursal = 0, 1, ISNULL(@Sucursal,1)) AS VARCHAR(10));

		SET @STRING = CONCAT(''UPDATE '',QUOTENAME(@TABLE_1),'' SET  Email_Emisor = '',@EMAILEMISOR,'', 
		correos_adicionales_1 ='',@correos_adicionales1,'', 
		correos_adicionales_2 ='',@correos_adicionales2,'', 
		correos_adicionales_3 ='',@correos_adicionales3,'', 
		correos_adicionales_4 ='',@correos_adicionales4,'',
		Sucursal = '',@SUCURSAL2,''
		WHERE CodOper = '',@CODOPER2);

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;

		-- ACTUALIZANDO Factor_Cambio en SAFACT_0X
		UPDATE SAACAMPOS SET AliasCpo = IIF(@Si_FC = 1, ''No Mostrar Factor Cambio'', ''Mostrar Factor Cambio'')
		WHERE	CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NombCpo = ''Factor_Cambio'';

		-- ACTUALIZANDO Codigo_Actividad_S en SAFACT_0X
		UPDATE SAACAMPOS SET Value = CONCAT(''Seleccionar'', CHAR(13), CHAR(10), 
		@Nombre_Actividad_1, CHAR(13), CHAR(10),
		@Nombre_Actividad_2, CHAR(13), CHAR(10),
		@Nombre_Actividad_3, CHAR(13), CHAR(10),
		@Nombre_Actividad_4, CHAR(13), CHAR(10),
		@Nombre_Actividad_5, CHAR(13), CHAR(10),
		@Nombre_Actividad_6, CHAR(13), CHAR(10),
		@Nombre_Actividad_7, CHAR(13), CHAR(10),
		@Nombre_Actividad_8, CHAR(13), CHAR(10),
		@Nombre_Actividad_9, CHAR(13), CHAR(10),
		@Nombre_Actividad_10)
		WHERE	CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NombCpo = ''Codigo_Actividad_S'';

		-- ACTUALIZANDO SUCURSAL DE DEPOSITOS
		SET @NUMGRP_1 = NULL;
		SET @NOM_TABLE_1_M = NULL;
		SET @NombreGrp_1 = NULL;
		SET @NOM_TABLE_1 = NULL;

		SET @NUMGRP_1 = 0;
		SET @NOM_TABLE_1_M = ''SADEPO'';
		SET @NombreGrp_1 = ''Factura_Electronica'';

		SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

		SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, ''_'',REPLICATE(''0'',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

		SET @STRING = CONCAT(''UPDATE '',QUOTENAME(@NOM_TABLE_1),'' SET Sucursal = '',@SUCURSAL2,'','',
							'' Llave_Sucursal = '','''''''',@Llave_Sucursal,'''''''',
							'' WHERE Sucursal IN (0,'',@SUCURSAL2,'')'');

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;

		-- ACTUALIZANDO CODIGO_ACTIVIDAD_S EN SADEPO_0X 
		UPDATE SAACAMPOS SET Value = CONCAT(''Seleccionar'', CHAR(13), CHAR(10), 
		@Nombre_Actividad_1, CHAR(13), CHAR(10),
		@Nombre_Actividad_2, CHAR(13), CHAR(10),
		@Nombre_Actividad_3, CHAR(13), CHAR(10),
		@Nombre_Actividad_4, CHAR(13), CHAR(10),
		@Nombre_Actividad_5, CHAR(13), CHAR(10),
		@Nombre_Actividad_6, CHAR(13), CHAR(10),
		@Nombre_Actividad_7, CHAR(13), CHAR(10),
		@Nombre_Actividad_8, CHAR(13), CHAR(10),
		@Nombre_Actividad_9, CHAR(13), CHAR(10),
		@Nombre_Actividad_10)
		WHERE	CodTbl = @NOM_TABLE_1_M AND NumGrp = @NUMGRP_1 AND NombCpo = ''Codigo_Actividad_S'';

		UPDATE ConsecutivosFE SET Sucursal = @SUCURSAL2
		WHERE Sucursal = 0;
	END;

	-- ACTUALIZANDO LISTADO MONEDAS EN SAFACT_0X
	SET @LISTADO = ''Seleccionar'';
	SET @TOTAL = 0;
	SET @LINEA = 1;

	SELECT @TOTAL = COUNT(*)
	FROM FECodigoMoneda WITH (NOLOCK)
	WHERE Nivel > 0;

	WHILE (@LINEA <= @TOTAL)
		BEGIN
			SELECT @LISTADO = CONCAT(@LISTADO,  CHAR(13), CHAR(10), CODMONEDA, ''-'', NOMBRE_MONEDA, ''-'', PAIS) 
			FROM FECodigoMoneda WITH (NOLOCK)
			WHERE Nivel = @LINEA;

			SET @LINEA +=1;
		END;

	UPDATE SAACAMPOS SET Value = @LISTADO
	WHERE	CodTbl = @NOM_TABLE_2_M AND NumGrp = @NUMGRP_2 AND NombCpo = ''Moneda'';

	UPDATE SAACAMPOS SET Value = @LISTADO
	WHERE	CodTbl = @NOM_TABLE_10_M AND NumGrp = @NUMGRP_10 AND NombCpo = ''Moneda'';

	-- LIMPIANDO TABLA SAOPER_02
	SET @STRING = CONCAT(
	''DELETE FROM '', QUOTENAME(@TABLE_1),''
	WHERE CodOper <> '',''''''FACTELEC'''''');

	EXECUTE sp_executesql @STRING;
	SET @STRING = NULL;
END');
GO
/*RECARGA LISTADO DE MONEDAS EN VENTAS Y COMPRAS*/
DECLARE
@NUMGRP_1 AS INT,  -- TABLA Corrige_datos_de_Cliente_FE
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Corrige_datos_de_Cliente_FE
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SAOPER';
SET @NombreGrp_1 = 'Datos_para_facturacion_electronica';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

EXEC (N'UPDATE '+@NOM_TABLE_1+' SET Cod_Moneda_Prinicpal = Cod_Moneda_Prinicpal,
	Cod_Moneda_Referencial = Cod_Moneda_Referencial,
	Incluir_Receptor = 0
WHERE CodOper = '+''''+'FACTELEC'+'''');
GO
/*EDICION DE RECEPTOR*/
/*TRIGGER SACLIE*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_RECEPTOR_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_RECEPTOR_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_RECEPTOR_FE]
ON [dbo].[SACLIE] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodClie AS VARCHAR(15),
@ID3 AS VARCHAR(25),
@TipoID3 AS smallint,
@Telef AS VARCHAR(30),
@CLASE AS VARCHAR(10),
@Contador AS INT,
@Descrip AS VARCHAR(60),
@DescripExt AS VARCHAR(250),
@Direc1 AS VARCHAR(60),
@Direc2 AS VARCHAR(60),
@Estado int,
@Ciudad int,
@Municipio int,
@Email VARCHAR(60),
@Fax VARCHAR(30),
@TipoCli smallint,
@Valid_Nombre AS smallint,
-- VARIABLES TABLA ENCABEZADO
@receptor_nombre varchar(100),
@receptor_tipo varchar(2),
@receptor_identificacion varchar(12),
@pasaporte varchar(20),
@receptor_provincia varchar(1),
@receptor_canton varchar(2),
@receptor_distrito varchar(2),
@receptor_direccion varchar(160),
@receptor_telefono varchar(20),
@receptor_fax varchar(20),
@receptor_email varchar(100),
-- TABLA ADICIONAL
@NUMGRP_5 AS INT,  -- TABLA Exoneracion
@NOM_TABLE_5_M AS VARCHAR(50),
@NOM_TABLE_5 AS VARCHAR(50),
@NombreGrp_5 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX),
@Tipo_documento	int,
@Numero_Documento varchar(17),
@Nombre_Institucion varchar(99),
@Fecha_Documento datetime,
@Porcentaje int,
@Pais AS INT,
@Impuesto int,
@No_Exoneacion tinyint,
@No_Impuesto tinyint,
@Usa_Descrip_Ampli smallint,
@Represent varchar(40),
@cambios int

SET NOCOUNT ON;

SELECT	@CodClie = CodClie,
		@ID3 = ID3,
		@TipoID3 = TipoID3,
		@Telef = Telef,
		@CLASE = Clase,
		@Descrip = RTRIM(LTRIM(Descrip)),
		@Direc1 = RTRIM(LTRIM(Direc1)),
		@Direc2 = RTRIM(LTRIM(Direc2)),
		@Estado = Estado,
		@Ciudad = Ciudad,
		@Municipio = Municipio,
		@Email = RTRIM(LTRIM(REPLACE(Email, ' ',''))),
		@TipoCli = TipoCli,
		@Fax = LEFT(Fax,20),
		@Represent = Represent,
		@DescripExt = RTRIM(LTRIM(DescripExt))
FROM inserted;

SET @CLASE = IIF(@CLASE NOT IN ('EN','ED','P','T','E'), 'N', @CLASE);
-- EN = NITE, ED = DIMEX, P = PASAPORTE, CEDULA EXTRANJEROS, T = TIQUETES ELECTRONICOS, E = EXPORTACION, N = CEDULAS NACIONALES
SET @Valid_Nombre = IIF(@Descrip LIKE '?%CONTADO%', 1, 0);

IF (@Valid_Nombre <> 1)
	BEGIN 
		SET @NUMGRP_5 = NULL;
		SET @NOM_TABLE_5_M = NULL;
		SET @NOM_TABLE_5 = NULL;
		SET @NombreGrp_5 = NULL;
		SET @STRING = NULL;
		SET @Usa_Descrip_Ampli = 0;

		-- DESBLOQUEANDO COMPROBANTES
		-- UBICANDO DATOS DE TABLA TABLA Exoneracion
		SET @NUMGRP_5 = 0;
		SET @NOM_TABLE_5_M = 'SACLIE';
		SET @NombreGrp_5 = 'Exoneracion';

		SELECT @NUMGRP_5 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));

		SET @STRING = CONCAT(
		'SELECT 	@Tipo_documento = Tipo_documento,
					@Numero_Documento = Numero_Documento,
					@Nombre_Institucion = Nombre_Institucion,
					@Fecha_Documento = Fecha_Documento,
					@Porcentaje = Porcentaje
		FROM ', QUOTENAME(@NOM_TABLE_5),' WITH (NOLOCK)
		WHERE CodClie = ', '''',@CodClie, '''');

		EXECUTE sp_executesql @STRING, N'
		@Tipo_documento AS int OUTPUT,
		@Numero_Documento varchar(17) OUTPUT,
		@Nombre_Institucion varchar(99) OUTPUT,
		@Fecha_Documento datetime OUTPUT,
		@Porcentaje AS INT OUTPUT',
		@Tipo_documento = @Tipo_documento OUTPUT,
		@Numero_Documento = @Numero_Documento OUTPUT,
		@Nombre_Institucion = @Nombre_Institucion OUTPUT,
		@Fecha_Documento = @Fecha_Documento OUTPUT,
		@Porcentaje = @Porcentaje OUTPUT;

		SET @STRING = NULL;

		-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
		SET @NUMGRP_5 = 0;
		SET @NOM_TABLE_5_M = 'SACLIE';
		SET @NombreGrp_5 = 'Correos_y_Datos_Adiconales';

		SELECT @NUMGRP_5 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));

		SET @STRING = CONCAT(
		'SELECT @Impuesto = Impuesto,
				@Usa_Descrip_Ampli = Usa_Descrip_Ampli
		FROM ', QUOTENAME(@NOM_TABLE_5),' WITH (NOLOCK)
		WHERE CodClie = ', '''',@CodClie, '''');

		EXECUTE sp_executesql @STRING, N'
		@Impuesto AS INT OUTPUT,		
		@Usa_Descrip_Ampli smallint OUTPUT',
		@Impuesto = @Impuesto OUTPUT,		
		@Usa_Descrip_Ampli = @Usa_Descrip_Ampli OUTPUT;

		SET @STRING = NULL;
		SET @Usa_Descrip_Ampli = ISNULL(@Usa_Descrip_Ampli,0);

		SELECT @cambios = COUNT(*)
		FROM [dbo].[CambiosMaestrosFE]
		WHERE ([tipo] = 'CLI') AND ([codigo] = @CodClie) AND ([estado_sync] = 0);

		SET @cambios = ISNULL(@cambios, 0);

		IF (@cambios = 0)
			BEGIN
				-- CREANDO REGISTRO PARA DESBLOQUEAR COMPROBANTES
				INSERT INTO [dbo].[CambiosMaestrosFE]
							([tipo]
							,[codigo]
							,[estado_sync]
							,[mensaje_sync]
							,[fechaCE])
				VALUES
							('CLI'
							,@CodClie
							,0
							,NULL
							,GETDATE());
			END;

		-- VALIDANDO SI COLOCAR CLIENTE EXPORTACION
		IF (@CLASE = 'E')
			BEGIN
				UPDATE SACLIE SET TipoCli = 2
				WHERE CodClie = @CodClie;

				SET @TipoCli = 2;
			END;
		ELSE
			BEGIN
				UPDATE SACLIE SET TipoCli = 0
				WHERE CodClie = @CodClie;

				SET @TipoCli = 0;
			END;

		-- VALIDANDO SI COLOCAR CLIENTE INTERNO NO GRAVABLE
		IF (@CLASE NOT IN ('T','P','E'))
			BEGIN
				IF (@Tipo_documento <> 0)
					BEGIN
						IF (ISNULL(LEN(@Tipo_documento),0) > 0)
							BEGIN
								IF (ISNULL(LEN(@Numero_Documento),0) > 0)
									BEGIN
										IF (ISNULL(LEN(@Nombre_Institucion),0) > 0)
											BEGIN
												IF (ISNULL(LEN(@Fecha_Documento),0) > 0)
													BEGIN
														IF (ISNULL(LEN(@Porcentaje),0) > 0)
															BEGIN
																IF (@Porcentaje >= 1)
																	BEGIN
																		-- COLOCANDO TIPOCLI GRAVABLE
																		UPDATE SACLIE SET TipoCli = CASE WHEN @Porcentaje >= 13 THEN 3 ELSE 0 END
																		WHERE CodClie = @CodClie;

																		SET @No_Exoneacion = 0;
																		SET @No_Impuesto = 1;
																	END;
																ELSE
																	BEGIN
																		SET @No_Exoneacion = 1;
																		SET @No_Impuesto = 0;
																	END;
															END;
														ELSE
															BEGIN
																SET @No_Exoneacion = 1;
																SET @No_Impuesto = 0;
															END;
													END
												ELSE
													BEGIN
														SET @No_Exoneacion = 1;
														SET @No_Impuesto = 0;
													END;
											END
										ELSE
											BEGIN
												SET @No_Exoneacion = 1;
												SET @No_Impuesto = 0;
											END;
									END
								ELSE
									BEGIN
										SET @No_Exoneacion = 1;
										SET @No_Impuesto = 0;
									END;
							END
						ELSE
							BEGIN
								SET @No_Exoneacion = 1;
								SET @No_Impuesto = 0;
							END;
					END
				ELSE
					BEGIN
						SET @No_Exoneacion = 1;
						SET @No_Impuesto = 0;
					END;
			END;
		ELSE
			BEGIN
				SET @No_Exoneacion = 1;
				SET @No_Impuesto = 1;
			END;
	END;
ELSE
	BEGIN
		SET @No_Exoneacion = 1;
		SET @No_Impuesto = 1;
		SET @Clase = 'T';
	END;

IF (@No_Exoneacion = 1)
	BEGIN
		-- BORRANDO DATOS DE EXONEARACION
		SET @NUMGRP_5 = NULL;
		SET @NOM_TABLE_5_M = NULL;
		SET @NOM_TABLE_5 = NULL;
		SET @NombreGrp_5 = NULL;

		-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
		SET @NUMGRP_5 = 0;
		SET @NOM_TABLE_5_M = 'SACLIE';
		SET @NombreGrp_5 = 'Exoneracion';

		SELECT @NUMGRP_5 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));

		SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_5),' SET Porcentaje = 0',		
							' WHERE CodClie = ', '''',@CodClie, '''');

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;

		IF (@Valid_Nombre <> 1)
			BEGIN
				IF (@CLASE NOT IN ('T','P','E'))
					BEGIN
						IF (@Impuesto = 1)
							BEGIN
								-- COLOCANDO CLIENTE INTERNO NO GRABABLE
								UPDATE SACLIE SET TipoCli = 3
								WHERE CodClie = @CodClie;

								SET @No_Impuesto = 0;
							END;
						ELSE
							BEGIN							
								-- COLOCANDO CLIENTE GRABABLE
								UPDATE SACLIE SET TipoCli = 0
								WHERE CodClie = @CodClie;

								SET @No_Impuesto = 0;
							END;																						
					END;
			END;
	END;

IF (@No_Impuesto = 1)
	BEGIN
		-- QUITANDO CAMPO IMPUESTO 														
		SET @NUMGRP_5 = NULL;
		SET @NOM_TABLE_5_M = NULL;
		SET @NOM_TABLE_5 = NULL;
		SET @NombreGrp_5 = NULL;

		-- UBICANDO DATOS DE TABLA TABLA Correos_y_Datos_Adiconales
		SET @NUMGRP_5 = 0;
		SET @NOM_TABLE_5_M = 'SACLIE';
		SET @NombreGrp_5 = 'Correos_y_Datos_Adiconales';

		SELECT @NUMGRP_5 = ISNULL(NumGrp,0)
		FROM SAAGRUPOS WITH (NOLOCK)
		WHERE CodTbl = @NOM_TABLE_5_M AND NombreGrp = @NombreGrp_5;

		SET @NOM_TABLE_5 = CONCAT(@NOM_TABLE_5_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_5 AS VARCHAR(2)))),CAST(@NUMGRP_5 AS VARCHAR(2)));

		SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_5),' SET Impuesto = 0',		
							' WHERE CodClie = ', '''',@CodClie, '''');

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;
	END;

UPDATE SACLIE SET Clase = IIF(@CLASE NOT IN ('EN','ED','P','T','E'), 'N', @CLASE)
WHERE CodClie = @CodClie;
END;
GO
/*DEPURANDO CEDULA DE EMISOR*/
/*TRIGGER SACONF*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_EMISOR_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_EMISOR_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_EMISOR_FE]
ON [dbo].[SACONF] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodSucu AS VARCHAR(5),
@RIF AS VARCHAR(30),
@Telef AS VARCHAR(40)

SET NOCOUNT ON;

IF (UPDATE(RIF)) OR (UPDATE(Telef))
	BEGIN
		SELECT	@CodSucu = CodSucu,
				@RIF = RIF,
				@Telef = Telef		
		FROM inserted;

		IF (REPLACE(REPLACE(@RIF, '-',''), ' ','') NOT LIKE '%[^0-9]%')
			BEGIN
				IF (LEN(REPLACE(REPLACE(@RIF, '-',''), ' ','')) BETWEEN 9 AND 12)
					BEGIN
						UPDATE SACONF SET RIF = REPLACE(REPLACE(@RIF, '-',''), ' ','')
						WHERE CodSucu = @CodSucu;
					END;
				ELSE
					BEGIN 
						UPDATE SACONF SET RIF = NULL
						WHERE CodSucu = @CodSucu;
					END;
			END;
		ELSE
			BEGIN
				UPDATE SACONF SET RIF = NULL
				WHERE CodSucu = @CodSucu;
			END;

		IF (REPLACE(REPLACE(@Telef, '-',''), ' ','') NOT LIKE '%[^0-9]%')
			BEGIN
				IF (LEN(REPLACE(REPLACE(@Telef, '-',''), ' ','')) = 8)
					BEGIN
						UPDATE SACONF SET Telef = REPLACE(REPLACE(@Telef, '-',''), ' ','')
						WHERE CodSucu = @CodSucu;
					END;
				ELSE
					BEGIN 
						UPDATE SACONF SET Telef = NULL
						WHERE CodSucu = @CodSucu;
				END;		

			END;
		ELSE
			BEGIN
				UPDATE SACONF SET Telef = NULL
				WHERE CodSucu = @CodSucu;
			END;
	END;
END;
GO
/*REENVIO DE COMPROBANTES RECHAZADOS*/
/*TRIGGER SAOPER_0X+1*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_REENVIO_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_REENVIO_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,  -- TABLA Corrige_datos_de_Cliente_FE
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Corrige_datos_de_Cliente_FE
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SAOPER';
SET @NombreGrp = 'Corrige_datos_de_Cliente_FE';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_REENVIO_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER UPDATE, INSERT
AS
BEGIN
	DECLARE
	@CodOper VARCHAR(10),
	@consecutivo varchar(20),
	@resultado nvarchar(max),
	@NUMGRP_1 int,  -- TABLA Atributos (SAOPER)
	@NOM_TABLE_1_M varchar(50),
	@NOM_TABLE_1 varchar(50),
	@NombreGrp_1 varchar(50),
	@STRING nvarchar(max)

	SET NOCOUNT ON;

	SELECT 	@CodOper = [CodOper],
			@consecutivo = LTRIM(RTRIM([Nro_Factura]))
	FROM inserted;

	-- Ubicando datos de tabla adicional en SAOPER_0X
	SET @NUMGRP_1 = 0;
	SET @NOM_TABLE_1_M = ''SAOPER'';
	SET @NombreGrp_1 = ''Corrige_datos_de_Cliente_FE'';

	SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
	FROM SAAGRUPOS WITH (NOLOCK)
	WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

	SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, ''_'',REPLICATE(''0'',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

	IF (@CodOper = ''REENVIAR'')
		BEGIN
			IF (ISNULL(LEN(@consecutivo),0) > 0)
				BEGIN
					EXECUTE SP_REENVIO_FE @consecutivo, @resultado OUTPUT;			

					INSERT INTO [dbo].[FEAuditoria] ([Nro_Factura],[Tipo],[Mensaje],[FechaP])
				    VALUES (@consecutivo,''REE'',@resultado,GETDATE());

					SET @STRING = CONCAT(
					''UPDATE '', QUOTENAME(@NOM_TABLE_1),'' SET [Mensaje] = '','''''''',@resultado,'''''''', '', [Nro_Factura] = NULL'',
					'' WHERE [CodOper] = '', '''''''', @CodOper,'''''''');

					EXECUTE sp_executesql @STRING;	
					SET @STRING = NULL;
				END;
		END;

	-- LIMPIANDO TABLA SAOPER_0X
	SET @STRING = CONCAT(
	''DELETE FROM '', QUOTENAME(@NOM_TABLE_1),
	'' WHERE [CodOper] <> '','''''''',''REENVIAR'','''''''')

	EXECUTE sp_executesql @STRING;
	SET @STRING = NULL;
END');
GO
/*EDICION DE PROVEEDORES PARA FACT COMPRAS REGIMEN SIMPLIFICADO*/
/*TRIGGER SAPROV*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_PROV_RC_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_PROV_RC_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_PROV_RC_FE]
ON [dbo].[SAPROV] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodProv AS VARCHAR(15),
@EsReten smallint,
@cambios int

SET NOCOUNT ON;

SELECT	@CodProv = CodProv,
				@EsReten = EsReten		
FROM inserted;

IF (@EsReten = 0)
	BEGIN
		SELECT @cambios = COUNT(*)
		FROM [dbo].[CambiosMaestrosFE]
		WHERE ([tipo] = 'PRV') AND ([codigo] = @CodProv) AND ([estado_sync] = 0);

		SET @cambios = ISNULL(@cambios, 0);

		IF (@cambios = 0)
			BEGIN
				-- CREANDO REGISTRO PARA DESBLOQUEAR COMPROBANTES
				INSERT INTO [dbo].[CambiosMaestrosFE]
							([tipo]
							,[codigo]
							,[estado_sync]
							,[mensaje_sync]
							,[fechaCE])
				VALUES
							('PRV'
							,@CodProv
							,0
							,NULL
							,GETDATE())
			END;		
	END;
END;
GO
/*DESBLOQUEO POR RECEPTOR*/
/*TRIGGER SACLIE_0X*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_DESBL_X_RECEPTOR_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_DESBL_X_RECEPTOR_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,  -- TABLA Correos_y_Datos_Adiconales
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Correos_y_Datos_Adiconales
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SACLIE';
SET @NombreGrp = 'Correos_y_Datos_Adiconales';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_DESBL_X_RECEPTOR_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER INSERT, UPDATE
AS 
BEGIN
DECLARE 
@CodClie AS VARCHAR(15),
@cambios int

SET NOCOUNT ON;

IF UPDATE([receptor_direccion_extranjero])
	BEGIN
		SELECT	@CodClie = CodClie
		FROM inserted;

		SELECT @cambios = COUNT(*)
		FROM [dbo].[CambiosMaestrosFE]
		WHERE ([tipo] = ''CLI'') AND ([codigo] = @CodClie) AND ([estado_sync] = 0);

		SET @cambios = ISNULL(@cambios, 0);

		IF (@cambios = 0)
			BEGIN
				-- CREANDO REGISTRO PARA DESBLOQUEAR COMPROBANTES
				INSERT INTO [dbo].[CambiosMaestrosFE]
							([tipo]
							,[codigo]
							,[estado_sync]
							,[mensaje_sync]
							,[fechaCE])
				VALUES
							(''CLI''
							,@CodClie
							,0
							,NULL
							,GETDATE());
			END;
	END;
END;');
GO
/*ASIGNA CLASE A MEDIO DE PAGOS*/
/*TRIGGER SATARJ*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_MEDIO_PAGO_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_MEDIO_PAGO_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_MEDIO_PAGO_FE]
ON [dbo].[SATARJ] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodTarj AS VARCHAR(10),
@CLASE AS VARCHAR(10),
@Descrip AS VARCHAR(40)

SET NOCOUNT ON;

SELECT	@CodTarj = CodTarj		
FROM inserted;

UPDATE SATARJ SET Clase = CASE 	WHEN Descrip LIKE '%Tarj%Credito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Crédito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Debito%' THEN 'DF'
								WHEN Descrip LIKE '%Tarj%Débito%' THEN 'DF'
								WHEN Descrip LIKE '%datafono%' THEN 'DF'
								WHEN Descrip LIKE '%Transf%' THEN 'TD'
								WHEN Descrip LIKE '%Deposito%' THEN 'TD'
								WHEN Descrip LIKE '%Depósito%' THEN 'TD'
								ELSE Clase END
WHERE CodTarj = @CodTarj;
END;
GO
/*DESBLOQUEO POR TERCEROS OTROS CARGOS*/
/*TRIGGER SAMECA*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_DESBL_X_TERC_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_DESBL_X_TERC_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_DESBL_X_TERC_FE]
ON [dbo].[SAMECA] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodMeca AS VARCHAR(10),
@ID3 AS VARCHAR(25),
@Descrip AS VARCHAR(60),
@Contador AS INT

SET NOCOUNT ON;

SELECT	@CodMeca = CodMeca,
		@ID3 = LEFT(ID3,12),
		@Descrip = Descrip			
FROM inserted;

SET @Contador = 0;

-- CONTANDO LINEAS DE COMPROBANTES CON CARGOS BLOQUEADOS CON ESTE TERCERO
SELECT @Contador = COUNT(*)
FROM encabezado AS X WITH (NOLOCK) INNER JOIN
(SELECT   A.TipoFac,
		 A.NumeroD,
		 A.Clave,
		 B.NroLinea,
		 B.CodMeca
FROM SAFACT AS A WITH (NOLOCK) INNER JOIN
(SELECT  TipoFac,
		 NumeroD,
		 NroLinea,
		 CodMeca
FROM SAITEMFAC WITH (NOLOCK)
WHERE TipoFac IN ('A','B') AND EsServ = 1 AND CodMeca IS NOT NULL) AS B
ON A.TipoFac = B.TipoFac AND A.NumeroD = B.NumeroD) AS Y
ON X.clave = Y.Clave
WHERE X.estado_envio = '7' AND Y.CodMeca = @CodMeca;

IF (@Contador > 0)
	BEGIN
		-- ACTUALIZANDO TABLA OTROS_CARGOS
		UPDATE O SET O.otc_identificacion = @ID3,
					 O.otc_nombre = @Descrip
		FROM otros_cargos AS O INNER JOIN
		(SELECT   X.Clave,
				 Y.NroLinea,
				 Y.CodMeca
		FROM encabezado AS X INNER JOIN
		(SELECT   A.TipoFac,
				 A.NumeroD,
				 A.Clave,
				 B.NroLinea,
				 B.CodMeca
		FROM SAFACT AS A INNER JOIN
		(SELECT  TipoFac,
				 NumeroD,
				 NroLinea,
				 CodMeca
		FROM SAITEMFAC
		WHERE TipoFac IN ('A','B') AND EsServ = 1 AND CodMeca IS NOT NULL) AS B
		ON A.TipoFac = B.TipoFac AND A.NumeroD = B.NumeroD) AS Y
		ON X.clave = Y.Clave
		WHERE X.estado_envio = '7' AND Y.CodMeca = @CodMeca) AS P
		ON O.clave = P.clave AND O.otc_nrolinea = P.NroLinea;

		-- ACTUALIZANDO TABLA ENCABEZADO
		UPDATE O SET O.estado_envio = '0'
		FROM encabezado AS O INNER JOIN
		(SELECT DISTINCT X.Clave,
						 Y.CodMeca
		FROM encabezado AS X INNER JOIN
		(SELECT   A.TipoFac,
				 A.NumeroD,
				 A.Clave,
				 B.NroLinea,
				 B.CodMeca
		FROM SAFACT AS A INNER JOIN
		(SELECT  TipoFac,
				 NumeroD,
				 NroLinea,
				 CodMeca
		FROM SAITEMFAC
		WHERE TipoFac IN ('A','B') AND EsServ = 1 AND CodMeca IS NOT NULL) AS B
		ON A.TipoFac = B.TipoFac AND A.NumeroD = B.NumeroD) AS Y
		ON X.clave = Y.Clave
		WHERE X.estado_envio = '7' AND Y.CodMeca = @CodMeca) AS P
		ON O.clave = P.clave;
	END;
END;
GO
/*DESBLOQUEO POR PARTIDA ARANCELARIA Y CABYS PRODUCTOS*/
/*TRIGGER SAPROD_0X*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_DESBL_X_PART_ARAN_CABYS_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_DESBL_X_PART_ARAN_CABYS_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,  -- TABLA Datos_Adicionales (Productos)
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Datos_Adicionales (Productos)
SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SAPROD';
SET @NombreGrp = 'Datos_Adicionales';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_DESBL_X_PART_ARAN_CABYS_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER INSERT, UPDATE
AS 
BEGIN
DECLARE 
@CodProd varchar(15),
@cambios int

SET NOCOUNT ON;

IF UPDATE([partida_arancelaria]) OR UPDATE([codigo_hacienda])
	BEGIN
		SELECT 	@CodProd = CodProd
		FROM inserted;

		SELECT @cambios = COUNT(*)
		FROM [dbo].[CambiosMaestrosFE]
		WHERE ([tipo] = ''PRD'') AND ([codigo] = @CodProd) AND ([estado_sync] = 0);

		SET @cambios = ISNULL(@cambios, 0);

		IF (@cambios = 0)
			BEGIN
				-- CREANDO REGISTRO PARA DESBLOQUEAR COMPROBANTES
				INSERT INTO [dbo].[CambiosMaestrosFE]
							([tipo]
							,[codigo]
							,[estado_sync]
							,[mensaje_sync]
							,[fechaCE])
				VALUES
							(''PRD''
							,@CodProd
							,0
							,NULL
							,GETDATE());
			END;
	END;
END')
GO
/*DESBLOQUEO POR CABYS SERVICIOS*/
/*TRIGGER SASERV_0X*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_DESBL_X_SERV_CABYS_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_DESBL_X_SERV_CABYS_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DECLARE
@NUMGRP AS INT,  -- TABLA Datos_Adicionales (Productos)
@NOM_TABLE_M AS VARCHAR(50),
@NOM_TABLE AS VARCHAR(50),
@NombreGrp AS VARCHAR(50)

-- UBICANDO DATOS DE TABLA Datos_Adicionales (Productos)SET @NUMGRP = 0;
SET @NOM_TABLE_M = 'SASERV';
SET @NombreGrp = 'Datos_Adicionales';

SELECT @NUMGRP = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_M AND NombreGrp = @NombreGrp;

SET @NOM_TABLE = CONCAT(@NOM_TABLE_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP AS VARCHAR(2)))),CAST(@NUMGRP AS VARCHAR(2)));

EXEC (N'CREATE TRIGGER [dbo].[TR_DESBL_X_SERV_CABYS_FE] ON [dbo].['+@NOM_TABLE+'] with encryption
AFTER INSERT, UPDATE
AS 
BEGIN
DECLARE 
@CodServ AS VARCHAR(15),
@cambios int

SET NOCOUNT ON;

IF UPDATE([codigo_hacienda])
	BEGIN
		SELECT 	@CodServ = CodServ
		FROM inserted;

		SELECT @cambios = COUNT(*)
		FROM [dbo].[CambiosMaestrosFE]
		WHERE ([tipo] = ''SRV'') AND ([codigo] = @CodServ) AND ([estado_sync] = 0);

		SET @cambios = ISNULL(@cambios, 0);

		IF (@cambios = 0)
			BEGIN
				-- CREANDO REGISTRO PARA DESBLOQUEAR COMPROBANTES
				INSERT INTO [dbo].[CambiosMaestrosFE]
							([tipo]
							,[codigo]
							,[estado_sync]
							,[mensaje_sync]
							,[fechaCE])
				VALUES
							(''SRV''
							,@CodServ
							,0
							,NULL
							,GETDATE());
			END;
	END;
END')
GO
/*EDICION DE SUCURSAL*/
/*TRIGGER SADEPO*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_SUCURSAL_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_SUCURSAL_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_SUCURSAL_FE]
ON [dbo].[SADEPO] with encryption
AFTER INSERT, UPDATE
AS
BEGIN
DECLARE
@CodUbic AS varchar(10),
@NUMGRP_1 AS INT,  -- TABLA Atributos (SADEPO)
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX),
@Sucursal_principal int,
@llave_sucursal_principal varchar(20),
@Sucursal int,
@llave_sucursal varchar(20),
@contador int

SET NOCOUNT ON;

SELECT @CodUbic = CodUbic
FROM inserted;

-- UBICANDO SUCURSAL PRINCIPAL
SELECT TOP (1) 	@Sucursal_principal = Sucursal,
				@llave_sucursal_principal = Llave_Sucursal
FROM [dbo].[SACONF];

-- LIMPIANDO TABLA SADEPO_0X
SET @NUMGRP_1 = NULL;
SET @NOM_TABLE_1_M = NULL;
SET @NombreGrp_1 = NULL;
SET @NOM_TABLE_1 = NULL;

SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SADEPO';
SET @NombreGrp_1 = 'Factura_Electronica';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @STRING = CONCAT('SELECT @Sucursal = [Sucursal],
	@llave_sucursal = [Llave_Sucursal]
FROM ', QUOTENAME(@NOM_TABLE_1), ' WITH (NOLOCK)
WHERE CodUbic = ', '''', @CodUbic, '''');

EXECUTE sp_executesql @STRING, N'
@Sucursal int OUTPUT,
@llave_sucursal varchar(20) OUTPUT',					
@Sucursal = @Sucursal OUTPUT,
@llave_sucursal = @llave_sucursal OUTPUT;

SET @Sucursal = ISNULL(@Sucursal,0);
SET @STRING = NULL;

-- Asignando Sucursal segun validacion
IF (@Sucursal IN (0, @Sucursal_principal))
	BEGIN
		SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_1),' SET [Sucursal] = ',@Sucursal_principal,',',
						' [Llave_Sucursal] = ','''',@llave_sucursal_principal,'''', 
					 	' WHERE CodUbic = ', '''', @CodUbic, '''');

		EXECUTE sp_executesql @STRING;
		SET @STRING = NULL;
	END;
ELSE
	BEGIN
		IF (ISNULL(LEN(@llave_sucursal),0) = 0)
			BEGIN
				SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_1),' SET [Sucursal] = ',@Sucursal_principal,',',
										' [Llave_Sucursal] = ','''',@llave_sucursal_principal,'''', 
									 	' WHERE CodUbic = ', '''', @CodUbic, '''');

				EXECUTE sp_executesql @STRING;
				SET @STRING = NULL;
			END;
		ELSE
			SET @STRING = CONCAT('SELECT @contador = COUNT(*)
			  FROM ', QUOTENAME(@NOM_TABLE_1), ' WITH (NOLOCK)
			  WHERE Sucursal <> ', @Sucursal, ' AND Llave_Sucursal = ', '''',@llave_sucursal,'''');

			EXECUTE sp_executesql @STRING, N'
			@contador int OUTPUT',
			@contador = @contador OUTPUT;

			SET @contador = ISNULL(@contador,0);
			SET @STRING = NULL;

			IF (@contador <> 0)
				BEGIN
					SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_1),' SET [Sucursal] = ',@Sucursal_principal,',',
										' [Llave_Sucursal] = ','''',@llave_sucursal_principal,'''', 
									 	' WHERE CodUbic = ', '''', @CodUbic, '''');

					EXECUTE sp_executesql @STRING;
					SET @STRING = NULL;
				END;
			ELSE
				BEGIN
					IF (@llave_sucursal = @llave_sucursal_principal)
						BEGIN
							SET @STRING = CONCAT('UPDATE ', QUOTENAME(@NOM_TABLE_1),' SET [Sucursal] = ',@Sucursal_principal,',',
										' [Llave_Sucursal] = ','''',@llave_sucursal_principal,'''', 
									 	' WHERE CodUbic = ', '''', @CodUbic, '''');

							EXECUTE sp_executesql @STRING;
							SET @STRING = NULL;
						END;
				END;
	END;

-- DESBLOQUEANDO COMPROBANTE BLOQUEADOS POR NO TENER ACCESO SUCURSAL
SET @STRING = CONCAT('SELECT @Sucursal = [Sucursal],
					 @llave_sucursal = [Llave_Sucursal]
			  FROM ', QUOTENAME(@NOM_TABLE_1), ' WITH (NOLOCK)
			  WHERE CodUbic = ', '''', @CodUbic, '''');

EXECUTE sp_executesql @STRING, N'
@Sucursal int OUTPUT,
@llave_sucursal varchar(20) OUTPUT',					
@Sucursal = @Sucursal OUTPUT,
@llave_sucursal = @llave_sucursal OUTPUT;

SET @STRING = NULL;

IF EXISTS (SELECT COUNT(*) FROM [dbo].[encabezado] WITH (NOLOCK) WHERE [estado_envio] = '7' AND [mensaje_hacienda] LIKE '%sucursal sin acceso%' AND CAST(LEFT([consecutivo_completo],3) AS int) = @Sucursal) 
	BEGIN
		UPDATE [dbo].[encabezado] SET 	[llave_sucursal] = @llave_sucursal, [estado_envio] = '0'
		WHERE [estado_envio] = '7' AND [mensaje_hacienda] LIKE '%sucursal sin acceso%' AND CAST(LEFT([consecutivo_completo],3) AS int) = @Sucursal;
	END;
END;
GO
/*CARGA DE LISTADO DE IMPUESTOS EN FICHA DE CLIENTES*/
/*TRIGGER SATAXES*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_TAX_RECEPTOR_FE') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_TAX_RECEPTOR_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_TAX_RECEPTOR_FE]
   ON  [dbo].[SATAXES] with encryption
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
DECLARE
@CodTaxs varchar(5),
@NUMGRP_1 AS INT,  -- TABLA Correos_y_Datos_Adiconales (SACLIE)
@NOM_TABLE_1_M AS VARCHAR(50),
@NOM_TABLE_1 AS VARCHAR(50),
@NombreGrp_1 AS VARCHAR(50),
@STRING AS NVARCHAR(MAX),
@Cambio varchar(1),
@orden int,
@Listado NVARCHAR(MAX),
@Total int,
@Linea int

SET NOCOUNT ON;

IF EXISTS(SELECT * FROM inserted)
	BEGIN
		SET @Cambio = 'U';

		SELECT 	@CodTaxs = CodTaxs		
		FROM inserted;
	END;
ELSE
	BEGIN
		SET @Cambio = 'D';
	END;

IF (@Cambio = 'U')
	BEGIN
		-- ASIGNADO ORDEN A IMPUESTOS ORIGINALES
		UPDATE SATAXES SET Orden = CASE WHEN CodTaxs = 'IVA1R' THEN 2
										WHEN CodTaxs = 'IVA2R' THEN 3
										WHEN CodTaxs = 'IVA4R' THEN 4
										WHEN CodTaxs = 'IVA4T' THEN 5
										WHEN CodTaxs = 'IVA8T' THEN 6
										ELSE Orden END
		WHERE CodTaxs = @CodTaxs AND Orden IS NULL;

		-- ASIGNANDO ORDEN A IMPUESTO SECUNDARIO
		SELECT @Orden = ISNULL(MAX(Orden),1)
		FROM SATAXES WITH (NOLOCK)
		WHERE Orden IS NOT NULL;

		UPDATE A SET Orden = @Orden + A.Linea
		FROM
		(SELECT	ROW_NUMBER() OVER(ORDER BY CodTaxs) Linea,
				Codtaxs,
				orden
		FROM SATAXES
		WHERE	(Orden IS NULL) AND	
				((Descrip LIKE '%IVA1R%' AND CodTaxs <> 'IVA1R') OR 
				(Descrip LIKE '%IVA2R%' AND CodTaxs <> 'IVA2R') OR 
				(Descrip LIKE '%IVA4R%' AND CodTaxs <> 'IVA4R') OR 
				(Descrip LIKE '%IVA4T%' AND CodTaxs <> 'IVA4T') OR 
				(Descrip LIKE '%IVA8T%' AND CodTaxs <> 'IVA8T'))) A;	
	END;

-- RECARGANDO COMBO IMPUESTO EN FICHA CLIENTE
SET @NUMGRP_1 = 0;
SET @NOM_TABLE_1_M = 'SACLIE';
SET @NombreGrp_1 = 'Correos_y_Datos_Adiconales';

SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
FROM SAAGRUPOS WITH (NOLOCK)
WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

SET @Listado = CONCAT('Seleccionar',CHAR(13), CHAR(10),'Es Exento');
SET @Total = 0;
SET @Linea = 1;

SELECT @Total = COUNT(*)
FROM SATAXES WITH (NOLOCK)
WHERE Orden >= 2;

WHILE (@Linea <= @TOTAL)
	BEGIN
		SELECT @Listado = CONCAT(@Listado, CHAR(13), CHAR(10), A.Descrip) 
		FROM
		(SELECT ROW_NUMBER() OVER(ORDER BY Orden) Linea,
				Descrip
		FROM SATAXES WITH (NOLOCK)
		WHERE Orden >= 2) A
		WHERE A.Linea = @Linea;

		SET @Linea +=1;
	END;

IF (SELECT NombCpo FROM SAACAMPOS WITH (NOLOCK) WHERE CodTbl = 'SACLIE' AND NumGrp = @NUMGRP_1 AND NombCpo = 'Impuesto') IS NULL
	BEGIN
		INSERT INTO SAACAMPOS (CodTbl,NumGrp,NombCpo,AliasCpo,TipoCpo,Longitud,Requerido,CBusqueda,Value)
		VALUES	('SACLIE', @NUMGRP_1, 'Impuesto', 'Impuesto', 56, 4, 0, 0, @LISTADO);
	END;
ELSE 
	BEGIN
		UPDATE SAACAMPOS SET 	AliasCpo = 'Impuesto',
								TipoCpo = 56,
					 			Longitud = 4,
					 			Value = @LISTADO
		WHERE	CodTbl = 'SACLIE' AND
				NumGrp = @NUMGRP_1 AND 
				NombCpo	= 'Impuesto';
	END;
END;
GO
/*REASIGNA PARTES A PRODUCTOS COMPUESTOS*/
/*TRIGGER SAFACT*/
IF (SELECT name FROM SYS.triggers WHERE name = 'TR_COMPUESTOS') IS NOT NULL
	BEGIN
		DROP TRIGGER [dbo].[TR_COMPUESTOS];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_COMPUESTOS]
   ON  [dbo].[SAFACT] with encryption
   AFTER UPDATE
AS 
BEGIN
	DECLARE
	@NumeroD varchar(20),
	@TipoFac varchar(1),
	@CodItem varchar(15),
	@NroLinea int,
	@NroLineaC int,
	@Cont_Compuestos int,
	@Cont_Partes int,
	@CodProd varchar(15),
	@CodAlte varchar(15),
	@CodUbic varchar(10),
	@CodVend varchar(10),
	@CantMayor decimal(28,4),
	@Cantidad decimal(28,4),
	@PriceO_Comp decimal(28,4),
	@TipoPVP tinyint,
	@Signo smallint,
	@FechaE datetime,
	@FechaL datetime,
	@FechaV datetime,
	@EsUnid smallint,
	@Cant_inv decimal(28,4),
	@Fecha_inv varchar(25),
	@Periodo varchar(6),
	@Procesar tinyint

	SET NOCOUNT ON;

	IF UPDATE(CostoPrd) OR UPDATE(CostoSrv)
		BEGIN
			SET @Procesar = 1;

			SELECT 	@TipoFac = [TipoFac],
					@NumeroD = [NumeroD]
			FROM inserted;
		END;
	ELSE
		BEGIN
			SET @Procesar = 0;
		END;

	IF (@Procesar = 1)
		BEGIN
			-- VALIDANDO PARTES DE ITEMS COMPUESTOS
			SELECT @Cont_Compuestos = COUNT(*)
			FROM SAITEMFAC A WITH (NOLOCK) INNER JOIN SAPROD B WITH (NOLOCK)
			ON A.CodItem = B.CodProd
			WHERE A.TipoFac = @TipoFac AND A.NumeroD = @NumeroD AND B.DEsComp = 1 AND A.NroLineaC = 0;

			SET @Cont_Compuestos = ISNULL(@Cont_Compuestos,0);

			WHILE (@Cont_Compuestos > 0)
				BEGIN
					SET @Cont_Partes = 0;
					SET @CodProd = NULL;
					SET @CodUbic = NULL;
					SET @NroLinea = NULL;
					SET @CodVend = NULL;
					SET @Signo = NULL;
					SET @CantMayor = NULL;
					SET @FechaE = NULL;
					SET @FechaL = NULL;
					SET @FechaV = NULL;
					SET @NroLineaC = NULL;
					SET @PriceO_Comp = NULL;
					SET @Periodo = NULL;

					SELECT  @CodProd = C.CodItem,
							@CodUbic = C.CodUbic,
							@NroLinea = C.NroLinea,
							@CodVend = C.CodVend,
							@Signo = C.Signo,
							@CantMayor = C.Cantidad,
							@FechaE = C.FechaE,
							@FechaL = C.FechaL,
							@FechaV = C.FechaV,
							@PriceO_Comp = C.PriceO
					FROM
					(SELECT ROW_NUMBER() OVER(ORDER BY NroLinea DESC) Linea,
							CodItem,
							CodUbic,
							NroLinea,
							CodVend,
							Signo,
							Cantidad,
							FechaE,
							FechaL,
							FechaV,
							PriceO
					FROM SAITEMFAC A WITH (NOLOCK) INNER JOIN SAPROD B WITH (NOLOCK)
					ON A.CodItem = B.CodProd
					WHERE A.TipoFac = @TipoFac AND A.NumeroD = @NumeroD AND B.DEsComp = 1 AND A.NroLineaC = 0) C
					WHERE C.Linea = @Cont_Compuestos;

					SELECT @Cont_Partes = COUNT(*)
					FROM SAPART WITH (NOLOCK)
					WHERE CodProd = @CodProd;

					SET @Cont_Partes = ISNULL(@Cont_Partes,0);

					SELECT @NroLineaC = COUNT(*)
					FROM SAITEMFAC WITH (NOLOCK)
					WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD AND NroLinea = @NroLinea AND NroLineaC > 0;

					SET @NroLineaC = ISNULL(@NroLineaC,0);

					IF (@NroLineaC = 0)
						BEGIN
							SET @NroLineaC +=1; 

							SELECT @TipoPVP = CASE WHEN Precio1 = @PriceO_Comp THEN 1 WHEN Precio2 = @PriceO_Comp THEN 2 ELSE 3 END
							FROM SAPROD WITH (NOLOCK)
							WHERE CodProd = @CodProd; 

							SET @Periodo = CONCAT(CAST(YEAR(@FechaE) AS VARCHAR(4)), IIF(LEN(CAST(MONTH(@FechaE) AS VARCHAR(2))) = 1, CONCAT(REPLICATE('0',1), CAST(MONTH(@FechaE) AS VARCHAR(2))), CAST(MONTH(@FechaE) AS VARCHAR(2))));

							WHILE (@Cont_Partes > 0)
								BEGIN
									SET @CodAlte = NULL;
									SET @Cantidad = NULL;
									SET @EsUnid = NULL;

									SELECT 	@CodAlte = CodAlte,
											@Cantidad = Cantidad,
											@EsUnid = EsUnid
									FROM
									(SELECT ROW_NUMBER() OVER(ORDER BY A.NroUnico DESC) Linea,
											A.*
									FROM SAPART A WITH (NOLOCK) LEFT OUTER JOIN SAPROD B WITH (NOLOCK)
									ON A.CodAlte = B.CodProd
									WHERE A.CodProd = @CodProd) P
									WHERE P.Linea = @Cont_Partes;

									IF NOT EXISTS (SELECT CodItem FROM SAITEMFAC WITH (NOLOCK) WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD AND CodItem = @CodAlte AND NroLinea = @NroLinea AND NroLineaC > 0)
										BEGIN			
											IF EXISTS (SELECT CodProd FROM SAPROD WITH (NOLOCK) WHERE CodProd = @CodAlte)
												BEGIN					
													-- VALIDANDO QUE ESTE ASIGNADO LA PARTE A LA BODEGA
													IF NOT EXISTS (SELECT CodProd FROM SAEXIS WITH (NOLOCK) WHERE CodProd = @CodAlte AND CodUbic = @CodUbic)
														BEGIN
															INSERT INTO [dbo].[SAEXIS] ([CodProd],[CodUbic])
															VALUES (@CodAlte,@CodUbic);
														END;
											
													-- ACTUALIZANDO ESTADISTICA DE VENTAS DEL PRODUCTO
													IF (@TipoFac IN ('A','B'))
														BEGIN
															IF NOT EXISTS(SELECT TOP 1 CodProd FROM SAEPRD WITH (NOLOCK) WHERE (CodProd=@CodAlte) And (Periodo=@Periodo))
																BEGIN
																	INSERT INTO SAEPRD ([CodProd],[Periodo])
																	VALUES (@CodAlte,@Periodo);
																END;

															UPDATE A WITH (ROWLOCK) SET A.[MtoVentas]=A.[MtoVentas]+ROUND((CASE WHEN @TipoPVP = 1 THEN ISNULL(B.[Precio1],0) WHEN @TipoPVP = 2 THEN ISNULL(B.[Precio2],0) ELSE ISNULL(B.[Precio3],0) END)*@Cantidad*@CantMayor,2), 
																						A.[CntVentas]=A.[CntVentas]+(@cantidad*@CantMayor), 
																						A.[Costo]=[Costo]+(ISNULL(B.[CostPro],0)*@Cantidad*@CantMayor)
															FROM SAEPRD A INNER JOIN SAPROD B
															ON A.CodProd = B.CodProd
															WHERE (A.CodProd=@CodAlte) And (A.Periodo=@Periodo);
														END;

													-- INSERTANDO PARTE EN SAITEMFACT
													INSERT INTO [dbo].[SAITEMFAC]
																([CodSucu]
																,[TipoFac]
																,[NumeroD]
																,[NroLinea]
																,[NroLineaC]
																,[CodItem]
																,[CodUbic]
																,[CodVend]
																,[Descrip1]
																,[Descrip2]
																,[Descrip3]
																,[Refere]
																,[Signo]
																,[CantMayor]
																,[Cantidad]
																,[ExistAntU]
																,[ExistAnt]
																,[TotalItem]
																,[Costo]
																,[Precio]
																,[PriceO]
																,[FechaE]
																,[FechaL]
																,[FechaV]
																,[EsServ]
																,[EsUnid]
																,[EsPesa]
																,[EsExento]
																,[UsaServ]
																,[DEsLote]
																,[DEsSeri]
																,[DEsComp])

													SELECT 		'00000'
																,@TipoFac
																,@NumeroD
																,@NroLinea
																,@NroLineaC
																,@CodAlte -- [CodItem]
																,@CodUbic
																,@CodVend
																,A.[Descrip]
																,A.[Descrip2]
																,A.[Descrip3]
																,A.[Refere]
																,@Signo
																,@CantMayor
																,@Cantidad
																,ISNULL(B.[ExUnidad],0)
																,ISNULL(B.[Existen],0)
																,(CASE WHEN @TipoPVP = 1 THEN ISNULL(A.[Precio1],0) WHEN @TipoPVP = 2 THEN ISNULL(A.[Precio2],0) ELSE ISNULL(A.[Precio3],0) END)*@Cantidad -- [TotalItem]
																,A.[CostPro]
																,(CASE WHEN @TipoPVP = 1 THEN ISNULL(A.[Precio1],0) WHEN @TipoPVP = 2 THEN ISNULL(A.[Precio2],0) ELSE ISNULL(A.[Precio3],0) END) -- [Precio]
																,(CASE WHEN @TipoPVP = 1 THEN ISNULL(A.[Precio1],0) WHEN @TipoPVP = 2 THEN ISNULL(A.[Precio2],0) ELSE ISNULL(A.[Precio3],0) END) -- [PriceO]
																,@FechaE
																,@FechaL
																,@FechaV
																,0 -- [EsServ]
																,@EsUnid
																,ISNULL(A.[EsPesa],0)
																,ISNULL(A.[EsExento],0)
																,0 --[UsaServ]
																,ISNULL(A.[DEsLote],0)
																,ISNULL(A.[DEsSeri],0)
																,ISNULL(A.[DEsComp],0)
													FROM SAPROD A WITH (NOLOCK) LEFT OUTER JOIN SAEXIS B WITH (NOLOCK)
													ON A.CodProd = B.CodProd
													WHERE A.CodProd = @CodAlte AND B.CodUbic = @CodUbic;

													-- ACTUALIZANDO EXISTENCIA EN SAINT DE PRODUCTO		
													IF (@TipoFac IN ('A','B','C','D'))
														BEGIN							
															SET @Cant_inv = CASE WHEN @TipoFac IN ('A','C') THEN (@Cantidad*@CantMayor)*-1 ELSE (@Cantidad*@CantMayor) END;
															SET @Fecha_inv = CAST(CAST(@FechaE AS date) AS VARCHAR(25));

															EXEC TR_ADM_UPDATE_EXISTENCIAS @CodAlte,@CodUbic,@Cant_inv,@EsUnid,@Fecha_inv;
														END;

													-- ACTUALIZANDO COMPROMETIDOS
													IF (@TipoFac = 'E')
														BEGIN
															UPDATE [dbo].[SAEXIS] WITH (ROWLOCK) SET 
																	[CantCom]= CASE WHEN @EsUnid = 0 THEN [CantCom]+(@Cantidad*@CantMayor) ELSE [CantCom] END,
																	[UnidCom]= CASE WHEN @EsUnid = 1 THEN [UnidCom]+(@Cantidad*@CantMayor) ELSE [UnidCom] END
															WHERE ([CodProd]=@CodAlte) AND ([CodUbic]=@CodUbic);

															UPDATE [dbo].[SAPROD] WITH (ROWLOCK) SET 
																	[Compro]= [Compro]+ IIF(@EsUnid = 0, (@Cantidad*@CantMayor), IIF(CantEmpaq = 0, 0, (@Cantidad*@CantMayor)/[CantEmpaq]))
															WHERE ([CodProd]=@CodAlte);
														END;
												END;
										END;

									SET @NroLineaC +=1;
									SET @Cont_Partes -=1;
								END;
						END;

					SET @Cont_Compuestos -=1;
				END;
		END;
END;
GO
/***STORED PROCEDURES***/
/*SOLAPA CONSECUTIVO DE HACIENDA EN NUMEROD DE SAINT*/
/*SP_CONSECUTIVOFE*/
IF (SELECT name FROM SYS.PROCEDURES WHERE name = 'SP_CONSECUTIVOFE') IS NOT NULL
	BEGIN
		DROP PROCEDURE [dbo].[SP_CONSECUTIVOFE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE SP_CONSECUTIVOFE
with encryption
AS
BEGIN
	DECLARE 
	@Conta_Reg AS INT,
	@Conta_tablas AS INT,
	@NroUnicoFAC AS INT,
	@NumeroD AS VARCHAR(20),
	@TipoFac AS VARCHAR(2),
	@CONSECUTIVODE AS VARCHAR(20),
	@NroUnico AS INT,
	@CodTbl AS VARCHAR(20),
	@NumGrp AS INT,
	@STRING AS NVARCHAR(1000),
	@TABLE AS SYSNAME

	SET NOCOUNT ON;

	-- SECCION FAC Y DEV MODULO DE VENTAS
	SET @Conta_Reg = 0;

	SELECT @Conta_Reg = COUNT(*)
	FROM
	(SELECT TOP (50) NroUnico
	FROM SAFACT WITH (NOLOCK)
	WHERE 	(TipoFac IN ('A','B') AND FechaT < DATEADD(SS, -180, GETDATE()) AND ISNULL(LEN(Num_Original),0) = 0 AND ISNULL(LEN(ConsecutivoDE),0) > 0)) AS A;

	WHILE (@Conta_Reg > 0)
		BEGIN
			SET @Tipofac = NULL;
			SET @NumeroD = NULL;
			SET @CONSECUTIVODE = NULL;
			SET @NroUnicoFAC = NULL;
			SET @NroUnico = NULL;

			SELECT TOP (1) 	@Tipofac = Tipofac,
							@NumeroD = NumeroD,
							@CONSECUTIVODE = CONSECUTIVODE,
							@NroUnicoFAC = NroUnico
			FROM SAFACT WITH (NOLOCK)
			WHERE 	(TipoFac IN ('A','B') AND FechaT < DATEADD(SS, -180, GETDATE()) AND ISNULL(LEN(Num_Original),0) = 0 AND ISNULL(LEN(ConsecutivoDE),0) > 0)
			ORDER BY FechaT;

			-- ACTUALIZANDO CONSECUTIVODE EN NumeroD
			UPDATE SAFACT SET NumeroD = @CONSECUTIVODE 
			WHERE NroUnico = @NroUnicoFAC;

			UPDATE SAITEMFAC SET NumeroD = @CONSECUTIVODE 
			WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD;

			UPDATE SAIPAVTA SET NumeroD = @CONSECUTIVODE 
			WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD;

			UPDATE SATAXVTA SET NumeroD = @CONSECUTIVODE 
			WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD;	
			
			UPDATE SATAXITF SET NumeroD = @CONSECUTIVODE 
			WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD;	

			UPDATE SASEPRFAC SET NumeroD = @CONSECUTIVODE 
			WHERE TipoFac = @TipoFac AND NumeroD = @NumeroD;

			UPDATE SAACXC SET NumeroD = @CONSECUTIVODE 
			WHERE TipoCxc = '10' AND NumeroD = @NumeroD AND FromTran = 0;		

			SELECT @NroUnico = NroUnico
			FROM SAACXC WITH (NOLOCK)
			WHERE TipoCxc = '41' AND NumeroD = @NumeroD	AND FromTran = 0;

			UPDATE SAACXC SET 	NumeroN = @CONSECUTIVODE,
								Document = CONCAT('PAGO FACTURA ',@CONSECUTIVODE)
			WHERE NroUnico = @NroUnico;

			UPDATE SAPAGCXC SET NumeroD = @CONSECUTIVODE,
								Descrip = CONCAT('PAGO FACTURA ',@CONSECUTIVODE)
			WHERE NroPpal = @NroUnico;

			SET @Conta_tablas = 0;

			SELECT @Conta_tablas = COUNT(*)
			FROM SAAGRUPOS WITH (NOLOCK)
			WHERE CodTbl = 'SAFACT';

			WHILE (@Conta_tablas > 0)
				BEGIN
					SET @CodTbl = NULL;
					SET @NumGrp = NULL;

					SELECT 	@CodTbl = A.CodTbl,
							@NumGrp = A.NumGrp
					FROM
					(SELECT ROW_NUMBER () OVER (ORDER BY NumGrp) AS Linea,
							CodTbl,
							NumGrp
					FROM SAAGRUPOS WITH (NOLOCK)
					WHERE CodTbl = 'SAFACT') AS A
					WHERE A.Linea = @Conta_tablas;

					SET @TABLE = CONCAT(@CodTbl, '_',REPLICATE('0',2 - LEN(CAST(@NumGrp AS VARCHAR(2)))),CAST(@NumGrp AS VARCHAR(2)));

					SET @STRING = CONCAT('UPDATE ',QUOTENAME(@TABLE),' SET NumeroD = ', '''',@CONSECUTIVODE, '''',
										' WHERE TipoFac = ','''' ,@TipoFac, '''',' AND NumeroD = ', '''', @NumeroD,'''');

					EXECUTE sp_executesql @STRING;
					SET @STRING = NULL;
					SET @Conta_tablas -=1;
				END;

			SET @Conta_tablas = 0;

			SELECT @Conta_tablas = COUNT(*)
			FROM SAAGRUPOS WITH (NOLOCK)
			WHERE CodTbl = 'SAITEPRDFAC';

			WHILE (@Conta_tablas > 0)
				BEGIN
					SET @CodTbl = NULL;
					SET @NumGrp = NULL;

					SELECT 	@CodTbl = A.CodTbl,
							@NumGrp = A.NumGrp
					FROM
					(SELECT ROW_NUMBER () OVER (ORDER BY NumGrp) AS Linea,
							CodTbl,
							NumGrp
					FROM SAAGRUPOS WITH (NOLOCK)
					WHERE CodTbl = 'SAITEPRDFAC') AS A
					WHERE A.Linea = @Conta_tablas;

					SET @TABLE = CONCAT(@CodTbl, '_',REPLICATE('0',2 - LEN(CAST(@NumGrp AS VARCHAR(2)))),CAST(@NumGrp AS VARCHAR(2)));

					SET @STRING = CONCAT('UPDATE ',QUOTENAME(@TABLE),' SET NumeroD = ', '''', @CONSECUTIVODE, '''',
										' WHERE TipoFac = ','''' ,@TipoFac, '''',' AND NumeroD = ', '''', @NumeroD,'''');

					EXECUTE sp_executesql @STRING;
					SET @STRING = NULL;
					SET @Conta_tablas -=1;
				END;

			SET @Conta_tablas = 0;

			SELECT @Conta_tablas = COUNT(*)
			FROM SAAGRUPOS WITH (NOLOCK)
			WHERE CodTbl = 'SAITESERVFAC';

			WHILE (@Conta_tablas > 0)
				BEGIN
					SET @CodTbl = NULL;
					SET @NumGrp = NULL;

					SELECT 	@CodTbl = A.CodTbl,
							@NumGrp = A.NumGrp
					FROM
					(SELECT ROW_NUMBER () OVER (ORDER BY NumGrp) AS Linea,
							CodTbl,
							NumGrp
					FROM SAAGRUPOS WITH (NOLOCK)
					WHERE CodTbl = 'SAITESERVFAC') AS A
					WHERE A.Linea = @Conta_tablas;

					SET @TABLE = CONCAT(@CodTbl, '_',REPLICATE('0',2 - LEN(CAST(@NumGrp AS VARCHAR(2)))),CAST(@NumGrp AS VARCHAR(2)));

					SET @STRING = CONCAT('UPDATE ',QUOTENAME(@TABLE),' SET NumeroD = ', '''', @CONSECUTIVODE, '''',
										' WHERE TipoFac = ','''' ,@TipoFac, '''',' AND NumeroD = ', '''', @NumeroD,'''');

					EXECUTE sp_executesql @STRING;
					SET @STRING = NULL;
					SET @Conta_tablas -=1;
				END;

			UPDATE SAFACT SET Num_Original = @NumeroD 
			WHERE NroUnico = @NroUnicoFAC;

			IF OBJECT_ID('MPAuditTransacciones') IS NOT NULL
				BEGIN
					UPDATE A SET A.NumeroC = @CONSECUTIVODE
					FROM  MPAuditTransacciones A INNER JOIN SAFACT B
					ON A.TipoFac = B.TipoFac AND A.NumeroC = B.Num_Original AND A.CodClie = B.CodClie 
					WHERE B.NroUnico = @NroUnicoFAC;
				END;

		SET @Conta_Reg -=1; 
	END;

	-- SECCION ND, NC MODULO CXC FROMTRAN 1
	SET @Conta_Reg = 0;

	SELECT @Conta_Reg = COUNT(*)
	FROM
	(SELECT TOP (50) NroUnico
	FROM SAACXC WITH (NOLOCK)
	WHERE 	(TipoCxc IN ('31','20') AND FromTran = 1 AND FechaT < DATEADD(SS, -180, GETDATE()) AND ISNULL(LEN(Num_Original),0) = 0 AND ISNULL(LEN(ConsecutivoDE),0) > 0)) AS A;

	WHILE (@Conta_Reg > 0)
		BEGIN
			SET @Tipofac = NULL;
			SET @NumeroD = NULL;
			SET @CONSECUTIVODE = NULL;
			SET @NroUnicoFAC = NULL;
			SET @NroUnico = NULL;

			SELECT TOP (1) 	@Tipofac = TipoCxc,
							@NumeroD = NumeroD,
							@CONSECUTIVODE = CONSECUTIVODE,
							@NroUnico = NroUnico
			FROM SAACXC WITH (NOLOCK)
			WHERE (TipoCxc IN ('31','20') AND FromTran = 1 AND FechaT < DATEADD(SS, -180, GETDATE()) AND ISNULL(LEN(Num_Original),0) = 0 AND ISNULL(LEN(ConsecutivoDE),0) > 0)
			ORDER BY FechaT;

			IF (@TipoFac = '31')
				BEGIN
					UPDATE SAACXC SET NumeroD = @CONSECUTIVODE,
									  Num_Original = @NumeroD
					WHERE NroUnico = @NroUnico;

					UPDATE A SET 	A.NumeroD = B.NumeroD, 
									A.Descrip = (CASE WHEN B.TipoCxc = '10' THEN CONCAT('NOTA CREDITO A FACT ', B.NumeroD) WHEN B.TipoCxc = '20' THEN CONCAT('NOTA CREDITO A ND ', B.NumeroD) ELSE A.Descrip END)
					FROM SAPAGCXC AS A INNER JOIN SAACXC AS B
					ON A.NroRegi = B.NroUnico
					WHERE A.NroPpal = @NroUnico;
				END;

			IF (@TipoFac = '20')
				BEGIN
					UPDATE SAACXC SET NumeroD = @CONSECUTIVODE,
									  Num_Original = @NumeroD
					WHERE NroUnico = @NroUnico;
				END;

			SET @Conta_Reg -=1; 
		END;

	-- SECCION PAG CXC 
	UPDATE PAG SET 	PAG.NumeroD = FAC.NumeroD,
					PAG.Descrip = (CASE WHEN FAC.TipoCxc = '10' THEN CONCAT('PAGO A FACTURA ', FAC.NumeroD) WHEN FAC.TipoCxc = '20' THEN CONCAT('PAGO A N/DEBITO ', FAC.NumeroD) ELSE PAG.Descrip END)
	FROM
	(SELECT A.NroPpal, A.NroRegi, A.NumeroD, A.Descrip
	FROM SAPAGCXC AS A INNER JOIN SAACXC AS B
	ON A.NroPpal = B.NroUnico
	WHERE B.TipoCxc = '41' AND B.FromTran = 1 AND LEN(A.NumeroD) <> 20) AS PAG INNER JOIN SAACXC AS FAC
	ON PAG.NroRegi = FAC.NroUnico
	WHERE LEN(FAC.NumeroD) = 20;
END;
GO
/*REENVIO DE COMPROBANTES RECHAZADOS*/
/*SP_REENVIO_FE*/
IF (SELECT name FROM SYS.PROCEDURES WHERE name = 'SP_REENVIO_FE') IS NOT NULL
	BEGIN
		DROP PROCEDURE [dbo].[SP_REENVIO_FE];
	END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE SP_REENVIO_FE
@consecutivo as varchar(20),
@resultado as nvarchar(max) OUTPUT
with encryption
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION REENVIO_FE
	SET DATEFORMAT YMD;

	DECLARE
		@Numerrors int,
		@clave varchar(50),
		@id int,
		@id_nuevo int,
		@NroUnico int,
		@tipo_documento varchar(2),
		@consecutivoFE varchar(20),
		@Estacion int,
		@Sucursal int,
		@emisor_identificacion varchar(12),
		@LenCorrelFE int,
		@PrxFactFE int,
		@PrxNDFE int,
		@PrxNCFE int,
		@PrxTickEFE int,
		@PrxCompCFE int,
		@PrxFExpFE int,
		@PrxComFE int,
		@consecutivo_incompleto	varchar(8),
		@consecutivo_origen varchar(10),
		@consecutivo_completo varchar(20),
		@dia varchar(2),
		@mes varchar(2),
		@ano varchar(2),
		@idclave varchar(12),
		@validador tinyint,
		@Cod_receptor varchar(15),
		@descrip varchar(60),
		@receptor_nombre varchar(100),
		@receptor_tipo varchar(2),
		@receptor_identificacion varchar(12),
		@pasaporte varchar(20),
		@Pais int,
		@estador int,
		@ciudadr int,
		@municipior int,
		@receptor_direccion varchar(160),
		@receptor_telefono varchar(20),
		@receptor_fax varchar(20),
		@receptor_email varchar(100),
		@receptor_provincia varchar(1),
		@receptor_canton varchar(2),
		@receptor_distrito varchar(2),
		@receptor_direccion_extranjero varchar(300),
		@NUMGRP_1 int,  -- TABLA Atributos (SASERV)
		@NOM_TABLE_1_M varchar(50),
		@NOM_TABLE_1 varchar(50),
		@NombreGrp_1 varchar(50),
		@NUMGRP_2 int,
		@NOM_TABLE_2_M varchar(50),
		@NOM_TABLE_2 varchar(50),
		@NombreGrp_2 varchar(50),
		@STRING nvarchar(max),
		@receptor_incluir int,
		@ubicacion_incluir int,
		@Incluir_Receptor_G smallint,
		@Clase varchar(10),
		@REF_CLAVE varchar(50),
		@Usa_Descrip_Ampli smallint

		SET NOCOUNT ON;

		SET @Numerrors = 0;
		SET @Numerrors=@Numerrors+@@ERROR;
		SET @consecutivoFE = @consecutivo;
		SET @Numerrors=@Numerrors+@@ERROR;

		IF (LEN(@consecutivoFE) = 20)
			BEGIN
				-- UBICANDO DATOS DE COMPROBANTE RECHAZADO
				SELECT	@id = id,
						@clave = clave,
						@tipo_documento = tipo_documento,
						@consecutivo_incompleto = consecutivo_incompleto
				FROM [dbo].[encabezado] WITH (NOLOCK)
				WHERE [consecutivo_completo] = @consecutivoFE AND ([mensaje_hacienda] LIKE 'rechazado%') AND [estado_envio] = '4';

				SET @Numerrors=@Numerrors+@@ERROR;
				SET @id = ISNULL(@id,0);
				SET @Numerrors=@Numerrors+@@ERROR;				

				IF EXISTS (SELECT [id] FROM [dbo].[encabezado] WITH (NOLOCK) WHERE [id] = @id AND ([mensaje_hacienda] LIKE 'rechazado%') AND [estado_envio] = '4')
					BEGIN
						SET @Estacion = CAST(SUBSTRING(@consecutivoFE,4,5) AS int);
						SET @Numerrors=@Numerrors+@@ERROR;
						SET @Sucursal = CAST(LEFT(@consecutivoFE,3) AS int);
						SET @Numerrors=@Numerrors+@@ERROR;

						-- UBICANDO DATOS DE EMISOR
						SELECT	@emisor_identificacion = REPLACE(REPLACE([RIF], '-',''), ' ',''),
								@Incluir_Receptor_G = Incluir_Receptor
						FROM [dbo].[SACONF] WITH (NOLOCK)
						WHERE CodSucu = '00000';

						SET @Numerrors=@Numerrors+@@ERROR;
						SET @Incluir_Receptor_G = ISNULL(@Incluir_Receptor_G,0);
						SET @Numerrors=@Numerrors+@@ERROR;
						SET @validador = 0;
						SET @Numerrors=@Numerrors+@@ERROR;

						-- VALIDANDO CLAVE EN SAFACT Y SAACXC
						IF EXISTS (SELECT [clave] FROM [dbo].[SAFACT] WITH (NOLOCK) WHERE [clave] = @clave)
							BEGIN
								-- UBICANDO DATOS DE SAFACT
								SELECT 	@NroUnico = [NroUnico], 
										@Cod_receptor = [CodClie],
										@descrip = [Descrip]
								FROM [dbo].[SAFACT] WITH (NOLOCK)
								WHERE [clave] = @clave;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @validador = 1;
								SET @Numerrors=@Numerrors+@@ERROR;
							END;
						ELSE
							BEGIN
								IF EXISTS (SELECT [clave] FROM [dbo].[SAACXC] WITH (NOLOCK) WHERE [clave] = @clave)
									BEGIN
										-- UBICANDO DATOS DE SAFACT
										SELECT 	@NroUnico = [NroUnico],
												@Cod_receptor = [CodClie]
										FROM [dbo].[SAACXC] WITH (NOLOCK)
										WHERE [clave] = @clave;

										SET @Numerrors=@Numerrors+@@ERROR;
										SET @validador = 2;
										SET @Numerrors=@Numerrors+@@ERROR;
									END;
								ELSE
									BEGIN
										IF EXISTS (SELECT [clave] FROM [dbo].[SACOMP] WITH (NOLOCK) WHERE [clave] = @clave)
											BEGIN
												-- UBICANDO DATOS DE SAFACT
												SELECT 	@NroUnico = [NroUnico],
														@Cod_receptor = [CodProv]
												FROM [dbo].[SACOMP] WITH (NOLOCK)
												WHERE [clave] = @clave;

												SET @Numerrors=@Numerrors+@@ERROR;
												SET @validador = 3;
												SET @Numerrors=@Numerrors+@@ERROR;
											END;
										ELSE
											BEGIN
												SET @validador = 0;
												SET @Numerrors=@Numerrors+@@ERROR;
											END;
									END;
							END;

						IF (@validador > 0)
							BEGIN
								-- ASIGNADO CONSECUTIVO EN NUMERODE (FACTURACION ELECTRONICA 10 DIGITOS SEGUN TIPO DE DOCUMENTO)
								SELECT 	@LenCorrelFE = LenCorrelFE, 
										@PrxFactFE = PrxFactFE,
										@PrxNDFE = PrxNDFE,
										@PrxNCFE = PrxNCFE,
										@PrxTickEFE = PrxTickEFE,
										@PrxCompCFE = PrxCompCFE,
										@PrxFExpFE = PrxFExpFE,
										@PrxComFE = PrxComFE
								FROM ConsecutivosFE WITH (NOLOCK)
								WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

								SET @Numerrors=@Numerrors+@@ERROR;

								-- FACTURA VENTA
								IF (@tipo_documento = '01')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxFactFE AS VARCHAR(10)))) + CAST(@PrxFactFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxFactFE = PrxFactFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- NOTA DE DEBITO
								IF (@tipo_documento = '02')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxNDFE AS VARCHAR(10)))) + CAST(@PrxNDFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxNDFE = PrxNDFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- NOTA DE CREDITO
								IF (@tipo_documento = '03')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxNCFE AS VARCHAR(10)))) + CAST(@PrxNCFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxNCFE = PrxNCFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- TIQUETE ELECTRONICO
								IF (@tipo_documento = '04')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxTickEFE AS VARCHAR(10)))) + CAST(@PrxTickEFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxTickEFE = PrxTickEFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- FACTURA DE COMPRA REGIMEN SIMPLIFICADO
								IF (@tipo_documento = '08')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxComFE AS VARCHAR(10)))) + CAST(@PrxComFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxComFE = PrxComFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;						
									END;
								
								-- FACTURA DE EXPORTACION VENTA
								IF (@tipo_documento = '09')
									BEGIN
										SET @consecutivo_origen	= CAST(REPLICATE('0', @LenCorrelFE - DATALENGTH(CAST(@PrxFExpFE AS VARCHAR(10)))) + CAST(@PrxFExpFE AS VARCHAR(10)) AS VARCHAR(10));
										UPDATE ConsecutivosFE SET PrxFExpFE = PrxFExpFE + 1
										WHERE Sucursal = @Sucursal AND Estacion = @Estacion;

										SET @Numerrors=@Numerrors+@@ERROR;						
									END;

								-- CREANDO CONSECUTIVO COMPLETO
								SET @consecutivo_completo = CONCAT(@consecutivo_incompleto, @tipo_documento, @consecutivo_origen);
								SET @Numerrors=@Numerrors+@@ERROR;
								-- CREANDO CLAVE
								SET @dia = IIF(LEN(CAST(DAY(GETDATE()) AS VARCHAR(2))) = 1, CONCAT('0', CAST(DAY(GETDATE()) AS VARCHAR(2))), CAST(DAY(GETDATE()) AS VARCHAR(2)));
								SET @Numerrors=@Numerrors+@@ERROR;
								SET @mes = IIF(LEN(CAST(MONTH(GETDATE()) AS VARCHAR(2))) = 1, CONCAT('0', CAST(MONTH(GETDATE()) AS VARCHAR(2))), CAST(MONTH(GETDATE()) AS VARCHAR(2)));
								SET @Numerrors=@Numerrors+@@ERROR;
								SET @ano = RIGHT(CAST(YEAR(GETDATE()) AS VARCHAR(4)), 2);
								SET @Numerrors=@Numerrors+@@ERROR;
								SET @idclave = RIGHT(CONCAT('00000000000', @emisor_identificacion), 12);
								SET @Numerrors=@Numerrors+@@ERROR;
								SET @clave = CONCAT('506', @dia, @mes, @ano, @idclave, @consecutivo_completo, '1', '44332211');
								SET @Numerrors=@Numerrors+@@ERROR;

								-- UBICANDO DATOS DE RECEPTOR
								IF (@validador IN (1,2))
									BEGIN
										SET @Usa_Descrip_Ampli = 0;
										SET @Numerrors=@Numerrors+@@ERROR;

										-- UBICANDO DIRECCION DE EXTRANJERO
										SET @NUMGRP_1 = 0;
										SET @NOM_TABLE_1_M = 'SACLIE';
										SET @NombreGrp_1 = 'Correos_y_Datos_Adiconales';

										SET @Numerrors=@Numerrors+@@ERROR;

										SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
										FROM SAAGRUPOS WITH (NOLOCK)
										WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

										SET @Numerrors=@Numerrors+@@ERROR;
										SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));
										SET @Numerrors=@Numerrors+@@ERROR;							

										SET @STRING = CONCAT(
										'SELECT @receptor_direccion_extranjero = receptor_direccion_extranjero, 
												@Receptor_Incluir = Receptor_Incluir,
												@Usa_Descrip_Ampli = Usa_Descrip_Ampli
										FROM ', QUOTENAME(@NOM_TABLE_1),' WITH (NOLOCK)
										WHERE CodClie = ', '''', @Cod_receptor,'''');

										SET @Numerrors=@Numerrors+@@ERROR;

										EXECUTE sp_executesql @STRING, N'										
										@receptor_direccion_extranjero AS VARCHAR(300) OUTPUT,
										@Receptor_Incluir AS int OUTPUT,
										@Usa_Descrip_Ampli smallint OUTPUT',
										@receptor_direccion_extranjero = @receptor_direccion_extranjero OUTPUT,
										@Receptor_Incluir = @Receptor_Incluir OUTPUT,
										@Usa_Descrip_Ampli = @Usa_Descrip_Ampli OUTPUT;	

										SET @Numerrors=@Numerrors+@@ERROR;
										SET @Usa_Descrip_Ampli = ISNULL(@Usa_Descrip_Ampli,0);
										SET @Numerrors=@Numerrors+@@ERROR;

										-- UBICANDO DATOS DEL RECEPTOR
										SELECT	@receptor_nombre = IIF(LEFT(Descrip,1) = '?', @DESCRIP, CASE WHEN @Usa_Descrip_Ampli = 1 THEN IIF(LTRIM(RTRIM(DescripExt))+'' = '', Descrip, LEFT(DescripExt,100)) ELSE Descrip END),
												@receptor_tipo = CASE WHEN Clase = 'N' THEN IIF(TipoID3 = 1, '01', '02') WHEN Clase = 'ED' THEN '03' WHEN Clase = 'EN' THEN '04' WHEN Clase IN ('P','E') THEN '' ELSE IIF(TipoID3 = 1, '01', '02') END,
												@receptor_identificacion = IIF(Clase IN ('P','E'), '', DBO.RemoveChars(ID3)),
												@pasaporte = IIF(Clase IN ('P','E'), DBO.RemoveChars(ID3), ''),
												@estador = [Estado],
												@ciudadr = [Ciudad],
												@municipior = [Municipio],
												@receptor_direccion = LTRIM(RTRIM(CONCAT([Direc1], ' ', [Direc2]))),
												@receptor_telefono = LEFT(DBO.RemoveChars([Telef]),8),
												@receptor_fax = LEFT(DBO.RemoveChars([Fax]),20),
												@receptor_email = REPLACE(REPLACE([Email], CHAR(0x1f), ''), CHAR(32), ''),
												@Clase = [Clase]
										FROM [dbo].[SACLIE] WITH (NOLOCK)
										WHERE [CodClie] = @Cod_receptor;

										SET @Numerrors=@Numerrors+@@ERROR;

										IF (@tipo_documento NOT IN ('02','03'))
											BEGIN
												IF (@CLASE IN ('P','T'))
													BEGIN								
														SET @receptor_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
														SET @ubicacion_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
													END;
												ELSE
													BEGIN
														IF (@receptor_nombre LIKE '?%CONTADO%')
															BEGIN									
																SET @receptor_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
																SET @ubicacion_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
															END;
														ELSE
															BEGIN
																SET @receptor_incluir = 1;
																SET @ubicacion_incluir = 1;
															END;
													END;
											END;
										ELSE
											BEGIN
												SELECT @REF_CLAVE = REF_CLAVE
												FROM encabezado WITH (NOLOCK)
												WHERE id = @id;

												IF EXISTS (SELECT id FROM encabezado WITH (NOLOCK) WHERE clave = @REF_CLAVE)
													BEGIN
														SELECT 	@receptor_incluir = receptor_incluir,
																@ubicacion_incluir = ubicacion_incluir 
														FROM encabezado WITH (NOLOCK)
														WHERE clave = @REF_CLAVE;
													END;
												ELSE
													BEGIN
														IF (@CLASE IN ('P','T'))
															BEGIN
																SET @receptor_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
																SET @ubicacion_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
															END;
														ELSE
															BEGIN
																IF (@receptor_nombre LIKE '?%CONTADO%')
																	BEGIN									
																		SET @receptor_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
																		SET @ubicacion_incluir = CASE WHEN @Incluir_Receptor_G = 0 THEN @receptor_incluir ELSE 0 END;
																	END;
																ELSE
																	BEGIN
																		SET @receptor_incluir = 1;
																		SET @ubicacion_incluir = 1;
																	END;
															END;
													END;
											END;
									END;
								ELSE
									BEGIN
										-- UBICANDO DATOS DEL RECEPTOR
										SELECT	@receptor_nombre = [Descrip],
												@receptor_tipo = CASE [Clase] WHEN 'N' THEN IIF([TipoID3] = 1, '01', '02') WHEN 'ED' THEN '03' WHEN 'EN' THEN '04' WHEN 'P' THEN '' ELSE IIF([TipoID3] = 1, '01', '02') END,
												@receptor_identificacion = IIF([Clase] = 'P', '', DBO.RemoveChars(ID3)),
												@pasaporte = IIF([Clase] = 'P', DBO.RemoveChars(ID3), ''),
												@estador = [Estado],
												@ciudadr = [Ciudad],
												@municipior = [Municipio],
												@receptor_direccion = LTRIM(RTRIM(CONCAT([Direc1], ' ', [Direc2]))),
												@receptor_telefono = LEFT(DBO.RemoveChars([Telef]),8),
												@receptor_fax = LEFT(DBO.RemoveChars([Fax]),20),
												@receptor_email = REPLACE(REPLACE([Email], CHAR(0x1f), ''), CHAR(32), '')
										FROM [dbo].[SAPROV] WITH (NOLOCK)
										WHERE [CodProv] = @Cod_receptor;

										SET @Numerrors=@Numerrors+@@ERROR;
										SET @receptor_direccion_extranjero = NULL;
										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								IF (LEFT(@receptor_nombre,1) = '?')
									BEGIN
										SET @receptor_nombre = @Descrip;
									END;
								ELSE
									BEGIN
										IF (ISNULL(LEN(@receptor_nombre),0) = 0)
											BEGIN
												SET @receptor_nombre = NULL;
											END;
									END;

								IF (ISNULL(LEN(@receptor_tipo),0) = 0)
									BEGIN
										SET @receptor_tipo = NULL;
									END;
								IF (ISNULL(LEN(@receptor_identificacion),0) = 0)
									BEGIN
										SET @receptor_identificacion = NULL;
									END;
								IF (ISNULL(LEN(@pasaporte),0) = 0)
									BEGIN
										SET @pasaporte = NULL;
									END;
								IF (ISNULL(LEN(@receptor_direccion_extranjero),0) = 0)
									BEGIN
										SET @receptor_direccion_extranjero = NULL;
									END;
								IF (ISNULL(LEN(@receptor_telefono),0) = 0)
									BEGIN
										SET @receptor_telefono = NULL;
									END;
								IF (ISNULL(LEN(@receptor_fax),0) = 0)
									BEGIN
										SET @receptor_fax = NULL;
									END;
								IF (ISNULL(LEN(@receptor_email),0) = 0)
									BEGIN
										SET @receptor_email = NULL;
									END;

								SELECT @Pais = [Pais]
								FROM [dbo].[SAPAIS] WITH (NOLOCK)
								WHERE [IDPais] = 506;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @receptor_provincia = NULL;

								SELECT @receptor_provincia = CAST([IDESTADOFE] AS VARCHAR(1))
								FROM [dbo].[SAESTADO] WITH (NOLOCK)
								WHERE [Estado] = @estador AND [Pais] = @Pais;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @receptor_canton = NULL; 

								SELECT @receptor_canton = IIF(LEN(CAST([IDCIUDADFE] AS VARCHAR(2))) = 1, CONCAT('0', CAST([IDCIUDADFE] AS VARCHAR(2))), CAST([IDCIUDADFE] AS VARCHAR(2)))
								FROM [dbo].[SACIUDAD] WITH (NOLOCK)
								WHERE Ciudad = @ciudadr AND Pais = @Pais;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @receptor_distrito = NULL;

								SELECT @receptor_distrito = IIF(LEN(CAST([IDMUNICIPIOFE] AS VARCHAR(2))) = 1, CONCAT('0', CAST([IDMUNICIPIOFE] AS VARCHAR(2))), CAST([IDMUNICIPIOFE] AS VARCHAR(2))) 
								FROM [dbo].[SAMUNICIPIO] WITH (NOLOCK)
								WHERE Municipio = @municipior AND Pais = @Pais;

								SET @Numerrors=@Numerrors+@@ERROR;

								IF (ISNULL(LEN(@receptor_provincia),0) = 0)
									BEGIN
										SET @receptor_provincia = '1';
										SET @Numerrors=@Numerrors+@@ERROR;
									END;
								IF (ISNULL(LEN(@receptor_canton),0) = 0)
									BEGIN
										SET @receptor_canton = '01';
										SET @Numerrors=@Numerrors+@@ERROR;
									END;
								IF (ISNULL(LEN(@receptor_distrito),0) = 0)
									BEGIN
										SET @receptor_distrito = '01';
										SET @Numerrors=@Numerrors+@@ERROR;
									END;
								IF ISNULL(LEN(@receptor_direccion),0) = 0
									BEGIN
										SET @receptor_direccion = CONCAT(@receptor_provincia,'-',@receptor_canton,'-',@receptor_direccion);
										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- INSERTANDO CABECERA DE COMPROBANTE NUEVO
								INSERT INTO [dbo].[encabezado]
											([clave]
											,[tipo_documento]
											,[fecha]
											,[consecutivo_origen]
											,[consecutivo_incompleto]
											,[consecutivo_completo]
											,[emisor_nombre]
											,[emisor_nom_comercial]
											,[emisor_tipo]
											,[emisor_identificacion]
											,[emisor_provincia]
											,[emisor_canton]
											,[emisor_distrito]
											,[emisor_direccion]
											,[emisor_telefono]
											,[emisor_fax]
											,[emisor_email]
											,[receptor_incluir]
											,[receptor_nombre]
											,[receptor_tipo]
											,[receptor_identificacion]
											,[pasaporte]
											,[ubicacion_incluir]
											,[receptor_provincia]
											,[receptor_canton]
											,[receptor_distrito]
											,[receptor_direccion]
											,[receptor_telefono]
											,[receptor_fax]
											,[receptor_email]
											,[condicion_venta]
											,[plazo_credito]
											,[medio_pago1]
											,[medio_pago2]
											,[medio_pago3]
											,[medio_pago4]
											,[moneda]
											,[tipo_cambio]
											,[ref_tipo_doc]
											,[ref_clave]
											,[ref_fecha]
											,[ref_codigo]
											,[ref_razon]
											,[Otros]
											,[xml]
											,[estado_envio]
											,[mensaje_envio]
											,[estado_hacienda]
											,[mensaje_hacienda]
											,[correos_adicionales]
											,[Reintentos]
											,[FechaCE]
											,[codigo_actividad]
											,[receptor_direccion_extranjero]
											,[iva_devuelto]
											,[tipodoc]
											,[numerod]
											,[reenviado]
											,[llave_sucursal]
											,[codclie]
											,[codprov])

								SELECT 	@clave
										,[tipo_documento]
										,GETDATE()
										,@consecutivo_origen
										,@consecutivo_incompleto
										,@consecutivo_completo
										,[emisor_nombre]
										,[emisor_nom_comercial]
										,[emisor_tipo]
										,[emisor_identificacion]
										,[emisor_provincia]
										,[emisor_canton]
										,[emisor_distrito]
										,[emisor_direccion]
										,[emisor_telefono]
										,[emisor_fax]
										,[emisor_email]
										,IIF([tipo_documento] = '08', [receptor_incluir], @receptor_incluir)
										,IIF((@receptor_nombre IS NULL),[receptor_nombre],@receptor_nombre)
										,IIF((@receptor_tipo IS NULL),[receptor_tipo],@receptor_tipo)
										,IIF((@receptor_identificacion IS NULL),[receptor_identificacion],@receptor_identificacion)
										,IIF((@pasaporte IS NULL),[pasaporte],@pasaporte)
										,IIF([tipo_documento] = '08', [ubicacion_incluir], @ubicacion_incluir)
										,IIF((@receptor_provincia IS NULL),[receptor_provincia],@receptor_provincia)
										,IIF((@receptor_canton IS NULL),[receptor_canton],@receptor_canton)
										,IIF((@receptor_distrito IS NULL),[receptor_distrito],@receptor_distrito)
										,IIF((@receptor_direccion IS NULL),[receptor_direccion],@receptor_direccion)
										,IIF((@receptor_telefono IS NULL),[receptor_telefono],@receptor_telefono)
										,IIF((@receptor_fax IS NULL),[receptor_fax],@receptor_fax)
										,IIF((@receptor_email IS NULL),[receptor_email],@receptor_email)
										,[condicion_venta]
										,[plazo_credito]
										,[medio_pago1]
										,[medio_pago2]
										,[medio_pago3]
										,[medio_pago4]
										,[moneda]
										,[tipo_cambio]
										,[ref_tipo_doc]
										,[ref_clave]
										,[ref_fecha]
										,[ref_codigo]
										,[ref_razon]
										,[Otros]
										,[xml]
										,'-1'
										,''
										,'0'
										,''
										,[correos_adicionales]
										,0
										,GETDATE()
										,[codigo_actividad]
										,IIF((@receptor_direccion_extranjero IS NULL),[receptor_direccion_extranjero],@receptor_direccion_extranjero)
										,[iva_devuelto]
										,[tipodoc]
										,[numerod]
										,0
										,[llave_sucursal]
										,[codclie]
										,[codprov]
								FROM [dbo].[encabezado] WITH (NOLOCK)
								WHERE [id] = @id; 

								SET @Numerrors=@Numerrors+@@ERROR;

								-- UBICANDO ID NUEVO
								SELECT	@id_nuevo = [id]
								FROM [dbo].[encabezado] WITH (NOLOCK)
								WHERE [clave] = @clave;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @NUMGRP_1 = NULL;
								SET @NOM_TABLE_1_M = NULL;
								SET @NOM_TABLE_1 = NULL;
								SET @NombreGrp_1 = NULL;
								SET @NUMGRP_2 = NULL;
								SET @NOM_TABLE_2_M = NULL;
								SET @NOM_TABLE_2 = NULL;
								SET @NombreGrp_2 = NULL;
								SET @Numerrors=@Numerrors+@@ERROR;

								-- UBICANDO DIRECCION DE EXTRANJERO
								SET @NUMGRP_1 = 0;
								SET @NOM_TABLE_1_M = 'SAPROD';
								SET @NombreGrp_1 = 'Datos_Adicionales';

								SET @Numerrors=@Numerrors+@@ERROR;

								SELECT @NUMGRP_1 = ISNULL(NumGrp,0)
								FROM SAAGRUPOS WITH (NOLOCK)
								WHERE CodTbl = @NOM_TABLE_1_M AND NombreGrp = @NombreGrp_1;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @NOM_TABLE_1 = CONCAT(@NOM_TABLE_1_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_1 AS VARCHAR(2)))),CAST(@NUMGRP_1 AS VARCHAR(2)));

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @NUMGRP_2 = 0;
								SET @NOM_TABLE_2_M = 'SASERV'
								SET @NombreGrp_2 = 'Datos_Adicionales';

								SET @Numerrors=@Numerrors+@@ERROR;

								SELECT @NUMGRP_2 = ISNULL(NumGrp,0)
								FROM SAAGRUPOS WITH (NOLOCK)
								WHERE CodTbl = @NOM_TABLE_2_M AND NombreGrp = @NombreGrp_2;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @NOM_TABLE_2 = CONCAT(@NOM_TABLE_2_M, '_',REPLICATE('0',2 - LEN(CAST(@NUMGRP_2 AS VARCHAR(2)))),CAST(@NUMGRP_2 AS VARCHAR(2)));
								SET @Numerrors=@Numerrors+@@ERROR;

								-- INSERTANDO LINEA DE COMPROBANTE
								SET @STRING = CONCAT('INSERT INTO [dbo].[linea]
			([id_encabezado]
			,[clave]
			,[numero_linea]
			,[tipo_cod_producto]
			,[codigo_producto]
			,[servicio]
			,[cantidad]
			,[unidad_medida]
			,[detalle]
			,[precio_unitario]
			,[monto_total]
			,[monto_descuento]
			,[naturaleza_descuento]
			,[subtotal]
			,[impuesto1_codigo]
			,[impuesto1_porcentaje]
			,[impuesto1_monto]
			,[exoneracion1_incluir]
			,[exoneracion1_tipo]
			,[exoneracion1_numero_doc]
			,[exoneracion1_institucion]
			,[exoneracion1_fecha_doc]
			,[exoneracion1_monto_imp]
			,[exoneracion1_porc_compra]
			,[impuesto2_codigo]
			,[impuesto2_porcentaje]
			,[impuesto2_monto]
			,[exoneracion2_incluir]
			,[exoneracion2_tipo]
			,[exoneracion2_numero_doc]
			,[exoneracion2_institucion]
			,[exoneracion2_fecha_doc]
			,[exoneracion2_monto_imp]
			,[exoneracion2_porc_compra]
			,[monto_total_linea]
			,[codigo_hacienda]
			,[partida_arancelaria]
			,[monto_descuento2]
			,[naturaleza_descuento2]
			,[monto_descuento3]
			,[naturaleza_descuento3]
			,[monto_descuento4]
			,[naturaleza_descuento4]
			,[monto_descuento5]
			,[naturaleza_descuento5]
			,[base_imponible]
			,[factor_iva1]
			,[codigo_tarifa1]
			,[factor_iva2]
			,[codigo_tarifa2]
			,[monto_impuesto_exportacion1]
			,[monto_impuesto_exportacion2]
			,[monto_impuesto_neto]
			,[unidad_medida_comercial])

SELECT 		',@id_nuevo,'
			,','''',@clave,'''','
			,A.[numero_linea]
			,A.[tipo_cod_producto]
			,A.[codigo_producto]
			,A.[servicio]
			,A.[cantidad]
			,A.[unidad_medida]
			,A.[detalle]
			,A.[precio_unitario]
			,A.[monto_total]
			,A.[monto_descuento]
			,A.[naturaleza_descuento]
			,A.[subtotal]
			,A.[impuesto1_codigo]
			,A.[impuesto1_porcentaje]
			,A.[impuesto1_monto]
			,A.[exoneracion1_incluir]
			,A.[exoneracion1_tipo]
			,A.[exoneracion1_numero_doc]
			,A.[exoneracion1_institucion]
			,A.[exoneracion1_fecha_doc]
			,A.[exoneracion1_monto_imp]
			,A.[exoneracion1_porc_compra]
			,A.[impuesto2_codigo]
			,A.[impuesto2_porcentaje]
			,A.[impuesto2_monto]
			,A.[exoneracion2_incluir]
			,A.[exoneracion2_tipo]
			,A.[exoneracion2_numero_doc]
			,A.[exoneracion2_institucion]
			,A.[exoneracion2_fecha_doc]
			,A.[exoneracion2_monto_imp]
			,A.[exoneracion2_porc_compra]
			,A.[monto_total_linea]
			,IIF(((CASE WHEN A.[servicio] = 0 THEN B.[codigo_hacienda] WHEN A.[servicio] = 1 THEN C.[codigo_hacienda] ELSE NULL END) IS NULL) OR ((CASE WHEN A.[servicio] = 0 THEN B.[codigo_hacienda] WHEN A.[servicio] = 1 THEN C.[codigo_hacienda] ELSE NULL END) = ','''','''','),A.[codigo_hacienda],CASE WHEN A.[servicio] = 0 THEN B.[codigo_hacienda] WHEN A.[servicio] = 1 THEN C.[codigo_hacienda] ELSE NULL END) [codigo_hacienda]
			,IIF(((CASE WHEN ','''',@tipo_documento,'''',' = ','''','09','''',' THEN B.[partida_arancelaria] ELSE A.[partida_arancelaria] END) IS NULL) OR ((CASE WHEN ','''',@tipo_documento,'''',' = ','''','09','''',' THEN B.[partida_arancelaria] ELSE A.[partida_arancelaria] END) = ','''','''','), A.[partida_arancelaria], CASE WHEN ','''',@tipo_documento,'''',' = ','''','09','''',' THEN B.[partida_arancelaria] ELSE A.[partida_arancelaria] END) [partida_arancelaria]
			,A.[monto_descuento2]
			,A.[naturaleza_descuento2]
			,A.[monto_descuento3]
			,A.[naturaleza_descuento3]
			,A.[monto_descuento4]
			,A.[naturaleza_descuento4]
			,A.[monto_descuento5]
			,A.[naturaleza_descuento5]
			,A.[base_imponible]
			,A.[factor_iva1]
			,A.[codigo_tarifa1]
			,A.[factor_iva2]
			,A.[codigo_tarifa2]
			,A.[monto_impuesto_exportacion1]
			,A.[monto_impuesto_exportacion2]
			,A.[monto_impuesto_neto]
			,A.[unidad_medida_comercial]
FROM [dbo].[linea] A WITH (NOLOCK) LEFT OUTER JOIN ', QUOTENAME(@NOM_TABLE_1),' B WITH (NOLOCK)
ON A.[codigo_producto] = B.[CodProd]
LEFT OUTER JOIN ', QUOTENAME(@NOM_TABLE_2),' C WITH (NOLOCK)
ON A.[codigo_producto] = C.[CodServ]
WHERE A.[id_encabezado] = ','''',@id,'''');						

								SET @Numerrors=@Numerrors+@@ERROR;
								EXECUTE sp_executesql @STRING;
								SET @Numerrors=@Numerrors+@@ERROR;
								SET @STRING = NULL;

								-- INSERTANDO OTROS_CARGOS COMPROBANTE NUEVO
								INSERT INTO [dbo].[otros_cargos] ([id_encabezado],[clave],[otc_tipo],[otc_monto],[otc_detalle],[otc_porcentaje],[otc_nombre],[otc_identificacion],[otc_nrolinea])
								SELECT 	@id_nuevo,@clave,[otc_tipo],[otc_monto],[otc_detalle],[otc_porcentaje],[otc_nombre],[otc_identificacion],[otc_nrolinea]
								FROM [dbo].[otros_cargos] WITH (NOLOCK)
								WHERE [id_encabezado] = @id;

								SET @Numerrors=@Numerrors+@@ERROR;

								-- VALIDANDO QUE TABLA DE SAINT ACTUALIZAR
								IF (@validador = 1)
									BEGIN
										-- ACTUALIZANDO SAFACT
										UPDATE [dbo].[SAFACT] SET 	[NumeroDE] = @consecutivo_origen
																	,[Clave] = @clave
																	,[ConsecutivoDE] = @consecutivo_completo
																	,[Num_Original] = NULL
										WHERE [NroUnico] = @NroUnico;		

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								IF (@validador = 2)
									BEGIN
										-- ACTUALIZANDO SAACXC
										UPDATE [dbo].[SAACXC] SET 	[NumeroDE] = @consecutivo_origen
																	,[Clave] = @clave
																	,[ConsecutivoDE] = @consecutivo_completo
																	,[Num_Original] = NULL
										WHERE [NroUnico] = @NroUnico;		

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								IF (@validador = 3)
									BEGIN
										-- ACTUALIZANDO SACOMP
										UPDATE [dbo].[SACOMP] SET 	[NumeroDE] = @consecutivo_origen
																	,[Clave] = @clave
																	,[ConsecutivoDE] = @consecutivo_completo
										WHERE [NroUnico] = @NroUnico;		

										SET @Numerrors=@Numerrors+@@ERROR;
									END;

								-- ACTIVANDO ENVIO DE COMPROBANTE NUEVO
								UPDATE [dbo].[encabezado] SET [estado_envio] = '0'
								WHERE [id] = @id_nuevo AND [estado_envio] = '-1';

								SET @Numerrors=@Numerrors+@@ERROR;

								-- ACTUALIZANDO REENVIOS EN COMPROBANTE RECHAZADO
								UPDATE [dbo].[encabezado] SET [reenviado] = [reenviado] + 1
								WHERE [id] = @id;

								SET @Numerrors=@Numerrors+@@ERROR;
								SET @resultado = CONCAT('Se creo exitosamente un ',CHAR(13), CHAR(10),
														'nuevo comprobante con ',CHAR(13), CHAR(10),
														'el consecutivo ',CHAR(13), CHAR(10),
														@consecutivo_completo);
								SET @Numerrors=@Numerrors+@@ERROR;
							END;
						ELSE
							BEGIN
								SET @resultado = CONCAT('No se encontro el comprobante ',CHAR(13), CHAR(10),
														'Nro. ',@consecutivoFE,CHAR(13), CHAR(10),
														'en las tablas de Saint, ',CHAR(13), CHAR(10),
														'debe verificar que el comprobante ',CHAR(13), CHAR(10),
														'tiene asignado una Referencia Saint');
								SET @Numerrors=@Numerrors+@@ERROR;
							END;
					END;
				ELSE
					BEGIN
						SET @resultado = CONCAT('El comprobante Nro. ',CHAR(13), CHAR(10),
												@consecutivoFE,CHAR(13), CHAR(10),
												'no existe o no se encuentra ',CHAR(13), CHAR(10),
												'rechazado por hacienda');
						SET @Numerrors=@Numerrors+@@ERROR;
					END;		
			END;
		ELSE
			BEGIN
				SET @resultado = CONCAT('El comprobante Nro. ',CHAR(13), CHAR(10),
										@consecutivoFE,CHAR(13), CHAR(10),
										'debe tener 20 caracteres',CHAR(13), CHAR(10),
										'y tiene ', CAST(LEN(@consecutivoFE) AS varchar(50)));

				SET @Numerrors=@Numerrors+@@ERROR;
			END;

	IF (@Numerrors > 0)
		BEGIN
			SET @resultado = CONCAT('Ocurrio un error inesperado',CHAR(13), CHAR(10),
									'por favor contactar al ',CHAR(13), CHAR(10),
									'Administrador del Sistema');

	   		ROLLBACK TRANSACTION REENVIO_FE;
		END;
	ELSE
		BEGIN
	   		COMMIT TRANSACTION REENVIO_FE;
	   	END;
END;
GO
/***FIN DE SCRIPT***/
/*Actualizando CodClie en encabezado de FACT Y DEV*/
UPDATE A SET A.codclie = B.CodClie
FROM encabezado A INNER JOIN SAFACT B
ON A.tipodoc = B.TipoFac AND A.numerod = IIF(ISNULL(LEN(B.num_original),0) = 0, B.NumeroD, B.num_original)
WHERE ISNULL(LEN(A.codclie),0) = 0;
GO
/*Actualizando CodClie en encabezado de NC y ND*/
UPDATE A SET A.codclie = B.CodClie
FROM encabezado A INNER JOIN SAACXC B
ON A.clave = B.Clave
WHERE ISNULL(LEN(A.codclie),0) = 0;
GO
/*Actualizando CodProv en encabezado de COMP*/
UPDATE A SET A.codprov = B.CodProv
FROM encabezado A INNER JOIN SACOMP B
ON A.clave = B.Clave
WHERE ISNULL(LEN(A.codprov),0) = 0;
GO
/*ACTUALIZA VERSION FE EN SACONF*/
UPDATE SACONF SET VersionFE = '4.3.6.4'
GO